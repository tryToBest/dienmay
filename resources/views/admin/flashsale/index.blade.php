@extends('admin.partials.master')
@section('title', 'Quản lý sản phẩm')
@section('css')
<link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}?{{time()}}">
<style>
    .daterangepicker.dropdown-menu{
        left: 20%!important;
        right: auto!important;
    }
    .daterangepicker.opensleft:before {
        left: 9px!important;right: auto!important;
    }

    .daterangepicker.opensleft:after {
        left: 10px!important;right: auto!important;
    }
    span.page {
        padding: 10px 15px;
        border: 1px solid #000;
        cursor: pointer;
        margin-right: 12px;
        font-size: 16px;
        font-weight: bold;
    }
    span.page_current {
        padding: 10px 15px;
        border: 1px solid #000;
        cursor: pointer;
        color: #fff;
        margin-right: 12px;
        font-size: 16px;
        font-weight: bold;
        background-color: #dddddd;
        background-color: #3c8dbc;
    }
</style>        
@endsection
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Flash Sale</h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i>Trang chủ</li>
            <li>Quản lý Flash sale</li>
            <li class="active">Flash sale</li>
        </ol>
    </section>
    <section class="content-header">
        <div class="static-content-wrapper">
            <div class="static-content">
                <div class="page-content">
                    @include('errors.message')
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-lg-12">
                                <div class="box box-warning">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Flash Sale</h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>

                                    <div class="box-body" style="">
                                        <form role="form" method="POST" action="{{route('admin.flashsale.update')}}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="titleFlashSale">Tiêu đề</label>
                                                    <input type="text" name="title" class="form-control" value="{{$setting->title_flash_sale}}" id="titleFlashSale" placeholder="Nhập tiêu đề" required>
                                                </div>
                                                <div class="form-group" style="display: flex;align-items: center">
                                                    <label class="control-label">Trạng thái: </label>
                                                    <label class="switch">
                                                        <input type="checkbox" name="statusFlashSale" value="0" {{$setting->status_flash_sale == 1 ? "checked" : ""}}>
                                                        <span class="slider round"></span>
                                                        <input type="hidden" name="id" value="1">
                                                    </label>
                                                </div>
                                                <div class="form-group" style="display: flex;align-items: center">
                                                    <label class="control-label">Auto Time: </label>
                                                    <label class="switch">
                                                        <input type="checkbox" name="flash_sale_auto" value="0" {{$setting->flash_sale_auto == 1 ? "checked" : ""}}>
                                                        <span class="slider round"></span>
                                                        <input type="hidden" name="id" value="1">
                                                    </label>
                                                </div>
                                                <div class="form-group" style="display: flex;align-items: center">
                                                    <label class="control-label">Ẩn\Hiện Background Flash Sale: </label>
                                                    <label class="switch">
                                                        <input type="checkbox" name="status_image_sale" value="0" {{$setting->status_image_sale == 1 ? "checked" : ""}}>
                                                        <span class="slider round"></span>
                                                        <input type="hidden" name="id" value="1">
                                                    </label>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="control-label">Ảnh nền: </label>
                                                    <input type="file" name="image_sale" class="form-control" id="image_sale">
                                                </div>
                                                @if($setting->title_flash_sale != null)
                                                <img src="{{asset($setting->image_sale)}}" alt="ảnh flash sale">
                                                @endif
                                                <div class="form-group">
                                                    <label for="timestart">Chọn thời gian</label>
                                                    <input type="text" name="time" class="form-control pull-right" id="reservationtime">
                                                </div>
                                                
                                            </div>

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary">Lưu</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-lg-12">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Danh sách sản phẩm sale</h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                        </div>
                                        <div class="supportSearch">   
                                            <input type="text" name="productName" id="searchNameProduct" class="form-control" placeholder="Nhập vào tên sản phẩm cần tìm">
                                            <div class="listCateProduct">  
                                                @foreach($cate_products as $cate)
                                                <button data-id_cate_product="{{$cate->id}}" class="btn btn-default getProductByCateProductName">{{$cate->name_vi}}</button>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <form method="post" action="{{ route('checkbox') }}">
                                            {{ csrf_field() }}
                                            <table id="example" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th><input type="checkbox" class="checkall" name="checkall" /></th>
                                                        <th>STT</th>
                                                        <th>Ảnh</th>
                                                        <th>Tên sản phẩm</th>
                                                        <th>Thuộc</th>
                                                        <th>Trạng thái</th>
                                                        <th>Vị trí</th>
                                                        <th>Sp sale</th>   
                                                        <th>Sp Home</th>  
                                                        <th>Cập nhật</th>
                                                        <th>Xử lý</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($saleProducts as $key => $product)
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" name="checkbox[]" class="checkbox"
                                                            value="{{ $product->id }}">
                                                        </td>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>
                                                            <img src="{{ asset($product->image) }}"
                                                            style="height: 60px; width: 60px;">
                                                        </td>
                                                        <td>{{ $product->name_vi }}</td>
                                                        <td>{{ $product->cate_product->name_vi }}</td>
                                                        <td>
                                                            @if($product->status == 1)
                                                                <a href="#" title="Ẩn" id="status" data-id="{{ $product->id }}">
                                                                    <span id="status{{ $product->id }}"><span class="label label-success">Hiện</span></span>
                                                                </a>
                                                            @else
                                                                <a href="#" title="Hiện" id="status" data-id="{{ $product->id }}">
                                                                    <span id="status{{ $product->id }}"><span class="label label-danger">Ẩn</span></span>
                                                                </a>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{ $product->position }}
                                                        </td>
                                                        <td>
                                                            @if($product->is_sale == 1)
                                                                <a href="#" title="Ẩn" id="is_sale" data-id="{{ $product->id }}">
                                                                    <span id="is_sale{{ $product->id }}"><span class="label label-success">Hiện</span></span>
                                                                </a>
                                                            @else
                                                                <a href="#" title="Hiện" id="is_sale" data-id="{{ $product->id }}">
                                                                    <span id="is_sale{{ $product->id }}"><span class="label label-danger">Ẩn</span></span>
                                                                </a>
                                                            @endif
                                                        </td> 
                                                       
                                                        <td>
                                                            @if($product->is_home == 1)
                                                                <a href="#" title="Ẩn" id="is_home" data-id="{{ $product->id }}">
                                                                    <span id="is_home{{ $product->id }}"><span class="label label-success">Hiện</span></span>
                                                                </a>
                                                            @else
                                                                <a href="#" title="Hiện" id="is_home" data-id="{{ $product->id }}">
                                                                    <span id="is_home{{ $product->id }}"><span class="label label-danger">Ẩn</span></span>
                                                                </a>
                                                            @endif
                                                        </td> 
                                                        <td>{{ $product->updated_at }}</td>
                                                        <td>
                                                            <a href="{{ route('admin.product.update', $product->id)}}" target="_blank">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                            <a href="{{ route('admin.product.destroy', $product->id) }}"
                                                                type="button"
                                                                onclick="return confirm_delete('Bạn có muốn xóa không ?')">
                                                                <i class="fa fa-times-circle"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>

                                                <tfoot>
                                                    <tr>
                                                        <th><input type="checkbox" class="checkall" name="checkall" /></th>
                                                       <th>STT</th>
                                                        <th>Ảnh</th>
                                                        <th>Tên sản phẩm</th>
                                                        <th>Thuộc</th>
                                                        <th>Trạng thái</th>
                                                        <th>Vị trí</th> 
                                                        <th>Sp sale</th>  
                                                        <th>Sp Home</th>  
                                                        <th>Cập nhật</th>
                                                        <th>Xử lý</th>
                                                    </tr>
                                                </tfoot>
                                                
                                            </table>
                                            <div class="pagination pull-right" id="pagination">
                                                    
                                                </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div><!-- /.content-wrapper -->
@endsection
@section('script')
<script type="text/javascript" src="{{asset('js/moment.min.js')}}?{{time()}}"></script>
<script type="text/javascript" src="{{asset('js/daterangepicker.js')}}?{{time()}}"></script>
<script type="text/javascript">
    $(function () {
        $("#example2").DataTable();
    });
</script>
<script type='text/javascript'>
    $(document).ready(function(){
        $(".checkall").change(function(){
            var checked = $(this).is(':checked');
            if(checked){
                $(".checkbox").each(function(){
                    $(this).prop("checked",true);
                });
            }else{
                $(".checkbox").each(function(){
                    $(this).prop("checked",false);
                });
            }
        });

        $(".checkbox").click(function(){
            if($(".checkbox").length == $(".checkbox:checked").length) {
                $(".checkall").prop("checked", true);
            } else {
                $(".checkall").removeAttr("checked");
            }
        });
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click', '#status', function(event){
        event.preventDefault();
        var id = $(this).data('id');
        $.post('{{ route('admin.product.status') }}', {id:id}, function(data){
            if (data.status == 1)
            {
                $('#status'+id).html('<span class="label label-success">Hiện</span>');
            } else {
                $('#status'+id).html('<span class="label label-danger">Ẩn</span>');
            }
        });
    });
    
    $(document).on('click', '#is_home', function(event){
        event.preventDefault();
        var id = $(this).data('id');
        $.post('{{ route('admin.product.is_home') }}', {id:id}, function(data){
            if (data.is_home == 1)
            {
                $('#is_home'+id).html('<span class="label label-success">Hiện</span>');
            } else {
                $('#is_home'+id).html('<span class="label label-danger">Ẩn</span>');
            }
        });
    });
    $(document).on('click', '#is_hot', function(event){
        event.preventDefault();
        var id = $(this).data('id');
        $.post('{{ route('admin.product.is_hot') }}', {id:id}, function(data){
            if (data.is_hot == 1)
            {
                $('#is_hot'+id).html('<span class="label label-success">Hiện</span>');
            } else {
                $('#is_hot'+id).html('<span class="label label-danger">Ẩn</span>');
            }
        });
    });
    $(document).on('click', '#is_sale', function(event){
        event.preventDefault();
        var id = $(this).data('id');
        $.post('{{ route('admin.product.is_sale') }}', {id:id}, function(data){
            if (data.is_sale == 1)
            {
                $('#is_sale'+id).html('<span class="label label-success">Hiện</span>');
            } else {
                $('#is_sale'+id).html('<span class="label label-danger">Ẩn</span>');
            }
        });
    });
    $('#searchNameProduct').keyup(function(){
        var value = $(this).val();
        var url = '{{ route("admin.flashsale.searchByName") }}';
        var table = $('#example');

        $.ajax({
            url: url,
            data: {
                keyword: value,
                page: 1,
                _token: '{{ csrf_token() }}'
            },
            type: 'POST',
            success: function(response) {
                // Hiển thị kết quả tìm kiếm
                displayResults(response.data);

                // Hiển thị phân trang
                displayPagination(response,false,value);
            },
            error: function(xhr, status, error) {
                console.error(error);
            }
        });
    });
    $('.getProductByCateProductName').click(function(){
        var keyword = $(this).data("id_cate_product");
        var url = '{{ route("admin.flashsale.searchByCateProductName") }}';
        var table = $('#example');

        $.ajax({
            url: url,
            data: {
                keyword: keyword,
                page: 1,
                _token: '{{ csrf_token() }}'
            },
            type: 'POST',
            success: function(response) {
                // Hiển thị kết quả tìm kiếm
                displayResults(response.data);

                // Hiển thị phân trang
                displayPagination(response, true,keyword);
            },
            error: function(xhr, status, error) {
                console.error(error);
            }
        });
    });
    
function displayResults(data) {
    var results = $('#example').find('tbody');
    results.empty();

    // Hiển thị danh sách sản phẩm tìm được
    $.each(data, function(index, product) {
        var statusLabel = (product.status == 1) ? '<span class="label label-success">Hiện</span>' : '<span class="label label-danger">Ẩn</span>';
        var isSaleLabel = (product.is_sale == 1) ? '<span class="label label-success">Hiện</span>' : '<span class="label label-danger">Ẩn</span>';
        var isHomeLabel = (product.is_home == 1) ? '<span class="label label-success">Hiện</span>' : '<span class="label label-danger">Ẩn</span>';
        
        var html = `<tr>
            <td>
                <input type="checkbox" name="checkbox[]" class="checkbox" value="${product.id}">
            </td>
            <td>${index + 1}</td>
            <td>
                <img src="{{ asset('') }}${product.image}" style="height: 60px; width: 60px;">
            </td>
            <td>${product.name_vi}</td>
            <td>${product.cate_product.name_vi}</td>
            <td>
                <a href="#" title="${(product.status == 1) ? 'Ẩn' : 'Hiện'}" id="status" data-id="${product.id}">
                    <span id="status${product.id}">${statusLabel}</span>
                </a>
            </td>
            <td>${product.position}</td>
            <td>
                <a href="#" title="${(product.is_sale == 1) ? 'Ẩn' : 'Hiện'}" id="is_sale" data-id="${product.id}">
                    <span id="is_sale${product.id}">${isSaleLabel}</span>
                </a>
            </td> 
            <td>
                <a href="#" title="${(product.is_home == 1) ? 'Ẩn' : 'Hiện'}" id="is_home" data-id="${product.id}">
                    <span id="is_home${product.id}">${isHomeLabel}</span>
                </a>
            </td> 
            <td>${product.updated_at}</td>
            <td>
                <a href="{{ route('admin.product.update', '') }}${product.id}" target="_blank">
                    <i class="fa fa-pencil"></i>
                </a>
                <a href="{{ route('admin.product.destroy', '') }}${product.id}" type="button" onclick="return confirm_delete('Bạn có muốn xóa không ?')">
                    <i class="fa fa-times-circle"></i>
                </a>
            </td>
        </tr>`;
        $('#example').find('tbody').append(html); // Append the generated HTML to the table
    });
}
                
function displayPagination(data, isCate,value) {
    var pagination = $('#pagination');
    pagination.empty();

    if (data.last_page > 1) {
        var totalPages = data.last_page;
        var visiblePages = 5; // Số trang hiển thị tối đa
        var currentPage = data.current_page;

        var startPage, endPage;
        if (totalPages <= visiblePages) {
            startPage = 1;
            endPage = totalPages;
        } else {
            var half = Math.floor(visiblePages / 2);
            if (currentPage <= half) {
                startPage = 1;
                endPage = visiblePages;
            } else if (currentPage + half >= totalPages) {
                startPage = totalPages - visiblePages + 1;
                endPage = totalPages;
            } else {
                startPage = currentPage - half;
                endPage = currentPage + half;
            }
        }

        for (var i = startPage; i <= endPage; i++) {
            var pageLink = '<span class="page" data-page="' + i + '">' + i + '</span>';
            if (i === currentPage) {
                pageLink = '<span class="page_current">' + i + '</span>';
            }
            pagination.append(pageLink);
        }

        pagination.find('.page').on('click', function() {
            var page = $(this).data('page');
            loadPage(page,isCate,value);
        });

        // Thêm liên kết 'Previous' và 'Next'
        if (currentPage > 1) {
            pagination.prepend('<span class="page" data-page="1">First</span>');
            pagination.prepend('<span class="page" data-page="' + (currentPage - 1) + '">Previous</span>');
        }
        if (currentPage < totalPages) {
            pagination.append('<span class="page" data-page="' + (currentPage + 1) + '">Next</span>');
            pagination.append('<span class="page" data-page="' + totalPages + '">Last</span>');
        }
    }
}


function loadPage(page, isCate,value) {
    var keyword = value;
    var url = isCate ? '{{ route("admin.flashsale.searchByCateProductName") }}' : '{{ route("admin.flashsale.searchByName") }}';

    $.ajax({
        url: url,
        type: 'POST',
        data: {
            _token: '{{ csrf_token() }}',
            keyword: keyword,
            page: page
        },
        success: function(response) {
            // Hiển thị kết quả tìm kiếm mới trên trang
            displayResults(response.data);

            // Cập nhật phân trang
            displayPagination(response, isCate, value);
        },
        error: function(xhr, status, error) {
            console.error(error);
        }
    });
}
$(function(){
//Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, locale: { format: 'MM/DD/YYYY hh:mm A' }})
});
</script>
@endsection
