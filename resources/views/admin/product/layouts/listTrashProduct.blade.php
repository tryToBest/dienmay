<tbody>
    @foreach($products as $key => $product)
    <tr>
        <td>
            <input type="checkbox" name="checkbox[]" class="checkbox"
            value="{{ $product->id }}">
        </td>
        <td>{{ $key + 1 }}</td>
        <td>
            <img src="{{ asset($product->image) }}"
            style="height: 60px; width: 60px;">
        </td>
        <td>{{ $product->name_vi }}</td>
        <td>{{ $product->cate_product->name_vi }}</td>
        <td>{{ $product->updated_at }}</td>
        <td>
            <a href="{{ route('admin.trash.recover') }}">
                <i class="fa fa-undo" onclick="return confirm_delete('Bạn chắc chắn khôi phục sản phẩm này')" data-id="{{$product->id}}" class="recover-product"></i>
            </a>
            <a href="{{ route('admin.trash.destroy') }}"
                type="button"
                onclick="return confirm_delete('Bạn có muốn xóa không ?')" data-id="{{$product->id}}" class="delete-product">
                <i class="fa fa-times-circle"></i>
            </a>
        </td>
    </tr>
    @endforeach
</tbody>