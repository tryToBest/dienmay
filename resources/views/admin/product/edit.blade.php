@extends('admin.partials.master')
@section('title', 'Sửa sản phẩm')
@section('css')
<link rel="stylesheet" href="{{asset('css/select2.min.css')}}?{{time()}}">
@endsection
@section('content')
<div class="content-wrapper">
<section class="content-header">
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Sửa sản phẩm</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">
                    <li>Trang chủ</li>
                    <li>Quản lý sản phẩm</li>
                    <li>Sản phẩm</li>
                    <li class="active">Sửa</li>
                </ol>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
                                @include('errors.message')
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal" method="POST"
                                    action="{{ route('admin.product.postUpdate', $product->id) }}"
                                    enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên sản phẩm: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="name_vi" value="{{ $product->name_vi }}">
                                                </div>
                                            </div>
                                           {{--  <div class="form-group">
                                                <label class="col-md-3 control-label">Tên sản phẩm_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="name_en" value="{{ $product->name_en }}">
                                                </div>
                                            </div>
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Tên sản phẩm_tq: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="name_tq" value="{{ $product->name_tq }}">
                                                </div>
                                            </div> --}}
                            {{--      <div class="form-group">
                                                <label class="col-md-3 control-label">Tiêu đề home: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="code" value="{{ $product->code }}">
                                                </div>
                                            </div>  --}}
                                            <div class="form-group{{ $errors->has('cate_product_id') ? 'has-error' : '' }}">
                                                <label class="col-md-3 control-label">Danh mục: </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" name="cate_product_id">
                                                      {{--   @foreach($data as $l)
                                                        <option {{$product->cate_product_id == $l->id ? 'selected': ''}} value="{{$l->id}}">{{$l->name_vi}}</option>
                                                        @endforeach --}}
                                                         @foreach($danhmuccha as $c)
<option {{$product->cate_product_id == $c->id ? 'selected': ''}} value="{{$c->id}}">
{{$c->name_vi}}
</option>
@php
$level_2 = DB::table('cate_products')->where([['status', 1], ['parent_id', $c->id]])
->orderBy('position', 'ASC')->get();
@endphp
@foreach($level_2 as $l2)
<option {{$product->cate_product_id == $l2->id ? 'selected': ''}} value="{{$l2->id}}">
---cấp 2- {{$l2->name_vi}}
</option>
@php
$level_3 = DB::table('cate_products')->where([['status', 1], ['parent_id', $l2->id]])
->orderBy('position', 'ASC')->get();
@endphp
                                                @foreach($level_3 as $l3)
                                                <option {{$product->cate_product_id == $l3->id ? 'selected': ''}} value="{{$l3->id}}">
                                                -------cấp 3-- {{$l3->name_vi}}
                                                </option>
                                                @endforeach
@endforeach
  @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Bộ lọc: </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" name="boloc_id">
                                                        @foreach($bolocs as $boloc)
                                                            <option {{isset($boloc_boss) && $boloc_boss != null && $boloc_boss == $boloc->id ? 'selected' : ''}} value="{{$boloc->id}}">
                                                                {{$boloc->name_vi}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    @if(isset($bolocs) && $bolocs != null )
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <div class="col-md-8">
                                                                <ul id="listboloc">
                                                                    @foreach ($boloc_child as $b)
                                                                        <li>
                                                                            <input {{isset($listBoLocParentOfProduct) && $listBoLocParentOfProduct != null && in_array($b->id, $listBoLocParentOfProduct)  ? 'checked' : ''}} id="boloc_{{$b->id}}" data-id="{{$b->id}}" type="checkbox" name="child_boloc[]" value="{{$b->id}}">
                                                                            <label for="boloc_{{$b->id}}">{{$b->name_vi}}</label>
                                                                            <select class="form-control select2" name="chitietboloc[]" id="chitietboloc{{$b->id}}" multiple>
                                                                                @foreach($b->Boloc as $child_b)
                                                                                <option {{isset($listBolocIdOfProduct) && $listBolocIdOfProduct != null && in_array($child_b->id, $listBolocIdOfProduct)  ? 'selected' : ''}} value="{{$child_b->id}}">{{$child_b->name_vi}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </li>
                                                                    @endforeach 
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh bìa: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-8">
                                                    <img src="{{ asset($product->image) }}"
                                                    style="height: 50px; width: 50px">
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh chi tiết sản phẩm: </label>
                                                <div class="col-md-8">
                                                    @foreach($img_detail as $i)
                                                        <div class="image">
                                                            <img src="{{ asset($i->name) }}" style="height: 50px; width: 50px">
                                                            <label><span class="btn btn-danger delImage" id="data-{{ $i->id }}" data-id="{{ $i->id }}"> X </span></label>
                                                        </div>
                                                    @endforeach
                                                    <a class="btn btn-primary btn-sm addimg" style="margin-top: 2px; margin-left: 0">+ Thêm ảnh</a>
                                                </div>
                                            </div>
                                            <div class="form-group" id="contentimg">
                                            </div> 
                                       <div class="form-group">
                                                <label class="col-md-3 control-label">Giá: </label>
                                                <div class="col-md-8">
                                                    <input type="number" class="form-control"
                                                    placeholder="Nhập giá" name="price" value="{{ $product->price }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Giá cũ: </label>
                                                <div class="col-md-8">
                                                    <input type="number" class="form-control" name="price_unit" value="{{ $product->price_unit }}">
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Trạng thái: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="status"
                                                    {{ ($product->status) ? 'checked': '' }}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">SP khuyến mại: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="is_sale"
                                                    {{ ($product->is_home) ? 'checked': '' }}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">SP home: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="is_home"
                                                    {{ ($product->is_home) ? 'checked': '' }}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Vị trí: </label>
                                                <div class="col-md-8">
                                                    <input type="number" class="form-control"
                                                    placeholder="Nhập vị trí" name="position"
                                                    value="{{ $product->position }}">
                                                </div>
                                            </div>

                                               <div class="form-group">
                                                <label class="col-md-3 control-label">Khuyến Mãi: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" name="chucnang" id="chucnang">{{ $product->chucnang }}</textarea>
                                                    
                                                </div>
                                            </div>
                                          

   <div class="form-group">
                                                <label class="col-md-3 control-label">Đặc điểm nổi bật: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" name="title_vi" id="title_vi">{{ $product->title_vi }}</textarea>
                                                </div>
                                            </div>





                                               <div class="form-group">
                                                <label class="col-md-3 control-label">Miêu tả chi tiết: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" name="description_vi" id="description_vi">{{ $product->description_vi }}</textarea>
                                                </div>
                                            </div>
                                        
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">THÔNG SỐ KỸ THUẬT: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" name="thongtin_vi" id="thongtin_vi">{{ $product->thongtin_vi }}</textarea>
                                                </div>
                                            </div>

   

                                            {{-- <div class="form-group">
                                                <label class="col-md-3 control-label">Chọn để lọc: </label>
                                                <div class="col-md-8">
                                            
                                                </div>
                                            </div>   --}}
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Title_SEO: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Title SEO"
                                                    name="title_seo_vi" value="{{ $product->title_seo_vi }}">
                                                </div>
                                            </div>
                                          
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_key: </label>
                                                <div class="col-md-8">
                                                    <textarea type="text" class="form-control" placeholder="Meta_key"
                                                    name="meta_key_vi">{{ $product->meta_key_vi }}</textarea>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_des: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="meta_des_vi">{{ $product->meta_des_vi }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <button class="btn-success btn">Lưu</button>
                                                    <a class="btn-default btn"
                                                    href="{{ route('admin.product.create') }}">
                                                        Hủy
                                                    </a>
                                                    <a class="btn-default btn" href='javascript:goback()'>Quay lại</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection
<style>
    #themmodels input{
        max-width: 100%;min-width:200px;float: left;margin-bottom: 10px;margin-right: 7px;
    }
    #themmodels a:hover{
cursor: pointer;
    }
    #themmodels input{
        color: #000;
    }
    #themmodels textarea{
        width:100%;
    }
    .add_models{
        margin-top: 0!important;margin-bottom: 10px;
    }
    .removemodels {
        margin-left: 15px;margin-bottom:10px;    position: absolute;
    }
</style>
@section('script')
<script type="text/javascript" src="{{asset('js/select2.min.js')}}?{{time()}}"></script>
<script>
    CKEDITOR.replace( 'chucnang', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',  contentsCss :'{{asset('frontend/css/style.css')}}?{{time()}}',
    } );  
        CKEDITOR.replace( 'description_vi', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',  contentsCss :'{{asset('frontend/css/style.css')}}?{{time()}}',
    } );

          
        CKEDITOR.replace( 'title_vi', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',  contentsCss :'{{asset('frontend/css/style.css')}}?{{time()}}',
    } );  
          CKEDITOR.replace( 'thongtin_vi', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',  contentsCss :'{{asset('frontend/css/style.css')}}?{{time()}}',
    } );  

        
    $(document).ready(function() {
        $('.addimg').on('click', function(){
            $('#contentimg').append('<label class="col-md-3 control-label"></label><div class="col-md-8"><input type="file" class="form-control" name="img[]"></div>');
        });
        $('.delImage').on('click',function(){
            id = $(this).data('id');
            if(confirm('Bạn có muốn xóa ?')){
                flag = $(this).parent('label').parent('div.image').hide();
                $.ajax({
                    url: '{{route('admin.product.dellimg')}}',
                    type: 'GET',
                    data: {id:id},
                    success: function(response) {
                        // Display a success message
                        $('#message').html('<p class="alert alert-success">Data submitted successfully!</p>');
                    },
                    error: function(response) {
                        // Display an error message
                        $('#message').html('<p class="alert alert-danger">Error submitting data. Please try again.</p>');
                    }
                });
            }
        });
 // $('a.addmodels').click(function() {
 //          var i = parseInt($('#demmodels').val()) + 1;
 //        $('#demmodels').val(i);
 //            $('#themmodels').append('<div id="loaimodels'+i+'"><div class="col-md-3"><p>Tên models '+i+'</p><input name="tenmodels'+i+'" type="text"/></div><div class="col-md-3"><p>Số models '+i+'</p><input name="somodels'+i+'" type="text"/></div><div class="col-md-4"><p>Mô tả models '+i+'</p><textarea name="motamodels'+i+'" type="text" rows="5" required=""></textarea><a class="removemodels btn btn-danger" nam="'+i+'">Xóa models '+i+'</a></div></div><div style="clear:both;"></div>').animate({ opacity: "show" }, "slow");
 //        });
 //        $('a.removemodels').on('click', function() {
 //                var i=$(this).attr('nam');
 //                  // $('#loaimodels'+i).parent().parent().remove();
 //   $('#loaimodels'+i).hide('slow', function(){ $('#loaimodels'+i).remove(); });
 //        });
    });
</script>
<script>
    $(document).ready(function(){

    $('.select2').select2();

    // Sử dụng each() thay vì forEach() để lặp qua các phần tử jQuery
        $("input[name='child_boloc[]']").each(function(){
            let id_checked = $(this).data('id');
            if ($(this).is(':checked')) {
                $("select#chitietboloc" + id_checked).css('display', 'block');
            } else {
                $("select#chitietboloc" + id_checked).css('display', 'none');
            }
        });
    });

// Ẩn tất cả các dropdown khi trang web được tải lần đầu
$("select[name='chitietboloc[]']").css('display', 'none');

// Ẩn tất cả các dropdown khi trang web được tải lần đầu
$(document).on('change', "input[name='child_boloc[]']", function() {
    let id_boloc_child = $(this).data('id');
    if ($(this).is(':checked')) {
        $("select#chitietboloc" + id_boloc_child).css('display', 'block');
    } else {
        $("select#chitietboloc" + id_boloc_child).css('display', 'none');
    }
});


// Xử lý sự kiện thay đổi của select box "Chọn bộ lọc"
$("select[name='boloc_id']").change(function() {
    var boloc_id = $(this).val();

    // Sử dụng Ajax để gửi yêu cầu và lấy dữ liệu mới
    $.ajax({
        url: '{{ route('admin.product.getboloc') }}',
        method: 'POST',
        data: {
            boloc_id: boloc_id,
            _token: "{{ csrf_token() }}",
        },
        success: function(data) {
            // Cập nhật giá trị của bolocChild
            bolocChild = data;

            updateUI();
        }
    });
});

// Hàm cập nhật giao diện người dùng
function updateUI() {
    // Lặp qua mảng bolocChild để xây dựng danh sách checkbox và dropdown tương ứng
    var html = '';
    bolocChild.forEach(function(b) {
        html += '<li>';
        html += '<input id="boloc_' + b.id + '" data-id="' + b.id + '" type="checkbox" name="child_boloc[]" value="' + b.id + '">';
        html += '<label for="boloc_' + b.id + '">' + b.name_vi + '</label>';
        html += '<select class="form-control select2" name="chitietboloc[]" id="chitietboloc' + b.id + '" multiple>';
        b.boloc_child.forEach(function(child_b) {
            html += '<option value="' + child_b.id + '">' + child_b.name_vi + '</option>';
        });
        html += '</select>';
        html += '</li>';
    });

    // Cập nhật danh sách trong ul#listboloc
    // $('#listboloc').empty();
    $('#listboloc').html(html);
    $("select[name='chitietboloc[]']").css('display', 'none');
    $('.select2').select2();
}


</script>
@endsection