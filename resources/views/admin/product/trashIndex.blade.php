@extends('admin.partials.master')
@section('title', 'Quản lý sản phẩm')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Thùng rác</h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i>Trang chủ</li>
            <li class="active">Thùng rác</li>
        </ol>
    </section>
    <section class="content-header">
        <div class="static-content-wrapper">
            <div class="static-content">
                <div class="page-content">
                    @include('errors.message')
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-lg-12">
                                <div class="box">
                                    <div class="box-body">
                                        <form method="post" action="{{ route('admin.trash.checkbox') }}">
                                            {{ csrf_field() }}
                                            <table id="example2" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th><input type="checkbox" class="checkall" name="checkall" /></th>
                                                        <th>STT</th>
                                                        <th>Ảnh</th>
                                                        <th>Tên sản phẩm</th>
                                                        <th>Thuộc</th>
                                                        <th>Cập nhật</th>
                                                        <th>Xử lý</th>
                                                    </tr>
                                                </thead>
                                                @include('admin.product.layouts.listTrashProduct') 
                                            </table>
                                            <select class="" name="select_action">
                                                <option value="0">Lựa chọn</option>
                                                <option value="1">Xóa</option>
                                                <option value="2">Khôi phục</option>
                                                {{-- <option value="3">Ẩn</option> --}}
                                            </select>
                                            <button id="delete_all" class="btn-success">Thực hiện</button>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div><!-- /.content-wrapper -->
@endsection
@section('script')
<script type="text/javascript">
    $(function () {
        $("#example2").DataTable();
    });
</script>
<script>
$(document).ready(function () {
    $('.delete-product').on('click', function (e) {
        e.preventDefault();
        var productId = $(this).data('id');

        $.ajax({
            type: 'POST',
            url: '{{ route('admin.trash.destroy') }}',
            data: {
                productId:productId,
                "_token": "{{ csrf_token() }}"
            },
            success: function (data) {
                if (data.success) {
                    // Xóa hàng sản phẩm khỏi bảng
                    $(e.target).closest('tr').remove();
                    // Hiển thị thông báo thành công
                    alert('Xóa thành công');
                } else {
                    alert('Xóa không thành công');
                }
            },
            error: function () {
                alert('Đã xảy ra lỗi trong quá trình xóa sản phẩm.');
            }
        });
    });
});
</script>
<script>
$(document).ready(function () {
    $('.recover-product').on('click', function (e) {
        e.preventDefault();
        var productId = $(this).data('id');
        $.ajax({
            type: 'POST',
            url: '{{ route('admin.trash.recover') }}',
            data: {
                productId:productId,
                "_token": "{{ csrf_token() }}"
            },
            success: function (data) {
                if (data.success) {
                    // Xóa hàng sản phẩm khỏi bảng
                    $(e.target).closest('tr').remove();
                    // Hiển thị thông báo thành công
                    alert('Xóa thành công');
                } else {
                    alert('Xóa không thành công');
                }
            },
            error: function () {
                alert('Đã xảy ra lỗi trong quá trình xóa sản phẩm.');
            }
        });
    });
});
</script>
<script>
    $(document).ready(function(){
        $(".checkall").change(function(){
            var checked = $(this).is(':checked');
            if(checked){
                $(".checkbox").each(function(){
                    $(this).prop("checked",true);
                });
            }else{
                $(".checkbox").each(function(){
                    $(this).prop("checked",false);
                });
            }
        });

        $(".checkbox").click(function(){
            if($(".checkbox").length == $(".checkbox:checked").length) {
                $(".checkall").prop("checked", true);
            } else {
                $(".checkall").removeAttr("checked");
            }
        });
    });
</script>
@endsection
