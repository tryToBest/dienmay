@extends('admin.partials.master')
@section('title', 'Quản lý sản phẩm')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Sản phẩm</h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i>Trang chủ</li>
            <li class="active">Quản lý sản phẩm nổi bật</li>
        </ol>
    </section>
    <section class="content-header">
        <div class="static-content-wrapper">
            <div class="static-content">
                <div class="page-content">
                    @include('errors.message')
            
                    <div class="search">
                        <form class="" method="get" enctype="multipart/form-data" action="{{ route('admin.product.search') }}">
                        {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-8">
                                    <a href="{{ route('admin.product.create') }}" class="btn btn-primary">
                                        <i class=" fa fa-fw fa-plus"></i>
                                        Thêm mới
                                    </a>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-lg-12">
                                <div class="box">
                                    <div class="box-body">
                                        <form method="post" action="{{ route('checkbox') }}">
                                            {{ csrf_field() }}
                                            <table id="example2" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th><input type="checkbox" class="checkall" name="checkall" /></th>
                                                        <th>STT</th>
                                                        <th>Ảnh</th>
                                                        <th>Tên sản phẩm</th>
                                                        <th>Thuộc</th>
                                                        <th>Trạng thái</th>
                                                        <th>Vị trí</th>
                                                        <!-- <th>So sánh</th> -->
                                                        <!-- <th>Sp flash sale</th>   -->
                                                        <th>SPHot</th>  
                                                         
                                                        <th>Cập nhật</th>
                                                        <th>Xử lý</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($productHots as $key => $product)
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" name="checkbox[]" class="checkbox"
                                                            value="{{ $product->id }}">
                                                        </td>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>
                                                            <img src="{{ asset($product->image) }}"
                                                            style="height: 60px; width: 60px;">
                                                        </td>
                                                        <td>{{ $product->name_vi }}</td>
                                                        <td>{{ $product->cate_product->name_vi }}</td>
                                                        <td>
                                                            @if($product->status == 1)
                                                                <a href="#" title="Ẩn" id="status" data-id="{{ $product->id }}">
                                                                    <span id="status{{ $product->id }}"><span class="label label-success">Hiện</span></span>
                                                                </a>
                                                            @else
                                                                <a href="#" title="Hiện" id="status" data-id="{{ $product->id }}">
                                                                    <span id="status{{ $product->id }}"><span class="label label-danger">Ẩn</span></span>
                                                                </a>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <input type="number" data-position_hot={{$product->id}} name="position_hot" value="{{$product->position_hot}}">
                                                        </td>
                                                        <td>
                                                            @if($product->is_hot == 1)
                                                                <a href="#" title="Ẩn" id="is_hot" data-id="{{ $product->id }}">
                                                                    <span id="is_hot{{ $product->id }}"><span class="label label-success">Hiện</span></span>
                                                                </a>
                                                            @else
                                                                <a href="#" title="Hiện" id="is_hot" data-id="{{ $product->id }}">
                                                                    <span id="is_hot{{ $product->id }}"><span class="label label-danger">Ẩn</span></span>
                                                                </a>
                                                            @endif
                                                        </td> 
                                                        <td>{{ $product->updated_at }}</td>
                                                        <td>
                                                            <a href="{{ route('admin.product.update', $product->id)}}" target="_blank">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                            <a href="{{ route('admin.product.destroy', $product->id) }}"
                                                                type="button"
                                                                onclick="return confirm_delete('Bạn có muốn xóa không ?')">
                                                                <i class="fa fa-times-circle"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>

                                                
                                            </table>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div><!-- /.content-wrapper -->
@endsection
@section('script')
<script type="text/javascript">
    $(function () {
        $("#example2").DataTable();
    });
</script>
<script type='text/javascript'>
    $(document).ready(function(){
        $(".checkall").change(function(){
            var checked = $(this).is(':checked');
            if(checked){
                $(".checkbox").each(function(){
                    $(this).prop("checked",true);
                });
            }else{
                $(".checkbox").each(function(){
                    $(this).prop("checked",false);
                });
            }
        });

        $(".checkbox").click(function(){
            if($(".checkbox").length == $(".checkbox:checked").length) {
                $(".checkall").prop("checked", true);
            } else {
                $(".checkall").removeAttr("checked");
            }
        });
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click', '#status', function(){
        var id = $(this).data('id');
        $.post('{{ route('admin.product.status') }}', {id:id}, function(data){
            if (data.status == 1)
            {
                $('#status'+id).html('<span class="label label-success">Hiện</span>');
            } else {
                $('#status'+id).html('<span class="label label-danger">Ẩn</span>');
            }
        });
    });
    $(document).on('click', '#is_hot', function(){
        var id = $(this).data('id');
        $.post('{{ route('admin.product.is_hot') }}', {id:id}, function(data){
            if (data.is_hot == 1)
            {
                $('#is_hot'+id).html('<span class="label label-success">Hiện</span>');
            } else {
                $('#is_hot'+id).html('<span class="label label-danger">Ẩn</span>');
            }
        });
    });
    $(document).on("change", "input[name='position_hot']", function(){
        var id = $(this).data('position_hot');
        var position = $(this).val();
        var $input = $(this);
        $.ajax({
            url: '{{route('admin.product.positionSpHot')}}',
            type: 'POST',
            data: {
                _token: '{{csrf_token()}}',
                id: id,
                position:position,
            },
            success: function(data){
                $input.val(data);
            },
            error: function(xhr, status, error) {
                console.error(error); // In lỗi ra console nếu có lỗi trong ajax request
            }
        });
    });
</script>
@endsection
