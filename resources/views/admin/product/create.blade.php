@extends('admin.partials.master')
@section('title', 'Thêm sản phẩm')
@section('content')
@section('css')
<link rel="stylesheet" href="{{asset('css/select2.min.css')}}?{{time()}}">
@endsection
<div class="content-wrapper">
<section class="content-header">
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Thêm sản phẩm</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">
                    <li>Trang chủ</li>
                    <li>Quản lý sản phẩm</li>
                    <li>Sản phẩm</li>
                    <li class="active">Thêm</li>
                </ol>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
                                @include('errors.message')
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal" method="POST"
                                    action="{{ route('admin.product.createPost') }}"
                                    enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên sản phẩm: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="name_vi" value="{{ old('name_vi') }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Danh mục: </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" name="cate_product_id">
                                                        <?php  menu($data);?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Bộ lọc: </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" name="boloc_id">
                                                        @foreach($bolocs as $boloc)
                                                            <option value="{{$boloc->id}}">{{$boloc->name_vi}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if(isset($bolocs) && $bolocs != null )
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <div class="col-md-8">
                                                                <ul id="listboloc">
                                                                    @foreach ($boloc_child as $b)
                                                                        <li>
                                                                            <input id="boloc_{{$b->id}}" data-id="{{$b->id}}" type="checkbox" name="child_boloc[]" value="{{$b->id}}">
                                                                            <label for="boloc_{{$b->id}}">{{$b->name_vi}}</label>
                                                                            <select class="form-control select2" name="chitietboloc[]" id="chitietboloc{{$b->id}}" multiple>
                                                                                @foreach($b->Boloc as $child_b)
                                                                                <option value="{{$child_b->id}}">{{$child_b->name_vi}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </li>
                                                                    @endforeach 
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh đại diện: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh chi tiết sản phẩm: </label>
                                                <div class="col-md-8">
                                                    <a class="btn btn-primary btn-sm addimg" style="margin-top: 2px">+ Thêm ảnh</a>
                                                </div>
                                            </div>
                                            <div class="form-group" id="contentimg">
                                            </div> 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Trạng thái: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="status" value="0">
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Sản phẩm home: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="is_home" value="0">
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="idd" value="1">
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Sản phẩm khuyến mại: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="is_sale" value="0">
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="idd" value="1">
                                                </label>
                                            </div>
                                            
                                   <div class="form-group">
                                                <label class="col-md-3 control-label">Giá: </label>
                                                <div class="col-md-8">
                                                    <input type="number" class="form-control"
                                                    placeholder="Nhập giá" name="price" value="0">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Giá cũ: </label>
                                                <div class="col-md-8">
                                                    <input type="number" class="form-control" name="price_unit" value="0">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Vị trí: </label>
                                                <div class="col-md-8">
                                                    <input type="number" class="form-control"
                                                    placeholder="Nhập vị trí" name="position" value="{{ old('position') }}">
                                                </div>
                                            </div>
                                   
                                               <div class="form-group">
                                                <label class="col-md-3 control-label">Khuyến mại: </label>
                                                <div class="col-md-8">
                                                    <textarea name="chucnang" class="form-control"
                                                    id="chucnang" value="">{{ old('chucnang') }}</textarea>
                                                 
                                                </div>
                                            </div>
                                
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Đặc điểm nổi bật: </label>
                                                <div class="col-md-8">
                                                    <textarea name="title_vi" class="form-control"
                                                    id="title_vi" value="">{{ old('title_vi') }}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Miêu tả chi tiết: </label>
                                                <div class="col-md-8">
                                                    <textarea name="description_vi" class="form-control"
                                                    id="description_vi" value="">{{ old('description_vi') }}</textarea>
                                                </div>
                                            </div>

                                                <div class="form-group">
                                                <label class="col-md-3 control-label">THÔNG SỐ KỸ THUẬT: </label>
                                                <div class="col-md-8">
                                                    <textarea name="thongtin_vi" class="form-control"
                                                    id="thongtin_vi" value="">{{ old('thongtin_vi') }}</textarea>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Title_SEO: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Title SEO"
                                                    name="title_seo_vi" value="{{ old('title_seo_vi') }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_key: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_key"
                                                    name="meta_key_vi" value="{{ old('meta_key_vi') }}"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_Des: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="meta_des_vi" value="{{ old('meta_des_vi') }}"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <button class="btn-success btn">Lưu</button>
                                                    <a class="btn-default btn"
                                                    href="{{ route('admin.product.create') }}">
                                                        Hủy
                                                    </a>
                                                    <a class="btn-default btn" href='javascript:goback()'>Quay lại</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{asset('js/select2.min.js')}}?{{time()}}"></script>
{{-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> --}}
<script>
       CKEDITOR.replace( 'chucnang', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',  contentsCss :'{{asset('frontend/css/style.css')}}?{{time()}}',
    } );  
        CKEDITOR.replace( 'title_vi', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',  contentsCss :'{{asset('frontend/css/style.css')}}?{{time()}}',
    } );  
        CKEDITOR.replace( 'description_vi', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',  contentsCss :'{{asset('frontend/css/style.css')}}?{{time()}}',
    } );  
            CKEDITOR.replace( 'thongtin_vi', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',  contentsCss :'{{asset('frontend/css/style.css')}}?{{time()}}',
    } );  
        
    $(document).ready(function() {
        $('.addimg').on('click', function(){
            $('#contentimg').append('<label class="col-md-3 control-label"></label><div class="col-md-8"><input type="file" class="form-control" name="img[]"></div>');
        });
    });

</script>
<script>
    $(document).ready(function(){
        $('.select2').select2();
    });
// Ẩn tất cả các dropdown khi trang web được tải lần đầu
$("select[name='chitietboloc[]']").css('display', 'none');

// Ẩn tất cả các dropdown khi trang web được tải lần đầu
$(document).on('change', "input[name='child_boloc[]']", function() {
    let id_boloc_child = $(this).data('id');
    if ($(this).is(':checked')) {
        $("select#chitietboloc" + id_boloc_child).css('display', 'block');
    } else {
        $("select#chitietboloc" + id_boloc_child).css('display', 'none');
    }
});


// Xử lý sự kiện thay đổi của select box "Chọn bộ lọc"
$("select[name='boloc_id']").change(function() {
    var boloc_id = $(this).val();

    // Sử dụng Ajax để gửi yêu cầu và lấy dữ liệu mới
    $.ajax({
        url: '{{ route('admin.product.getboloc') }}',
        method: 'POST',
        data: {
            boloc_id: boloc_id,
            _token: "{{ csrf_token() }}",
        },
        success: function(data) {
            // Cập nhật giá trị của bolocChild
            bolocChild = data;

            updateUI();
        }
    });
});

// Hàm cập nhật giao diện người dùng
function updateUI() {
    // Lặp qua mảng bolocChild để xây dựng danh sách checkbox và dropdown tương ứng
    var html = '';
    bolocChild.forEach(function(b) {
        html += '<li>';
        html += '<input id="boloc_' + b.id + '" data-id="' + b.id + '" type="checkbox" name="child_boloc[]" value="' + b.id + '">';
        html += '<label for="boloc_' + b.id + '">' + b.name_vi + '</label>';
        html += '<select class="form-control select2" name="chitietboloc[]" id="chitietboloc' + b.id + '" multiple>';
        b.boloc_child.forEach(function(child_b) {
            html += '<option value="' + child_b.id + '">' + child_b.name_vi + '</option>';
        });
        html += '</select>';
        html += '</li>';
    });

    // Cập nhật danh sách trong ul#listboloc
    // $('#listboloc').empty();
    $('#listboloc').html(html);
    $("select[name='chitietboloc[]']").css('display', 'none');
    // Kích hoạt Select2 cho các select box mới
    $('.select2').select2();
}

</script>
@endsection
