@extends('admin.partials.master')
@section('title', 'Cấu hình web')

@section('content')
<div class="content-wrapper">
<section class="content-header">
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Cấu hình web</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">
                    <li>Trang chủ</li>
                    <li class="active">Cấu hình</li>
                </ol>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
                                @include('errors.message')
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal" method="post"
                                    action="{{ route('admin.setting.update') }}"
                                    enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên công ty: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Tên công ty" name="name_vi" value="{{ $settings->name_vi }}">
                                                </div>
                                            </div>
                                   {{--          <div class="form-group">
                                                <label class="col-md-3 control-label">Tên công ty_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Tên công ty" name="name_en" value="{{ $settings->name_en }}">
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Tên công ty_tq: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Tên công ty" name="name_tq" value="{{ $settings->name_tq }}">
                                                </div>
                                            </div> --}}
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Email: </label>
                                                <div class="col-md-8">
                                                    <input type="email" class="form-control"
                                                    placeholder="Email" name="email" value="{{ $settings->email }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Địa chỉ: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Địa chỉ" name="address_vi" value="{{ $settings->address_vi }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Cơ sở 2: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Địa chỉ" name="address2nd" value="{{ $settings->address2nd }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Cơ sở 3: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Địa chỉ" name="address3rd" value="{{ $settings->address3rd }}">
                                                </div>
                                            </div>
                                            
                                         {{--    <div class="form-group">
                                                <label class="col-md-3 control-label">Địa chỉ_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Địa chỉ_en" name="address_en" value="{{ $settings->address_en }}">
                                                </div>
                                            </div>
                                               <div class="form-group">
                                                <label class="col-md-3 control-label">Địa chỉ_tq: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Địa chỉ_tq" name="address_tq" value="{{ $settings->address_tq }}">
                                                </div>
                                            </div> --}}
                                          {{--   <div class="form-group">
                                                <label class="col-md-3 control-label">Điện thoại: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Điện thoại" name="phone" value="{{ $settings->phone }}">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Điện thoại2: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Điện thoại" name="phone2" value="{{ $settings->phone2 }}">
                                                </div>
                                            </div> --}}
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Hotline: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Hotline" name="hotline" value="{{ $settings->hotline }}">
                                                </div>
                                            </div>
                                             

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">website: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="website" name="website" value="{{ $settings->website }}">
                                                </div>
                                            </div>
                                                    <div class="form-group">
                                                <label class="col-md-3 control-label">youtube: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="xd2" value="{{ $settings->xd2 }}">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Messeger: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="xd3" value="{{ $settings->xd3 }}">
                                                </div>
                                            </div>

                                            
                                           {{--  <div class="form-group">
                                                <label class="col-md-3 control-label">Slogan: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Slogan" name="slogan_vi" value="{{ $settings->slogan_vi }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Slogan_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Slogan_en" name="slogan_en" value="{{ $settings->slogan_en }}">
                                                </div>
                                            </div>    --}}
                                    
                                      
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Fb: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="xd5" value="{{ $settings->xd5 }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Zalo: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="xd4" value="{{ $settings->xd4 }}">
                                                </div>
                                            </div>
                                                   {{--  <div class="form-group">
                                                <label class="col-md-3 control-label">Tiktok: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="xd1" value="{{ $settings->xd1 }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tiki: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="g4" value="{{ $settings->g4 }}">
                                                </div>
                                            </div>
                                    
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Lazada: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="cc1" value="{{ $settings->cc1 }}">
                                                </div>
                                            </div> --}}
                                       {{--      <div class="form-group">
                                                <label class="col-md-3 control-label">Shoppe: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="ship" value="{{ $settings->ship }}" min="0">
                                                </div>
                                            </div> --}}
                                            {{--  <div class="form-group">
                                                <label class="col-md-3 control-label">Shoppe: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="g1" value="{{ $settings->g1 }}" min="0">
                                                </div>
                                            </div> --}}
                                           
                                           {{--  
                                           
                                             --}}
{{-- <div class="form-group">
                                                <label class="col-md-3 control-label">Tiều đề lý do: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="g2" value="{{ $settings->g2 }}">
                                                </div>
                                            </div>
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Lý do home: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="map"
                                                    name="g1" rows="5">{{ $settings->g1 }}</textarea>
                                                </div>
                                            </div>  --}}
                                            <!--   <div class="form-group">
                                                <label class="col-md-3 control-label">Phần trăm giảm giá nếu đặt trên 3 sp: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Phần trăm giảm giá nếu đặt trên 3 sp:" name="phantramsale" value="{{ $settings->phantramsale }}">
                                                </div>
                                            </div> -->
     <div class="form-group">
                                                <label class="col-md-3 control-label">Thông tin thanh toán online trang đặt hàng  </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Nội dung chân trang "
                                                    name="tt1" id="tt1">{{ $settings->tt1 }}</textarea>
                                                </div>
                                            </div>
 





    <div class="form-group">
                                                <label class="col-md-3 control-label">TƯ VẤN 247 </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="google maps"
                                                    name="cc2" id="cc2">{{ $settings->cc2 }}</textarea>
                                                </div>
                                            </div>
                                  
                                  <div class="form-group">
                                                <label class="col-md-3 control-label">Info chân trang: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="địa chỉ trang liên hệ"
                                                    name="tt2" id="tt2">{{ $settings->tt2 }}</textarea>
                                                </div>
                                            </div> 
                                                <div class="form-group">
                                                <label class="col-md-3 control-label">Google maps: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="tt3" id="tt3">{{ $settings->tt3 }}</textarea>
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Thẻ head: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Thẻ head:"
                                                    name="thead" id="thead">{{ $settings->thead }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Thẻ body: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Thẻ body"
                                                    name="tbody" id="tbody">{{ $settings->tbody }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">File-robot: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="robot" id="robot">{{ $file }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Logo: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="logo">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($settings->logo)}}" style="max-width: 150px; "/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Icon: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="icon">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($settings->icon)}}" style="max-width: 150px; "/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh banner trên cùng </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="banner">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($settings->banner)}}" style="max-height: 50px;"/>
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh banner - Mobile </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="banner_mobile">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($settings->banner_mobile)}}" style="max-height: 50px;"/>
                                                </div>
                                            </div> 
                                             
                                               <!-- <div class="form-group">
                                                <label class="col-md-3 control-label">Tiều đề câu chuyện home 1 : </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" name="g3" value="{{ $settings->g3 }}">
                                                </div>
                                            </div>

                                            
                                          
 -->

{{-- 
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Tiều đề câu chuyện home 2: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Mã video"
                                                    name="video" value="{{ $settings->video }}">
                                                </div>
                                            </div>

 --}}

          <!--     <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh phiếu bảo hành </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="phieubaohanh">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($settings->phieubaohanh)}}" style="max-width: 150px;"/>
                                                </div>
                                            </div> 
 -->



                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Title_SEO: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Title SEO"
                                                    name="title_seo" value="{{ $settings->title_seo }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_key: </label>
                                                <div class="col-md-8">
                                                    <textarea type="text" class="form-control" placeholder="Meta_key"
                                                        name="meta_key">{{ $settings->meta_key }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_des: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="meta_des">{{ $settings->meta_des }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_og:image : </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control"
                                                    name="meta_og_image">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($setting->meta_og_image)}}"/>
                                                </div>
                                            </div> 
                                            
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <button class="btn-success btn">Lưu</button>
                                                    <a class="btn-default btn" href='javascript:goback()'>Quay lại</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection
@section('script')
<script>


    CKEDITOR.replace( 'tt1', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',
  
    } );


    CKEDITOR.replace( 'tt2', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',
  
    } );

    CKEDITOR.replace( 'tt3', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',
  
    } );



  
        CKEDITOR.replace( 'cc2', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',
  
    } );
       

</script>
@endsection