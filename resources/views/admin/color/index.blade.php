@extends('admin.partials.master')
@section('title', 'Danh mục voucher')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>voucher</h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i>Trang chủ</li>
            <li>Quản lý voucher</li>
        </ol>
        <a href="{{ route('admin.color.create') }}" class="btn btn-primary">
            <i class=" fa fa-fw fa-plus"></i>
            Thêm mới
        </a>
    </section>
    <section class="content-header">
        <div class="row">
            <div class="col-md-12">
                <div class="static-content-wrapper">
                    <div class="static-content">
                        <div class="page-content">
                            @include('errors.message')
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <div class="options">
                                                </div>
                                            </div>
                                            <div class="panel-body" style="overflow-x:auto;">
                                                <table id="datatable_color" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>STT</th>
                                                            <th>Mã</th>
                                                            <th>Giảm</th>
                                                            <th>Trạng thái</th>
                                                            <th>Cập nhật</th>
                                                           
                                                            <th>Xử lý</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($color as $key => $c)
                                                        <tr>
                                                            <td>{{ $key + 1 }}</td>
                                                            <td>{{ $c->value }}</td>
                                                            <td>{{ $c->ma }}</td>
                                                            @php
                                                                $bill = DB::table('bills')->where('status',3)->where('magiamgia',$c->value)->first();
                                                            @endphp
                                                            <td> 
                                                                    @if ($bill) Đã sử dụng @else Chưa sử dụng @endif
                                                              
                                                              
                                                            </td>
                                                            <td>{{ $c->updated_at }}</td>
                                                            <td>
                                                                <a href="{{ route('admin.color.update', $c->id)}}">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>
                                                                <a href="{{ route('admin.color.destroy', $c->id) }}"
                                                                    type="button"
                                                                    onclick="return confirm_delete('Bạn có muốn xóa không ?')">
                                                                    <i class="fa fa-times-circle"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div><!-- /.content-wrapper -->
@endsection
<style type="text/css">
    body{
    margin:0;
    padding:0;
    background:#f1f1f1;
    /*font:70% Arial, Helvetica, sans-serif;*/
    color:#555;
    line-height:150%;
    text-align:left;
    }
    a{
        text-decoration:none;
        color:#057fac;
    }
    a:hover{
        text-decoration:none;
        color:#999;
    }
    h1{
        font-size:140%;
        margin:0 20px;
        line-height:80px;
    }
    #container{
        margin:0 auto;
        width:680px;
        background:#fff;
        padding-bottom:20px;
        padding-top: 20px;
    }
    #content{margin:0 20px;}
    p{
        margin:0 auto;
        width:680px;
        padding:1em 0;
    }
    #datatable_color{
        font-size: 14px;
    }
</style>
@section('script')

@endsection