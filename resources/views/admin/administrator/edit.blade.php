@extends('admin.partials.master')
@section('title', 'Sửa admin')
@section('content')
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Sửa Admin</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">
                    <li>Trang chủ</li>
                    <li>Quản lý admin</li>
                    <li class="active">Sửa</li>
                </ol>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
                                @include('errors.message')
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal" method="POST"
                                    action="{{ route('admin.administrator.postUpdate', $administrator->id) }}"
                                    enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"placeholder="Name" name="name"
                                                    value="{{ $administrator->name }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Email </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"placeholder="Name" name="email"
                                                    value="{{ $administrator->email }}">
                                                </div>
                                            </div>

                                                <div class="form-group">
                                                <label class="col-md-3 control-label">Loại tài khoản </label>
                                                <div class="col-md-8">
                                                    <select name="role" id="" required="">
                                                        @if(Auth::user()->role ==2)
                                                        <option value="2" {{$administrator->role == 2 ? 'selected': ''}}>Admin full quyền</option>
                                                        @endif
                                                          <!-- <option value="3" {{$administrator->role == 3 ? 'selected': ''}}>Biên tập viên</option> -->
                                                    </select>
                                                   
                                                </div>
                                            </div>
<!-- @if(Auth::user()->role == 2)
   <div class="form-group">
                    <label for="quyen" class="col-md-3 control-label">Chọn quyền cho user:</label>
                    <div class="col-md-8">

                 <tr id="tr_q" >
                    <td width="140" class="label"></td>
                    <td>
                        <table class="table1" style="width:80% !important" border="1"  >
                          <tr   >
                             <td>#</td> 
                            
                             
                             <td>Xóa{{-- </br><input type="checkbox" name="all" id="checkall2" /> --}}</td>
                             <td>Sửa {{-- </br><input type="checkbox" name="all" id="checkall3" /> --}}</td>
                             <td>Thêm{{-- </br><input type="checkbox" name="all" id="checkall4" /> --}}</td>
                        
                            <td>Modul</td>
                          </tr>
                          <?php $quyen=explode('|',$administrator->quyen);

                      
                          $mang=array();$j=0; $modul=array(); $n=1;

                                               


                                for($i=0;$i<count($quyen)-1;$i++){
                                  
                                    $bien=explode('-',$quyen[$i]);

                                    $mang[$j++]=$bien[1];
                                    $mang[$j++]=$bien[2];
                                    $mang[$j++]=$bien[3];
              
                                
                                
                                }
               
                            $i=1;
                           $name[$i++]='Danh mục sản phẩm ';
                            $name[$i++]='Danh sách sản phẩm';
                            $name[$i++]='Bộ đặc điểm so sánh';
                            $name[$i++]='Danh mục bài viết';
                            $name[$i++]='Danh sách bài viết';
                            $name[$i++]='Thông tin điền form';
                            $name[$i++]='Ảnh slide';
                            $name[$i++]='Cấu hình';
                                    $name[$i++]='Logo đối tác';
                            $name[$i++]='Giới thiệu';
                              $name[$i++]='Phần mềm';
                            $i=1;
                            $modul[$i++]='cateproducts';
                            $modul[$i++]='products';
                            $modul[$i++]='filters';
                            $modul[$i++]='cateposts';
                            $modul[$i++]='posts';
                            $modul[$i++]='contacts';
                            $modul[$i++]='slides';
                            $modul[$i++]='settings';
                             
                          $modul[$i++]='uudais';
                              $modul[$i++]='gioithieus';
                                 $modul[$i++]='phanmems';
                           $j=0;
                          ?>
                         <?php  for($i=1;$i<12;$i++) { ?> 
                          <tr  >
                             <td><?php echo $i?></td> 
                             <input type="hidden" name="modul<?php echo $i?>" value="<?php echo $modul[$i];?>" />
                     
                             
                             <td><input type="checkbox" <?php if(isset($mang[$j]) && $mang[$j++]==1) echo 'checked';?> class="col2" name="sub2<?php echo $i?>" value="1" /></td>
                             <td><input type="checkbox" <?php if(isset($mang[$j]) && $mang[$j++]==1) echo 'checked';?> class="col3" name="sub3<?php echo $i?>" value="1" /></td>
                             <td><input type="checkbox" <?php if(isset($mang[$j]) && $mang[$j++]==1) echo 'checked';?> class="col4" name="sub4<?php echo $i?>" value="1" /></td>
                           
                              <td><?php echo $name[$i]?></td>
                          </tr>
                          <?php } ?>




                
                    
                      </table>
                    </td>
                </tr>
                </div>
            </div>
@endif -->

                                            <div class="form-group">
                                                <label for="password" class="col-md-3 control-label">{{ __('Password mới') }}</label>
                                                <div class="col-md-8">
                                                    <input id="password" type="password"
                                                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <button class="btn-success btn">Lưu</button>
                                                    <a class="btn-default btn" href="{{ route('admin.administrator.create') }}">
                                                        Hủy
                                                    </a>
                                                    <a class="btn-default btn" href='javascript:goback()'>Quay lại</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
</div><!-- /.content-wrapper -->
@endsection
