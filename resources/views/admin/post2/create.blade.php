@extends('admin.partials.master')
@section('title', 'Thêm bài viết')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="static-content-wrapper">
            <div class="static-content">
                <div class="page-content">
                    <div class="page-heading">
                        <h1>Thêm bài viết</h1>
                        <div class="options">
                            <div class="btn-toolbar">
                            </div>
                        </div>
                    </div>
                    <ol class="breadcrumb">
                        <li>Trang chủ</li>
                        <li>Quản lý bài viết</li>
                        <li>Bài viết</li>
                        <li class="active">Thêm</li>
                    </ol>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                    </div>
                                    @include('errors.message')
                                    <div class="panel-body">
                                        <form role="form" class="form-horizontal" method="POST"
                                        action="{{ route('admin.post2.createPost2') }}"
                                        enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Tên: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập tên" name="name_vi" value="{{ old('name_vi') }}">
                                                    </div>
                                                </div>
   <div class="form-group">
                                                    <label class="col-md-3 control-label">Danh mục: </label>
                                                    <div class="col-md-8">
                                                        <select class="form-control" name="cate_post2_id">
                                                            <?php  menu($data);?>
                                                        </select>
                                                    </div>
                                                </div>
                                             {{--    <div class="form-group">
                                                    <label class="col-md-3 control-label">Tên_en: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập tên" name="name_en" value="{{ old('name_en') }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Tên_tq: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập tên" name="name_tq" value="{{ old('name_tq') }}">
                                                    </div>
                                                </div>
 --}}
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Ảnh bìa: </label>
                                                    <div class="col-md-8">
                                                        <input type="file" class="form-control" name="image">
                                                    </div>
                                                </div>

{{-- 
                                                  <div class="form-group">
                                                    <label class="col-md-3 control-label">upload tài liệu (nếu có): </label>
                                                    <div class="col-md-8">
                                                        <input type="file" class="form-control" name="pdf_tailieu">
                                                    </div>
                                                </div> --}}
                                              

    


                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Hạng mục: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập " name="hangmuc" value="{{ old('hangmuc') }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">khách hàng: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập khách hàng" name="khachhang" value="{{ old('khachhang') }}">
                                                    </div>
                                                </div>
                                                      <div class="form-group">
                                                    <label class="col-md-3 control-label">Địa Điểm : </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập Địa Điểm" name="diadiem" value="{{ old('diadiem') }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Diện tích: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập Diện tích" name="dientich" value="{{ old('dientich') }}">
                                                    </div>
                                                </div>
                                                  <div class="form-group">
                                                    <label class="col-md-3 control-label">Phong cách: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập Phong cách" name="phongcach" value="{{ old('phongcach') }}">
                                                    </div>
                                                </div>
    <div class="form-group">
                                                    <label class="col-md-3 control-label">Năm thực hiện: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập " name="namthuchien" value="{{ old('namthuchien') }}">
                                                    </div>
                                                </div>

    <div class="form-group">
                                                    <label class="col-md-3 control-label">Thời gian thi công: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập " name="thoigianthicong" value="{{ old('thoigianthicong') }}">
                                                    </div>
                                                </div>
                                                    <div class="form-group">
                                                    <label class="col-md-3 control-label">Thời gian thiết kế: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập " name="thoigianthietke" value="{{ old('thoigianthietke') }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Trạng thái: </label>
                                                    <label class="switch">
                                                        <input type="checkbox" name="status" value="0">
                                                        <span class="slider round"></span>
                                                        <input type="hidden" name="id" value="1">
                                                    </label>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Tin hot: </label>
                                                    <label class="switch">
                                                        <input type="checkbox" name="is_home" value="0">
                                                        <span class="slider round"></span>
                                                        <input type="hidden" name="idd" value="1">
                                                    </label>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Vị trí: </label>
                                                    <div class="col-md-8">
                                                        <input type="number" class="form-control"
                                                        placeholder="Nhập vị trí" name="position" value="{{ old('position') }}">
                                                    </div>
                                                </div>

                                           

                                           <div class="form-group">
                                                    <label class="col-md-3 control-label">Mô tả ngắn: </label>
                                                    <div class="col-md-8">
                                                        <textarea name="title_vi" class="form-control " id="title_vi">
                                                            {{ old('title_vi') }}
                                                        </textarea>
                                                    </div>
                                                </div>  
{{-- 
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Giới thiệu_en: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" placeholder="Giới thiệu"
                                                        name="title_en" value="{{ old('title_en') }}">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Giới thiệu_tq: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" placeholder="Giới thiệu"
                                                        name="title_tq" value="{{ old('title_tq') }}">
                                                    </div>
                                                </div> --}}


                                             {{--    <div class="form-group">
                                                    <label class="col-md-3 control-label">Miêu tả_en: </label>
                                                    <div class="col-md-8">
                                                        <textarea name="description_en" class="form-control " id="editor2">
                                                            {{ old('description_en') }}
                                                        </textarea>
                                                    </div>
                                                </div>
                                                   <div class="form-group">
                                                    <label class="col-md-3 control-label">Miêu tả_tq: </label>
                                                    <div class="col-md-8">
                                                        <textarea name="description_tq" class="form-control " id="editor22">
                                                            {{ old('description_tq') }}
                                                        </textarea>
                                                    </div>
                                                </div> --}}

                                             <div class="form-group">
                                                    <label class="col-md-3 control-label">Thông tin giới thiệu: </label>
                                                    <div class="col-md-8">
                                                        <textarea name="description_vi" class="form-control " id="description_vi">
                                                            {{ old('description_vi') }}
                                                        </textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Title_SEO: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" placeholder="Title SEO"
                                                        name="title_seo_vi" value="{{ old('title_seo_vi') }}">
                                                    </div>
                                                </div>
{{-- 
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Title_SEO_en: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" placeholder="Title SEO"
                                                        name="title_seo_en" value="{{ old('title_seo_en') }}">
                                                    </div>
                                                </div> --}}

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Meta_key: </label>
                                                    <div class="col-md-8">
                                                        <textarea class="form-control" placeholder="Meta_key" name="meta_key_vi">
                                                            {{ old('meta_key_vi') }}
                                                        </textarea>
                                                    </div>
                                                </div>

                                               {{--  <div class="form-group">
                                                    <label class="col-md-3 control-label">Meta_key_en: </label>
                                                    <div class="col-md-8">
                                                        <textarea class="form-control" placeholder="Meta_key" name="meta_key_en">
                                                            {{ old('meta_key_en') }}
                                                        </textarea>
                                                    </div>
                                                </div> --}}

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Meta_Des: </label>
                                                    <div class="col-md-8">
                                                        <textarea class="form-control" placeholder="Meta_Des" name="meta_des_vi">
                                                            {{ old('meta_des_vi') }}
                                                        </textarea>
                                                    </div>
                                                </div>

                                               {{--  <div class="form-group">
                                                    <label class="col-md-3 control-label">Meta_Des_en: </label>
                                                    <div class="col-md-8">
                                                        <textarea class="form-control" placeholder="Meta_Des" name="meta_des_en">
                                                            {{ old('meta_des_en') }}
                                                        </textarea>
                                                    </div>
                                                </div> --}}
                                            </div>
                                            <div class="panel-footer">
                                                <div class="row">
                                                    <div class="col-sm-8 col-sm-offset-2">
                                                        <button class="btn-success btn">Lưu</button>
                                                        <a class="btn-default btn" href="{{ route('admin.post2.create') }}">Hủy</a>
                                                        <a class="btn-default btn" href='javascript:goback()'>Quay lại</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('script')
<script>
   
              CKEDITOR.replace( 'description_vi', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',  contentsCss :'{{asset('frontend/css/font.css')}}?{{time()}}',
    } );

      CKEDITOR.replace( 'title_vi', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',  contentsCss :'{{asset('frontend/css/font.css')}}?{{time()}}',
    } );

</script>

@endsection