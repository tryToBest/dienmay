@extends('admin.partials.master')
@section('title', 'Sửa bài viết')
@section('content')
<div class="content-wrapper">
<section class="content-header">
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Sửa bài viết</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">
                    <li>Trang chủ</li>
                    <li>Quản lý bài viết</li>
                    <li>Bài viết</li>
                    <li class="active">Sửa</li>
                </ol>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
                                @include('errors.message')
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal" method="POST"
                                    action="{{ route('admin.post2.post2Update', $post2->id) }}"
                                    enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="name_vi" value="{{ $post2->name_vi }}">
                                                </div>
                                            </div>

                                        <!--     <div class="form-group">
                                                <label class="col-md-3 control-label">Tên_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="name_en" value="{{ $post2->name_en }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên_tq: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="name_tq" value="{{ $post2->name_tq }}">
                                                </div>
                                            </div>
 -->             <div class="form-group">
                                                <label class="col-md-3 control-label">Danh mục: </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" name="cate_post2_id">
                                                        <?php $hihi = DB::table('cate_post2s')
                                                        ->where('id',$post2->cate_post2_id)->first(); ?>
                                                        @if(isset($hihi))
                                                        <option value="{{ $post2->cate_post2_id }}">
                                                            {{ $hihi->name_vi }}
                                                        </option>
                                                        @else
                                                        <option value="0">
                                                            {{ $post2->name_vi }}
                                                        </option>
                                                        @endif
                                                        <?php  menu($data);?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh bìa: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-8">
                                                    <img src="{{ asset($post2->image) }}"
                                                    style="height: 50px; width: 50px">
                                                </div>
                                            </div>

<!-- 
                                                    <div class="form-group">
                                                <label class="col-md-3 control-label">upload tài liệu(nếu có): </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="pdf_tailieu">
                                                </div>
                                            </div>
                                            @if($post2->pdf_tailieu != null)
                                            <div class="form-group">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-8">
                                                  link tài liệu: {{asset($post2->pdf_tailieu)}}
                                                </div>
                                            </div>
                                            @endif -->


                                             

        <div class="form-group">

                                                <label class="col-md-3 control-label">Ảnh chi tiết dự án: </label>

                                                <div class="col-md-8">

                                                    @foreach($img_detail as $i)

                                                        <div class="image">

                                                            <img src="{{ asset($i->name) }}" style="height: 50px; width: 50px">

                                                            <label><span class="btn btn-danger delImage" id="data-1" data-id="{{ $i->id }}"> X </span></label>

                                                        </div>

                                                    @endforeach

                                                    <a class="btn btn-primary btn-sm addimg" style="margin-top: 2px; margin-left: 0">+ Thêm ảnh</a>

                                                </div>

                                            </div>



                                            <div class="form-group" id="contentimg">

                                            </div>





                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Hạng mục: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập " name="hangmuc" value="{{ $post2->hangmuc }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">khách hàng: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập khách hàng" name="khachhang" value="{{ $post2->khachhang }}">
                                                    </div>
                                                </div>
                                                      <div class="form-group">
                                                    <label class="col-md-3 control-label">Địa Điểm : </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập Địa Điểm" name="diadiem" value="{{ $post2->diadiem }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Diện tích: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập Diện tích" name="dientich" value="{{ $post2->dientich }}">
                                                    </div>
                                                </div>
                                                  <div class="form-group">
                                                    <label class="col-md-3 control-label">Phong cách: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập Phong cách" name="phongcach" value="{{ $post2->phongcach }}">
                                                    </div>
                                                </div>
                                                  <div class="form-group">
                                                    <label class="col-md-3 control-label">Năm thực hiện: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập " name="namthuchien" value="{{ $post2->namthuchien }}">
                                                    </div>
                                                </div>

                                                  <div class="form-group">
                                                    <label class="col-md-3 control-label">Thời gian thi công: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập " name="thoigianthicong" value="{{ $post2->thoigianthicong }}">
                                                    </div>
                                                </div>
                                                   <div class="form-group">
                                                    <label class="col-md-3 control-label">Thời gian thiết kế: </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                        placeholder="Nhập " name="thoigianthietke" value="{{ $post2->thoigianthietke }}">
                                                    </div>
                                                </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Trạng thái: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="status"
                                                    {{ ($post2->status) ? 'checked': '' }}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tin hot: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="is_home"
                                                    {{ ($post2->is_home) ? 'checked': '' }}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Vị trí: </label>
                                                <div class="col-md-8">
                                                    <input type="number" class="form-control"
                                                    placeholder="Nhập vị trí" name="position"
                                                    value="{{ $post2->position }}">
                                                </div>
                                            </div>

                                      

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Mô tả ngắn: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Giới thiệu"
                                                    name="title_vi" id="title_vi">
                                                        {{ $post2->title_vi }}
                                                    </textarea>
                                                </div>
                                            </div>
                                        

                                         <!--    <div class="form-group">
                                                <label class="col-md-3 control-label">Giới thiệu_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Giới thiệu"
                                                    name="title_en"  value="{{ $post2->title_en }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Giới thiệu_tq: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Giới thiệu"
                                                    name="title_tq"  value="{{ $post2->title_tq }}">
                                                </div>
                                            </div>
 -->
                                  
<!-- 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Miêu tả_en: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Miêu tả"
                                                    name="description_en" id="editor2">
                                                        {{ $post2->description_en }}
                                                    </textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Miêu tả_tq: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Miêu tả"
                                                    name="description_tq" id="editor22">
                                                        {{ $post2->description_tq }}
                                                    </textarea>
                                                </div>
                                            </div> -->
      <div class="form-group">
                                                <label class="col-md-3 control-label">Thông tin giới thiệu: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Giới thiệu"
                                                    name="description_vi" id="description_vi">
                                                        {{ $post2->description_vi }}
                                                    </textarea>
                                                </div>
                                            </div>
                               

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Title_SEO: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Title SEO"
                                                    name="title_seo_vi" value="{{ $post2->title_seo_vi }}">
                                                </div>
                                            </div>
{{-- 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Title_SEO_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Title SEO"
                                                    name="title_seo_en" value="{{ $post2->title_seo_en }}">
                                                </div>
                                            </div> --}}

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_key: </label>
                                                <div class="col-md-8">
                                                    <textarea type="text" class="form-control" placeholder="Meta_key"
                                                    name="meta_key_vi">{{ $post2->meta_key_vi }}</textarea>
                                                </div>
                                            </div>
{{-- 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_key_en: </label>
                                                <div class="col-md-8">
                                                    <textarea type="text" class="form-control" placeholder="Meta_key"
                                                    name="meta_key_en">{{ $post2->meta_key_en }}</textarea>
                                                </div>
                                            </div> --}}

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_des: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="meta_des_vi">{{ $post2->meta_des_vi }}</textarea>
                                                </div>
                                            </div>
{{-- 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_des_en: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="meta_des_en">{{ $post2->meta_des_en }}</textarea>
                                                </div>
                                            </div> --}}
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <button class="btn-success btn">Lưu</button>
                                                    <a class="btn-default btn"
                                                    href="{{ route('admin.post2.create') }}">
                                                        Hủy
                                                    </a>
                                                    <a class="btn-default btn" href='javascript:goback()'>Quay lại</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection
@section('script')
<script>
 
      CKEDITOR.replace( 'description_vi', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',  contentsCss :'{{asset('frontend/css/font.css')}}?{{time()}}',
    } );
           CKEDITOR.replace( 'title_vi', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',  contentsCss :'{{asset('frontend/css/font.css')}}?{{time()}}',
    } );
          $(document).ready(function() {

        $('.addimg').on('click', function(){

            $('#contentimg').append('<label class="col-md-3 control-label"></label><div class="col-md-8"><input type="file" class="form-control" name="img[]"></div>');

        });

        $('.delImage').on('click',function(){

            id = $(this).data('id');

            if(confirm('Bạn có muốn xóa ?')){

                flag = $(this).parent('label').parent('div.image').hide();

                $.get('{{ route('index') }}/admin/post2/delImage/', {id:id},function(data){

                });

            }

        });

    });

</script>


@endsection