@extends('admin.partials.master')
@section('title', 'Sửa danh mục')
@section('content')
<div class="content-wrapper">
<section class="content-header">
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Sửa danh mục</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-plus"></i>Thêm mới</a>
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">
                    <li>Trang chủ</li>
                    <li>Quản lý tin</li>
                    <li>Danh mục</li>
                    <li class="active">Sửa</li>
                </ol>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
                                @include('errors.message')
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal" method="POST"
                                        action="{{ route('admin.cate_ttbh.postUpdate', $cate_ttbh->id) }}"
                                        enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên danh mục: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Name"
                                                    name="name_vi" value="{{ $cate_ttbh->name_vi }}">
                                                </div>
                                            </div>

                                      {{--       <div class="form-group">
                                                <label class="col-md-3 control-label">Tên danh mục_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Name"
                                                    name="name_en" value="{{ $cate_ttbh->name_en }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên danh mục_tq: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Name"
                                                    name="name_tq" value="{{ $cate_ttbh->name_tq }}">
                                                </div>
                                            </div> --}}

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh bìa: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image"
                                                    value="{{ $cate_ttbh->image }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($cate_ttbh->image)}}" style="max-width: 30%;"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh Banner: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="banner"
                                                    value="{{ $cate_ttbh->banner }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($cate_ttbh->banner)}}" style="max-width: 30%;"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Vị trí: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Vị trí"
                                                    name="position" value="{{ $cate_ttbh->position }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Địa chỉ: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="address"
                                                    name="diachi_vi" value="{{ $cate_ttbh->diachi_vi }}">
                                                </div>
                                            </div>
                                               <div class="form-group">
                                                <label class="col-md-3 control-label">Email: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="email"
                                                    name="email" value="{{ $cate_ttbh->email }}">
                                                </div>
                                            </div>



                                    <div class="form-group">
                                                <label class="col-md-3 control-label">Hotline </label>
                                                <div class="col-md-8">
                                                   
                                                      <textarea class="form-control" name="hotline" id="editor1">{{ $cate_ttbh->hotline }}</textarea>
                                                </div>
                                            </div>
                                           {{--  <div class="form-group">
                                                <label class="col-md-3 control-label">Miêu tả: </label>
                                                <div class="col-md-8">
                                                   
                                                      <textarea class="form-control" name="description_vi" id="editor1">{{ $cate_ttbh->description_vi }}</textarea>
                                                </div>
                                            </div> --}}
{{-- 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Miêu tả_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Miêu tả"
                                                    name="description_en" value="{{ $cate_ttbh->description_en }}">
                                                </div>
                                            </div>
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Miêu tả_tq: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Miêu tả"
                                                    name="description_tq" value="{{ $cate_ttbh->description_tq }}">
                                                </div>
                                            </div> --}}

                                            <div style="margin-top: 20px;margin-bottom: 20px;">
                                                <label class="col-md-3 control-label">Trạng thái: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="status"
                                                    value="0" {{($cate_ttbh->status)?'checked':''}}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>

                                          <!--   <div style="margin-top: 20px;margin-bottom: 20px;">
                                                <label class="col-md-3 control-label">Menu: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="menu"
                                                    value="0" {{($cate_ttbh->menu)?'checked':''}}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>

                                            <div style="margin-top: 20px;margin-bottom: 20px;">
                                                <label class="col-md-3 control-label">Footer: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="ft"
                                                    value="0" {{($cate_ttbh->ft)?'checked':''}}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div> -->

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Danh mục</label>
                                                <div class="col-md-9">
                                                    <select class="form-control" name="parent_id">
                                                   @if($cate_ttbh->parent_id==0)
                                                  <option {{$cate_ttbh->parent_id == 0 ? 'selected': ''}} value="0">Danh mục cha
                                                  </option>
                                                @endif
                                            @foreach($get_cha as $l)
                                                
                                    <option {{$cate_ttbh->parent_id == $l->id ? 'selected': ''}} value="{{$l->id}}">
                                       
                                        {{$l->name_vi}}
                                      

                                    </option>
                                        @php
                                        $get_cap2 =DB::table('cate_ttbhs')->where('status',1)->where('parent_id',$l->id)->get();
                                        @endphp
                                          @foreach($get_cap2 as $l2)
                                                <option {{$cate_ttbh->parent_id == $l2->id ? 'selected': ''}} value="{{$l2->id}}">
                                       
                                                   -- {{$l2->name_vi}}
                                                </option>
                                                 @endforeach
                                    @endforeach

                                                    </select>



                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Title_SEO: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Title SEO"
                                                    name="title_seo_vi" value="{{ $cate_ttbh->title_seo_vi }}">
                                                </div>
                                            </div>

                                          {{--   <div class="form-group">
                                                <label class="col-md-3 control-label">Title_SEO_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Title SEO"
                                                    name="title_seo_en" value="{{ $cate_ttbh->title_seo_en }}">
                                                </div>
                                            </div>
 --}}
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_key: </label>
                                                <div class="col-md-8">
                                                    <textarea type="text" class="form-control" placeholder="Meta_key"
                                                    name="meta_key_vi">{{ $cate_ttbh->meta_key_vi }}</textarea>
                                                </div>
                                            </div>

                                    {{--         <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_key_en: </label>
                                                <div class="col-md-8">
                                                    <textarea type="text" class="form-control" placeholder="Meta_key"
                                                    name="meta_key_en">{{ $cate_ttbh->meta_key_en }}</textarea>
                                                </div>
                                            </div> --}}

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_des: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="meta_des_vi">{{ $cate_ttbh->meta_des_vi }}</textarea>
                                                </div>
                                            </div>

                                           {{--  <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_des_en: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="meta_des_en">{{ $cate_ttbh->meta_des_en }}</textarea>
                                                </div>
                                            </div> --}}
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <button class="btn-success btn">Lưu</button>
                                                    <a class="btn-default btn" href="{{ route('admin.cate_ttbh.home') }}">
                                                        Hủy
                                                    </a>
                                                    <a class="btn-default btn" href='javascript:goback()'>Quay lại</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection
@section('script')

<script>
    CKEDITOR.replace( 'editor1', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',
    } );


</script>
@endsection