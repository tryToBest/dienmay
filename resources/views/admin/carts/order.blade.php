@extends('admin.partials.master')
@section('title', 'Quản lý đơn hàng')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Quản lý đơn hàng</h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i>Trang chủ</li>
            <li>Quản lý đơn hàng</li>
            <li class="active">Đơn hàng</li>
        </ol>
    </section>
    <section class="content-header">
        <div class="static-content-wrapper">
            <div class="static-content">
                <div class="page-content">
                    @include('errors.message')
                    <div class="container-fluid">

                            <div class="xuatexcel">
                            <form action="{{ route('xuat_excel') }}" method="get">
                                @csrf
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="">Chọn ngày</label>
                                        <input type="date" name="start" class="form-control" required="">
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">Chọn ngày</label>
                                        <input type="date" name="end" class="form-control" required="">
                                    </div>
                                    <div class="col-md-3">
                                        <label for=""></label>
                                        <button class="btn btn-success">Xuất excel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body" style="overflow-x:auto;">
                                        <form method="post" action="{{ route('admin.cart.checkbox') }}">
                                            {{ csrf_field() }}
                                            <table id="datatable_order" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th><input type="checkbox" class="checkall" name="checkall" /></th>
                                                        <th style="max-width: 20px!important">STT</th>
                                                        <th>Mã ĐH</th>
                                                        <th>Ngày đặt</th>
                                                        <th>Tên</th>
                                                        <th>SĐT</th>
                                                        <th>Thanh toán</th>
                                                        <th>Tổng tiền</th>
                                                        <th>Trạng thái</th>
                                                        <th>Xử lý</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($order as $key => $c)
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" name="checkbox[]" class="checkbox" value="{{ $c->id }}">
                                                        </td>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>{{ $c->orderId }}</td>
                                                        <td>{{ $c->created_at }}</td>
                                                        @php
                                                            $tinh = DB::table('province')->where('id', $c->tinh)->first();
                            $huyen = DB::table('district')->where('id', $c->huyen)->first();
                            $xa = DB::table('ward')->where('id', $c->xa)->first();
                                                        @endphp
                                                        <td>{{ $c->name }}</td>
                                                        <td>{{ $c->phone }}</td>
                                                        <td>
                                                            @if($c->payment == 1)
                                                                Thanh toán khi nhận hàng
                                                            @endif
                                                            @if($c->payment == 2)
                                                                Chuyển khoản
                                                            @endif
                                                            @if($c->payment == 3)
                                                                Thanh toán online
                                                            @endif
                                                        </td>
                                                        <td>{{ number_format($c->total) }} đ</td>
                                                        <td>
                                                            @if($c->status == 1)
                                                            Chờ xử lý
                                                            @elseif($c->status == 2)
                                                            Đang xử lý
                                                            @elseif($c->status == 2)
                                                            Đang giao hàng
                                                            @elseif($c->status == 3)
                                                            Hoàn thành
                                                            @elseif($c->status == 4)
                                                            Hệ thống hủy
                                                            @elseif($c->status == 5)
                                                            Thất bại
                                                            @elseif($c->status == 6)
                                                            Khách hủy
                                                            @elseif($c->status == 7)
                                                           Hãng vận chuyển hủy
                                                                @elseif($c->status == 8)
                                                          Đang chuyển hoàn
                                                            @elseif($c->status == 9)
                                                          Đã chuyển hoàn
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('admin.bill', $c->id)}}">
                                                                <i class="glyphicon glyphicon-eye-open"></i>
                                                            </a>
                                                            <a href="{{ route('admin.destroyOrder', $c->id) }}"
                                                                type="button"
                                                                onclick="return confirm_delete('Bạn có muốn xóa không ?')">
                                                                <i class="fa fa-times-circle"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <th><input type="checkbox" class="checkall" name="checkall" /></th>
                                                    <th>STT</th>
                                                    <th>Mã ĐH</th>
                                                        <th>Ngày đặt</th>
                                                        <th>Tên</th>
                                                        <td>SĐT</td>
                                                        <th>Thanh toán</th>
                                                        <th>Tổng tiền</th>
                                                        <th>Trạng thái</th>
                                                        <th>Xử lý</th>
                                                </tfoot>
                                            </table>
                                            <select class="" name="select_action">
                                                <option value="0">Lựa chọn</option>
                                                <option value="1">Xóa</option>
                                            </select>
                                            <button id="delete_all" class="btn-success">Thực hiện</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div><!-- /.content-wrapper -->
@endsection
@section('script')
<script type="text/javascript">
    $('#datatable_order').DataTable( {
        "oLanguage": {
           "sSearch": "Tìm kiếm",
           "sEmptyTable": "Không có dữ liệu trong bảng",
           "sLengthMenu": 'Hiển thị <select>'+
                '<option value="10">10</option>'+
                '<option value="20">20</option>'+
                '<option value="30">30</option>'+
                '<option value="40">40</option>'+
                '<option value="50">50</option>'+
                '<option value="-1">All</option>'+
                '</select>',
            "sInfo": "Tổng cộng _TOTAL_ mục (_START_ trên _END_)",
            "sInfoFiltered": " - Tìm kiếm từ _MAX_ mục",
         },
        "pageLength": 20,
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value="">Chọn</option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .css( 'width', '60px' )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
</script>
</script>
<script type='text/javascript'>
    $(document).ready(function(){
        $(".checkall").change(function(){
            var checked = $(this).is(':checked');
            if(checked){
                $(".checkbox").each(function(){
                    $(this).prop("checked",true);
                });
            }else{
                $(".checkbox").each(function(){
                    $(this).prop("checked",false);
                });
            }
        });
        $(".checkbox").click(function(){
            if($(".checkbox").length == $(".checkbox:checked").length) {
                $(".checkall").prop("checked", true);
            } else {
                $(".checkall").removeAttr("checked");
            }
        });
    });
</script>
@endsection