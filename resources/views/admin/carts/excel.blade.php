<table>
    <thead>
        <tr>
            <th>STT</th>
            <th>Ngày đặt</th>
            <th>Tên</th>
             <th>SĐT</th>
            <th>Địa chỉ</th>
                                                        <th>Tỉnh</th>
                                                        <th>Huyện</th>
            {{-- <th>Thanh toán</th> --}}
            <th>Sản phẩm</th>
            <th>Tổng tiền</th>
             <th>Ghi chú</th>
            <th>Trạng thái</th>
        </tr>
    </thead>
    <tbody>
        @foreach($order as $key => $c)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $c->created_at }}</td>
            <td>{{ $c->name }}</td>
             <th>{{ $c->phone }}</th>
             @php
                $tinh = DB::table('province')->where('id', $c->tinh)->first();
                            $huyen = DB::table('district')->where('id', $c->huyen)->first();
            @endphp
            <td>{{ $c->address }}</td>
            @if ($tinh)
                <td>{{ $tinh->_name }}</td>
            @endif
                <td>{{ $c->huyen }}</td>
        {{--     
            <td>
                @if($c->payment == 1)
                    Thanh toán khi nhận hàng
                @endif
                @if($c->payment == 2)
                    Chuyển khoản
                @endif
                @if($c->payment == 3)
                    Thanh toán online
                @endif
            </td> --}}
            <td>
                @php
                    $sp = DB::table('bill_details')->where('bill_id', $c->id)->get();
                @endphp
                @if ($sp)
                    @foreach ($sp as $s)
                        @php
                            $cc = DB::table('products')->where('id', $s->product_id)->first();
                        @endphp
                        {{ $cc->name_vi }} ({{ $s->quantity }}),
                    @endforeach
                @endif
            </td>
            <td>{{ $c->total }}</td>
            <td>{{$c->note}}</td>
            <td>
                @if($c->status == 1)
                Chờ xử lý
                @elseif($c->status == 2)
                Đang xử lý
                @elseif($c->status == 3)
                Hoàn thành
                @else
                Hủy
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>