@extends('admin.partials.master')
@section('title', 'Sửa phần mềm')
@section('content')
<div class="content-wrapper">
<section class="content-header">
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Sửa phần mềm</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-plus"></i>Thêm mới</a>
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">
                    <li>Trang chủ</li>
                    <li>Quản lý phần mềm</li>
                    <li>phần mềm</li>
                    <li class="active">Sửa</li>
                </ol>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
                                @include('errors.message')
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal" method="POST"
                                        action="{{ route('admin.phanmem.postUpdate', $phanmem->id) }}"
                                        enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên phần mềm: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Name"
                                                    name="name_vi" value="{{ $phanmem->name_vi }}">
                                                </div>
                                            </div>

                                         {{--    <div class="form-group">
                                                <label class="col-md-3 control-label">Tên phần mềm_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Name"
                                                    name="name_en" value="{{ $phanmem->name_en }}">
                                                </div>
                                            </div>  --}}

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tài liệu: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="tailieu"
                                                    value="{{ $phanmem->tailieu }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($phanmem->image)}}" style="max-width: 30%;"/>
                                                </div>
                                            </div>
                                         {{--    <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh Banner: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="banner"
                                                    value="{{ $phanmem->banner }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($phanmem->banner)}}" style="max-width: 30%;"/>
                                                </div>
                                            </div> --}}

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Vị trí: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Vị trí"
                                                    name="position" value="{{ $phanmem->position }}">
                                                </div>
                                            </div>

                                       

                               
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Miêu tả: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" name="description_vi" id="editor1">{{ $phanmem->description_vi }}</textarea>
                                                </div>
                                            </div>

                                   {{--           <div class="form-group">
                                                <label class="col-md-3 control-label">Miêu tả_en: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" name="description_en" id="editor2">{{ $phanmem->description_en }}</textarea>
                                                </div>
                                            </div> --}}



                                            <div style="margin-top: 20px;margin-bottom: 20px;">
                                                <label class="col-md-3 control-label">Trạng thái: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="status"
                                                    value="0" {{($phanmem->status)?'checked':''}}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>

                                            <div style="margin-top: 20px;margin-bottom: 20px;">
                                                <label class="col-md-3 control-label">Menu: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="menu"
                                                    value="0" {{($phanmem->menu)?'checked':''}}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>

                                        {{--     <div style="margin-top: 20px;margin-bottom: 20px;">
                                                <label class="col-md-3 control-label">Footer: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="ft"
                                                    value="0" {{($phanmem->ft)?'checked':''}}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div> --}}

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">phần mềm</label>
                                                <div class="col-md-9">
                                                    <select class="form-control" name="parent_id">
                                                        <?php $hihi = DB::table('cate_products')
                                                        ->where('id',$phanmem->parent_id)->first(); ?>
                                                        @if(isset($hihi))
                                                        <option value="{{ $phanmem->parent_id }}">
                                                            {{ $hihi->name_vi }}
                                                        </option>
                                                        @else
                                                        <option value="0">
                                                            {{ $phanmem->name_vi }}
                                                        </option>
                                                        @endif
                                                        <?php  menu($data);?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Title_SEO: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Title SEO"
                                                    name="title_seo_vi" value="{{ $phanmem->title_seo_vi }}">
                                                </div>
                                            </div>

                                            {{-- <div class="form-group">
                                                <label class="col-md-3 control-label">Title_SEO_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Title SEO"
                                                    name="title_seo_en" value="{{ $phanmem->title_seo_en }}">
                                                </div>
                                            </div>
 --}}
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_key: </label>
                                                <div class="col-md-8">
                                                    <textarea type="text" class="form-control" placeholder="Meta_key"
                                                    name="meta_key_vi">{{ $phanmem->meta_key_vi }}</textarea>
                                                </div>
                                            </div>
{{-- 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_key_en: </label>
                                                <div class="col-md-8">
                                                    <textarea type="text" class="form-control" placeholder="Meta_key"
                                                    name="meta_key_en">{{ $phanmem->meta_key_en }}</textarea>
                                                </div>
                                            </div> --}}

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_des: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="meta_des_vi">{{ $phanmem->meta_des_vi }}</textarea>
                                                </div>
                                            </div>

                                          {{--   <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_des_en: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="meta_des_en">{{ $phanmem->meta_des_en }}</textarea>
                                                </div>
                                            </div> --}}
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <button class="btn-success btn">Lưu</button>
                                                    <a class="btn-default btn" href="{{ route('admin.phanmem.home') }}">
                                                        Hủy
                                                    </a>
                                                    <a class="btn-default btn" href='javascript:goback()'>Quay lại</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection
@section('script')
<script>
    CKEDITOR.replace( 'editor1', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',
    } );


  
</script>
@endsection