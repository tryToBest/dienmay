@extends('admin.partials.master')
@section('title', 'Sửa danh mục')
@section('content')
<div class="content-wrapper">
<section class="content-header">
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Sửa danh mục</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-plus"></i>Thêm mới</a>
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">
                    <li>Trang chủ</li>
                    <li>Quản lý tin</li>
                    <li>Danh mục</li>
                    <li class="active">Sửa</li>
                </ol>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
                                @include('errors.message')
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal" method="POST"
                                        action="{{ route('admin.cate_post3.post3Update', $cate_post3->id) }}"
                                        enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên danh mục: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Name"
                                                    name="name_vi" value="{{ $cate_post3->name_vi }}">
                                                </div>
                                            </div>
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Tên 2: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Name"
                                                    name="name2_vi" value="{{ $cate_post3->name2_vi }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên 3: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Name"
                                                    name="name3_vi" value="{{ $cate_post3->name3_vi }}">
                                                </div>
                                            </div>

                                      {{--       <div class="form-group">
                                                <label class="col-md-3 control-label">Tên danh mục_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Name"
                                                    name="name_en" value="{{ $cate_post3->name_en }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên danh mục_tq: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Name"
                                                    name="name_tq" value="{{ $cate_post3->name_tq }}">
                                                </div>
                                            </div> --}}

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh bìa: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image"
                                                    value="{{ $cate_post3->image }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($cate_post3->image)}}" style="max-width: 30%;"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh Banner: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="banner"
                                                    value="{{ $cate_post3->banner }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($cate_post3->banner)}}" style="max-width: 30%;"/>
                                                </div>
                                            </div>


 <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh giới thiệu trang con1: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image_gioithieu"
                                                    value="{{ $cate_post3->image_gioithieu }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($cate_post3->image_gioithieu)}}" style="max-width: 30%;"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Vị trí: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Vị trí"
                                                    name="position" value="{{ $cate_post3->position }}">
                                                </div>
                                            </div>

                                                    <div class="form-group">
                                                <label class="col-md-3 control-label">Giới thiệu 1: </label>
                                                <div class="col-md-8">
                                                   
                                                      <textarea class="form-control" name="title_vi" id="title_vi">{{ $cate_post3->title_vi }}</textarea>
                                                </div>
                                            </div>

 <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh giới thiệu trang con2: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image_gioithieu2"
                                                    value="{{ $cate_post3->image_gioithieu2 }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                    <img src="{{asset($cate_post3->image_gioithieu2)}}" style="max-width: 30%;"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Giới thiệu 2: </label>
                                                <div class="col-md-8">
                                                   
                                                      <textarea class="form-control" name="description_vi" id="description_vi">{{ $cate_post3->description_vi }}</textarea>
                                                </div>
                                            </div>
{{-- 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Miêu tả_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Miêu tả"
                                                    name="description_en" value="{{ $cate_post3->description_en }}">
                                                </div>
                                            </div>
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Miêu tả_tq: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Miêu tả"
                                                    name="description_tq" value="{{ $cate_post3->description_tq }}">
                                                </div>
                                            </div> --}}

                                            <div style="margin-top: 20px;margin-bottom: 20px;">
                                                <label class="col-md-3 control-label">Trạng thái: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="status"
                                                    value="0" {{($cate_post3->status)?'checked':''}}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>

                                            <div style="margin-top: 20px;margin-bottom: 20px;">
                                                <label class="col-md-3 control-label">Menu: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="menu"
                                                    value="0" {{($cate_post3->menu)?'checked':''}}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>

                                            <div style="margin-top: 20px;margin-bottom: 20px;">
                                                <label class="col-md-3 control-label">Footer: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="ft"
                                                    value="0" {{($cate_post3->ft)?'checked':''}}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Danh mục</label>
                                                <div class="col-md-9">
                                                    <select class="form-control" name="parent_id">
                                            
                                                  <option {{$cate_post3->parent_id == 0 ? 'selected': ''}} value="0">Danh mục cha
                                                  </option>
                                        
                                            @foreach($get_cha as $l)
                                                
                                    <option {{$cate_post3->parent_id == $l->id ? 'selected': ''}} value="{{$l->id}}">
                                       
                                        {{$l->name_vi}}
                                      

                                    </option>
                                        @php
                                        $get_cap2 =DB::table('cate_post3s')->where('status',1)->where('parent_id',$l->id)->get();
                                        @endphp
                                          @foreach($get_cap2 as $l2)
                                                <option {{$cate_post3->parent_id == $l2->id ? 'selected': ''}} value="{{$l2->id}}">
                                       
                                                   -- {{$l2->name_vi}}
                                                </option>
                                                 @endforeach
                                    @endforeach

                                                    </select>



                                                </div>
                                            </div>



                                                <hr style="clear: both;">
<style>
   #div_faq .ul_faq{
    width:25%;float: left;
   }
      #div_faq .ul_faq li.li_con{
        text-align: left;list-style-type: none;display: flex;
      }
</style>
            
   <hr style="border-color: #000;">
               <div class="form-group">
        <label class="col-md-12 text-center " style="font-size: 24px;">FAQS: </label>
    </div>
                   @php

                    $mang_loc_faq111 = explode('|',$cate_post3->faq_id);
                      
                         
                  $mang_loc_faq111 = array_filter($mang_loc_faq111);
            
            
                    @endphp
                             <hr style="clear: both;">
                                <div id="div_faq" >
                           @if(isset($list_Faq)) 
                           @foreach($list_Faq as $k=>$v)
                            <ul class="ul_faq">
                            <li class="active_real">

<input class="checkbox checkbox<?php echo $k?>" type="checkbox"  <?php if(in_array($v['title']['id'],$mang_loc_faq111)) echo 'checked=""'?> style="float:left" name="faq[<?php echo $v['title']['id']?>]" value="<?php echo $v['title']['id']?>">

                                <b><?php echo $v['title']['name_vi']?></b></li>
                    
                            </ul>
                          @endforeach
                          @endif
                        </div>
           <div style="clear: both;"></div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Title_SEO: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Title SEO"
                                                    name="title_seo_vi" value="{{ $cate_post3->title_seo_vi }}">
                                                </div>
                                            </div>

                                          {{--   <div class="form-group">
                                                <label class="col-md-3 control-label">Title_SEO_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" placeholder="Title SEO"
                                                    name="title_seo_en" value="{{ $cate_post3->title_seo_en }}">
                                                </div>
                                            </div>
 --}}
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_key: </label>
                                                <div class="col-md-8">
                                                    <textarea type="text" class="form-control" placeholder="Meta_key"
                                                    name="meta_key_vi">{{ $cate_post3->meta_key_vi }}</textarea>
                                                </div>
                                            </div>

                                    {{--         <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_key_en: </label>
                                                <div class="col-md-8">
                                                    <textarea type="text" class="form-control" placeholder="Meta_key"
                                                    name="meta_key_en">{{ $cate_post3->meta_key_en }}</textarea>
                                                </div>
                                            </div> --}}

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_des: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="meta_des_vi">{{ $cate_post3->meta_des_vi }}</textarea>
                                                </div>
                                            </div>

                                           {{--  <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_des_en: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="meta_des_en">{{ $cate_post3->meta_des_en }}</textarea>
                                                </div>
                                            </div> --}}
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <button class="btn-success btn">Lưu</button>
                                                    <a class="btn-default btn" href="{{ route('admin.cate_post3.home') }}">
                                                        Hủy
                                                    </a>
                                                    <a class="btn-default btn" href='javascript:goback()'>Quay lại</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection
@section('script')

<script>
    CKEDITOR.replace( 'title_vi', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',
    } );
    CKEDITOR.replace( 'description_vi', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',
    } );

</script>
@endsection