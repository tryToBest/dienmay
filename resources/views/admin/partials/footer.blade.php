<footer class="main-footer">
<div class="pull-right hidden-xs">
  <b>Version</b> 1.0
</div>
<strong>Copyright &copy; 2022 by <a href="https://conex-agency.com/" target="_blank">conex-agency.com</a>.</strong> All rights reserved.
</footer>