@extends('admin.partials.master')
@section('title', 'Chi tiết ')
@section('content')
<div class="content-wrapper">
<section class="content-header">
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Chi tiết </h1>
                    {{-- <div class="options">
                        <div class="btn-toolbar">
                            <a href="#" class="btn btn-default"><i class="fa fa-fw fa-plus"></i>Thêm mới</a>
                        </div>
                    </div> --}}
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
                                @include('errors.message')
                                <div class="panel-body">


                                       <form role="form" class="form-horizontal" method="POST"
                                    action="{{ route('admin.size.postUpdate', $size->id) }}"
                                    enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">

                                                        <div class="form-group">
                                                <label class="col-md-3 control-label">Tên : </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="name" value="{{ $size->name }}">
                                                </div>
                                            </div>
                                             <!--       <div class="form-group">
                                                <label class="col-md-3 control-label">Phòng ban: </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" name="dislay">
                                                        <option <?php if($size->dislay ==1) echo 'selected="selected"';?> value="1">
                                            Hỗ trợ bán hàng
                                                        </option>
                                                    <option <?php if($size->dislay ==2) echo 'selected="selected"';?> value="2">
                                                 Hỗ trợ kỹ thuật
                                                        </option>

                                                        <option <?php if($size->dislay ==3) echo 'selected="selected"';?> value="3">
                                              Phòng kế toán
                                                        </option>
                                         
                                                    </select>
                                                </div>
                                            </div> -->
       <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh bìa: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-8">
                                                    <img src="{{ asset($size->image) }}"
                                                    style="max-height: 150px;">
                                                </div>
                                            </div>

                                                        <div class="form-group">
                                                <label class="col-md-3 control-label">Vị trí : </label>
                                                <div class="col-md-8">
                                                    <input type="number" class="form-control"
                                                    placeholder="Nhập vị trí" name="position" value="{{ $size->position }}">
                                                </div>
                                            </div>

                                               <div class="form-group">
                                                <label class="col-md-3 control-label">Nội dung: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" name="note" id="note">{{ $size->note }}</textarea>
                                                </div>
                                            </div>

        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <button class="btn-success btn">Lưu</button>
                                                    <a class="btn-default btn"
                                                    href="{{ route('admin.size.create') }}">
                                                        Hủy
                                                    </a>
                                                    <a class="btn-default btn" href='javascript:goback()'>Quay lại</a>
                                                </div>
                                            </div>
                                        </div>

                                        </div>
                                    </form>


                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection
