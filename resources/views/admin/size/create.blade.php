@extends('admin.partials.master')
@section('title', 'Thêm ')
@section('content')
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Thêm </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i>Trang chủ</li>
        <li class="active">Thêm </li>
    </ol>
</section>
<section class="content-header">
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
                                @include('errors.message')
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal" method="POST"
                                    action="{{ route('admin.size.createPost') }}"
                                    enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên : </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Tên" name="name" required="true">
                                                </div>
                                            </div>
                                                <div class="form-group">
                                                <label class="col-md-3 control-label">Vị trí : </label>
                                                <div class="col-md-8">
                                                    <input type="number" class="form-control"
                                                    placeholder="Vị trí" name="position" required="true">
                                                </div>
                                            </div>
                                       <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image">
                                                </div>
                                            </div>

                                                        <!--  <div class="form-group">
                                                <label class="col-md-3 control-label">Thuộc phòng: </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" name="dislay">
                                                        <option value="1"> Hỗ trợ bán hàng</option>
                                                      <option value="2">Hỗ trợ kỹ thuật</option>
                                                          <option value="3">Phòng kế toán</option>
                                  
                                                    </select>
                                                </div>
                                            </div> -->

                                        <div class="form-group">
                                                <label class="col-md-3 control-label">Nội dung </label>
                                                <div class="col-md-8">
                                                    <textarea name="note" class="form-control"
                                                    id="note" value="">{{ old('note') }}</textarea>
                                                </div>
                                            </div>




                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <button class="btn-success btn">Lưu</button>
                                                    <a class="btn-default btn"
                                                    href="{{ route('admin.size.create') }}">
                                                        Hủy
                                                    </a>
                                                    <a class="btn-default btn" href='javascript:goback()'>Quay lại</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection
