@extends('admin.partials.master')
@section('title', 'Quản lý video')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Quản lý video</h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i>Trang chủ</li>
            <li>Quản lý video</li>
            <li class="active">video</li>
        </ol>


        <a href="{{ route('admin.size.create') }}" class="btn btn-primary">

            <i class=" fa fa-fw fa-plus"></i>

            Thêm mới

        </a>
    </section>
    <section class="content-header">
        <div class="static-content-wrapper">
            <div class="static-content">
                <div class="page-content">
                    @include('errors.message')
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="options">
                                        </div>
                                    </div>
                                    <div class="panel-body" style="overflow-x:auto;">
                                                <table id="datatable_order" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>STT</th>
                                                            <th>Name</th>
                                                            <th>Vị trí</th>
                                                            <!-- <th>Email</th> -->
                                                            <!-- <th>Số sao</th> -->
                                                        <th>Ảnh</th> 
                                                            <th>Trạng thái</th>
                                                            <th>Cập nhật</th>
                                                            <th>Xử lý</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($size as $key => $c)
                                                        <tr>
                                                            <td>{{ $key + 1 }}</td>
                                                            <td><a href="{{ route('admin.size.update', $c->id)}}">{{ $c->name }}</a></td>



  <td>
                                                      {{ $c->position }}
                                               
                                                    </td>
                                                            <!-- <td>{{ $c->email }}</td> -->
                                                            <!-- <td>{{ $c->vote }}</td> -->
                                                            <td>
                                                                <img src="{{asset($c->image)}}" alt="" style="max-width: 150px;">
                                                            </td>
                                                            <td>
                                                                @if($c->status == 1)
                                                                    <a href="#" title="Ẩn" id="status" data-id="{{ $c->id }}">
                                                                        <span id="status{{ $c->id }}"><span class="label label-success">Hiện</span></span>
                                                                    </a>
                                                                @else
                                                                    <a href="#" title="Hiện" id="status" data-id="{{ $c->id }}">
                                                                        <span id="status{{ $c->id }}"><span class="label label-danger">Ẩn</span></span>
                                                                    </a>
                                                                @endif
                                                            </td>
                                                            <td>{{ $c->updated_at }}</td>
                                                            <td>
                                                                <a href="{{ route('admin.size.update', $c->id)}}">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a> 
                                                                <a href="{{ route('admin.size.destroy', $c->id) }}"
                                                                    type="button"
                                                                    onclick="return confirm_delete('Bạn có muốn xóa không ?')">
                                                                    <i class="fa fa-times-circle"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                    {{-- <tfoot>
                                                        <tr>
                                                            <th>STT</th>
                                                            <th>Name</th>
                                                            <th>Phone</th>
                                                            <th>Email</th>
                                                            <th>Số sao</th>
                                                            <th>Sản phẩm</th>
                                                            <th>Nội dung</th>
                                                            <th>Cập nhật</th>
                                                            <th>Xử lý</th>
                                                        </tr>
                                                    </tfoot> --}}
                                                </table>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div><!-- /.content-wrapper -->
@endsection
@section('script')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click', '#status', function(){
        var id = $(this).data('id');
        $.post('{{ route('admin.size.status') }}', {id:id}, function(data){
            if (data.status == 1)
            {
                $('#status'+id).html('<span class="label label-success">Hiện</span>');
            } else {
                $('#status'+id).html('<span class="label label-danger">Ẩn</span>');
            }
        });
    });
    $(document).ready(function() {
        $('#datatable_order').DataTable( {
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        } );
    } );
</script>
@endsection