@extends('admin.partials.master')
@section('title', 'Sửa')
@section('content')
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Sửa</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">
                    <li>Trang chủ</li>
                    <li>Giới thiệu</li>
                    <li class="active">Sửa</li>
                </ol>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
                                @include('errors.message')
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal" method="POST"
                                    action="{{ route('admin.intro.postUpdate', $intro->slug) }}"
                                    enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="name_vi" value="{{ $intro->name_vi }}">
                                                </div>
                                            </div>
                                                 <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh bìa: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-8">
                                                    <img src="{{ asset($intro->image) }}"
                                                    style="max-height: 150px;">
                                                </div>
                                            </div>

                                            {{--   <div class="form-group">
                                                <label class="col-md-3 control-label">Mã Video trang chủ: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="video" value="{{ $intro->video }}">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Tên Video trang chủ: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="ten_video" value="{{ $intro->ten_video }}">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Địa điểm Video trang chủ: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="diadiem" value="{{ $intro->diadiem }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tiêu đề 1 trang chủ: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="title_vi" value="{{ $intro->title_vi }}">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                          <label class="col-md-3 control-label">Tiêu đề 2 trang chủ: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="title2_vi" value="{{ $intro->title2_vi }}">
                                                </div>
                                            </div> --}}


                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Nội dung: </label>
                                                <div class="col-md-8">
                                                     <textarea class="form-control" placeholder="Miêu tả"
                                                    name="description_vi" id="editor1">
                                                        {{ $intro->description_vi }}
                                                    </textarea>
                                                </div>
                                            </div>

      <div class="form-group">
                                          <label class="col-md-3 control-label">Nội dung hợp tác: </label>
                                                <div class="col-md-8">
                                                   
                                                      <textarea class="form-control" placeholder="Miêu tả"
                                                    name="content_giatri1" rows="2" id="content_giatri1">
                                                        {{ $intro->content_giatri1 }}
                                                    </textarea>
                                                </div>
                                            </div>

{{-- 

                                             <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh Chặng đường hình thành và phát triển: </label>
                                                <div class="col-md-8">
                                                    @foreach($img_detail as $i)
                                                        <div class="image">
                                                            <img src="{{ asset($i->name) }}" style="height: 50px; width: 50px">
                                                            <label><span class="btn btn-danger delImage" id="data-1" data-id="{{ $i->id }}"> X </span></label>
                                                        </div>
                                                    @endforeach
                                                    <a class="btn btn-primary btn-sm addimg" style="margin-top: 2px; margin-left: 0">+ Thêm ảnh</a>
                                                </div>
                                            </div>
                                            <div class="form-group" id="contentimg">
                                            </div> --}}


{{-- 
<hr style="border-top:2px solid #000;">

                                    <div class="form-group">
                                          <label class="col-md-3 control-label">Tiêu đề 3: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="title3_vi" value="{{ $intro->title3_vi }}">
                                                </div>
                                            </div>

                                     <div class="form-group">
                                          <label class="col-md-3 control-label">Tiêu đề 4: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="title4_vi" value="{{ $intro->title4_vi }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                          <label class="col-md-3 control-label">Tiêu đề 5: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="title5_vi" value="{{ $intro->title5_vi }}">
                                                </div>
                                            </div>
                                                 <div class="form-group">
                                          <label class="col-md-3 control-label">Tiêu đề 6: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="title6_vi" value="{{ $intro->title6_vi }}">
                                                </div>
                                            </div>
                                              <div class="form-group">
                                          <label class="col-md-3 control-label">Tiêu đề 7: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="title7_vi" value="{{ $intro->title7_vi }}">
                                                </div>
                                            </div>
                                              <div class="form-group">
                                          <label class="col-md-3 control-label">Tiêu đề 8: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="title8_vi" value="{{ $intro->title8_vi }}">
                                                </div>
                                            </div>

        <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh giới thiệu 2: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image2">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-8">
                                                    <img src="{{ asset($intro->image2) }}"
                                                    style="max-height: 150px;">
                                                </div>
                                            </div>

<hr style="border-top:2px solid #000;">
                                            <div class="form-group">
                                          <label class="col-md-3 control-label">Tiêu đề giá trị cốt lõi 1: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="title9_vi" value="{{ $intro->title9_vi }}">
                                                </div>
                                            </div>
                                              <div class="form-group">
                                          <label class="col-md-3 control-label">Tiêu đề giá trị cốt lõi 2: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="title10_vi" value="{{ $intro->title10_vi }}">
                                                </div>
                                            </div>


                                                     <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh giá trị cốt lõi 1: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image_giatri1">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-8">
                                                    <img src="{{ asset($intro->image_giatri1) }}"
                                                    style="max-height: 150px;">
                                                </div>
                                            </div>
                                                   <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh giá trị cốt lõi 2: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image_giatri2">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-8">
                                                    <img src="{{ asset($intro->image_giatri2) }}"
                                                    style="max-height: 150px;">
                                                </div>
                                            </div>

       <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh giá trị cốt lõi 3: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image_giatri3">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-8">
                                                    <img src="{{ asset($intro->image_giatri3) }}"
                                                    style="max-height: 150px;">
                                                </div>
                                            </div>

       <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh giá trị cốt lõi 4: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image_giatri4">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-8">
                                                    <img src="{{ asset($intro->image_giatri4) }}"
                                                    style="max-height: 150px;">
                                                </div>
                                            </div>




                                    <div class="form-group">
                                          <label class="col-md-3 control-label">Tiêu đề giá trị cốt lõi 3: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="tieude_giatri1" value="{{ $intro->tieude_giatri1 }}">
                                                </div>
                                            </div>


                                         




    <div class="form-group">
                                          <label class="col-md-3 control-label">Tiêu đề giá trị cốt lõi 4: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="tieude_giatri2" value="{{ $intro->tieude_giatri2 }}">
                                                </div>
                                            </div>


                                               <div class="form-group">
                                          <label class="col-md-3 control-label">Nội dung giá trị cốt lõi 4: </label>
                                                <div class="col-md-8">
                                                   
                                                      <textarea class="form-control" placeholder="Miêu tả"
                                                    name="content_giatri2" rows="2">
                                                        {{ $intro->content_giatri2 }}
                                                    </textarea>
                                                </div>
                                            </div>





    <div class="form-group">
                                          <label class="col-md-3 control-label">Tiêu đề giá trị cốt lõi 5: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="tieude_giatri3" value="{{ $intro->tieude_giatri3 }}">
                                                </div>
                                            </div>
                                                     <div class="form-group">
                                          <label class="col-md-3 control-label">Nội dung giá trị cốt lõi 5: </label>
                                                <div class="col-md-8">
                                                   
                                                      <textarea class="form-control" placeholder="Miêu tả"
                                                    name="content_giatri3" rows="2">
                                                        {{ $intro->content_giatri3 }}
                                                    </textarea>
                                                </div>
                                            </div>


    <div class="form-group">
                                          <label class="col-md-3 control-label">Tiêu đề giá trị cốt lõi 6: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="tieude_giatri4" value="{{ $intro->tieude_giatri4 }}">
                                                </div>
                                            </div>



         <div class="form-group">
                                          <label class="col-md-3 control-label">Nội dung giá trị cốt lõi 6: </label>
                                                <div class="col-md-8">
                                                   
                                                      <textarea class="form-control" placeholder="Miêu tả"
                                                    name="content_giatri4" rows="2">
                                                        {{ $intro->content_giatri4 }}
                                                    </textarea>
                                                </div>
                                            </div>


 --}}





                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Vị trí: </label>
                                                <div class="col-md-8">
                                                    <input type="number" class="form-control" placeholder="Nhập vị trí"
                                                    name="position" value="{{ $intro->position }}">
                                                </div>
                                            </div>

                                           

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Trạng thái: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="status"
                                                    {{ ($intro->status) ? 'checked': '' }}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Title_SEO: </label>
                                                <div class="col-md-8">
                                                        <input type="text" class="form-control" placeholder="Title SEO"
                                                        name="title_seo_vi" value="{{ $intro->title_seo_vi }}">
                                                    @if($errors->has('title_seo_vi'))
                                                        <strong>
                                                            <span class="help-block">
                                                                {{ $errors->first('title_seo_vi') }}
                                                            </span>
                                                        </strong>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_key: </label>
                                                <div class="col-md-8">
                                                    <textarea type="text" class="form-control" placeholder="Meta_key"
                                                    name="meta_key_vi">{{ $intro->meta_key_vi }}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta_des: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" placeholder="Meta_Des"
                                                    name="meta_des_vi">{{ $intro->meta_des_vi }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <button class="btn-success btn">Lưu</button>
                                                    <a class="btn-default btn"
                                                    href="{{ route('admin.intro.create') }}">
                                                        Hủy
                                                    </a>
                                                    <a class="btn-default btn" href='javascript:goback()'>Quay lại</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection
@section('script')
<script>
    CKEDITOR.replace( 'editor1', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',
    } );
        CKEDITOR.replace( 'content_giatri1', {
        filebrowserBrowseUrl: '{{ route('ckfinder-customer') }}',
    } );
    //     $(document).ready(function() {
    //     $('.addimg').on('click', function(){
    //         $('#contentimg').append('<label class="col-md-3 control-label"></label><div class="col-md-8"><input type="file" class="form-control" name="img[]"></div>');
    //     });
    //     $('.delImage').on('click',function(){
    //         id = $(this).data('id');
    //         if(confirm('Bạn có muốn xóa ?')){
    //             flag = $(this).parent('label').parent('div.image').hide();
    //             $.get('{{ route('index') }}/admin/intro/delImage/', {id:id},function(data){
    //             });
    //         }
    //     });

    // });
</script>
@endsection