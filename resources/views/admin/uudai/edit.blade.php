@extends('admin.partials.master')
@section('title', 'Sửa')
@section('content')
<div class="content-wrapper">
<section class="content-header">
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Sửa</h1>
                    <div class="options">
                        <div class="btn-toolbar">
                        </div>
                    </div>
                </div>
                <ol class="breadcrumb">
                    <li>Trang chủ</li>
                    <li class="active">Sửa</li>
                </ol>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
                                @include('errors.message')
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal" method="POST"
                                    action="{{ route('admin.uudai.postUpdate', $uudai->id) }}"
                                    enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tên: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="name_vi" value="{{ $uudai->name_vi }}">
                                                </div>
                                            </div>
                                          {{--   <div class="form-group">
                                                <label class="col-md-3 control-label">Tên_en: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Nhập tên" name="name_en" value="{{ $uudai->name_en }}">
                                                </div>
                                            </div> --}}

                                           <!--  <div class="form-group">
                                                <label class="col-md-3 control-label">Số hotline: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control"
                                                    name="description_vi" id="editor1">{{ $uudai->description_vi }}</textarea>
                                                </div>
                                            </div> -->
                                          {{--   <div class="form-group">
                                                <label class="col-md-3 control-label">Miêu tả_en: </label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control"
                                                    name="description_en" id="editor2">{{ $uudai->description_en }}</textarea>
                                                </div>
                                            </div> --}}

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Link: </label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control"
                                                    placeholder="Link" name="link" value="{{ $uudai->link }}">
                                                </div>
                                            </div> 
                                        {{--     <div class="form-group">
                                                <label class="col-md-3 control-label">Ngày hết hạn: </label>
                                                <div class="col-md-8">
                                                    <input type="date" class="form-control"
                                                    name="time" value="{{ $uudai->time }}">
                                                </div>
                                            </div> --}}

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Vị trí: </label>
                                                <div class="col-md-8">
                                                    <input type="number" class="form-control"
                                                    placeholder="Nhập vị trí" name="position"
                                                    value="{{ $uudai->position }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ảnh bìa: </label>
                                                <div class="col-md-8">
                                                    <input type="file" class="form-control" name="image">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-8">
                                                    <img src="{{ asset($uudai->image) }}"
                                                    style="height: 50px; width: 50px">
                                                </div>
                                            </div>
               <div class="form-group">
                                                <label class="col-md-3 control-label">Xuất hiện: </label>
                                                <div class="col-md-8">
                                                    <select class="form-control" name="display">
                                                        <option <?php if($uudai->dislay ==1) echo 'selected="selected"';?> value="1">
                                         Quảng cáo 4 cột
                                                        </option>
                                              <option <?php if($uudai->dislay ==2) echo 'selected="selected"';?> value="2">
                                                          Quảng cáo 1 cột
                                                        </option>

                                                                  <option value="3" <?php if($uudai->dislay ==3) echo 'selected="selected"';?>>Video phải cạnh slide</option>
                                                                <option value="4" <?php if($uudai->dislay ==4) echo 'selected="selected"';?>>Multi ảnh bên phải slide</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Trạng thái: </label>
                                                <label class="switch">
                                                    <input type="checkbox" name="status"
                                                    {{ ($uudai->status) ? 'checked': '' }}>
                                                    <span class="slider round"></span>
                                                    <input type="hidden" name="id" value="1">
                                                </label>
                                            </div>

                                           
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2">
                                                    <button class="btn-success btn">Lưu</button>
                                                    <a class="btn-default btn"
                                                    href="{{ route('admin.uudai.create') }}">
                                                        Hủy
                                                    </a>
                                                    <a class="btn-default btn" href='javascript:goback()'>Quay lại</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection
