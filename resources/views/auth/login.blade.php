@extends('frontend.partials.master')
@section('title', 'Đăng nhập tài khoản')
@section('canonical')
{{ URL::current() }}
@stop
@section('css')
<style>

/* Full-width input fields */
  .layout_dangky input[type=text], .layout_dangky input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  width:100%;
  resize: vertical;
  padding:15px;
  border-radius:15px;
  border:0;
  box-shadow:4px 4px 10px rgba(0,0,0,0.2);
}
.layout_dangky input[type=text]:focus, .layout_dangky input[type=password]:focus {
  outline: none;
}
.layout_dangky hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}
/* Set a style for all buttons */
.layout_dangky button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}
.layout_dangky button:hover {
  opacity:1;
}
/* Extra styles for the cancel button */
.cancelbtn {
  padding: 14px 20px;
  background-color: #f44336;
}
/* Float cancel and signup buttons and add an equal width */
.signupbtn {
  float: left;
  width: 100%;
  border-radius:15px;
  border:0;
  box-shadow:4px 4px 10px rgba(0,0,0,0.2);
}

/* Clear floats */
.clearfix::after {
  content: "";
  clear: both;
  display: table;
}
.invalid-feedback strong{color: red;}
</style>
@stop
@section('body')
@stop
@section('content')
<div class="layout_dangky">
  <div class="container">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
<form action="{{ route('dangnhap') }}" method="post">
    @csrf
  <div class="">
    <h1>Đăng nhập tài khoản</h1>
    <p>Xin hãy nhập biểu mẫu bên dưới để đăng nhập.</p>
    <hr>

                                
                                 
    <label for="name"><b>Tên truy cập</b></label>
    <input type="text" placeholder="Tên truy cập" name="name_truycap" required class="{{ $errors->has('name_truycap') ? ' is-invalid' : '' }}"  value="{{ old('name_truycap') }}"  >
      @if ($errors->has('name_truycap'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name_truycap') }}</strong>
                                    </span>
                                @endif


    <label for="psw"><b>Mật Khẩu</b></label>
    <input type="password" placeholder="Nhập Mật Khẩu" name="password"  class="{{ $errors->has('password') ? ' is-invalid' : '' }}" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
  
  <div class="login-toggle-btn">
                                                    
                              <a href="{{ route('index') }}/password/reset" style="color: #3b8f4c;">Quên mật khẩu?</a>
                          </div>
    <div class="clearfix">
      <button type="submit" class="signupbtn">Đăng nhập</button>
    </div>
  </div>
</form>
    </div>
  </div>
</div>
@endsection
@section('script')
@endsection