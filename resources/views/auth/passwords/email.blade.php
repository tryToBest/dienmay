@extends('frontend.partials.master')
@section('css')
<style>
.layout_reset{padding:50px 0;}
   .layout_reset .btn-primary{
    background-color: #3b8f4c;    font-size: 18px;
   }
   .invalid-feedback{
    color: red;
   }
   @media(max-width: 767px){
    .layout_reset{padding:20px 0;}
   }
</style>
@stop
@section('content')
<div class="layout_reset">
    <div class="container ">
    <div class="row justify-content-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="card">
                   <h1 style="margin-bottom: 30px;">Reset mật khẩu tài khoản</h1>
                {{-- <div class="card-header">{{ __('Reset Password') }}</div> --}}
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="nutlcm">
                            <button type="submit" class="btn btn-primary">
                                Reset password mới
                                </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
