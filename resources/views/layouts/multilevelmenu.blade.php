<ul class="dropdown-menu">
	@foreach($parentCate as $key => $cate)
	<li>
		<a href="{{route('admin.product.ganCatebolocid',$cate->id)}}">{{$cate->name_vi}}</a>
		@if($cate->childCate()->count() > 0)
		<ul class="childmenu">
			@foreach($cate->childCate()->get() as $childCate)
			<li>
				<a href="{{route('admin.product.ganCatebolocid',$childCate->id)}}">{{$childCate->name_vi}}</a>
			</li>
			@endforeach
		</ul>
		@endif
	</li>
	@endforeach
</ul>
