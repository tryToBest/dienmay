@extends('frontend.partials.master')
@section('title', $setting->name_vi)
@section('title_seo', $setting->title_seo)
@section('meta_key', $setting->meta_key)
@section('meta_des', $setting->meta_des)
@section('body')

@stop
@section('css')
    <link rel="stylesheet" href="{{asset('css/flipclock.min.css')}}?{{time()}}">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css'>
    <link rel="stylesheet" href="{{asset('css/countdownclock.css')}}?{{time()}}">
    <meta property="og:image" content="{{asset($setting->meta_og_image)}}" />
<style type="text/css">
    body{
        background: #fff;
    }
    .bx-slider{margin-bottom: 20px;}
    .box-quangcao-home{padding:20px 0;}
      .box-quangcao-home  img{max-width: 100%;}
    .box-quangcao-home .row{
        margin: 0 -5px;
    }
    .box-quangcao-home .col-xs-12{padding:0 5px;}
    .box-quangcao-home .col-sm-3{
        width: 25%;padding:0 5px;margin: 5px 0;
    }
.bx-product .title-box{text-align: center;text-transform: uppercase;margin-bottom: 30px;}
.bx-product .title-box h2{color: #333333;}
.bx-banner-home{
    padding:30px 0;background-color: #f1f1f1;
}
.bx-banner-home .title-box{
    text-align: center;margin-bottom: 30px;text-transform: uppercase;
}

#list-slider-hinhanh img{width: 100%;}

.bx-banner-home .lSSlideOuter {
    overflow: hidden;
}
.bx-banner-home .lSSlideOuter .lSAction > a{
    background: url('{{asset('images/arr.png')}}')center no-repeat;
        background-size: contain;
}
.bx-banner-home .lSSlideOuter .lSAction > a.lSNext{
    background: url('{{asset('images/arr.png')}}')center no-repeat;
        background-size: contain;transform: rotate(180deg);
}
.bx-banner-home li.item {
    padding: 0px 15px!important;
}
/*.bx-banner-home li.item:nth-child(3) {
    padding-left: 0px;
}*/
.news-video img{max-width: 100%}
.video_home{position:relative;display: inline-flex;
    align-items: center;}
.news-video .play_vd{max-width: 50px;position:absolute;left: 0;right:0;margin:0 auto;}
.news-video .play_vd:hover{cursor: pointer;transform: scale(1.1);}
.multi_image_home{position:relative;    max-height: 215px;
    overflow: hidden;}
    .multi_image_home img{
        max-height: 215px;
    width: 100%;
    }
.xemthem_hinhanh:hover{cursor: pointer;color: #f9e600}
.xemthem_hinhanh{    color: #fff;
    position: absolute;
    width: 100%;
    background-color: #0000008a;
    bottom: 0;
    padding: 20px;}
    .xemthem_hinhanh i{
        color: #f9e600;
    }

.bx-news:nth-child(2n+1){
    /*background-color: #f1f1f1;*/
    padding:20px 0;
}


.col-12{
        width: 100%;    position: relative;
    clear: both;
    display: flow-root;
}

.slide_sanpham{width: 100%!important;overflow: hidden;}

.list-product .lSSlideOuter .lSAction > a{
    background: url('{{asset('frontend/images/arr2.png')}}')center no-repeat;
        background-size: contain;opacity: 1;
}
.list-product .lSSlideOuter .lSAction > a.lSNext{
    background: url('{{asset('frontend/images/arr2.png')}}')center no-repeat;
        background-size: contain;transform: rotate(180deg);opacity: 1;
}
    @media(max-width: 767px){
           .box-quangcao-home .col-sm-4{
        width:100%;padding:0 15px;margin: 10px 0;
    }
    .box-quangcao-home .col-xs-6{
        width: 50%;    float: left;
    }
    .box-quangcao-home .col-xs-6:nth-child(2n+1){clear: both;}
    .box-quangcao-home .row{
        margin:0;
      }
      .bx-product .tab-categories ul {
    white-space: nowrap;
    height: 64px;
    display: inline-flex;
    /* overflow: auto; */
    flex-direction: inherit;
    width: 100%;
    justify-content: normal;
    overflow-x: scroll;
}
    }
</style>
@stop
@section('content')
 
<section class="bx-slider">
    <div class="container">
        <div class="row">
            <div class="main-slider">
                <div class="slider">
                    <ul id="list-slider-home">
                        @foreach($slides as $key => $slide)
                        <li class=" {{$key == 0 ? 'active' : ''}}" >
                            <a href="{{$slide->link}}" title="{{$slide->name_vi}}">
                                <img src="{{asset($slide->image)}}" alt="{{$slide->name_vi}}"/>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
          {{--       <div class="list-text-slider">
                    <ul>
                        @foreach($slides as $key => $slide)
                        <li class="{{$key == 0 ? 'active' : ''}}" onClick="goToSlider(99901, {{$key+1}}, this)">{{$slide->name_vi}}</li>
                        
                        @endforeach
                    </ul>
                </div> --}}
            </div>
            <div class="news-video">
@php
$list_kinhnghiem = DB::table('cate_posts')->select('name_vi','id')->where('id',76)->orderBy('created_at','DESC')->limit(6)->get();
@endphp

{{-- @foreach($list_kinhnghiem as $kn)
    <h3>{{$kn->name_vi}} <i class="fas fa-caret-right"></i></h3>
        <ul>
        @php
                            $id        = $kn->id;
                            session()->put('id',$id);
                            $get_duan = DB::table('posts')->where('status', 1)->where('is_home', 1)->where(function($query)
                            {
                                $pro       = $query;
                                $id        = session('id');
                                $cate_post = DB::table('cate_posts')->where('id', $id)->first();
                                $pp = '"'.$cate_post->id.'"';
                                $pro       = $pro->orWhere('cate_post_id',$cate_post->id); // bài viết có id của danh muc cha cấp 1.
                                $com       = DB::table('cate_posts')->where('parent_id',$cate_post->id)->get();//danh mục cha cấp 2.
                                foreach ($com as $dt) {
                                    $pro = $pro->orWhere('cate_post_id',$dt->id);// bài viết có id của danh muc cha cấp 2.
                                      $com3       = DB::table('cate_posts')->where('parent_id',$dt->id)->get();
                foreach ($com3 as $key => $cc) {
                    $query = $query->orWhere('cate_post_id',$cc->id);
                }
                                }
                                session()->forget('id');//xóa session;
                            })->orderBy('id','DESC')->limit(6)->get();
                        @endphp
                        @foreach($get_duan as $post)

       
                    <li>
                        <a href="{{route('detail',$post->slug_vi)}}" title="{{$post->name_vi}}">{{$post->name_vi}}</a>
                    </li>
                    @endforeach
                </ul>

@endforeach --}}


@php
$video_home = DB::table('uudais')->where('status',1)->where('display',3)->orderBy('position','ASC')->take(1)->get();
$multi_image_home = DB::table('uudais')->where('status',1)->where('display',4)->orderBy('position','ASC')->get();

@endphp

@foreach($video_home as $vdh)

<div class="video_home" data-fancybox="" href="{{$vdh->link}}">

<img src="{{asset($vdh->image)}}" alt="{{$vdh->name_vi}}" >
<img src="{{asset('frontend/images/play_vd.png')}}" alt="play video" class="play_vd" >
 </div>

@endforeach 


<div class="multi_image_home" >
    <div id="list-slider-qc">
 @foreach($multi_image_home as $key_img => $im_m)
<div>
    <a href="{{$im_m->link}}">
        <img src="{{asset($im_m->image)}}" alt="{{$im_m->name_vi}}" >
    </a>
</div>
@endforeach

</div>
 </div>



{{-- @foreach($multi_image_home as $key_img => $im_m)

<div class="multi_image_home" data-fancybox="thu-vien-anh" href="{{$im_m->image}}" @if($key_img != 0) style="display: none;" @endif>

<img src="{{asset($im_m->image)}}" alt="{{$im_m->name_vi}}" >


@if($key_img == 0)
<div class="xemthem_hinhanh">
    <p>Xem thêm hình ảnh <i class="fa fa-caret-right" aria-hidden="true"></i></p>
</div>
@endif
 </div>

@endforeach  --}}

            </div>
        </div>
    </div>
</section>




<section class="box-quangcao-home">
    <div class="container">
        <div class="row">
            @php
$quangcao_home = DB::table('uudais')->where('display',1)->where('status',1)->orderBy('position','ASC')->get();
$quangcao_home2 = DB::table('uudais')->where('display',2)->where('status',1)->orderBy('position','ASC')->get();
@endphp

@foreach($quangcao_home as $key_qch1 =>  $qch1)
            <div class="col-xs-6 col-sm-3">
               <a href="{{$qch1->link}}"> <img src="{{asset($qch1->image)}}" alt="{{$qch1->name_vi}}"></a>
            </div>


@if($key_qch1 == 3)

@foreach($quangcao_home2 as $key_qc2 => $qch2)
@if($key_qc2==0)

   </div>
 <div class="row">

            <div class="col-xs-12">
               <a href="{{$qch2->link}}"> <img src="{{asset($qch2->image)}}" alt="{{$qch2->name_vi}}"></a>
            </div>
          </div>
     <div class="row">
@endif
  @endforeach
@endif


            @endforeach
        </div>

  
@foreach($quangcao_home2 as $key_qc2 => $qch2)
@if($key_qc2>0)
  <div class="row">
   


            <div class="col-xs-12">
               <a href="{{$qch2->link}}"> <img src="{{asset($qch2->image)}}" alt="{{$qch2->name_vi}}"></a>
            </div>
          
        </div>
@endif
  @endforeach





    </div>
</section>
@if($setting->status_flash_sale == 1)
<section id="flash-sale">
    <div class="container"> 
        
            <div class="flash-sale_head">
                <div class="sale_head-title">{{$setting->title_flash_sale}}</div>
                <div class="sale_head-countdown">
                    <div class="clock"></div>
                </div>
            </div>
            <div class="flash-sale_body">
                <div class="bx-product">
                <div class="list-product">
                    
                
                <div class="slide_sanpham">

                    <div id="slide_sanpham_flash_sale" >
                      @php
                      $products = DB::table('products')->where('is_sale',1)->orderBy('position','desc')->get();
                            @endphp
                            @foreach($products as $product)

                            <div class="item">
                                <div class="thumb">
                                    <a href="{{route('detail_product',$product->slug_vi)}}">
                                        <img src="{{asset($product->image)}}"/>
                                    </a>
                                </div>
                                <div style="clear: both;"></div>

                                <div class="info">
                                    {{-- @if($product->is_sale == 1)
                                    <p class="info_km">is Sale</p>
                                    @endif --}}
                                    <h3 class="name_product">
                                        <a href="{{route('detail_product',$product->slug_vi)}}">{{$product->name_vi}}</a>
                                    </h3>
                                    <div class="price">
                                        @if($product->price == null || $product->price == 0)
                                        Liên hệ
                                        @else
                                        {{ number_format($product->price,0)}}đ
                                        @endif
                                    </div>
                                    @php
                                    $tongdanhgia = DB::table('comments')->where('status',1)->where('product_id',$product->id)->count();
                                    $demso5sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',5)->count();
                                    $demso4sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',4)->count();
                                    $demso3sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',3)->count();
                                    $demso2sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',2)->count();
                                    $demso1sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',1)->count();
                                    if($tongdanhgia >0){
                                        $sosao = ($demso5sao*5 + $demso4sao*4 + $demso3sao*3 + $demso2sao*2 + $demso1sao*1)/$tongdanhgia;
                                    }
                                    else{
                                        $sosao = 0;
                                    }
                                    @endphp
                                    @if($product->price_unit > 0 && $product->price != null)
                                    <div class="sale">
                                        <span>{{number_format($product->price_unit,0)}}₫</span>
                                        <i>(-{{ number_format((($product->price_unit-$product->price)/$product->price_unit)*100,0)}}%)</i>
                                    </div>
                                    @endif
                                </div>
                                <div class="rate">
                                    <span class="point">{{number_format($sosao,0)}}/5 <i class="fas fa-star"></i></span>
                                    <span class="total-rate">{{$tongdanhgia}} đánh giá</span>
                                </div>
                            </div>
                            @endforeach
                    </div>     


                </div>
                </div>
                </div>
            </div>
        
    </div>
</section>
@endif








@php
$list_cat_product_home = DB::table('cate_products')->where('status', 1)->where('is_home', 1)->where('parent_id',0)->orderBy('position', 'ASC')->get();
@endphp

@foreach($list_cat_product_home as $cate_h1)
@php
$list_cat_product_home2 = DB::table('cate_products')->where('status', 1)->where('is_home', 1)->where('parent_id',$cate_h1->id)->orderBy('position', 'ASC')->get();
@endphp
<section class="bx-product">
    <div class="container">
     {{--     <div class="title-box">
            <h2>Sản phẩm</h2>
        </div> --}}
        <div class="tab-categories">
            @php
                $url = \Request::route()->getName();       
            @endphp
            <ul>    
                <li class="{{$url === 'index' ? 'active' : ''}}">
                    <a href="{{route('productByCate',$cate_h1->slug_vi)}}" title="{{$cate_h1->name_vi}}">
                            <img src="{{asset($cate_h1->image)}}" alt="{{$cate_h1->name_vi}}" >
                        {{$cate_h1->name_vi}}</a>
                </li>
                @foreach($list_cat_product_home2 as $cate_h2)
                <li ><a href="{{route('productByCate',[$cate_h1->slug_vi,$cate_h2->slug_vi])}}" title="{{$cate_h2->name_vi}}">{{$cate_h2->name_vi}}</a></li>

                @endforeach
                 <li class="xemtatca"><a href="{{route('productByCate',$cate_h1->slug_vi)}}">Xem tất cả</a></li>
            </ul>
        </div>
        <div class="list-product">
            <div class="slide_sanpham">

                <div id="slide_sanpham_{{$cate_h1->id}}" >
             	  @php
                            $id        = $cate_h1->id;
                            session()->put('id',$id);
                            $get_duan = DB::table('products')->where('status', 1)->where('is_home', 1)->where(function($query)
                            {
                                $pro       = $query;
                                $id        = session('id');
                                $cate_product = DB::table('cate_products')->where('id', $id)->first();
                                $pp = '"'.$cate_product->id.'"';
                                $pro       = $pro->orWhere('cate_product_id',$cate_product->id); // bài viết có id của danh muc cha cấp 1.
                                $com       = DB::table('cate_products')->where('parent_id',$cate_product->id)->get();//danh mục cha cấp 2.
                                foreach ($com as $dt) {
                                    $pro = $pro->orWhere('cate_product_id',$dt->id);// bài viết có id của danh muc cha cấp 2.
                                      $com3       = DB::table('cate_products')->where('parent_id',$dt->id)->get();
                foreach ($com3 as $key => $cc) {
                    $query = $query->orWhere('cate_product_id',$cc->id);
                }
                                }
                                session()->forget('id');//xóa session;
                            })->orderBy('id','DESC')->get();
                        @endphp
                        @foreach($get_duan as $sphot)
                
                <div class="item">
                    <div class="thumb">
                        <a href="{{route('detail_product',$sphot->slug_vi)}}">
                            <img src="{{asset($sphot->image)}}"/>
                        </a>

                    </div>
                    <div style="clear: both;"></div>
             
                    <div class="info">
                        {{-- @if($sphot->is_sale == 1)
                        <p class="info_km">is Sale</p>
                        @endif --}}
                        <h3 class="name_product">
                            <a href="{{route('detail_product',$sphot->slug_vi)}}">{{$sphot->name_vi}}</a>
                        </h3>
                        <div class="price">
                            @if($sphot->price == null || $sphot->price == 0)
                            Liên hệ
                            @else
                            {{ number_format($sphot->price,0)}}đ
                            @endif
                        </div>
                            @php
                                $tongdanhgia = DB::table('comments')->where('status',1)->where('product_id',$sphot->id)->count();
                                $demso5sao = DB::table('comments')->where('status',1)->where('product_id',$sphot->id)->where('rating',5)->count();
                                $demso4sao = DB::table('comments')->where('status',1)->where('product_id',$sphot->id)->where('rating',4)->count();
                                $demso3sao = DB::table('comments')->where('status',1)->where('product_id',$sphot->id)->where('rating',3)->count();
                                $demso2sao = DB::table('comments')->where('status',1)->where('product_id',$sphot->id)->where('rating',2)->count();
                                $demso1sao = DB::table('comments')->where('status',1)->where('product_id',$sphot->id)->where('rating',1)->count();
                                if($tongdanhgia >0){
                                    $sosao = ($demso5sao*5 + $demso4sao*4 + $demso3sao*3 + $demso2sao*2 + $demso1sao*1)/$tongdanhgia;
                                }
                                else{
                                    $sosao = 0;
                                }
                            @endphp
                        @if($sphot->price_unit > 0 && $sphot->price != null)
                            <div class="sale">
                                <span>{{number_format($sphot->price_unit,0)}}₫</span>
                                <i>(-{{ number_format((($sphot->price_unit-$sphot->price)/$sphot->price_unit)*100,0)}}%)</i>
                            </div>
                        @endif
                    </div>
                    <div class="rate">
                        <span class="point">{{number_format($sosao,0)}}/5 <i class="fas fa-star"></i></span>
                        <span class="total-rate">{{$tongdanhgia}} đánh giá</span>
                    </div>
                </div>
                @endforeach
             </div>     
    
    
			</div>        
		</div>
        <!--
        <div class="loadmore">
            <a href="">Xem thêm 104 sản phẩm giá gốc <i class="fas fa-caret-down"></i></a>
        </div>-->
    </div>
</section>
@endforeach



<section class="bx-banner-home">
    <div class="container">
          <div class="title-box">
            <h2>Hình ảnh</h2>
        </div>
        <div class="">
               <ul id="list-slider-hinhanh">
            @foreach($banners as $banner)
            <li class="item">
                <a href="{{$banner->link}}" title="{{$banner->name_vi}}">
                    <img src="{{asset($banner->image)}}" alt="{{$banner->name_vi}}"/>
                </a>
            </li>
            @endforeach
             </ul>
        </div>
    </div>
</section>

<section class="bx-news">
    <div class="container">


@foreach($catePostHome as $catePostHome2)
        <div class="col-6 col_news">
            <div class="header">
            <h2>{{$catePostHome2->name_vi}}</h2>
            <a href="{{route('list_post',$catePostHome2->slug_vi)}}" class="view-all" title="Xem tất cả">Xem tất cả 
                <i class="fas fa-chevron-right"></i>
            </a>
        </div>
        <div class="list-news">
            <div class="row">
                    @php
                            $id        = $catePostHome2->id;
                            session()->put('id',$id);
                            $get_duan_hotnew = DB::table('posts')->where('status', 1)->where('is_home', 1)->where(function($query)
                            {
                                $pro       = $query;
                                $id        = session('id');
                                $cate_post = DB::table('cate_posts')->where('id', $id)->first();
                                $pp = '"'.$cate_post->id.'"';
                                $pro       = $pro->orWhere('cate_post_id',$cate_post->id); // bài viết có id của danh muc cha cấp 1.
                                $com       = DB::table('cate_posts')->where('parent_id',$cate_post->id)->get();//danh mục cha cấp 2.
                                foreach ($com as $dt) {
                                    $pro = $pro->orWhere('cate_post_id',$dt->id);// bài viết có id của danh muc cha cấp 2.
                                      $com3       = DB::table('cate_posts')->where('parent_id',$dt->id)->get();
                foreach ($com3 as $key => $cc) {
                    $query = $query->orWhere('cate_post_id',$cc->id);
                }
                                }
                                session()->forget('id');//xóa session;
                            })->take(4)->orderBy('id','DESC')->get();
                        @endphp
    
                <div class="col-12 news-sm">
                    <div class="row">
               
         
                        @foreach($get_duan_hotnew as $key_news =>$hotNews)
             
                        <div class="col-12 row_news_hot">
                            <div class="item" style="background:url('{{asset($hotNews->image)}}')center no-repeat;background-size: cover;min-height: 150px;display: block;">
                             
                               
                            </div>
                             <div class="info">
                                    <h3 class="name_news"><a href="{{route('detail',$hotNews->slug_vi)}}" title="{{$hotNews->name_vi}}">{{$hotNews->name_vi}}</a></h3>
                                    <div class="motangan">
                                        <?php
                                  $descsk = substr(strip_tags($hotNews->title_vi ), 0, 170);
                                  
                                  if ((strlen($hotNews->title_vi )  ) > 170) {
                                   // trim the description back to the last period or space so words aren't cut off
                                   $period_pos = strrpos($descsk, ".");
                                   $space_pos = strrpos($descsk, " ");
                                   // find the character that we should trim back to. -1 on space pos for a space that follows a period, so we dont end up with 4 periods
                                   if ($space_pos - 1 > $period_pos) {
                                    $pos = $space_pos;
                                   }
                                   else {
                                    $pos = $period_pos;
                                   }
                                   $descsk = substr($descsk, 0, $pos);
                                   $descsk .= "...";
                                   echo $descsk;
                                  }else echo $hotNews->title_vi ;
                                ?>
                                    </div>

                  <div class="button-group">
<a class="btn btn-read-more" href="{{route('detail',$hotNews->slug_vi)}}">
<span class="btn-text">Chi tiết <i class="fas fa-long-arrow-alt-right"></i></span>
</a></div>
                                </div>

                        </div>
        



                 
                        @endforeach
             



                    </div>    
                                       
                </div>
            </div>
        </div>
        </div>
@endforeach





    </div>
</section>

{{-- <section class="bx-list-backlink">
    <div class="container">
        <div class="content-link">
            <span>Tìm kiếm nhiều: </span>
                            <a href="tu-khoa-san-pham/tivi-sony-55-inch" title="Tivi sony 55 inch">Tivi sony 55 inch</a>
                                <a href="danh-muc-san-pham/dieu-hoa-panasonic" title="Điều hòa panasonic">Điều hòa panasonic</a>
                                <a href="tu-khoa-san-pham/tivi-samsung-55-inch" title="Tivi samsung 55 inch">Tivi samsung 55 inch</a>
                                <a href="danh-muc-san-pham/tu-lanh-toshiba" title="Tủ lạnh toshiba">Tủ lạnh toshiba</a>
                                <a href="danh-muc-san-pham/tu-lanh-electrolux" title="Tủ lạnh electrolux">Tủ lạnh electrolux</a>
                                <a href="tu-khoa-san-pham/tivi-sony-65-inch" title="Tivi sony 65 inch">Tivi sony 65 inch</a>
                                <a href="tu-khoa-san-pham/tivi-samsung-65-inch" title="Tivi samsung 65 inch">Tivi samsung 65 inch</a>
                                <a href="" title="Tủ lạnh mini">Tủ lạnh mini</a>
                                <a href="" title="Tủ lạnh 4 cánh">Tủ lạnh 4 cánh</a>
                        </div>
    </div>
</section> --}}

@endsection
@section('script')

<script src='https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js'></script>
<script src="{{asset('js/flipclock.min.js')}}?{{time()}}"></script>
<script>
	var slider_sanpham = [];
$(document).ready(function () {
@foreach($list_cat_product_home as $cate_h1)
    slider_sanpham[{{$cate_h1->id}}] = $('#slide_sanpham_{{$cate_h1->id}}').lightSlider({
        item: 5,
        loop: true,
        slideMove: 1,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 1000,
        slideMargin: 0,
        mode: 'slide',
        auto: true,
        pause: 5000,
        controls: true,
        pager: false,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    item: 3,
                    slideMove: 1,
                    slideMargin: 6,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    item: 2,
                    slideMove: 1
                }
            }
        ]
    });
@endforeach
$('#slide_sanpham_flash_sale').lightSlider({
        item: 5,
        loop: true,
        slideMove: 1,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 1000,
        slideMargin: 0,
        mode: 'slide',
        auto: true,
        pause: 5000,
        controls: true,
        pager: false,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    item: 3,
                    slideMove: 1,
                    slideMargin: 6,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    item: 2,
                    slideMove: 1
                }
            }
        ]
    }); 

    
});
</script>

<script>
    $(document).ready(function() {
        var currentDate = new Date();
        var flashSaleAuto = "{{$setting->flash_sale_auto}}";
        var dateEndSale = new Date("{{$setting->date_end_sale}}");

        var futureDate;
        if (flashSaleAuto === "0" || dateEndSale > currentDate) {
            futureDate = dateEndSale;
        } else {
            var oneDayInMillis = 24 * 60 * 60 * 1000; // Một ngày trong mili giây
            futureDate = new Date(currentDate.getTime() + oneDayInMillis);
        }

        console.log(currentDate,futureDate);

        var diff = (futureDate.getTime() / 1000) - (currentDate.getTime() / 1000); // Số giây còn lại
        var clock = $('.clock').FlipClock(diff, {
            clockFace: 'HourlyCounter',
            countdown: true
        });
        var headFlashSale = $("section#flash-sale .flash-sale_head");
        var statusBackground = {{$setting->status_image_sale}};
        if(headFlashSale && statusBackground == 1){
            // Function to set the background image
            function setBackgroundImage(imageUrl) {
                headFlashSale.css({
                    'background-image': `url('${imageUrl}')`,
                    'background-size': 'cover',
                    'background-repeat': 'no-repeat'
                });
            }

            // Call the function with the image URL
            const imageUrl = '{{$setting->image_sale}}'; // Replace this with your image URL
            setBackgroundImage(imageUrl);
        }
    });

</script>
@endsection