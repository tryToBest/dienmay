@extends('frontend.partials.master')
@section('title', trans('home.gioithieu'))
@section('body')

@stop
@section('css')
<style type="text/css">
    
</style>
@stop
@section('content')
@include('frontend/partials/slide2')
{{-- <div class="bgintro" style="background: url('{{asset($intro->image)}}')center no-repeat;background-size: cover; padding: 150px;"></div> --}}
<nav aria-label="breadcrumb ">
         <ol class="breadcrumb ">
            <div class="container">
                <li class="breadcrumb-item"><a href="{{route('index')}}" class="mauxanh">Trang chủ</a></li>
                 <li class="breadcrumb-item"><a href="#" class="mauxanh">Giới Thiệu</a></li>
            </div>
  </ol>
</nav>
<div class="layout_blog_detail">
  	  <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-9">
        	 <div class="box_intro">
            <div class="title_chung text-center text-uppercase">{{$intro->name_vi}}</div>



<div class="content_detail_blog">
    {!!$intro->description_vi!!}
</div>

        </div>
    </div>
    <div class="col-xs-12 col-sm-3">
        @include('frontend/partials/right')
        </div>
    </div>
  </div>

</div>

@endsection
@section('script')
<script src='{{asset('frontend/js/slick.min.js')}}'></script>
<script>
	$('.slideshow').slick({
  arrows:true,
        infinite:true,
        slidesToShow:1,
        accessibility:true,
        autoplay:true,
        autoplaySpeed:4500,
  prevArrow:"<span class='arrow prev'><i class='fa fa-angle-left'></i></span>",
  nextArrow:"<span class='arrow next'><i class='fa fa-angle-right'></i></span>",
  customPaging : function(slider, i) {
    },
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
</script>
@endsection