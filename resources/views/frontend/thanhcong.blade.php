@extends('frontend.partials.master')
@section('title', trans('home.muahang'))
@section('body')

@stop
@section('css')

@stop
@section('content')
   
<style type="text/css">
    .gio_hang{display: block;float: right;padding: 15px 0px;color: #f00;}
</style><div class="bx-cart">
    <div class="container">
        <div class="content-cart">
            <p><strong><div class="woocommerce">
<div class="woocommerce-order">

    
        
            <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">Cảm ơn bạn. Đơn hàng của bạn đã được nhận.</p>

            <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

                <li class="woocommerce-order-overview__order order">
                    Mã đơn hàng:                    <strong>{{ $order->orderId }}</strong>
                </li>

                <li class="woocommerce-order-overview__date date">
                    Ngày:                   <strong>{{ $order->created_at }}</strong>
                </li>

                
                <li class="woocommerce-order-overview__total total">
                    Tổng cộng:                  <strong><span class="woocommerce-Price-amount amount">{{number_format($order->total)}}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></strong>
                </li>

                                    <li class="woocommerce-order-overview__payment-method method">
                        Phương thức thanh toán:                  
                           <strong>
                            @if($order->payment == 1)
                        Trả tiền mặt khi nhận hàng
                        @else
                       Thanh toán chuyển khoản
                        @endif
                    </strong>
                    </li>
                
            </ul>

        
        <p>     @if($order->payment == 1)
                        Trả tiền mặt khi nhận hàng
                        @else
                       Thanh toán chuyển khoản
                        @endif</p>
        <section class="woocommerce-order-details">
    
    <h2 class="woocommerce-order-details__title">Chi tiết đơn hàng</h2>

    <table class="woocommerce-table woocommerce-table--order-details shop_table order_details">

        <thead>
            <tr>
                <th class="woocommerce-table__product-name product-name">Sản phẩm</th>
                <th class="woocommerce-table__product-table product-total">Tổng</th>
            </tr>
        </thead>

        <tbody>
@php
$billdetail = DB::table('bill_details')->where('bill_id',$order->id)->get();
@endphp

@foreach($billdetail as $dt)
@php
$timsp = DB::table('products')->where('id',$dt->product_id)->first();
@endphp
@if(isset($timsp))
            <tr class="woocommerce-table__line-item order_item">

    <td class="woocommerce-table__product-name product-name">
        <a href="#">{{$timsp->name_vi}}</a> <strong class="product-quantity">&times; {{$dt->quantity}}</strong>  </td>

    <td class="woocommerce-table__product-total product-total">
        <span class="woocommerce-Price-amount amount">{{number_format($timsp->price*$dt->quantity)}}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span>    </td>

</tr>
@endif
@endforeach

        </tbody>

        <tfoot>
                            {{--     <tr>
                        <th scope="row">Tổng số phụ:</th>
                        <td><span class="woocommerce-Price-amount amount">{{$order->total}}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></td>
                    </tr> --}}
                                        <tr>
                        <th scope="row">Phương thức thanh toán:</th>
                        <td>     @if($order->payment == 1)
                        Trả tiền mặt khi nhận hàng
                        @else
                       Thanh toán chuyển khoản
                        @endif</td>
                    </tr>
                                        <tr>
                        <th scope="row">Tổng cộng:</th>
                        <td><span class="woocommerce-Price-amount amount">{{number_format($order->total)}}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></td>
                    </tr>
                                                <tr>
                    <th>Lưu ý:</th>
                    <td>{{$order->note}}</td>
                </tr>
                    </tfoot>
    </table>

    </section>


    
</div>
</div></strong></p>
        </div>
    </div>
</div>

@endsection
@section('script')
@endsection