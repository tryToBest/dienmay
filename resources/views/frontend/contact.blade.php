@extends('frontend.partials.master')
@section('title', trans('home.lienhe'))
@section('canonical')
{{ URL::current() }}
@stop
@section('css')
<style>
.map_contact{}
    .map_contact iframe{width: 100%;min-height: 350px;}
.content_contact{padding:30px 0;}
.title_contact{
    color: #389047  ;font-size: 16pt;margin-bottom: 15px;
}
.thongtinlienhe ul li{
    padding-left: 30px;position: relative;font-size: 2rem;    padding-bottom: 10px;
}
.thongtinlienhe ul li i{
    position: absolute;left: 0;    font-size: 14pt; color: #389047  ;
}
.thongtinlienhe ul li p b{

      color: #3c3d47;
}
.thongtinlienhe ul li a{
   color: #3c3d47;
}




</style>
@stop
@section('body')

@stop
@section('content')

<div class="layout_contact">
 {{-- <div class="map_contact">
    
         {!!$setting->cc2!!}
    
 </div> --}}
<section class="bx-breadcrum">
    <div class="container">
        <ul>
            <li><a href="{{route('index')}}" title="Trang chủ">Trang chủ</a></li>
            <li><span><i class="fas fa-angle-right"></i></span></li>
            <li><span>Liên hệ</span></li>
        </ul>
    </div>
</section>

 <div class="content_contact">
     <div class="container">
         <div class="title_chung text-center text-uppercase">
             Liên hệ chúng tôi
         </div>
         @foreach($contact as $c)
         {!! $c->description_vi !!}
         @endforeach
         {{-- <div class="row">
             <div class="col-xs-12 col-sm-6">
                <div class="title_contact">Thông tin liên hệ</div>

                <div class="thongtinlienhe">
                    <ul>
                        <li>
                            <a href="#" rel="nofollow">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <p><b>Địa chỉ</b></p>
                                {!!$setting->address_vi!!}
                            </a>
                        </li>
                        <li>
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <p><b>Điện thoại</b></p>
                            <p><a href="tel:{{$setting->hotline}}" rel="nofollow">+ {{$setting->hotline}}</a></p>
                              <p><a href="tel:{{$setting->phone}}" rel="nofollow">+ {{$setting->phone}}</a></p>
                                <p><a href="tel:{{$setting->phone2}}" rel="nofollow">+ {{$setting->phone2}}</a></p>
                        </li>
                        <li>
                    <a href="mailto:{{$setting->email}}" rel="nofollow"> <i class="fa fa-envelope" aria-hidden="true"></i>
                            <p><b>Email</b></p>
                            <p> {{$setting->email}}</p>
                        </a>
                        </li>
                       
                    </ul>
                </div>


                 
             </div>
              <div class="col2_contact col-xs-12 col-sm-6">
                <div class="title_contact">Gửi thông tin liên hệ</div>
                 
               <div class="row">
                     <form action="{{route('post_contact')}}" method="post" id="form_contact">
                     @csrf
                       <div class="col-xs-12">
                        <label for="">Họ và tên <span>*</span></label>
                     <input type="text" name="name" class="form-control" placeholder="" required="">
                 </div>
                  <div class="col-xs-12">
                        <label for="">Số điện thoại liên lạc <span>*</span></label>
                 
                      <input type="tel" name="phone" class="form-control" placeholder="" pattern="(\+84|0){1}(9|8|7|5|3){1}[0-9]{8}" autocomplete="off" required="">
                  </div>
                      <div class="col-xs-12">
                        <label for="">Email (nếu có) </label>
                       <input type="email" name="email" class="form-control" placeholder="" >
                   </div>
                      <div class="col-xs-12">
                        <label for="">Ghi chú <span>*</span></label>
                           <textarea name="content" id="" cols="30" rows="6" class="form-control" required=""></textarea>
                      </div>
                         <div class="col-xs-12">
                       <button type="submit" class="nuttuvan">Gửi đi</button>
                   </div>
                 </form>
               </div>
             </div>
         </div> --}}
     </div>
 </div>
</div>

@endsection
@section('script')

@endsection