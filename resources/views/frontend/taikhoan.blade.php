@extends('frontend.partials.master')

@section('title', trans('home.taikhoan'))

@section('content')

        <div class="breadcrumb-area pt-95 pb-95 bg-img" style="background-image:url('{{ asset('images/banner-2.jpg') }}');">

            <div class="container">

                <div class="breadcrumb-content text-center">

                    <h2>{{ trans('home.taikhoan') }}</h2>

                </div>

            </div>

        </div>

        <!-- my account start -->

        <div class="my-account-area pt-20 pb-70">

            <div class="container">
                <div class="breadcrumb-content duongdan">
                    <ul class="">
                        <li><i class="fa fa-home"></i><a href="{{ route('index') }}">{{ trans('home.home') }}</a></li>
                        <li class="active">{{ trans('home.taikhoan') }}</li>
                    </ul>
                </div>
                <div class="row">

                    <div class="col-lg-12">

                        <div class="checkout-wrapper">

                            <div id="faq" class="panel-group">

                                <div class="panel panel-default">

                                    <div class="panel-heading">

                                        <h5 class="panel-title"><span>1</span> <a data-toggle="collapse" data-parent="#faq" href="#my-account-1">{{ trans('home.thongtintaikhoan') }}</a></h5>

                                    </div>

                                    <form action="{{ route('thaydoitaikhoan') }}" method="post">

                                        @csrf

                                        <div id="my-account-1" class="panel-collapse collapse show">

                                            <div class="panel-body">

                                                <div class="billing-information-wrapper">

                                                    {{-- <div class="account-info-wrapper">

                                                        <h4>My Account Information</h4>

                                                        <h5>Your Personal Details</h5>

                                                    </div> --}}

                                                    <div class="row">

                                                        <div class="col-lg-12 col-md-12">

                                                            <div class="billing-info">

                                                                <label>{{ trans('home.hoten') }}</label>

                                                                <input type="text" value="{{ Auth::user()->name }}" name="name">

                                                            </div>

                                                        </div>

                                                        <div class="col-lg-12 col-md-12">

                                                            <div class="billing-info">

                                                                <label>Email</label>

                                                                <input type="email" value="{{ Auth::user()->email }}" disabled="">

                                                            </div>

                                                        </div>

                                                        <div class="col-lg-12 col-md-12">

                                                            <div class="billing-info">

                                                                <label>{{ trans('home.sodienthoai') }}</label>

                                                                <input type="text" name="phone" required="" minlength="10" maxlength="12" value="{{ Auth::user()->phone }}">

                                                            </div>

                                                        </div>

                                                        <div class="col-lg-12 col-md-12">

                                                            <div class="billing-info">

                                                                <label>{{ trans('home.diachi') }}</label>

                                                                <input type="text" name="address" required="" value="{{ Auth::user()->address }}">

                                                            </div>

                                                        </div>

                                                        <div class="col-lg-12 col-md-12">

                                                            <div class="billing-info">

                                                                <label>{{ trans('home.matkhaumoi') }}</label>

                                                                <input type="password" name="password">

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="billing-back-btn">

                                                        <div class="billing-back">

                                                            <a href="{{ route('logout') }}"

                                                            onclick="event.preventDefault();document.getElementById('logout-form').submit();" title="Đăng xuất" class="logout clickOut"> <i class="ti-arrow-up"></i> {{ trans('home.dangxuat') }}</a>

                                                                

                                                        </div>

                                                        <div class="billing-btn">

                                                            <button type="submit">{{ trans('home.luu') }}</button>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </form>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"

                                        style="display: none;">

                                        {{ csrf_field() }}

                                    </form>

                                </div>

                                <div class="panel panel-default">

                                    <div class="panel-heading">

                                        <h5 class="panel-title"><span>2</span> <a data-toggle="collapse" data-parent="#faq" href="#my-account-2">{{ trans('home.lichsumuahang') }}</a></h5>

                                    </div>

                                    <div id="my-account-2" class="panel-collapse collapse">

                                        <div class="panel-body">

                                            <div class="billing-information-wrapper">

                                                <table id="datatable" class="mb-0 table table-striped table-bordered">

                                                    <thead>

                                                        <tr>

                                                            <th>STT</th>

                                                            <th>ID</th>

                                                            <th>{{ trans('home.ngaydat') }}</th>

                                                            <th>{{ trans('home.tongtien') }}</th>

                                                            <th>{{ trans('home.trangthai') }}</th>

                                                            <th>{{ trans('home.thongtinchitiet') }}</th>

                                                        </tr>

                                                    </thead>

                                                    <tbody>

                                                        @foreach($bill as $key => $c)

                                                        <tr>

                                                            <td>{{ $key + 1 }}</td>

                                                            <td><a target="_blank" href="{{ route('chitietdon', $c->id) }}">{{ $c->orderId }}</a></td>

                                                            <td>{{ $c->created_at }}</td>

                                                            <td>{{ number_format($c->total) }}</td>

                                                            <td>

                                                                @if($c->status == 1)

                                                                {{ trans('home.choxuly') }}

                                                                @elseif($c->status == 2)

                                                                {{ trans('home.dangxuly') }}

                                                                @elseif($c->status == 3)

                                                                {{ trans('home.hoanthanh') }}

                                                                @else

                                                                {{ trans('home.huy') }}

                                                                @endif

                                                            </td>

                                                            <td><a target="_blank" href="{{ route('chitietdon', $c->id) }}" style="border-bottom: solid 1px;">Chi tiết</a></td>

                                                        </tr>

                                                        @endforeach

                                                    </tbody>

                                                </table>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div class="panel panel-default">

                                    <div class="panel-heading">

                                        <h5 class="panel-title"><span>3</span> <a data-toggle="collapse" data-parent="#faq" href="#my-account-3">{{ trans('home.danhsachyeuthich') }}</a></h5>

                                    </div>

                                    <div id="my-account-3" class="panel-collapse collapse">

                                        <div class="panel-body">

                                            <div class="billing-information-wrapper">

                                                <div class="table-content table-responsive">

                                                    <table class="mb-0 table table-striped table-bordered">

                                                        <thead>

                                                            <tr>

                                                                <th>{{ trans('home.hinhanh') }}</th>

                                                                <th>{{ trans('home.tensanpham') }}</th>

                                                                <th>{{ trans('home.gia') }}</th>

                                                                <th>{{ trans('home.themvaogio') }}</th>

                                                            </tr>

                                                        </thead>

                                                        <tbody>

                                                            @foreach ($yeuthich as $y)

                                                                <tr>

                                                                    <td class="product-thumbnail">

                                                                        <a href="{{ route('detail_product', $y->Product->slug_vi) }}"><img src="{{ asset($y->Product->image) }}" alt="" height="40px"></a>

                                                                    </td>

                                                                    <td class="product-name"><a href="{{ route('detail_product', $y->Product->slug_vi) }}">{{ $y->Product->$get_name }}</a></td>

                                                                    <td class="product-price-cart"><span class="amount">{{ number_format($y->Product->price) }}</span></td>

                                                                    <td class="product-wishlist-cart">

                                                                        <a href="{{ route('add-cart', $y->Product->id) }}">{{ trans('home.themvaogio') }}</a>

                                                                    </td>

                                                                </tr>

                                                            @endforeach

                                                        </tbody>

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div> 

@endsection

