@extends('frontend.partials.master')
@section('title', $cate_post3->title_seo_vi ." | ". $setting->name_vi)
@section('title_seo', $cate_post3->title_seo_vi)
@section('meta_key', $cate_post3->meta_key_vi)
@section('meta_des', $cate_post3->meta_des_vi)
@section('canonical')
{{ URL::current() }}
@stop
@section('body')

@stop
@section('css')
<link rel="stylesheet" href="{{asset('frontend/css/list_company.css')}}?{{time()}}">
@stop
@section('content')
 @php 
$slide = DB::table('slides')->where('status', 1)->where('dislay', 2)->orderBy('position','ASC')->get();

@endphp
<div class="box-slides">
    <div class="slideshow">
@foreach($slide as $sl)
<div class="slide_home">
<img class="topbanner" src="{{asset($sl->image)}}" alt="{{$sl->name_vi}}">
<span class="item-title">{{$sl->name_vi}}</span>
<img class="logo_company_con wow fadeInUp" data-wow-delay="0.5s" src="{{asset($cate_post3->banner)}}" alt="{{$cate_post3->name_vi}}">
</div>
@endforeach
          </div>
      </div>

<div class="intro_company">
	<div class="container">
		<div class="row">
			<div class="title_intro_company text-center wow fadeInUp" data-wow-delay="0.5s">
				GIỚI THIỆU
			</div>
		</div>
		<div class="row">
		<div class="col-xs-12 col-sm-10 col-sm-offset-1">
			<div class="col-xs-12 col-sm-6 wow fadeInUp" data-wow-delay="1s">
			<img src="{{asset($cate_post3->image_gioithieu)}}" alt="{!!$cate_post3->name_vi!!}">
		</div>
		<div class="description_intro_company col-xs-12 col-sm-6 wow fadeInUp" data-wow-delay="1s">
			{!!$cate_post3->title_vi!!}
		</div> 
	</div>
		</div>
	</div>
</div>

<div class="tamnhin_sumenh">
		<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1">
					<div class="col-xs-12 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
						<div class="">
							<div class="title_tnsm">
							Tầm nhìn <br> sứ mệnh
						</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
							<div class="around_tnsm">
						<img src="{{asset('frontend/images/icon_tamnhin.png')}}" alt="Tầm nhìn">
						<h3>Tầm nhìn</h3>
						<p>Trở thành đơn vị sản xuất và phân phối dược mỹ phẩm hàng đầu tại Việt Nam.</p>
					</div>
					</div>
					<div class="col-xs-12 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
						<div class="around_tnsm sumenh">
						<img src="{{asset('frontend/images/icon_sumenh.png')}}" alt="Sứ mệnh">
						<h3>Sứ mệnh</h3>
						<p>Luôn đồng hành cùng sắc đẹp và sức khỏe cộng đồng..</p>
					</div>
					</div>
			</div>
		</div>
	</div>
</div>


<div class="sanphamsxvaphanphoi">
	<div class="container">
	<div class="row">
			<h3 class="title_spsxvapp wow fadeInUp" data-wow-delay="0.5s">SẢN PHẨM SẢN XUẤT & PHÂN PHỐI</h3>
		 @php 
$spdv = DB::table('post3s')->where('status', 1)->where('cate_post3_id', $cate_post3->id)->orderBy('position','ASC')->get();

@endphp
@foreach($spdv as $key => $sp)
<div class="col-spdv col-xs-12 col-sm-4 wow fadeInUp" @if($key == 0) @elseif($key == 1) data-wow-delay="0.5s" @else data-wow-delay="1s" @endif>

	<div class="img-product hieuungzoom_bg" style="background:url('{{asset($sp->image)}}')center no-repeat;background-size: cover;min-height: 250px;">
		
	</div>
	<div class="name_sanpham">
<i class="fa fa-chevron-down" aria-hidden="true"></i>
		<a href="">{{$sp->name_vi}}</a>
	</div>
</div>
@endforeach
	</div>
	<div class="row">
		
		<div class="col-xs-12 truycap_webiste text-center">
			<a href="https://satopharmacy.com.vn/">
			Truy cập website
			</a>
		</div>
	</div>

	</div>




</div>

@endsection