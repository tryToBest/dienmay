@extends('frontend.partials.master')
@section('title', $cate_post3->title_seo_vi ." | ". $setting->name_vi)
@section('title_seo', $cate_post3->title_seo_vi)
@section('meta_key', $cate_post3->meta_key_vi)
@section('meta_des', $cate_post3->meta_des_vi)
@section('canonical')
{{ URL::current() }}
@stop
@section('body')

@stop
@section('css')
<link rel="stylesheet" href="{{asset('frontend/css/list_company.css')}}?{{time()}}">
<style>
	.description_intro_company {font-size: 15px;}
	.logo_company_con {
    position: absolute;
    right: auto;
    top: 42%;
    left: 5%;
    max-width: 500px;
}
@media(max-width: 767px){
		.logo_company_con {
    top: 60%;
    left: 5%;
    max-width: 150px;
}
}
</style>
@stop
@section('content')
 @php 
$slide = DB::table('slides')->where('status', 1)->where('dislay', 3)->orderBy('position','ASC')->get();

@endphp
<div class="box-slides">
    <div class="slideshow">
@foreach($slide as $sl)
<div class="slide_home">
<img class="topbanner" src="{{asset($sl->image)}}" alt="{{$sl->name_vi}}">
<span class="item-title">{{$sl->name_vi}}</span>
<img class="logo_company_con wow fadeInUp" data-wow-delay="0.5s" src="{{asset($cate_post3->banner)}}" alt="{{$cate_post3->name_vi}}">
</div>
@endforeach
          </div>
      </div>

<div class="intro_company">
	<div class="container">
		<div class="row">
			<div class="title_intro_company text-center wow fadeInUp" data-wow-delay="0.5s">
				GIỚI THIỆU
			</div>
		</div>
		<div class="row dong_gioithieu">
		<div class="col-xs-12 col-sm-10 col-sm-offset-1">
			<div class="col-xs-12 col-sm-6 wow fadeInUp" data-wow-delay="1s">
			<img src="{{asset($cate_post3->image_gioithieu)}}" alt="{!!$cate_post3->name_vi!!}">
		</div>
		<div class="description_intro_company col-xs-12 col-sm-6 wow fadeInUp" data-wow-delay="1s">
			{!!$cate_post3->title_vi!!}
		</div> 
	</div>
		</div>
		<div class="row">
		<div class="col-xs-12 col-sm-10 col-sm-offset-1">
			<div class="col-xs-12 col-sm-6 col-sm-push-6 wow fadeInUp" data-wow-delay="1s">
			<img src="{{asset($cate_post3->image_gioithieu2)}}" alt="{!!$cate_post3->name_vi!!}">
		</div>
		<div class="description_intro_company col-xs-12 col-sm-6 col-sm-pull-6 wow fadeInUp" data-wow-delay="1s">
			{!!$cate_post3->description_vi!!}
		</div> 
	</div>
		</div>
	</div>
</div>

<div class="tamnhin_sumenh">
		<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1">
					<div class="col-xs-12 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
						<div class="">
							<div class="title_tnsm">
							Tầm nhìn <br> sứ mệnh
						</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
							<div class="around_tnsm">
						<img src="{{asset('frontend/images/icon_tamnhin.png')}}" alt="Tầm nhìn">
						<h3>Tầm nhìn</h3>
						<p>Trở thành top 3 công ty hàng đầu trên mọi thị trường kinh doanh</p>
					</div>
					</div>
					<div class="col-xs-12 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
						<div class="around_tnsm sumenh">
						<img src="{{asset('frontend/images/icon_sumenh.png')}}" alt="Sứ mệnh">
						<h3>Sứ mệnh</h3>
						<p>Kết nối các doanh nghiệp và làm cho việc kinh doanh trở nên thành công hơn.</p>
					</div>
					</div>
			</div>
		</div>
	</div>
</div>


<div class="dichvu_quangcaotructuyen">
	<div class="container">

<div class="row">
	<div class="text-center col-xs-12 col-sm-4">
	<div class="title_dv wow fadeInUp" data-wow-delay="0.5s">
			DỊCH VỤ
	</div>
	</div>
	<div class="col-xs-12 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
	<div class="around_dv">
		<div class="around_border_dv ">	</div>
			<h3>PHÂN TÍCH HÀNH VI KHÁCH HÀNG</h3>
		<ul>
			<li>Nghiên cứu thị trường</li>
			<li>Tình trạng Doanh nghiệp</li>
			<li>Đối thủ cạnh tranh</li>
			<li>Hành vi khách hàng</li>
		</ul>
	</div>
	</div>
		<div class="col-xs-12 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
		<div class="around_dv">
			<div class="around_border_dv ">	</div>
			<h3>PHÂN TÍCH & XÂY DỰNG NỀN TẢNG DIGITAL</h3>
		<ul>
			<li>Website / Microsite</li>
			<li>Nền tảng Mạng xã hội</li>
			<li>Hệ thống CRM</li>
		
		</ul>
	</div>
		
	</div>
	<div class="col-xs-12 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
		<div class="around_dv">
			<div class="around_border_dv ">	</div>
			<h3>CHIẾN LƯỢC & SÁNG TẠO</h3>
		<ul>
			<li>Chiến lược Thương hiệu</li>
			<li>Chiến lược Mạng xã hội</li>
			<li>Chiến lược Digital</li>
			<li>Kế hoạch Tổng thể</li>
			<li>Quản lý khủng hoảng</li>
		</ul>
	</div>
		
	</div>
	<div class="yellow col-xs-12 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
		<div class="around_dv">
			<div class="around_border_dv ">	</div>
			<h3>TRIỂN KHAI & BÁO CÁO CHIẾN DỊCH</h3>
		<ul>
			<li>Thiết lập Quảng cáo</li>
			<li>Đặt mua Quảng cáo</li>
			<li>Các hoạt động offline</li>
			<li>Đo lường & Báo cáo</li>
	
		</ul>
	</div>
		
	</div>
	<div class="yellow col-xs-12 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
		<div class="around_dv">
			<div class="around_border_dv ">	</div>
			<h3>TỐI ƯU HÓA HIỆU QUẢ DIGITAL</h3>
		<ul>
			<li>Tăng độ phủ / Tần suất</li>
			<li>Tối ưu chiến dịch Bán hàng</li>
			<li>Kaizen</li>

	
		</ul>
	</div>
		
	</div>
</div>
	<div class="row">
		
		<div class="col-xs-12 truycap_webiste text-center">
			<a href="https://conex-agency.com/" target="_blank">
			Truy cập website
			</a>
		</div>
	</div>

	</div>




</div>


@endsection