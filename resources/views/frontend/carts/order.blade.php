@extends('frontend.partials.master')
@section('title', trans('home.giohang'))
@section('body')

@stop
@section('css')


@stop
@section('content')
 
<style type="text/css">
    .gio_hang{display: block;float: right;padding: 15px 0px;color: #f00;}
</style>

<div class="bx-cart">
    <div class="container">
        <div class="content-cart">
            <p><strong><div class="woocommerce"><div class="woocommerce-notices-wrapper"></div><div class="woocommerce-notices-wrapper"></div>
<form name="checkout" method="post" class="checkout woocommerce-checkout" action="{{ route('postOrder') }}" enctype="multipart/form-data">

    @csrf
        
        <div class="col2-set" id="customer_details">
            <div class="col-1">
                <div class="woocommerce-billing-fields">
    
        <h3>Thông tin thanh toán</h3>

    
   @if(Session::has('message'))
                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                @endif
               
    
  <div class="woocommerce-billing-fields__field-wrapper">
    <p class="form-row form-row-first validate-required" id="billing_first_name_field" data-priority="10">
        <label for="billing_first_name" class="">Tên&nbsp;<abbr class="required" title="bắt buộc">*</abbr>
        </label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="name" id="billing_first_name" placeholder=""  value="" autocomplete="given-name" required/></span>
    </p>
    <p class="form-row form-row-wide address-field validate-required" id="billing_address_1_field" data-priority="50">
        <label for="billing_address_1" class="">Địa chỉ&nbsp;<abbr class="required" title="bắt buộc">*</abbr>
        </label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="address" id="billing_address_1" placeholder="Địa chỉ"  value="" autocomplete="address-line1" required /></span>
    </p>
    <p class="form-row form-row-wide validate-required validate-phone" id="billing_phone_field" data-priority="100">
        <label for="billing_phone" class="">Số điện thoại&nbsp;<abbr class="required" title="bắt buộc">*</abbr>
        </label><span class="woocommerce-input-wrapper"><input type="tel" class="input-text " name="phone" id="billing_phone" placeholder=""  value="" autocomplete="tel" pattern="(\+84|0){1}(9|8|7|5|3){1}[0-9]{8}" autocomplete="off" required/></span>
    </p>
    <p class="form-row form-row-wide validate-required validate-phone" id="billing_phone_field" data-priority="100">
        <label for="billing_phone" class="">Số điện thoại khác (nếu có)
        </label><span class="woocommerce-input-wrapper"><input type="tel" class="input-text " name="phone2" id="billing_phone2" placeholder=""  value="" autocomplete="tel" pattern="(\+84|0){1}(9|8|7|5|3){1}[0-9]{8}" autocomplete="off"/></span>
    </p>
    
</div>

    </div>

            </div>

            <div class="col-2">
                <div class="woocommerce-shipping-fields">
    </div>
<div class="woocommerce-additional-fields">
    
    
        
            <h3>Thông tin bổ sung</h3>

        
        <div class="woocommerce-additional-fields__field-wrapper">
                            <p class="form-row notes" id="order_comments_field" data-priority=""><label for="order_comments" class="">Ghi chú đơn hàng&nbsp;<span class="optional">(tuỳ chọn)</span></label><span class="woocommerce-input-wrapper"><textarea name="note" class="input-text " id="order_comments" placeholder="Ghi chú về đơn hàng, ví dụ: thời gian hay chỉ dẫn địa điểm giao hàng chi tiết hơn."  rows="2" cols="5"></textarea></span></p>                  </div>

    
    </div>
            </div>
        </div>

        
        
        
    <h3 id="order_review_heading">Đơn hàng của bạn</h3>
    
    
    <div id="order_review" class="woocommerce-checkout-review-order">
        <table class="shop_table woocommerce-checkout-review-order-table">
    <thead>
        <tr>
            <th class="product-name">Sản phẩm</th>
            <th class="product-total">Tổng</th>
        </tr>
    </thead>
    <tbody>
              @forelse ($content as $c)
                            <tr class="cart_item">
                        <td class="product-name">
                            {{$c->name}}                        <strong class="product-quantity">&times; {{$c->qty}}</strong>                                                    </td>
                        <td class="product-total">
                            <span class="woocommerce-Price-amount amount">{{ number_format($c->price * $c->qty) }}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span>                        </td>
                    </tr>
                        @empty
                    Giỏ hàng rỗng
                @endforelse
                        </tbody>
    <tfoot>

        <tr class="cart-subtotal">
            <th>Tạm tính</th>
            <td><span class="woocommerce-Price-amount amount">{{ number_format($total_dau) }}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></td>
        </tr>

        
        
        
        
        
        <tr class="order-total">
            <th>Tổng</th>
            <td><strong><span class="woocommerce-Price-amount amount">{{ $total}}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></strong> </td>
        </tr>

        
    </tfoot>
</table>
<div id="payment" class="woocommerce-checkout-payment">
            <ul class="wc_payment_methods payment_methods methods">
            <li class="wc_payment_method payment_method_bacs">
    <input id="payment_method_bacs" type="radio" class="input-radio" name="payment" value="2"  checked='checked' data-order_button_text="" />

    <label for="payment_method_bacs">
        Chuyển khoản ngân hàng  </label>
            <div class="payment_box payment_method_bacs" >
       {!!$setting->tt1!!}
        </div>
    </li>
<li class="wc_payment_method payment_method_cod">
    <input id="payment_method_cod" type="radio" class="input-radio" name="payment" value="1"  data-order_button_text="" />

    <label for="payment_method_cod">
        Trả tiền mặt khi nhận hàng  </label>
            <div class="payment_box payment_method_cod" style="display:none;">
            <p>Trả tiền mặt khi giao hàng</p>
        </div>
    </li>
        </ul>
        <div class="form-row place-order">
        <noscript>
            Trình duyệt của bạn không hỗ trợ JavaScript, hoặc nó bị vô hiệu hóa, hãy đảm bảo bạn nhấp vào <em>Cập nhật giỏ hàng</em> trước khi bạn thanh toán. Bạn có thể phải trả nhiều hơn số tiền đã nói ở trên, nếu bạn không làm như vậy.          <br/><button type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Cập nhật tổng">Cập nhật tổng</button>
        </noscript>

            <div class="woocommerce-terms-and-conditions-wrapper">
        <div class="woocommerce-privacy-policy-text"><p>Thông tin cá nhân của bạn sẽ được sử dụng để xử lý đơn hàng, tăng trải nghiệm sử dụng website, và cho các mục đích cụ thể khác đã được mô tả trong <a href="" class="woocommerce-privacy-policy-link" target="_blank">chính sách riêng tư</a>.</p>
</div>
            </div>
    
        
        <button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Đặt hàng" data-value="Đặt hàng">Đặt hàng</button>
        
         </div>
</div>
    </div>

    
</form>

</div></strong></p>
        </div>
    </div>
</div>

@endsection
@section('script')
  
@endsection