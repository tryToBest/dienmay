@extends('frontend.partials.master')
@section('title', trans('home.giohang'))
@section('content')
        <div class="breadcrumb-area pt-95 pb-95 bg-img" style="background-image:url('{{ asset($setting->banner) }}');">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h2>{{ trans('home.giohang') }}</h2>
                </div>
            </div>
        </div>
         <!-- shopping-cart-area start -->
        <div class="cart-main-area pt-25 pb-100">
            <div class="container">
                <div class="breadcrumb-content duongdan">
                    <ul class="">
                        <li><i class="fa fa-home"></i><a href="{{ route('index') }}">{{ trans('home.home') }}</a></li>
                        <li class="active">{{ trans('home.giohang') }}</li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <form action="#">
                            <div class="table-content table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>{{ trans('home.hinhanh') }}</th>
                                            <th>{{ trans('home.tensanpham') }}</th>
                                            <th>{{ trans('home.gia') }}</th>
                                            <th>{{ trans('home.soluong') }}</th>
                                            <th>{{ trans('home.tong') }}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($content as $c)
                                            <tr>
                                                <td class="product-thumbnail">
                                                    <a><img src="{{ asset($c->options->img) }}" alt="" height="50"></a>
                                                </td>
                                                <td class="product-name"><a>{{ $c->name }}</a></td>
                                                <td class="product-price-cart"><span class="amount">{{ number_format($c->price) }}</span></td>
                                                <td class="product-quantity">
                                                    <input rowId="{{ $c->rowId }}" id="data-1" data-id="{{ $c->id }}" type="number" value="{{ $c->qty }}" name="update-cart" class="update-cart">
                                                </td>
                                                <td id="{{ $c->rowId }}" class="product-subtotal TongTien">{{ number_format($c->price * $c->qty) }} VNĐ</td>
                                                <td class="product-remove"><a href="{{ route('destroy_cart', $c->rowId) }}"><i class="ti-trash"></i></a></td>
                                            </tr>
                                        @empty
                                            Giỏ hàng rỗng
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="cart-shiping-update-wrapper">
                                        <div class="cart-shiping-update">
                                            <a href="{{ route('allProduct') }}">{{ trans('home.tieptucmuahang') }}</a>
                                            {{-- <button>Update Shopping Cart</button> --}}
                                        </div>
                                        {{-- <div class="cart-clear">
                                            <a href="#">Clear Shopping Cart</a>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="discount-code-wrapper">
                                    <h4 class="cart-bottom-title">MÃ GIẢM GIÁ: {{ $giamgia->ma }}</h4>
                                    <div class="discount-code">
                                        <form action="" method="post">
                                            @csrf
                                            <input type="text" required="" name="code">
                                            <button class="cart-btn-2" type="submit">Áp dụng</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="grand-totall">
                                    <span>{{ trans('home.tongcong') }}:   <span class="giatridonhang">{{ $total }}</span> VNĐ</span>
                                    <h5>{{ trans('home.sotienthanhtoan') }}:   <span class="tonggiatridonhang">{{ $total }}</span> VNĐ</h5>
                                    <a href="{{ route('order', $giamgia->id) }}">{{ trans('home.muangay') }}</a>
                                    {{-- <p>Checkout with Multiple Addresses</p> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('.update-cart').on('change', function(){
            var id    = $(this).data('id');
            var qty   = $(this).val();
            var rowId = $(this).attr('rowId');
            if(qty <= 0){
                alert('Phải lớn 0');
                location.href=""
            } else {
                $.get('cart/update',{id:id,qty:qty,rowId:rowId},function(data){
                    data = JSON.parse(data);
                        // console.log(data);
                        $('#' + rowId).html(data.TongTien);
                        $('.tonggiatridonhang').html(data.TongGiaTriDonHang);
                    });
            }
        });
    });
</script>
@endsection