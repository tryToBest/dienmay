@extends('frontend.partials.master')
@section('title', trans('home.giohang'))
@section('body')

@stop
@section('css')
<style>
 .items-count:hover{cursor: pointer;}
 .product-quantity .quantity{display: inline-flex;align-items: center;}
 .items-count{
    width: 28px;

    min-height: 28px;
    border: 1px solid #000;
    text-align: center;
}
.product-quantity .quantity input{
    min-height: 28px;
}
.product-quantity .quantity input::-webkit-input-placeholder {
    text-align:center;
}

/* Ẩn nút tăng giảm mặc định của input type number */
.product-quantity .quantity input[type=number]::-webkit-inner-spin-button,
.product-quantity .quantity input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

/* Đảm bảo rằng các style khác không bị ảnh hưởng */
.product-quantity .quantity input[type=number] {
    -moz-appearance: textfield;
    appearance: textfield;
}
.giahancamon{
    padding: 15px;
    background-color: #f9a11a;
}
.giahancamon .giahancamon_content{
    font-weight: bold;
    color: #000;
    text-align: center;
}
</style>
@stop
@section('content')
<style type="text/css">
    .gio_hang{display: block;float: right;padding: 15px 0px;color: #f00;}
</style>
<div class="bx-cart">
    <div class="container">
        <div class="content-cart">
            <div class="giahancamon">
                <p class="giahancamon_content">Điện máy Gia Hân cảm ơn quý khách mua hàng và sử dụng sản phẩm</p>
            </div>
            <p>
                <strong>
                    <div class="woocommerce">
                        <div class="woocommerce-notices-wrapper">
                            <div class="woocommerce-message" role="alert">
                                <a href="{{route('index')}}" tabindex="1" class="button wc-forward">Tiếp tục xem sản phẩm</a> Sản phẩm đã được thêm vào giỏ hàng.  </div>
                            </div>

                            @if(Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                            @endif
                            @if(Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                            @endif
                            @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                            @endif

                            <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="product-remove">&nbsp;</th>
                                        <th class="product-thumbnail">&nbsp;</th>
                                        <th class="product-name">Sản phẩm</th>
                                        <th class="product-price">Giá</th>
                                        <th class="product-quantity">Số lượng</th>
                                        <th class="product-subtotal">Tổng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @forelse ($content as $c)
                                  <tr class="woocommerce-cart-form__cart-item cart_item">

                                    <td class="product-remove">
                                        <a href="{{ route('destroy_cart', $c->rowId) }}" class="remove" aria-label="Xóa sản phẩm này" data-product_id="{{ $c->id }}" data-product_sku="65G3">&times;</a>                       </td>

                                        <td class="product-thumbnail">
                                            <a href="#"><img width="300" height="300" src="{{ asset($c->options->img) }}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" decoding="async" loading="lazy" srcset="{{ asset($c->options->img) }} 300w, {{ asset($c->options->img) }} 150w, {{ asset($c->options->img) }} 100w" sizes="(max-width: 300px) 100vw, 300px" /></a>                        </td>

                                            <td class="product-name" data-title="Sản phẩm">
                                                <a href="#">{{$c->name}}</a>                      </td>

                                                <td class="product-price" data-title="Giá">
                                                    <span class="woocommerce-Price-amount amount">{{ number_format($c->price) }}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span>                        </td>

                                                    <td class="product-quantity" data-title="Số lượng">
                                                        <div class="quantity">
                                                            <label class="screen-reader-text" for="quantity_64dc28e57b886">{{$c->name}} số lượng</label>
                                                            <div class="qtymin items-count">-</div>
                                                            <input
                                                            type="number"
                                                            rowId="{{ $c->rowId }}"
                                                            data-id="{{ $c->id }}"
                                                            class="input-text qty text"
                                                            step="1"
                                                            min="0"
                                                            max=""
                                                            name="update-cart"
                                                            value="{{ $c->qty }}"
                                                            title="SL"
                                                            size="4"
                                                            inputmode="numeric" />
                                                            <div class="qtyplus items-count">+</div>
                                                        </div>
                                                    </td>

                                                    <td class="product-subtotal" data-title="Tổng">
                                                        <span class="woocommerce-Price-amount amount" id="giasp{{ $c->rowId }}">{{ number_format($c->price*$c->qty) }}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span>                        </td>
                                                    </tr>

                                                    @endforeach   

                                                </tbody>
                                            </table>
                                            <style>
                                                .cart-collaterals .cart_khuyenmai{
                                                    width: 48%;
                                                    float: left;
                                                    
                                                }
                                                .cart-collaterals .cart_khuyenmai .cart_khuyenmai-h2{
                                                    font-weight: normal;
                                                    padding-bottom: 8px;
                                                }
                                                .cart-collaterals .cart_khuyenmai .cart_khuyenmai-content{
                                                    border: 1px solid #ebebeb;
                                                    border-radius: 3px;
                                                    padding: 8px;

                                                }
                                                
                                            </style>

                                            <div class="cart-collaterals">
                                                <div class="cart_khuyenmai">
                                                    @if(isset($khuyenmais) && $khuyenmais->count() > 0)
                                                        <h2 class="cart_khuyenmai-h2">Khuyến mại</h2>
                                                        <div class="cart_khuyenmai-content">
                                                            @foreach($khuyenmais as $khuyenmai)
                                                            {!!$khuyenmai->chucnang!!}
                                                            @endforeach
                                                        </div>
                                                    @else
                                                        <p></p>
                                                    @endif
                                                </div>
                                                <div class="cart_totals">
                                                    <h2>Cộng giỏ hàng</h2>
                                                    <table cellspacing="0" class="shop_table shop_table_responsive">
                                                        <tr class="cart-subtotal">
                                                            <th>Tạm tính</th>
                                                            <td data-title="Tạm tính"><span class="woocommerce-Price-amount amount tonggiatridonhang">{{$total}}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></td>
                                                        </tr>
                                                        <tr class="order-total">
                                                            <th>Tổng</th>
                                                            <td data-title="Tổng"><strong><span class="woocommerce-Price-amount amount tonggiatridonhang">{{$total}}<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></strong> </td>
                                                        </tr>
                                                    </table>

                                                    <div class="wc-proceed-to-checkout">
                                                        <div class="bk-btn" style="margin-top: 10px">

                                                        </div>

                                                        <a href="{{route('order')}}" class="checkout-button button alt wc-forward">
                                                        Tiến hành thanh toán</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </strong>
                                </p>
                            </div>
                        </div>
                    </div>



@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('.items-count').on("click", function () {
        var newVal = 1
        var $button = $(this)
        var oldValue = $button.parent().find('input').val()
        if ($button.text() == "+") {
            newVal = parseFloat(oldValue) + 1
        } else {
            if (oldValue > 1) {
                newVal = parseFloat(oldValue) - 1
            } else {
                newVal = 1
            }
        }
        $button.parent().find('input').val(newVal)
        var id    = $button.parent().find('input').data('id')
        var qty   = $button.parent().find('input').val()
        var rowId = $button.parent().find('input').attr('rowId')
        var voucher = $button.parent().find('input').attr('voucher')
        if(qty <= 0){
            alert('Phải lớn 0')
            location.href=""
        } else {
            $.get('cart/update',{id:id,qty:qty,rowId:rowId, voucher:voucher},function(data){
                data = JSON.parse(data)
                console.log(data);
                $('#' + rowId).html(data.TongTien)
                $('#giasp' + rowId).html(data.TongTien)
                        // $('#giamgia_'+id +'_'+rowId).html(data.Tonggiam_tungsp)
                $('.tonggiatridonhang').html(data.TongGiaTriDonHang)
                         // $('.tonggiatrigiamgia').html(data.tongGiaTriGiamGia)
            });
        }
    });
});
</script> 
@endsection
