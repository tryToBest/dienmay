@extends('frontend.partials.master')
@section('title', 'Search')


@section('body')

@stop
@section('css')
<style>
    .anhquangcao{
        margin-bottom: 15px;
    }
    .col_left{
        margin-top: -20px;
    }
    .bannerquangcao1 {
    margin-top: 25px!important;

}
.nutgiohangsp {
    font-size: 14px;
}
.show_allsanpham .col-sp:nth-child(4n+1){
    clear: both;
}
.title_danhmucsp_home .col-xs-12 {
    margin-bottom: 25px;
        margin-top: 20px;
}
.cotphai_productbycate{
    margin-top: 25px;
}
.bx-main .bx-left {
    width: 100%!important;
}
</style>
@stop
@section('content')
{{-- @include('frontend/partials/quangcaohome') --}}
{{-- @include('frontend/partials/slide2') --}}

<section class="bx-breadcrum">
    <div class="container">
        <ul>
            <li><a href="{{route('index')}}" title="Trang chủ">Trang chủ</a></li>
            <li><span><i class="fas fa-angle-right"></i></span></li>
            <li><span>Tìm kiếm </span></li>
        </ul>
    </div>
</section>
<br>
{{-- <section class="bx-slider-cate">
    <div class="container">
        <div class="list-slider">
            <ul id="list-slider-cate">

   <li><a href="#" title=""><img src="https://dienmaytinphat.com/wp-content/uploads/2019/06/29_05_2019_15_08_54_SAMSUNG-TV-640-280.png" alt=""/></a></li>
<li><a href="#" title=""><img src="https://dienmaytinphat.com/wp-content/uploads/2019/06/04_06_2019_10_13_13_LG-TV-640-280.png" alt=""/></a></li>
<li><a href="#" title=""><img src="https://dienmaytinphat.com/wp-content/uploads/2019/06/30_05_2019_10_04_49_TV-Sony-640-280.png" alt=""/></a></li>
<li><a href="#" title=""><img src="https://dienmaytinphat.com/wp-content/uploads/2019/06/30_05_2019_16_02_26_TCL-TV-640-280.png" alt=""/></a></li>

                            </ul>
             <script>
                slider[99909] = $('#list-slider-cate').lightSlider({
                    item: 3,
                    loop: true,
                    slideMove: 1,
                    easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                    speed: 1000,
                    slideMargin: 10,
                    mode: 'slide',
                    auto: true,
                    pause: 5000,
                    controls: false,
                    pager: false,
                    responsive: [
                        {
                            breakpoint: 800,
                            settings: {
                                item: 2,
                                slideMove: 1,
                                slideMargin: 6,
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                item: 2,
                                slideMove: 1
                            }
                        }
                    ]
                });
            </script>
            <div class="control">
                <span onClick="prevSlide(99909)" class="btn-prev"><i class="fas fa-chevron-left"></i></span>
                <span onClick="nextSlide(99909)" class="btn-next"><i class="fas fa-chevron-right"></i></span>
            </div>
        </div>

    </div>
</section> --}}
<section class="bx-main">
    <div class="container">
        <div class="bx-left">
           
            <div class="bx-product product-cate">
                <h2>Tất cả {{$countProducts}} Tìm kiếm "{{$input}}"</h2>
                
                <div class="list-product">
                    <div class="row">                     
               @foreach($products as $product)
                @php
                    $tongdanhgia = DB::table('comments')->where('status',1)->where('product_id',$product->id)->count();
                    $demso5sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',5)->count();
                    $demso4sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',4)->count();
                    $demso3sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',3)->count();
                    $demso2sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',2)->count();
                    $demso1sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',1)->count();
                    if($tongdanhgia >0){
                        $sosao = ($demso5sao*5 + $demso4sao*4 + $demso3sao*3 + $demso2sao*2 + $demso1sao*1)/$tongdanhgia;
                    }
                    else{
                        $sosao = 0;
                    }
                @endphp
                       <div class="item">
                        <div class="thumb">
                            <a href="{{route('detail_product',$product->slug_vi)}}" title="{{$product->name_vi}}">
                                <img src="{{asset($product->image)}}" alt="{{$product->name_vi}}"/>
                            </a>
                        </div>
                        <div class="info">
                            <h3><a href="{{route('detail_product',$product->slug_vi)}}" title="{{$product->name_vi}}">{{$product->name_vi}}</a></h3>
                           <div class="price"> @if($product->price != 0) {{number_format($product->price,0)}}đ @else Liên hệ @endif</div>

                                        @if($product->price_unit != null && $product->price_unit != 0)
                                        <div class="sale">
                                            <span>{{number_format($product->price_unit,0)}}₫</span>
                                            <i>(-{{ number_format((($product->price_unit- $product->price)/$product->price_unit)*100,0)}}%)</i>
                                        </div>
                                        @endif
                        </div>
                       <div class="rate">
                                        <span class="point">{{$sosao}}/5 <i class="fas fa-star"></i></span>
                                        <span class="total-rate">{{$tongdanhgia}} đánh giá</span>
                                    </div>
                    </div>

@endforeach
        
    </div>                
</div>
                
                                <div class="page">
                                    <ul class="pagination pagination-sm">{{$products->links()}}</ul>     
                                </div>
                                </div>
        </div>
   
    </div>
</section>


@endsection
@section('script')

@endsection