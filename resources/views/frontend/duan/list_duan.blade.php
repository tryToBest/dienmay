@extends('frontend.partials.master')
@section('title', $cate_post2->name_vi." | ". $setting->name_vi)
@section('title_seo', $cate_post2->title_seo_vi)
@section('meta_key', $cate_post2->meta_key_vi)
@section('meta_des', $cate_post2->meta_des_vi)
@section('canonical')
{{ URL::current() }}
@stop
@section('body')
data-rsssl=1 class="archive tax-tktb_type term-noi-that-nha-bep term-39"
@stop
@section('css')
<link rel="preload" href="{{asset('frontend/css/list_duan.css')}}?{{time()}}" data-rocket-async="style" as="style" onload="this.onload=null;this.rel='stylesheet'" media="all" data-minify="1" />
<style>
	#ff-section-breadcrumb {
		    margin-top: 60px;
	}
	#ff-section-breadcrumb ul li{
		color: #000;
	}
	#ff-section-breadcrumb ul li a{
		color: #000;
	}
</style>
@stop
@section('content')


<main id="tktb-wrapper">
    


    <section id="ff-section-breadcrumb">
        <div class="container-master">
            <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route('index')}}"><i
                                    class="fa fa-home"></i> Trang chủ</a>
                    </li>
                   
                    <li class="breadcrumb-item active" aria-current="page">
              {{$cate_post2->name_vi}}                 </li>
                </ul>
            </nav>
        </div>
    </section>

    <section id="ff-section-taxonomy">
        <div class="container-master">
            <div class="row-master">
                <div class="col-md-12">
                    <h1>{{$cate_post2->name_vi}}</h1>
                </div>
            </div>
        </div>

    </section>
    <section id="ff-section-content" class="ff-section">

        <div class="container-master">
              
                        <div id="ff-section-link">
                <div class="row-master">
                                                <div class="col-md-6 col-12">
                                <div class="item">
                                    <div class="item-pic">
                                        <img src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E" alt="Lựa chọn Thiết kế" data-lazy-src="https://flexfit.vn/wp-content/uploads/2020/07/Screen-Shot-2563-07-02-at-15.33.50.png"><noscript><img src="https://flexfit.vn/wp-content/uploads/2020/07/Screen-Shot-2563-07-02-at-15.33.50.png" alt="Lựa chọn Thiết kế"></noscript>
                                    </div>
                                    <div class="item-content">
                                        <div class="item-content-title">
                                            <h2> Lựa chọn Thiết kế </h2>
                                        </div>
                                        <div class="item-content-subtitle">
                                            Theo phong cách của bạn                                        </div>
                                    </div>
                                    <a href="{{route('contact')}}" class="item-readmore ">
                                        <span>Xem ngay <i
                                                    class="fa fa-caret-right"></i></span>
                                    </a>

                                </div>
                            </div>

                                                    <div class="col-md-6 col-12">
                                <div class="item">
                                    <div class="item-pic">
                                        <img src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E" alt="Tư Vấn Online" data-lazy-src="https://flexfit.vn/wp-content/uploads/2020/06/bn2.png"><noscript><img src="https://flexfit.vn/wp-content/uploads/2020/06/bn2.png" alt="Tư Vấn Online"></noscript>
                                    </div>
                                    <div class="item-content">
                                        <div class="item-content-title">
                                            <h2> Tư Vấn Online </h2>
                                        </div>
                                        <div class="item-content-subtitle">
                                            Với đội ngũ Kiến trúc sư đa dạng, <br>nhiều kinh nghiệm                                        </div>
                                    </div>
                                    <a href="{{route('contact')}}" class="item-readmore ">
                                        <span>Xem ngay <i
                                                    class="fa fa-caret-right"></i></span>
                                    </a>

                                </div>
                            </div>

                                        </div>


            </div>
            <div id="ff-section-listitem" class="row-master">


            	@foreach($all_project as $ap)
 <div class="col-md-6 col-12">
                            

    <div class="item">
                <a href="{{route('detail_duan',$ap->slug_vi)}}"
           title="{{$ap->name_vi}}" class="item-pic">
            <img width="1643" height="924" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201643%20924'%3E%3C/svg%3E" class="attachment- size- wp-post-image" alt="{{$ap->name_vi}}" data-lazy-src="{{asset($ap->image)}}" /><noscript><img width="1643" height="924" src="{{asset($ap->image)}}" class="attachment- size- wp-post-image" alt="{{$ap->name_vi}}" /></noscript>        </a>

        <div class="item-content">
            <div class="item-content-title">

                    <h2>                        <a href="{{route('detail_duan',$ap->slug_vi)}}"
                           title="{{$ap->name_vi}}">
                            {{$ap->name_vi}}                        </a>
                </h2>


                {{-- <span> 6 m2</span> --}}
            </div>
            <div class="item-content-subtitle">

             {{$ap->name_vi}}        </div>

        </div>
    </div>

                        </div>
  @endforeach           
               
                                        
                                                
                                                
                                               
                        
            </div>
                       {{--      <button type="button" id="btn-loadmore">Xem thêm <i
                            class="fa fa-angle-right"></i></button> --}}
                    </div>
    </section>
            <section id="ff-section-description">
            <div class="container-master">
                <div class="ff-section-description">
           <?php
                                  $descsk = substr(strip_tags($cate_post2->description_vi ), 0, 1000);
                                  
                                  if ((strlen($cate_post2->description_vi )  ) > 1000) {
                                   // trim the description back to the last period or space so words aren't cut off
                                   $period_pos = strrpos($descsk, ".");
                                   $space_pos = strrpos($descsk, " ");
                                   // find the character that we should trim back to. -1 on space pos for a space that follows a period, so we dont end up with 4 periods
                                   if ($space_pos - 1 > $period_pos) {
                                    $pos = $space_pos;
                                   }
                                   else {
                                    $pos = $period_pos;
                                   }
                                   $descsk = substr($descsk, 0, $pos);
                                   $descsk .= "...";
                                   echo $descsk;
                                  }else echo $cate_post2->description_vi ;
                                ?>
              <div class="ff-section-readmore">
                        <button type="button" onclick="openTaxModal()">
                            <img src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E" alt="down" data-lazy-src="{{asset('frontend/images/icon-3arrows.png')}}"><noscript><img src="{{asset('frontend/images/images/icon-3arrows.png')}}" alt="down"></noscript>
                        </button>
                    </div>
                </div>

            </div>
        </section>
        
    <section id="ff-section-boxcontact">
        <div class="container-master">
            <div class="row-master">
                <div class="ff-section-box">
                    <div class="ff-section-box-title">
                        <span>Liên hệ ngay với chúng tôi theo số  <a href="tel:{{$setting->hotline}}">{{$setting->hotline}}</a> để được tư vấn & giải đáp thắc mắc</span>
                    </div>
                    <a href="{{route('contact')}}" class="ff-section-box-url">
                        <span>Hỗ trợ tư vấn</span>
                    </a>

                </div>
            </div>
        </div>
    </section>
  
    <div id="taxDescription">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="modalBtn" onclick="closeTaxModal()">&times;</button>
                </div>

                <div class="modal-body">
   

{!!$cate_post2->description_vi!!}
                </div>
            </div>
        </div>
    </div>
    <div class="bg-black-modal"></div>
</main>

@endsection
@section('script')
<noscript><link rel="stylesheet" href="{{asset('frontend/css/list_duan.css')}}?{{time()}}" media="all" data-minify="1" /></noscript>
<script type="rocketlazyloadscript" data-rocket-type='text/javascript'
        src='{{asset('frontend/js/tktb.min.js')}}?{{time()}}' defer></script>
<script type="rocketlazyloadscript">
    openTaxModal = () => {
        $('body').addClass('no-scroll');
        $('#taxDescription').addClass('showModalMaster')
    }

    closeTaxModal = () => {
        $('body').removeClass('no-scroll');
        $('#taxDescription').removeClass('showModalMaster')
    }
    $(document).mouseup(function (e) {
        if ($(e.target).closest("#taxDescription").length=== 0) {
            closeTaxModal();
        }
    });
</script>
@endsection