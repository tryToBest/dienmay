@extends('frontend.partials.master')
@section('title', $detail_duan->name_vi)
@section('title_seo', $detail_duan->title_seo_vi)
@section('meta_key', $detail_duan->meta_key_vi)
@section('meta_des', $detail_duan->meta_des_vi)
@section('canonical')
{{ URL::current() }}
@stop
@section('body')
data-rsssl=1 class="tktb-template-default single single-tktb postid-4245 single-format-standard"
@stop
@section('css')
<link rel="preload" href="{{asset('frontend/css/detail_duan.css')}}?{{time()}}" data-rocket-async="style" as="style" onload="this.onload=null;this.rel='stylesheet'" media="all" data-minify="1" />
<style>
    #ff-section-description strong,#ff-section-description b{
        font-weight: bold!important;
    }
    .ff-section-description {max-width: 80%;margin:0 auto;}
    @media(max-width: 767px){
         .ff-section-description{max-width: 100%;margin:0 auto;}
    }
</style>
@stop
@section('content')


<main id="post-tktb-detail" class="site-main">
		<section id="ff-section-banner">
                <img src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E" alt="{{$detail_duan->name_vi}}" data-lazy-src="{{asset($detail_duan->image)}}"><noscript><img src="{{asset($detail_duan->image)}}" alt="{{$detail_duan->name_vi}}"></noscript>
        <div class="banner-title">
                    <h1>{{$detail_duan->name_vi}}</h1>
                <ul>
            <li>
                <a rel="nofollow" href="https://www.facebook.com/sharer/sharer.php?u={{route('detail_duan',$detail_duan->slug_vi)}}"
                   title="Share Facebook"
                   target="_blank"
                >
                    <i class="fa fa-facebook"></i>
                </a>
            </li>
            <li>
                <a rel="nofollow" href="https://twitter.com/home?status={{route('detail_duan',$detail_duan->slug_vi)}} "
                   title="Share Twitter"
                   target="_blank"
                >
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
            <li>
                <a rel="nofollow" href="https://pinterest.com/pin/create/button/?{{route('detail_duan',$detail_duan->slug_vi)}}"
                   title="Share Pinterst"
                   target="_blank"
                >
                    <i class="fa fa-pinterest"></i>
                </a>
            </li>
        </ul>
    </div>
</section>
<section id="ff-section-breadcrumb" class="type-3">
    <div class="container-master">
        <div class="row-master">
            <nav aria-label="breadcrumb">


                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route('index')}}"><i
                                    class="fa fa-home"></i>Trang chủ</a>
                    </li>
                                            <li class="breadcrumb-item">
                            <a href="{{route('list_duan',$cate_post2->slug_vi)}}"> {{$cate_post2->name_vi}}</a>
                        </li>
              

                                        <li class="breadcrumb-item active" aria-current="page">{{$detail_duan->name_vi}}</li>
                </ul>

            </nav>
        </div>

    </div>
</section>
<section id="ff-section-introduce" class="ff-section">
    <div class="ff-section-introduce-container">
        <div class="col-row">
            <div class="col-container-left col-container">
                <div class="col-left">
                   {{$detail_duan->name_vi}}            
                </div>
            </div>

            <div class="col-container-right col-container">
                <div class="col-right">
                    <ul>
                        <li>
                            <span class="col-title">Diện tích</span>
                            <span class="col-content">{{$detail_duan->dientich}}</span>
                        </li>
                        <li>
                            <span class="col-title">Địa chỉ</span>
                            <span class="col-content">{{$detail_duan->diadiem}}</span>
                        </li>
                        <li>
                            <span class="col-title">Năm hoàn thành</span>
                            <span class="col-content">{{$detail_duan->namthuchien}}</span>
                        </li>
                        <li>
                            <span class="col-title">Hạng mục thi công</span>
                            <span class="col-content">{{$detail_duan->hangmuc}}</span>
                        </li>
                    </ul>
                </div>
            </div>

        </div>

    </div>

</section>
{{-- 
<section id="ff-section-album" class="ff-section">
    <div class="container-master">
        <div class="row-master">
            <div class="ff-section-title">
                <h2>Hình ảnh dự án</h2>
            </div>
            <div class="ff-section-album">
                <ul>
                                                        </ul>

            </div>
        </div>
    </div>
</section> --}}

    <section id="ff-section-description">
        <div class="container-master">
            <div class="ff-section-description">
                {!!$detail_duan->description_vi!!}


                 
            </div>

        </div>
    </section>


<div class="bg-black-modal"></div>

         <section id="ff-section-formcommon">
             <div class="container-master">
                 <div class="row-master">
                     <div class="ff-section-form">
                         <div class="ff-section-form-title">
                             <h2>Trải nghiệm thiết kế cùng chúng tôi</h2>
                         </div>
                         <div class="ff-section-form-quote">
                             Cùng với đội ngũ thiết kế của chúng tôi, hãy hiện thực hoá nên từng không gian ngôi nhà mong ước của bạn.                         </div>
                         <div class="ff-section-form-content">
                           

<form action="{{route('post_contact')}}" method="post" class="wpcf7-form init" >
@csrf
<label><span class="wpcf7-form-control-wrap name_common"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Tên của bạn" /></span></label>
<label><span class="wpcf7-form-control-wrap tel_common"><input type="tel" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" aria-invalid="false" placeholder="Số điện thoại" /></span></label>
<label class="btn-submit"><input type="submit" value="Yêu cầu tư vấn" class="wpcf7-form-control has-spinner wpcf7-submit" /></label></form>                    </div>
                     </div>
                 </div>
             </div>
         </section>
     


<section id="ff-section-releated" class="ff-section">
    <div class="container-master">

        <div class="ff-section-title">
            <h2>Dự án tương tự</h2>
        </div>
        <div class="ff-section-content">

            <div class="row-master">
            	      @foreach($tinlq as $lq) 
                                        <div class="col-md-4 col-12">
                            <div class="item">
                                <div class="item-pic">
                                    <a href="{{route('detail_duan',$lq->slug_vi)}}"
                                       title="{{$lq->name_vi}}" target="_blank">
                                        <img width="1400" height="933" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201400%20933'%3E%3C/svg%3E" class="attachment- size- wp-post-image" alt="{{$lq->name_vi}}" data-lazy-src="{{asset($lq->image)}}" /><noscript><img width="1400" height="933" src="{{asset($lq->image)}}" class="attachment- size- wp-post-image" alt="{{$lq->name_vi}}" /></noscript>                                    </a>
                                </div>
                                <div class="item-title">
                                    <a href="{{route('detail_duan',$lq->slug_vi)}}" target="_blank"
                                       title="{{$lq->name_vi}}">
                                        {{$lq->name_vi}}                                    </a>

                                </div>
                            </div>
                        </div>
                             @endforeach                   
                                                
                                    </div>

        </div>

    </div>
</section>



</main><!-- #main -->






@endsection
@section('script')
<noscript><link rel="stylesheet" href="{{asset('frontend/css/detail_duan.css')}}?{{time()}}" media="all" data-minify="1" /></noscript>

<script type="rocketlazyloadscript" data-rocket-type='text/javascript'
        src='https://flexfit.vn/wp-content/themes/flexfit/assets/plugins/swiper/swiper.min.js' defer></script>
<script type="rocketlazyloadscript" data-rocket-type='text/javascript'
        src='https://flexfit.vn/wp-content/themes/flexfit/assets/plugins/fancybox/fancybox.min.js' defer></script>

<script type="rocketlazyloadscript">
window.onload = function() {   
    function changeTabs(elm) {
        var data_key = $(elm).attr('data-key');
        $(elm).parent().children('li').removeClass('ff-active');
        $(elm).addClass('ff-active');
        $('#ff-tabs-content .ff-tab-child').removeClass('ff-active');
        $('#ff-tabs-content .ff-tab-' + data_key).addClass('ff-active');
    }
    $(".item-fancybox").fancybox();
    var swiperAlbum = new Swiper('.swiperAlbum', {
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
    });


}
</script>

@endsection