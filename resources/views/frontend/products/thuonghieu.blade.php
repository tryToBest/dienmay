@extends('frontend.partials.master')
@section('title', 'Thương hiệu'.$tenth->name_vi)

@section('canonical')
{{ URL::current() }}
@stop
@section('body')

@stop
@section('css')
<style>
    .anhquangcao{
        margin-bottom: 15px;
    }
</style>
@stop
@section('content')

<div class="all_sanpham_home">
    
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3">
            @include('frontend/partials/left')
@php
$quangcaohome = DB::table('slides')->where('status',1)->where('dislay',8)->orderBy('position','ASC')->take(1)->get();
@endphp
<div class="anhquangcao">
    @foreach($quangcaohome as $qc2)
    <a href="{{$qc2->link}}"><img src="{{asset($qc2->image)}}" alt="{{$qc2->name_vi}}"></a>
    @endforeach
</div>

        </div>
        <div class="col-xs-12 col-sm-9">
<div class="title_danhmucsp_home">
    <div class="col-xs-12">
        <span>Thương hiệu {{$tenth->name_vi}}</span>
   
    </div>
</div>

<div style="clear: both;"></div>
<div class="show_allsanpham">
    @foreach($product as $p)
            <div class="col-xs-6 col-sm-4">
                <div class="baoquanh_sp">
                    <a href="{{route('detail_product',$p->slug_vi)}}">
                        <div class="img-sanpham hieuungzoom_bg" style="background:url('{{asset($p->image)}}')center no-repeat;background-size: cover;min-height: 250px;">
                            {{-- <img src="{{asset($p->image)}}" alt="{{$p->name_vi}}"> --}}
                    </div>
                    <div class="name-sanpham">
                        {{$p->name_vi}}
                    </div>
                    <div class="thuonghieu">
               <p>  Mã sản phẩm: {{$p->code}}</p>
                    </div>
                    <div class="tinhtrang">
                        <p>     Tình trạng : còn hàng</p>
                    </div>
                    <div class="price">
                        Giá: <b>@if($p->price != 0) {{number_format($p->price)}} đ @else Liên hệ @endif</b>
                    </div>
                    </a>    
                </div>
                </div>
    @endforeach
</div>
<div style="clear: both;"></div>
<div class="phantrang">
    {{$product->links()}}
</div>
            
        </div>

        </div>

    </div>
</div>



@endsection
@section('script')
<script>
        $('.acnav__label').click(function () {
    var label = $(this);
    var parent = label.parent('.has-children');
    var list = label.siblings('.acnav__list');
    if ( parent.hasClass('is-open') ) {
        list.slideUp('fast');
        parent.removeClass('is-open');
    }
    else {
        list.slideDown('fast');
        parent.addClass('is-open');
    }
});
</script>
<script src='{{asset('frontend/js/slick.min.js')}}'></script>
<script  src="{{asset('frontend/js/slick2.js')}}?{{time()}}"></script>
@endsection