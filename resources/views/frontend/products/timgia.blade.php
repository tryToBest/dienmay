@foreach ($product as $key => $p)

    <div class="product-width col-lg-6 col-xl-4 col-md-6 col-sm-6">

        <div class="product-wrapper mb-10">

            <div class="product-img">

                <a href="{{ route('detail_product', $p->slug_vi) }}">

                    <img src="{{ asset($p->image) }}" alt="">

                </a>

                <div class="product-action">

                    <a title="Quick View" data-toggle="modal" data-target="#exampleModal{{$key}}" href="#">

                        <i class="ti-plus"></i>

                    </a>

                    @php
                        $ccc = Cart::content()->where('id', $p->id)->first();
                    @endphp
                    @if ($ccc)
                        <span class="label label-danger xaolin"><i class="fa fa-fw fa-check-circle"></i> Đã thêm vào giỏ</span>
                    @else
                        {{-- false expr --}}
                        <a href="javascript:void(0)" id="addcart" data-id="{{ $p->id }}">
                            <span id="addcart{{ $p->id }}">
                                <span class="label label-success xaolin"><i class="ti-shopping-cart"></i></span>
                            </span>
                        </a>
                    @endif

                </div>

                <div class="product-action-wishlist">

                    <a title="Wishlist" href="{{ route('themyeuthich', $p->id) }}">

                        <i class="ti-heart"></i>

                    </a>

                </div>

            </div>

            <div class="product-content">

                <h4><a href="{{ route('detail_product', $p->slug_vi) }}">{{ $p->$get_name }}</a></h4>

                <div class="product-price">

                    <span class="new">{{ number_format($p->price) }} VNĐ</span>

                    @if ($p->price_unit != null)

                        <span class="old">{{ number_format($p->price_unit) }} VNĐ</span>

                    @endif

                </div>

            </div>

            <div class="product-list-content">

                <h4><a href="{{ route('detail_product', $p->slug_vi) }}">{{ $p->$get_name }}</a></h4>

                <div class="product-price">

                    <span class="new">{{ number_format($p->price) }} VNĐ</span>

                    @if ($p->price_unit != null)

                        <span class="old">{{ number_format($p->price_unit) }} VNĐ</span>

                    @endif

                </div>

                <p>{!! $p->$get_title !!}</p>

                <div class="product-list-action">

                    <div class="product-list-action-left">

                        <a class="addtocart-btn" title="Add to cart" href="{{ route('add-cart', $p->id) }}"><i class="ion-bag"></i> {{ trans('home.themvaogio') }}</a>

                    </div>

                    <div class="product-list-action-right">

                        <a title="Wishlist" href="{{ route('themyeuthich', $p->id) }}"><i class="ti-heart"></i></a>

                        <a title="Quick View" data-toggle="modal" data-target="#exampleModal{{ $key }}" href="#"><i class="ti-plus"></i></a>

                    </div>

                </div>

            </div>

        </div>

    </div>

    @php

        $img = DB::table('images')->where('product_id', $p->id)->take(2)->get();

    @endphp

    <!-- modal -->

    <div class="modal fade" id="exampleModal{{$key}}" tabindex="-1" role="dialog" aria-hidden="true">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

            <span class="ti-close" aria-hidden="true"></span>

        </button>

        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-body">

                    <div class="qwick-view-left">

                        <div class="quick-view-learg-img">

                            <div class="quick-view-tab-content tab-content">

                                <div class="tab-pane active show fade" id="modal{{ $key }}0" role="tabpanel">

                                    <img src="{{ asset($p->image) }}" alt="">

                                </div>

                                @foreach ($img as $key2 => $i)

                                    <div class="tab-pane fade" id="modal{{ $key }}{{ $key2 + 1 }}" role="tabpanel">

                                        <img src="{{ asset($i->name) }}" alt="">

                                    </div>

                                @endforeach

                            </div>

                        </div>

                        <div class="quick-view-list nav" role="tablist">

                            <a class="active" href="#modal{{ $key }}0" data-toggle="tab">

                                <img src="{{ asset($p->image) }}" alt="">

                            </a>

                            @foreach ($img as $key2 => $i)

                                <a class="" href="#modal{{ $key }}{{ $key2 + 1 }}" data-toggle="tab">

                                    <img src="{{ asset($i->name) }}" alt="">

                                </a>

                            @endforeach

                        </div>

                    </div>

                    <div class="qwick-view-right">

                        <div class="qwick-view-content">

                            <h3>{{ $p->$get_name }}</h3>

                            <div class="product-price">

                                <span class="new">{{ number_format($p->price) }} VNĐ</span>

                                @if ($p->price_unit != null)

                                    <span class="old">{{ number_format($p->price_unit) }} VNĐ</span>

                                @endif

                            </div>

                            <div class="product-rating">

                                <i class="icon-star theme-color"></i>

                                <i class="icon-star theme-color"></i>

                                <i class="icon-star theme-color"></i>

                                <i class="icon-star"></i>

                                <i class="icon-star"></i>

                            </div>

                            <p>{!! $p->$get_title !!}</p>

                                <form action="{{ route('add-cart', $p->id) }}" method="get">

                            <div class="quickview-plus-minus">
                                <label class="soluongcccc">Số lượng:</label>

                                    @csrf

                                    <div class="cart-plus-minus">

                                        <input type="number" value="1" name="quantity" class="cart-plus-minus-box" min="1" max="1000">

                                    </div>

                                    <div class="quickview-btn-cart">

                                        <button class="btn-style">add to cart</button>

                                    </div>

                                    <div class="quickview-btn-wishlist">

                                        <a class="btn-hover" href="#"><i class="ti-heart"></i></a>

                                    </div>

                            </div>

                                </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endforeach
@section('script')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('click', '#addcart', function(){
            var id = $(this).data('id');
            $.post('{{ route('add-cart2') }}', {id:id}, function(data){
                $('#addcart'+id).html('<span class="label label-danger xaolin"><i class="fa fa-fw fa-check-circle"></i> Đã thêm vào giỏ</span>');
                $('.mln').html(data);
            });
        });
    </script>
@endsection