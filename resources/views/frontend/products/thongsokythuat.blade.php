@extends('frontend.partials.master')

    @section('title', $detail_product->$get_name )
    @section('title_seo', $detail_product->title_seo)
    @section('meta_key', $detail_product->meta_key)
    @section('meta_des', $detail_product->meta_des)
    @section('css')

    @stop
@section('addclassbody')
    html not-front not-logged-in no-sidebars page-node page-node- page-node-1942 node-type-product-display role-anonymous-user i18n-en-us
    @stop
    @section('content')
<main class="main-container  ">
<div class="content-container">
<section>

<section id="block-system-main" class="block block-system ">
<div class="view view-fbc-products view-id-fbc_products view-display-id-page_1 view-dom-id-62f0bde1800753ae8ab05fb65551aeb3">
<div class="view-content">
<div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
<div about="" typeof="schema:Product sioc:Item foaf:Document" class="ds-1col node node-product-display view-mode-datasheet  clearfix">
<div class="group-card-wide field-group-div card-wide-layout"><div class="group-card-content field-group-div card-content"><div class="group-card-image field-group-div card-content"><div class="field field-name-field-teaser-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even" rel="" resource="{{asset($detail_product->image)}}"><img typeof="foaf:Image" src="{{asset($detail_product->image)}}" alt="{{$detail_product->name_vi}}" title="{{$detail_product->name_vi}}"></div></div></div></div><div class="group-card-text field-group-div card-text"><div class="field field-name-field-h1-title field-type-text field-label-hidden"><div class="field-items"><div class="field-item even" property=""><h3><a href="{{route('detail_product',$detail_product->slug_vi)}}">{{$detail_product->name_vi}}</a></h3>
</div></div></div><div class="field field-name-field-voc-statement field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even" property="">

  <p>{!!$detail_product->title_vi!!}</p>
</div></div></div>

</div></div></div><div class="field-group-htabs-wrapper"><h2 class="element-invisible">tabs</h2><div class="horizontal-tabs clearfix"><ul class="horizontal-tabs-list"><li class="horizontal-tab-button horizontal-tab-button-0 first selected" tabindex="-1"><a href="#undefined"><strong>Thông số kỹ thuật</strong><span id="active-horizontal-tab" class="element-invisible">(active tab)</span></a></li>




</ul><div class="horizontal-tabs-panes horizontal-tabs-processed"><fieldset class="form-wrapper horizontal-tabs-pane"><legend><span class="fieldset-legend">Thông số kỹ thuật</span></legend><div class="fieldset-wrapper"><div class="field field-name-field-datasheet-overview field-type-text-long field-label-hidden"><div class="field-items"><div class="field-item even" property=""><p>{!!$detail_product->thongtin_vi!!}</p>
</div></div></div></div></fieldset>



</div></div></div></div>
</div>
</div>
</div>
</section>
</section>
</div>
</main>
    @endsection

@section('script')

@endsection