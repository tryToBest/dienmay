@extends('frontend.partials.master')
@if($detail_product)
@section('title', $detail_product->name_vi)
@section('title_seo', $detail_product->title_seo_vi)
@section('meta_key', $detail_product->meta_key_vi)
@section('meta_des', $detail_product->meta_des_vi)
@section('css')
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}?{{time()}}" />
<link rel='stylesheet' id='woocommerce-general-css'  href='{{asset('css/devvn-woocommerce-reviews.css')}}?{{time()}}' media='all' />
<link rel="stylesheet" href="{{asset('frontend/slick/slick/slick.css')}}">
<link rel="stylesheet" href="{{asset('frontend/slick/slick/slick-theme.css')}}">
    <style>
        .lightSlider.lsGrab > *{
            text-align: center;
            background: #fff;
        }
        .lSSlideOuter .lightSlider, .lSSlideOuter .lSPager {
            max-height: 400px;
        }
        .lSSlideOuter .lightSlider li{max-height:400px}
        .lSSlideOuter .lightSlider li img{max-height:380px;}
        .bx-product-detail .info-product .images-control img{width:auto}
        .lSSlideOuter .lSPager.lSGallery li {
            max-height: 100px;background:#fff;text-align:center;
            width: 142px!important;
        }
        .lSSlideOuter .lSPager.lSGallery li img{
            max-height: 95px;margin:0 auto;

        }
        .bx-news-wg .list-item .item .content .info h3 a{
            text-decoration: none;
            color: #337ab7;
        }
        .bx-news-wg .list-item .item .content .info h3 a:hover{
            color: #ed1c24;
        }
        .sanphamlienquan img{max-height: 166px;}
        @media(max-width:767px){.lSSlideOuter .lSPager.lSGallery li {
            width: 70px!important;
        }}
    </style>       
@stop
@section('body')
class="single single-product woocommerce woocommerce-page"
@stop
@section('content')
    @php
        $tongdanhgia = DB::table('comments')->where('status',1)->where('product_id',$detail_product->id)->count();
        $demso5sao = DB::table('comments')->where('status',1)->where('product_id',$detail_product->id)->where('rating',5)->count();
        $demso4sao = DB::table('comments')->where('status',1)->where('product_id',$detail_product->id)->where('rating',4)->count();
        $demso3sao = DB::table('comments')->where('status',1)->where('product_id',$detail_product->id)->where('rating',3)->count();
        $demso2sao = DB::table('comments')->where('status',1)->where('product_id',$detail_product->id)->where('rating',2)->count();
        $demso1sao = DB::table('comments')->where('status',1)->where('product_id',$detail_product->id)->where('rating',1)->count();
    @endphp

  
<style type="text/css">
    .gio_hang{display: block;float: right;padding: 15px 0px;color: #f00;}
</style>
<section class="bx-breadcrum">
    <div class="container">
        
        <ul>
            <li><a href="{{route('index')}}" title="Trang chủ">Trang chủ</a></li>
            <li><span><i class="fas fa-angle-right"></i></span></li>
            <li><a href="{{route('productByCate',$cate_product->slug_vi)}}" title="{{$cate_product->name_vi}}">{{$cate_product->name_vi}}</a></li>
            <li><span><i class="fas fa-angle-right"></i></span></li>
            <li><span>{{$detail_product->name_vi}}</span></li>
        </ul>
    </div>
</section>
<section class="bx-main">
    <div class="container">
        <div class="notification" id="notification">Sản phẩm đã được thêm vào giỏ hàng</div>
        <div class="bx-product-detail">
            <div class="info-product">
                <div class="images-control">
                    <div class="list-images">
                        <ul id="images-product">

@if(count($img)>0)

@foreach($img as $i)
<li data-thumb="{{asset($i->name)}}"><img src="{{asset($i->name)}}" alt="{{$detail_product->name_vi}}" class="bk-product-image"/></li>
@endforeach
@else
<li data-thumb="{{asset($detail_product->image)}}"><img src="{{asset($detail_product->image)}}" alt="{{$detail_product->name_vi}}" class="bk-product-image"/></li>
@endif



</ul>
                        <script>
                            $('#images-product').lightSlider({
                                gallery: true,
                                item: 1,
                                vertical: false,
                                verticalHeight: 300,
                                vThumbWidth: 80,
                                thumbItem: 4,
                                thumbMargin: 10,
                                slideMargin: 0
                            });
                        </script>
                    <div class="description-product">
                        <h3>Đặc điểm nổi bật</h3>
                        <div class="content">
                         {!!$detail_product->title_vi!!}
                            <a href="javascript:;" onClick="readmoreDacDiem(this)" class="readmore">Xem thêm điểm nổi bật <i class="fas fa-caret-down"></i></a>
                        </div>
                    </div>
                    </div>
                    <div class="info">
                        <div class="title-product">
                            <h1 class="bk-product-name"><strong>{{$detail_product->name_vi}}</strong></h1>
                            <div class="rate">
                                <span class="star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="far fa-star"></i>
                                </span>
                                <span class="total-rate">{{$tongdanhgia}} đánh giá</span>
                            </div>
                        </div>
                        <div class="price-location">
                            <p>Giá tại <span>Gia Hân:</span></p>
                            <p style="text-align: center;">
                                <strong>
                                   {{ $detail_product->price_unit != 0 ? number_format($detail_product->price_unit).'đ' : number_format($detail_product->price).'đ'}}
                                </strong>
                            </p>
                        </div>
                        @if($detail_product->price_unit != null && $detail_product->price_unit != 0)
                        <div class="sale">
                            <h4>Mua online giảm sốc @if($detail_product->price_unit > $detail_product->price) <i>-{{ number_format((($detail_product->price_unit-$detail_product->price)/$detail_product->price_unit)*100,0)}}%</i> @endif còn</h4>
                            <div class="price-sale bk-product-price">{{$detail_product->price != 0 ? number_format($detail_product->price).'đ' : 'Liên hệ'}}</div>
                        </div>
                        @endif
                        @if($detail_product->chucnang != null)
                        <div class="khuyen-mai">
                            <div class="khuyenmai-title">
                                <h3>Thông tin khuyến mại</h3>
                            </div>
                            <div class="khuyenmai-content">
                                 {!!$detail_product->chucnang!!}
                            </div>
                        </div>
                        @endif
                        <div class="status-product">
                            <div class="status bk-check-out-of-stock">Còn hàng</div>
                            <div class="select-location">
                                <p><strong><i class="fas fa-map-marker-alt"></i> Địa chỉ: </strong> {{$setting->address_vi}}</p>
                                @if($setting->address2nd != null)
                                <p><strong><i class="fas fa-map-marker-alt"></i> Địa chỉ: </strong> {{$setting->address2nd}}</p>
                                @endif
                            </div>
                        </div>
                        <div class="control">
                            <form action="{{route('muangay',$detail_product->id)}}" method="GET">
                                @csrf 
                     
                                <input type="hidden" name="quantity" value="1" class="bk-product-qty"/>
                                <div class="buy-now hidden-buy-button">
                                    <button>
                                        <strong>Mua ngay</strong>
                                        <span>Giao tận nới hoặc tới siêu thị mua hàng</span>
                                    </button>
                                </div>
                            </form>
                            <button href="#" class="addToCart" data-id="{{$detail_product->id}}" id="addToCart">
                                Thêm vào giỏ hàng <i class="fa fa-plus"></i>
                            </button>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="cam-ket">
         
                <div class="list-cam-ket">
                    <h4>Yên tâm khi mua hàng tại Gia Hân</h4>
                    <ul>
                        <li>
                            <div class="icon">
                                <img src="{{asset('images/icon-van-chuyen-free.png')}}" alt=""/>
                            </div>
                            <div class="ctn">Vận chuyển miễn phí Hà Nội - Thái Nguyên</div>
                        </li>
                        <li>
                            <div class="icon">
                                <img src="{{asset('images/icon-doi-tra-2.png')}}" alt=""/>
                            </div>
                            <div class="ctn">1 đổi 1 trong 7 ngày (Lỗi do nhà sản xuất)</div>
                        </li>
                        <li>
                            <div class="icon">
                                <img src="{{asset('images/icon-doi-tra.png')}}" alt=""/>
                            </div>
                            <div class="ctn">Đổi trả và bảo hành cực dễ chỉ cần số điện thoại</div>
                        </li>
                        <li>
                            <div class="icon">
                                <img src="{{asset('images/icon-bao-hanh.png')}}" alt=""/>
                            </div>
                            <div class="ctn">Bảo hành chính hãng</div>
                        </li>
                        <li>
                            <div class="icon">
                                <img src="{{asset('images/icon-tich-xanh-1.png')}}" alt=""/>
                            </div>
                            <div class="ctn">Giao hàng miễn phí trong 4 tiếng (nếu đặt hàng sau 17h Điện máy Gia Hân sẽ giao hàng vào hôm sau)</div>
                        </li>
                        <li>
                            <div class="icon">
                                <img src="{{asset('images/icon-tich-xanh-1.png')}}" alt=""/>
                            </div>
                            <div class="ctn">Chính sách bảo hành tại nhà</div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="content-product">
                <div class="bx-left">
                    <div class="content">
                     {!!$detail_product->description_vi!!}
                    </div>
                    <div class="readmore">
                        <a href="javascript:;" onClick="readmoreContent(this)" title="">Đọc thêm</a>
                    </div>


<div style="clear: both;"></div>
<div id="reviews" class="style-v2 woocommerce-Reviews no_avatar devvn-style2">
  <div id="comments">
            <div class="woocommerce-Reviews-title">
      {{$tongdanhgia}} đánh giá cho <span>{{$detail_product->name_vi}}</span>    </div>

        <div class="star_box">
            <div class="star-average">
                                    <div class="woocommerce-product-rating">
                                                <span class="star_average">
@if($tongdanhgia >0)
 {{number_format(($demso5sao*5 + $demso4sao*4 + $demso3sao*3 + $demso2sao*2 + $demso1sao*1)/$tongdanhgia,2)}}
@else
0
@endif
                                                  <i class="devvn-star"></i></span>
                        <strong>Đánh giá trung bình</strong>
                                                                    </div>
                                </div>
            <div class="star_box_left">
                <div class="reviews_bar">
                                        <div class="devvn_review_row">
                        <span class="devvn_stars_value">5<i class="devvn-star"></i></span>
                        <span class="devvn_rating_bar">
                            <span style="background-color: #eee" class="devvn_scala_rating">
                                <span class="devvn_perc_rating" style="width: @if($tongdanhgia >0) {{number_format(($demso5sao/$tongdanhgia)*100),2}}% @else 0% @endif; background-color: #f5a623"></span>
                            </span>
                        </span>
                        <span class="devvn_num_reviews"><b>@if($tongdanhgia >0) {{number_format(($demso5sao/$tongdanhgia)*100),2}}% @else 0% @endif</b> | {{$demso5sao}} đánh giá</span>
                    </div>
                                        <div class="devvn_review_row">
                        <span class="devvn_stars_value">4<i class="devvn-star"></i></span>
                        <span class="devvn_rating_bar">
                            <span style="background-color: #eee" class="devvn_scala_rating">
                                <span class="devvn_perc_rating" style="width: @if($tongdanhgia >0) {{number_format(($demso4sao/$tongdanhgia)*100),2}}% @else 0% @endif; background-color: #f5a623"></span>
                            </span>
                        </span>
                        <span class="devvn_num_reviews"><b>@if($tongdanhgia >0) {{number_format(($demso4sao/$tongdanhgia)*100),2}}% @else 0% @endif</b> |{{$demso4sao}} đánh giá</span>
                    </div>
                                        <div class="devvn_review_row">
                        <span class="devvn_stars_value">3<i class="devvn-star"></i></span>
                        <span class="devvn_rating_bar">
                            <span style="background-color: #eee" class="devvn_scala_rating">
                                <span class="devvn_perc_rating" style="width: @if($tongdanhgia >0) {{number_format(($demso3sao/$tongdanhgia)*100),2}}% @else 0% @endif; background-color: #f5a623"></span>
                            </span>
                        </span>
                        <span class="devvn_num_reviews"><b>@if($tongdanhgia >0) {{number_format(($demso3sao/$tongdanhgia)*100),2}}% @else 0% @endif</b> | {{$demso3sao}} đánh giá</span>
                    </div>
                                        <div class="devvn_review_row">
                        <span class="devvn_stars_value">2<i class="devvn-star"></i></span>
                        <span class="devvn_rating_bar">
                            <span style="background-color: #eee" class="devvn_scala_rating">
                                <span class="devvn_perc_rating" style="width: @if($tongdanhgia >0) {{number_format(($demso2sao/$tongdanhgia)*100),2}}% @else 0% @endif; background-color: #f5a623"></span>
                            </span>
                        </span>
                        <span class="devvn_num_reviews"><b>@if($tongdanhgia >0) {{number_format(($demso2sao/$tongdanhgia)*100),2}}% @else 0% @endif</b> | {{$demso2sao}} đánh giá</span>
                    </div>
                                        <div class="devvn_review_row">
                        <span class="devvn_stars_value">1<i class="devvn-star"></i></span>
                        <span class="devvn_rating_bar">
                            <span style="background-color: #eee" class="devvn_scala_rating">
                                <span class="devvn_perc_rating" style="width: @if($tongdanhgia >0) {{number_format(($demso1sao/$tongdanhgia)*100),2}}% @else 0% @endif; background-color: #f5a623"></span>
                            </span>
                        </span>
                        <span class="devvn_num_reviews"><b>@if($tongdanhgia >0) {{number_format(($demso1sao/$tongdanhgia)*100),2}}% @else 0% @endif</b> | {{$demso1sao}} đánh giá</span>
                    </div>
                                    </div>
            </div>
                        <div class="star_box_right">
                <a href="#" data-toggle="modal" data-target="#smallModal" title="Đánh giá ngay" class="btn-reviews-now" data-wpel-link="internal">Đánh giá ngay</a>
            </div>
                    </div>







<div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel" style="    padding: 0 20px;font-weight: bold;">Đánh giá {{$detail_product->name_vi}}</h4>
      </div>
      <div class="modal-body">


       <div id="review_form_wrapper" class="mfp-hide">
                <div id="review_form">
                      <div id="respond" class="comment-respond">
  <form action="{{route('commentsp')}}" method="post" id="commentform" class="comment-form"  enctype="multipart/form-data" onsubmit="return Validate_fom(this);">         

      @csrf  
       <input type="hidden" class="form-control" name="parent_id"
                               value="0">  
  <input type="hidden" class="form-control" name="product_id"
                               value="{{$detail_product->id}}">
                        <input type="hidden" class="form-control" name="title"
                               value="{{$detail_product->name_vi}}">
                        <input type="hidden" class="form-control" name="set" value="0">
                        <input type="hidden" class="form-control" name="slug_id"
                               value="{{$detail_product->slug_vi}}">


         <div class="quick_reviews_tag">
                                            <label>
                            <input type="checkbox" name="quick_tag[]" value="Chất lượng sản phẩm tuyệt vời">
                            <span>Chất lượng sản phẩm tuyệt vời</span>
                        </label>
                                            <label>
                            <input type="checkbox" name="quick_tag[]" value="Đóng gói sản phẩm rất đẹp và chắc chắn">
                            <span>Đóng gói sản phẩm rất đẹp và chắc chắn</span>
                        </label>
                                            <label>
                            <input type="checkbox" name="quick_tag[]" value="Hàng 100% chính hãng">
                            <span>Hàng 100% chính hãng</span>
                        </label>
                                            <label>
                            <input type="checkbox" name="quick_tag[]" value="Shop phục vụ tốt">
                            <span>Shop phục vụ tốt</span>
                        </label>
                                            <label>
                            <input type="checkbox" name="quick_tag[]" value="Thời gian giao hàng rất nhanh">
                            <span>Thời gian giao hàng rất nhanh</span>
                        </label>
                                    </div>
                <div class="comment-form-comment"><textarea id="comment" name="content" cols="45" rows="8" minlength="1" required="" placeholder="Mời bạn chia sẻ thêm một số cảm nhận..." aria-required="true"></textarea></div>

                <div class="wrap-attaddsend">
                  <div class="review-attach"><span class="btn-attach devvn_insert_attach">Gửi ảnh thực tế</span>
                    <input name="userfile" type="file" id="userfile" size="50" style="display: block;">
                </div>
                {{-- <span id="countContent">(Tối thiểu 10)</span> --}}
            </div>

                <div class="list_attach"><ul class="devvn_attach_view"></ul><span class="devvn_insert_attach"><i class="devvn-plus">+</i></span></div>

                <div class="comment-form-rating"><label for="rating">Bạn cảm thấy thế nào về sản phẩm? (Chọn sao)</label>



                  <select name="rating" id="rating" required="" aria-required="true" style="display: none;">
            <option value="">Xếp hạng…</option>
            <option value="5">Rất tốt</option>
            <option value="4">Tốt</option>
            <option value="3">Trung bình</option>
            <option value="2">Không tệ</option>
            <option value="1">Rất tệ</option>
          </select></div>

          <div class="form_row_reviews"><p class="comment-form-author"><input id="author" name="name" type="text" value="" size="30" required="" placeholder="Họ tên (bắt buộc)" aria-required="true"></p>
<p class="comment-form-phone"><input id="phone" name="phone" type="text" size="30" required="" placeholder="Số điện thoại (bắt buộc)" aria-required="true" pattern="(\+84|0){1}(9|8|7|5|3){1}[0-9]{8}" autocomplete="off"></p>
<p class="comment-form-email"><input id="email" name="email" type="email" value="" size="30" placeholder="Email "></p></div>

<p class="form-submit"><input name="submit" type="submit" id="submit" class="submit" value="Gửi đánh giá"> 
</p></form> </div><!-- #respond -->
                  </div>
               
            </div> 


</div>
</div>
</div>
</div>





          @if(Session::has('binhluan'))
                    <div class="alert alert-success">{{ Session::get('binhluan') }}</div>
                @endif
                            
          <ol class="commentlist">
             @if (count($comment_detail) > 0)

               @foreach ($comment_detail as $row)
        <li class="review even thread-even depth-1" id="li-comment-{{$row->id}}">
  <div id="comment-{{$row->id}}" class="comment_container devvn_review_box">
            <div class="comment-text">
            <div class="devvn_review_top">
                
  <p class="meta">
    <strong class="woocommerce-review__author">{{$row->name}}</strong>
    <em class="woocommerce-review__verified verified">Đã mua tại {{$setting->website}}</em>  </p>

            </div>
    <div class="devvn_review_mid">
        <div class="star-rating" role="img" aria-label="Được xếp hạng {{$row->rating}} {{$row->rating}} sao">
            <span @if($row->rating >0) style="width:{{$row->rating*20}}%" @else style="width:100%" @endif >Được xếp hạng <strong class="rating">{{$row->rating}}</strong> {{$row->rating}} sao</span>
        </div>
        <div class="description"><p>{{$row->content}}</p></div>
    </div>



    @if($row->images != null)
    <div class="devvn_review_bottom">
            <div class="image_review"><img src="{{asset($row->images)}}" alt="{{$detail_product->name_vi}}"></div>
                {{-- <div class="reply"><a rel="nofollow" class="comment-reply-link traloicomement"  data-id="{{$row->id}}" data-postid="10376" data-belowelement="comment-{{$row->id}}" data-respondelement="respond" data-replyto="Phản hồi đến {{$row->name}}" aria-label="Phản hồi đến {{$row->name}}">Trả lời</a></div>                            <span> • </span>
            <a href="javascript:void(0)" class="cmtlike" data-like="" data-id="{{$row->id}}" title="" data-wpel-link="internal"><span class="cmt_count"></span> thích</a> --}}
    </div> 
    @endif




    </div>
  </div>







</li><!-- #comment-## -->
     @endforeach
           

@endif
      </ol>



    </div>
    <div style="clear: both;"></div>
</div>

<div style="clear: both;"></div>





                    
             {{--        
                    
                    <div class="comment">
                                                <h3>0 bình luận</h3>
                        <div class="list-comment">
                                                        <div class="bx-frm-comment">
                                                            </div>
                        </div>
                    </div> --}}
                </div>
                <div class="bx-right">
                    <div class="thong-so-ky-thuat">
                        <h3>Thông số kỹ thuật</h3>
                        <div class="list-thong-so">
                                        {!!$detail_product->thongtin_vi!!}
                        </div>
                        <div class="read-all">
                            <button type="button" class="btn btn-warrning" data-toggle="modal" data-target="#exampleModalCenter">
                                Xem chi tiết thông số kỹ thuật
                            </button>
                        </div>
                    </div>
                    
                </div>
            </div>
            <style>

            </style>
            {{-- start sản phẩm liên quan --}}
            <div class="bx-news-wg">
                <h3>Sản phẩm liên quan</h3>
                <div class="list-item sanphamlienquan">

                    @foreach($splq as $lq)
                    <div class="item">
                        <div class="thumb">
                            <a href="{{route('detail_product',$lq->slug_vi)}}" title="{{$lq->name_vi}}">
                                <img src="{{asset($lq->image)}}" alt="{{$lq->name_vi}}"/>
                            </a>
                        </div>
                        <div class="content">
                            <div class="info">
                                <h3><a href="{{route('detail_product',$lq->slug_vi)}}" title="{{$lq->name_vi}}">{{$lq->name_vi}}</a></h3>
                                <div class="price" style="color: red;"> @if($lq->price != 0) {{number_format($lq->price,0)}}đ @else Liên hệ @endif</div>

                                @if($lq->price_unit != null && $lq->price_unit != 0)
                                <div class="sale" >
                                    <span style="text-decoration:line-through;">{{number_format($lq->price_unit,0)}}₫</span>
                                    <i>(-{{ number_format((($lq->price_unit-$lq->price)/$lq->price_unit)*100,0)}}%)</i>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            {{-- end sản phẩm liên quan --}}
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Xem chi tiết thông số kỹ thuật</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height: 300px; overflow: auto;">
                {!!$detail_product->thongtin_vi!!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>

{{-- <div class="fullparametertran"></div>
<div class="fullparameter">
    <div class="close-attr" onclick="showHideAttr()"><i class="fas fa-times"></i></div>
    <div class="scroll">
                     {!!$detail_product->thongtin_vi!!}

    </div>
</div> --}}

    @endsection
    @section('script')
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}" ></script>
    <script>
    	$(function () {
    $('.quick_reviews_tag label input').click(function () {
var value_click = $(this).val();

         $("#comment").val(value_click);

    });
});


        $(document).ready(function() {
            $("#addToCart").click(function() {
                var id = $(this).data("id");
                var csrfToken = "{{ csrf_token() }}"; // Lấy token CSRF từ Laravel

                $.ajax({
                    url: "{{ route('add-cart3') }}",
                    type: 'POST',
                    dataType:'JSON',
                    data: { id: id },
                    headers: {
                        'X-CSRF-TOKEN': csrfToken // Đặt token CSRF trong header của yêu cầu
                    },
                    success: function(data) {
                        var notification = $("#notification");
                        
                        // Thêm class "show" để hiển thị thông báo
                        notification.addClass("active");
                        
                        // Ẩn thông báo sau 2 giây
                        setTimeout(function() {
                            notification.removeClass("active"); // Loại bỏ class "show" để ẩn thông báo
                        }, 2000);
                    },
                    error: function(error) {
                        // Xử lý lỗi nếu có
                    }
                });
            });
        });

    </script>         
       <script type='text/javascript'>
/* <![CDATA[ */
var wc_single_product_params = {"i18n_required_rating_text":"Vui l\u00f2ng ch\u1ecdn m\u1ed9t \u0111\u00e1nh gi\u00e1","review_rating_required":"yes","flexslider":{"rtl":false,"animation":"slide","smoothHeight":true,"directionNav":false,"controlNav":"thumbnails","slideshow":false,"animationSpeed":500,"animationLoop":false,"allowOneSlide":false},"zoom_enabled":"1","zoom_options":[],"photoswipe_enabled":"1","photoswipe_options":{"shareEl":false,"closeOnScroll":false,"history":false,"hideAnimationDuration":0,"showAnimationDuration":0},"flexslider_enabled":"1"};
/* ]]> */
</script>

<script type='text/javascript' src='{{asset('frontend/js/single-product.min.js')}}'></script>      
<script type='text/javascript' src='{{asset('frontend/js/jquery-migrate-1.2.1.min.js')}}'></script>      
<script type='text/javascript' src='{{asset('frontend/slick/slick/slick.min.js')}}'></script>
<script>
    $(document).ready(function(){
        $('.list-item').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
            speed: 300,
            infinite: true,
            autoplaySpeed: 5000,
            autoplay: true,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 767,
                        settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });
    });
</script>              
    @endsection
@endif