@extends('frontend.partials.master')
@section('title', 'Search')

@section('canonical')
{{ URL::current() }}
@stop
@section('body')
class="amm-page  "
@stop
@section('css')
<style>
    .collection .category-products{
        margin:0;
    }
</style>
@stop
@section('content')
<section class="bread-crumb  ">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">                  
                    <li class="home">
                        <a itemprop="url" href="{{route('index')}}" ><span itemprop="title">Trang chủ</span></a>                     
                        <span><i class="fa">/</i></span>
                    </li>
                    <li><strong ><span itemprop="title"> Tìm kiếm</span></strong></li>
                </ul>
            </div>
        </div>
    </div>
</section>


<div class="container">
    <div class="row">   
        <div class="main-category-page col-md-12 col-sm-12 col-xs-12 no-padding">
            <section class="section-main-products padding-small main_container collection margin-bottom-30 col-md-9 col-lg-9 pull-right col-sm-12 col-xs-12">
                <div class="text-sm-left">          
                    <div class="tt">
                        <h1 class="title-head margin-top-0">Tìm kiếm "{{$input}}" ({{$countProduct}})</h1>
                    </div>
                </div>      
                <div class="category-products products">
                    

                    <section class="products-view products-view-grid collection_reponsive">
    <div class="row-small row">


              @foreach($product as $p)
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 padding-small">
            <div class="product-col">
<div class="product-box">                                                           
    <div class="product-thumbnail">
        <a class="image_link display_flex" href="{{route('detail_product',$p->slug_vi)}}" title="{{$p->name_vi}}">
            <img src="{{asset('frontend/images/product-1.jpg')}}"  data-lazyload="{{asset($p->image)}}" alt="{{$p->name_vi}}">
        </a>
       {{--  <div class="product-action-grid clearfix">
            <form action="{{route('detail_product',$p->slug_vi)}}" method="post" class="variants form-nut-grid" enctype="multipart/form-data">
                <div>
                
                    
                    <button class="button_wh_40 btn-cart left-to" title="Đặt mua">
                        Mua hàng            
                    </button>
                    <a title="Xem chi tiết" href="{{route('detail_product',$p->slug_vi)}}" class="hidden-md hidden-sm hidden-xs button_wh_40 btn_view right-to quick-view">
                        <i class="fa fa-eye"></i>
                        <span class="style-tooltip">Xem chi tiết</span>
                    </a>
                </div>
            </form>
        </div> --}}
    </div>
    <div class="product-info effect a-left">
        <div class="info_hhh">
            <h3 class="product-name product-name-hover"><a href="{{route('detail_product',$p->slug_vi)}}" title="{{$p->name_vi}}">{{$p->name_vi}}</a></h3>
       
            <div class="price-box clearfix">
                <span class="price product-price">{{number_format($p->price)}}₫</span>
                   <span class="icon_plus"><a href="{{route('detail_product',$p->slug_vi)}}"><i class="fa fa-plus" aria-hidden="true"></i></a></span>
            </div>
        </div>
    </div>
</div>          
            </div>
        </div>
    
        @endforeach
        
        
        
    </div>
    <div class="text-right">
        <nav class="clearfix">
  <ul class="pagination clearfix">
     {{$product->links()}}
  </ul>
</nav>
    </div>
</section>      
                </div>
            </section>
            <div class="section-main-sidebar padding-small col-lg-3 col-md-3 margin-bottom-50 pull-left">
                <aside class="dqdt-sidebar sidebar left-content">
<aside class="aside-item sidebar-category collection-category">
    <div class="aside-title">
        <h2 class="title-head margin-top-0"><span>Danh mục sản phẩm</span></h2>
    </div>
    <div class="aside-content">
        <nav class="nav-category navbar-toggleable-md">
            <ul class="nav navbar-pills">
                @php
    $cate_product_cha =DB::table('cate_products')->where('status',1)->where('parent_id',0)->orderBy('position','ASC')->get();
    @endphp
@foreach($cate_product_cha as $key => $cate_c) 
  @php
    $cate_product_cap2 =DB::table('cate_products')->where('status',1)->where('parent_id',$cate_c->id)->orderBy('position','ASC')->get();
    @endphp
                <li class="nav-item lv1">
                    <a href="{{route('productByCate',$cate_c->slug_vi)}}" class="nav-link">{{$cate_c->name_vi}} </a>
                    @if(count($cate_product_cap2)>0)
                    <i class="fa fa-angle-down"></i>

                    <ul class="dropdown-menu dropdown-menulv2">
                        @foreach($cate_product_cap2 as $key2 => $cate_c2) 
                        <li class="nav-item lv2">
                            <a href="{{route('productByCate',$cate_c2->slug_vi)}}" class="nav-link">{{$cate_c2->name_vi}} </a>
                        </li>
                       @endforeach
               
                    </ul>
                          @endif
                </li>
        @endforeach
            </ul>
        </nav>
    </div>
</aside>

  
@include('frontend/partials/quangcao')
                </aside>
            </div>
        </div>
    
    </div>
</div>
@endsection