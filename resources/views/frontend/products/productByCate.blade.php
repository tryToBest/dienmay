@extends('frontend.partials.master')
@section('title', $cate_product->name_vi)
@section('meta_key', $cate_product->meta_key_vi)
@section('meta_des', $cate_product->meta_des_vi)

@section('body')

@stop
@section('css')
<style>
    .anhquangcao{
        margin-bottom: 15px;
    }
    .col_left{
        margin-top: -20px;
    }
    .bannerquangcao1 {
    margin-top: 25px!important;

}
.nutgiohangsp {
    font-size: 14px;
}
.show_allsanpham .col-sp:nth-child(4n+1){
    clear: both;
}
.title_danhmucsp_home .col-xs-12 {
    margin-bottom: 25px;
        margin-top: 20px;
}
.cotphai_productbycate{
    margin-top: 25px;
}
.bx-product .list-product .item .info {
    font-size: 14px!important;
    font-weight: normal!important;
}
.bx-product .list-product .item .info h3 {
    font-size: 14px!important;
    font-weight: normal!important;margin: 10px 0;
}
@media(max-width: 768px){
    .bx-product .list-product .item .info h3 {
     height: auto!important; 
     overflow: inherit!important; 
}
}
</style>
@stop
@section('content')
{{-- @include('frontend/partials/quangcaohome') --}}
{{-- @include('frontend/partials/slide2') --}}

<section class="bx-breadcrum">
    <div class="container">
        <ul>
            <li><a href="{{route('index')}}" title="Trang chủ">Trang chủ</a></li>
            <li><span><i class="fas fa-angle-right"></i></span></li>
            <li><span>{{$cate_product->name_vi}} </span></li>
        </ul>
    </div>
</section>
<br>
{{-- <section class="bx-slider-cate">
    <div class="container">
        <div class="list-slider">
            <ul id="list-slider-cate">

   <li><a href="#" title=""><img src="https://dienmaytinphat.com/wp-content/uploads/2019/06/29_05_2019_15_08_54_SAMSUNG-TV-640-280.png" alt=""/></a></li>
<li><a href="#" title=""><img src="https://dienmaytinphat.com/wp-content/uploads/2019/06/04_06_2019_10_13_13_LG-TV-640-280.png" alt=""/></a></li>
<li><a href="#" title=""><img src="https://dienmaytinphat.com/wp-content/uploads/2019/06/30_05_2019_10_04_49_TV-Sony-640-280.png" alt=""/></a></li>
<li><a href="#" title=""><img src="https://dienmaytinphat.com/wp-content/uploads/2019/06/30_05_2019_16_02_26_TCL-TV-640-280.png" alt=""/></a></li>

                            </ul>
             <script>
                slider[99909] = $('#list-slider-cate').lightSlider({
                    item: 3,
                    loop: true,
                    slideMove: 1,
                    easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                    speed: 1000,
                    slideMargin: 10,
                    mode: 'slide',
                    auto: true,
                    pause: 5000,
                    controls: false,
                    pager: false,
                    responsive: [
                        {
                            breakpoint: 800,
                            settings: {
                                item: 2,
                                slideMove: 1,
                                slideMargin: 6,
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                item: 2,
                                slideMove: 1
                            }
                        }
                    ]
                });
            </script>
            <div class="control">
                <span onClick="prevSlide(99909)" class="btn-prev"><i class="fas fa-chevron-left"></i></span>
                <span onClick="nextSlide(99909)" class="btn-next"><i class="fas fa-chevron-right"></i></span>
            </div>
        </div>

    </div>
</section> --}}
        <form action="" method="GET">
<section class="bx-main">
    <div class="container">
        <div class="bx-left">
            <div class="bx-feature-product">
                <h2>{{$cate_product->name_vi}} giá sốc</h2>
                <div class="bx-product product-cate">
                    <div class="list-product">
                        <ul id="list-product-feature" style="display: flex">
                            @foreach($products as $product)
                            @php
                                $tongdanhgia = DB::table('comments')->where('status',1)->where('product_id',$product->id)->count();
                                $demso5sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',5)->count();
                                $demso4sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',4)->count();
                                $demso3sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',3)->count();
                                $demso2sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',2)->count();
                                $demso1sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',1)->count();
                                if($tongdanhgia >0){
                                    $sosao = ($demso5sao*5 + $demso4sao*4 + $demso3sao*3 + $demso2sao*2 + $demso1sao*1)/$tongdanhgia;
                                }
                                else{
                                    $sosao = 0;
                                }
                            @endphp
                            <li style="height: 100%;background: #fff;">
                                <div class="item">
                                    <div class="thumb">
                                        <a href="{{route('detail_product',$product->slug_vi)}}" title="{{$product->name_vi}}">
                                            <img src="{{asset($product->image)}}" alt="{{$product->name_vi}}"/>
                                        </a>
                                    </div>
                                    <div class="info">
                            <h3><a href="{{route('detail_product',$product->slug_vi)}}" title="{{$product->name_vi}}">{{$product->name_vi}}</a></h3>
                           <div class="price"> @if($product->price != 0) {{number_format($product->price,0)}}đ @else Liên hệ @endif</div>

                                          @if($product->price_unit != null && $product->price_unit > $product->price)
                                        <div class="sale">
                                            <span>{{number_format($product->price_unit,0)}}₫</span>
                                            <i>(-{{ number_format((($product->price_unit- $product->price)/$product->price_unit)*100,0)}}%)</i>
                                        </div>
                                        @endif
                        </div>
                                    <div class="rate">
                                        <span class="point">{{$sosao}}/5 <i class="fas fa-star"></i></span>
                                        <span class="total-rate">{{$tongdanhgia}} đánh giá</span>
                                    </div>
                                </div>
                            </li>
                            @endforeach     
                        </ul>
                        <div class="control">
                            <span onClick="prevSlide(99902)" class="btn-prev"><i class="fas fa-chevron-left"></i></span>
                            <span onClick="nextSlide(99902)" class="btn-next"><i class="fas fa-chevron-right"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bx-product product-cate">
                <h2>Tất cả {{$countProduct}} {{$cate_product->name_vi}}</h2>
                <div class="sort-product-cate">
            
                        <span>Xem theo</span>
                             <label><input  @if(isset($_GET['order']) && $_GET['order'] == 'ASC') checked @endif onchange='this.form.submit();' name="order" value="ASC" type="radio"/> Giá thấp đến cao</label>
                        <label><input @if(isset($_GET['order']) && $_GET['order'] == 'DESC') checked @endif onchange='this.form.submit();' name="order" value="DESC" type="radio"/> Giá cao đến thấp</label>
            
     
                </div>
                <div class="list-product">
                    <div class="row">                     
               @foreach($products as $product)
                @php
                    $tongdanhgia = DB::table('comments')->where('status',1)->where('product_id',$product->id)->count();
                    $demso5sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',5)->count();
                    $demso4sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',4)->count();
                    $demso3sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',3)->count();
                    $demso2sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',2)->count();
                    $demso1sao = DB::table('comments')->where('status',1)->where('product_id',$product->id)->where('rating',1)->count();
                    if($tongdanhgia >0){
                        $sosao = ($demso5sao*5 + $demso4sao*4 + $demso3sao*3 + $demso2sao*2 + $demso1sao*1)/$tongdanhgia;
                    }
                    else{
                        $sosao = 0;
                    }
                @endphp
                       <div class="item">
                        <div class="thumb">
                            <a href="{{route('detail_product',$product->slug_vi)}}" title="{{$product->name_vi}}">
                                <img src="{{asset($product->image)}}" alt="{{$product->name_vi}}"/>
                            </a>
                        </div>
                        <div class="info">
                            <h3><a href="{{route('detail_product',$product->slug_vi)}}" title="{{$product->name_vi}}">{{$product->name_vi}}</a></h3>
                           <div class="price"> @if($product->price != 0) {{number_format($product->price,0)}}đ @else Liên hệ @endif</div>

                                        @if($product->price_unit != null && $product->price_unit > $product->price)
                                        <div class="sale">
                                            <span>{{number_format($product->price_unit,0)}}₫</span>
                                            <i>(-{{ number_format((($product->price_unit-$product->price)/$product->price_unit)*100,0)}}%)</i>
                                        </div>
                                        @endif
                        </div>
                       <div class="rate">
                                        <span class="point">{{$sosao}}/5 <i class="fas fa-star"></i></span>
                                        <span class="total-rate">{{$tongdanhgia}} đánh giá</span>
                                    </div>
                    </div>

@endforeach
        
    </div>                
</div>
                
                                <div class="page">
                                    <ul class="pagination pagination-sm">{{$products->links()}}</ul>     
                                </div>
                                </div>
        </div>


         <div class = "bx-right">
            <div class = "search-product">
@php
    $mang_loc_faq111 = explode('|',$cate_product->id_loc);   
    $mang_loc_faq111 = array_filter($mang_loc_faq111);
@endphp


 @php
             $list_loc = DB::table('faqs')->whereIn('id',$mang_loc_faq111)->orderBy('position','ASC')->get();
                 @endphp
            @foreach($list_loc as $loc)


             
                                    <div class = "wg-search">
                            <h3>{{$loc->name_vi}}</h3>
                            <div class = "control-search">

			                            	 @php
			             $list_loc3 = DB::table('faqs')->where('status',1)->where('parent_id',$loc->id)->orderBy('position','ASC')->get();
			                 @endphp
			                 @foreach($list_loc3 as $loc3)

                                      <label class="in-50">
                                        <input name="filter-{{$loc->id}}" @if(isset($_GET['filter'.'-'.$loc->id]) && $_GET['filter'.'-'.$loc->id] == $loc3->id) checked @endif onchange='this.form.submit();' value="{{$loc3->id}}" type="checkbox" class="filter_check checkbox{{$loc->id}}" data-id="{{$loc->id}}"/>
                                        @if($loc3->image != '')
                                        <img src="{{asset($loc3->image)}}" alt="{{$loc3->name_vi}}"/>
                                        @else
                                        {{$loc3->name_vi}}
                                        @endif
                                    </label>
                              @endforeach
                                                                     
                                                                       
                                                                </div>
                        </div> 

@endforeach




                                            <div class = "wg-search">
                        <h3>Giá bán</h3>
                        <div class = "control-search">
<label class = " in-50"><input class="filter_check checkbox1a" data-id="1a"  name="pricef" @if(isset($_GET['pricef']) && $_GET['pricef'] == '0-5000000') checked @endif onchange='this.form.submit();' value="0-5000000" type = "checkbox" /> Dưới 5 triệu</label>
<label class = " in-50"><input  class="filter_check checkbox2a" data-id="2a" name="pricef" @if(isset($_GET['pricef']) && $_GET['pricef'] == '5000000-7000000') checked @endif onchange='this.form.submit();' value="5000000-7000000" type = "checkbox" /> Từ 5 - 7 triệu</label>
<label class = " in-50"><input  class="filter_check checkbox3a" data-id="3a" name="pricef" @if(isset($_GET['pricef']) && $_GET['pricef'] == '7000000-10000000') checked @endif onchange='this.form.submit();' value="7000000-10000000" type = "checkbox" /> Từ 7 - 10 triệu</label>
<label class = " in-50"><input  class="filter_check checkbox4a" data-id="4a" name="pricef" @if(isset($_GET['pricef']) && $_GET['pricef'] == '10000000-12000000') checked @endif onchange='this.form.submit();' value="10000000-12000000" type = "checkbox" /> Từ 10 - 12 triệu</label>
<label class = " in-50"><input  class="filter_check checkbox5a" data-id="5a" name="pricef" @if(isset($_GET['pricef']) && $_GET['pricef'] == '12000000-15000000') checked @endif onchange='this.form.submit();' value="12000000-15000000" type = "checkbox" /> Từ 12 - 15 triệu</label>
<label class = " in-50"><input  class="filter_check checkbox6a" data-id="6a" name="pricef" @if(isset($_GET['pricef']) && $_GET['pricef'] == '15000000-20000000') checked @endif onchange='this.form.submit();' value="15000000-20000000" type = "checkbox" /> Từ 15 - 20 triệu</label>
<label class = " in-50"><input  class="filter_check checkbox7a" data-id="7a" name="pricef" @if(isset($_GET['pricef']) && $_GET['pricef'] == '20000000-50000000') checked @endif onchange='this.form.submit();' value="20000000-50000000" type = "checkbox" /> Từ 20 - 50 triệu</label>
<label class = " in-50"><input  class="filter_check checkbox8a" data-id="8a" name="pricef" @if(isset($_GET['pricef']) && $_GET['pricef'] == '50000000-900000000') checked @endif onchange='this.form.submit();' value="50000000-900000000" type = "checkbox" /> Trên 50 triệu</label>
                                                        </div>
                    </div>{{-- 
                                            <div class = "wg-search">
                            <h3>Độ phân giải tivi</h3>
                            <div class = "control-search">
                                                                    <label><input name="term[pa_do-phan-giai][]" type = "checkbox" value="1525"/> 2K</label>
                                                                        <label><input name="term[pa_do-phan-giai][]" type = "checkbox" value="229"/> HD</label>
                                                                        <label><input name="term[pa_do-phan-giai][]" type = "checkbox" value="224"/> Full HD</label>
                                                                        <label><input name="term[pa_do-phan-giai][]" type = "checkbox" value="232"/> 4K</label>
                                                                        <label><input name="term[pa_do-phan-giai][]" type = "checkbox" value="273"/> 8K</label>
                                                                </div>
                        </div> --}}
                            {{--                     <div class = "wg-search">
                            <h3>Kích thước tivi</h3>
                            <div class = "control-search">
                                                                    <label><input name="term[pa_inch][]" type = "checkbox" value="287"/> 24 inch</label>
                                                                        <label><input name="term[pa_inch][]" type = "checkbox" value="1564"/> 25 inch</label>
                                                                        <label><input name="term[pa_inch][]" type = "checkbox" value="1560"/> 27 inch</label>
                                                                        <label><input name="term[pa_inch][]" type = "checkbox" value="221"/> 32 Inch</label>
                                                                        <label><input name="term[pa_inch][]" type = "checkbox" value="293"/> 40 inch</label>
                                                                        <label><input name="term[pa_inch][]" type = "checkbox" value="1648"/> 42 inch</label>
                                                                     
                                                                </div>
                        </div> --}}
                    {{--                             <div class = "wg-search">
                            <h3>Loại tivi</h3>
                            <div class = "control-search">
                                                                    <label><input name="term[pa_loai-tivi][]" type = "checkbox" value="1524"/> Google TV</label>
                                                                        <label><input name="term[pa_loai-tivi][]" type = "checkbox" value="328"/> Internet Tivi</label>
                                                                        <label><input name="term[pa_loai-tivi][]" type = "checkbox" value="2158"/> Neo QLED</label>
                                                                        <label><input name="term[pa_loai-tivi][]" type = "checkbox" value="222"/> Smart Tivi</label>
                                                                        <label><input name="term[pa_loai-tivi][]" type = "checkbox" value="228"/> Tivi Android</label>
                                                                        <label><input name="term[pa_loai-tivi][]" type = "checkbox" value="288"/> Tivi Led</label>
                                                                        <label><input name="term[pa_loai-tivi][]" type = "checkbox" value="230"/> Tivi OLED</label>
                                                                        <label><input name="term[pa_loai-tivi][]" type = "checkbox" value="1084"/> Tivi Qled</label>
                                                                        <label><input name="term[pa_loai-tivi][]" type = "checkbox" value="236"/> Tivi thường</label>
                                                                </div>
                        </div> --}}
                             {{--                    <div class = "wg-search">
                            <h3>Tiện ích tivi</h3>
                            <div class = "control-search">
                                                                    <label><input name="term[pa_tien-ich-tivi][]" type = "checkbox" value="227"/> Chiếu điện thoại lên TV (không dây)</label>
                                                                        <label><input name="term[pa_tien-ich-tivi][]" type = "checkbox" value="226"/> Điều khiển được bằng điện thoại</label>
                                                                        <label><input name="term[pa_tien-ich-tivi][]" type = "checkbox" value="1100"/> Nhận dạng giọng nói qua Remote</label>
                                                                        <label><input name="term[pa_tien-ich-tivi][]" type = "checkbox" value="225"/> Tìm kiếm bằng giọng nói tiếng Việt (hỗ trợ 3 miền Bắc, Trung, Nam)</label>
                                                                </div>
                        </div> --}}
                                          
       
            </div>
        </div>
   
    </div>
</section>

</form>
@endsection
@section('script')
<script>
    $(document).ready(function(){
  $('.filter_check').click(function(){
     data = $(this).attr('data-id');
    vl= $(this).is(':checked');
    $('.checkbox'+data).prop('checked', false);
    $(this).prop('checked', vl);
      // go_filter();
  });

  // function go_filter(){
  //   var mang='';
  //     dem=0;
  //   $('.filter_check').each(function(){
  //      if ($(this).is(':checked')) {
  //         dem++;
  //        vl = $(this).val();
  //        if(dem==1){
  //          mang ="?pricef="+vl; 
  //        }else{ mang +="&pricef="+vl;}
  //                   }
  //   });
  //   window.location.href="{{route('index')}}"+mang;
  // }




    });
</script>
@endsection