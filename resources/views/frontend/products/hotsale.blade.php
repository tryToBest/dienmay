@extends('frontend.partials.master')
@section('title', 'Bán chạy')

@section('canonical')
{{ URL::current() }}
@stop
@section('body')

@stop
@section('css')
<style>
    .anhquangcao{
        margin-bottom: 15px;
    }
    .cotphai_productbycate {
    margin-top: 25px;
}
.bannerquangcao1 {
    margin-top: 25px!important;
}
</style>
@stop
@section('content')
@include('frontend/partials/quangcaohome')
 <nav aria-label="breadcrumb ">
         <ol class="breadcrumb ">
            <div class="container">
                <li class="breadcrumb-item"><a href="{{route('index')}}" class="mauxanh">Trang chủ</a></li>
                 <li class="breadcrumb-item"><a href="#" class="mauxanh">Bán chạy</a></li>
          
            </div>
  </ol>
</nav>
<div class="all_sanpham_home">
    
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-9">
           <div class="title_danhmucsp_home">
    <div class="col-xs-12">
        <span>Sản phẩm Bán chạy</span>
   
    </div>
</div>

<div style="clear: both;"></div>
<div class="show_allsanpham">
    @foreach($product as $p)
          <div class="col-xs-6 col-sm-3">
                <div class="baoquanh_sp">
                    <a href="{{route('detail_product',$p->slug_vi)}}">
                        <div class="img-sanpham " style="background:url('{{asset($p->image)}}')center no-repeat;background-size: contain;min-height: 175px;">
                    </div>
                    <div class="name-sanpham">
                        {{str_limit($p->name_vi,40)}}
                    </div>
                    <div class="thuonghieu mauden">
                    <p> {!!str_limit($p->code,40)!!}</p>
                    </div>
                    <div class="price">
                        <b>Giá: @if($p->price != 0) {{number_format($p->price)}} đ @else Liên hệ @endif</b>
                    </div>
                    </a>
                    <button class="nutgiohangsp bold">Thêm vào giỏ hàng</button>    
                    <div class="tieudedanhmuc">
                        Sản phẩm bán chạy
                    </div>
                </div>
                </div>
    @endforeach
</div>
<div style="clear: both;"></div>
<div class="phantrang">
    {{$product->links()}}
</div>
        </div>
        <div class="cotphai_productbycate col-xs-12 col-sm-3">
              @include('frontend/partials/right')  
        </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
        $('.acnav__label').click(function () {
    var label = $(this);
    var parent = label.parent('.has-children');
    var list = label.siblings('.acnav__list');
    if ( parent.hasClass('is-open') ) {
        list.slideUp('fast');
        parent.removeClass('is-open');
    }
    else {
        list.slideDown('fast');
        parent.addClass('is-open');
    }
});
</script>
<script src='{{asset('frontend/js/slick.min.js')}}'></script>
<script  src="{{asset('frontend/js/slick2.js')}}?{{time()}}"></script>
@endsection