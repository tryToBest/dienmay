<div class="lydohome">
	<div class="container">
		<div class="baoquanh_lydo">
			<div class="row">
				@php
				$lydohome =DB::table('uudais')->where('status',1)->orderBy('position','ASC')->get();
				@endphp
				@foreach($lydohome as $lydo)
				<div class="col-xs-6 col-sm-4 text-center">
					<div class="khung_lydo">
		
								<div class="img-lydo">
							<img src="{{asset($lydo->image)}}" alt="{{$lydo->name_vi}}">
						</div>
						<div class="content_lydo">
							<p>{{$lydo->name_vi}}</p>
							<p>{{$lydo->description_vi}}</p>
						</div>
				
					</div>
				</div>
				@endforeach
				

			</div>
		</div>
	</div>
</div>