<div class="danhmucsanpham_left">
	<div class="title_danhmuc">
		Danh mục sản phẩm
	</div>

  <div class="accordion-option">
  
    <a href="javascript:void(0)" class="toggle-accordion active" accordion-id="#accordion"></a>
  </div>
  <div class="clearfix"></div>
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

 @php
$danhmucsp = DB::table('cate_products')->where('status',1)->where('parent_id',0)->get();
@endphp

<section class="nav-wrap">
	<nav class="acnav" role="navigation">
		<ul class="acnav__list acnav__list--level1">
@foreach($danhmucsp as $sp1)
		 @php
$danhmucsp2 = DB::table('cate_products')->where('status',1)->where('parent_id',$sp1->id)->get();
@endphp
			<li class="has-children">
				<div class="acnav__label @if(count($danhmucsp2)>0) active2 @endif">
			{{$sp1->name_vi}}
				</div>
				<!-- start level 2 -->
				<ul class="acnav__list acnav__list--level2" style="display: none;">
	
@foreach($danhmucsp2 as $sp2)
					<li>
						<a class="acnav__link acnav__link--level2" href="{{route('productByCate',$sp2->slug_vi)}}">{{$sp2->name_vi}}</a>
					</li>
@endforeach
				</ul>
				<!-- end level 2 -->
			</li>
			<!-- end group 1 -->
@endforeach
		</ul>
		<!-- end level 1 -->
	</nav>
	<!-- end accordion nav block -->
</section>






  </div>


</div>
