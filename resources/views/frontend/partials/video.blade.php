
     <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css'>
<link rel='stylesheet' href='https://codepen.io/fancyapps/pen/Kxdwjj.css'>
<style>
    #video .row .col-md-3:nth-child(4n+1){
        clear: both;
    }
</style>
<section id="video">
	<div class="container">
	        <div class="content-video">
            <div class="row ">
@php
$video_all = DB::table('sizes')->where('status',1)->orderBy('position','ASC')->get();
@endphp

@foreach($video_all as $vd)
                <div class=" col-xs-12 col-sm-4 col-md-3">

 	<div class="img_video img_po_1" style="background:url('{{asset($vd->image)}}')center no-repeat;background-size:cover;min-height: 250px;position:relative;    display: inline-flex;
    align-items: center;
    width: 100%; " data-fancybox href="{{$vd->note}}">
			<img src="{{asset('frontend/images/play_vd.png')}}" alt="play video">
</div>
 <p class="text-left bold" style="margin:15px 0" data-fancybox href="{{$vd->note}}">{{$vd->name}}</p>

</div>

@endforeach

</div> 


            </div>
        </div>
        	
</section>
<div class="container">
    <div class="line"></div>
</div>

