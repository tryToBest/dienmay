 @php 
$topthuonghieu = DB::table('faqs')->where('status', 1)->orderBy('position','ASC')->get();

@endphp

<div class="topthuonghieu">
	<div class="container">
		<div class="row">
			<div class="title_chung text-uppercase text-center">
				Top thương hiệu
			</div>
			<div class="slide_thuonghieu">
				@foreach($topthuonghieu as $th)
				<div class="col-xs-12 col-sm-3">
					<div class="img-thuonghieu">
						<a href="{{route('thuonghieu',$th->id)}}" >
						<div class="img-topthuonghieu" style="background:url('{{asset($th->image)}}')center no-repeat;background-size: contain;min-height: 50px;display: block;"></div>
						</a>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>