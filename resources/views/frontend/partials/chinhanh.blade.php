@php
$chinhanh = DB::table('cate_ttbhs')->where('status',1)->orderBy('position','ASC')->get();
@endphp

<div class="chinhanh_home bg-gray">
	<div class="container">
		<div class="row">
			<div class="title_chung text-uppercase text-center">
				Hệ thống showroom
			</div>
			<div class="slide_chinhanh">
				@foreach($chinhanh as $cn)
		<div class="col-sn col-xs-12 col-sm-6">
			<div class="name-cn">{{$cn->name_vi}}</div>
			<div class="img-cn"><img src="{{asset($cn->image)}}" alt="{{$cn->name_vi}}"></div>
			<div class="content-cn">
				<ul>
					<li><i class="fa fa-map-marker" aria-hidden="true"></i>
<p><b>Địa chỉ:</b></p>
						 <p>{{$cn->diachi_vi}}</p>
					</li>
					<li>
						<i class="fa fa-phone" aria-hidden="true"></i>
 <p><b>Số hotline: </b></p>
					{!!$cn->hotline!!}
					</li>
					<li>
						<i class="fa fa-envelope" aria-hidden="true"></i>
						<p><b>Mail</b></p>
						<p><a href="mailto:{{$cn->email}}">{{$cn->email}}</a></p>
					</li>
				</ul>
			</div>



		</div>
@endforeach		
			</div>
		</div>
	</div>
</div>