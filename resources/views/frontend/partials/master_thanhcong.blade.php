<!DOCTYPE html>

<html lang="vi">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="@yield('meta_des')">


    <meta name="keywords" content="@yield('meta_key')">

    <meta name="twitter:card" content="summary" />

    <meta property="og:site_name" content="{{ $setting->$get_name }}" />

    <meta property="og:url" content="{{ Request::url() }}" />

    <meta property="og:locale" content="vi_VN" />

    <meta property="og:type" content="article" />

    <meta property="og:title" content="@yield('title_seo')" />

    <meta name="twitter:description" content="@yield('meta_des')" />

    <meta name="twitter:title" content="@yield('title_seo')" />

    <title>@yield('title')</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset($setting->icon) }}">

    <link rel="canonical" href="@yield('canonical')"/>

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/simple-line-icons.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/themify-icons.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/slick.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/meanmenu.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">

    <script src="{{ asset('assets/js/modernizr-2.8.3.min.js') }}"></script>

    {{-- <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"> --}}

    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome-all.css') }}"> --}}

    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/phonering.css') }}"> --}}

    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/menu.css') }}"> --}}

    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/style.css') }}"> --}}

    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/mobile.css') }}"> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! $setting->thead !!}
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '493285641336241'); 
    fbq('track', 'Purchase');

    </script>
    <noscript>
     <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=493285641336241&ev=Purchase
    &noscript=1"/>
    </noscript>

    <!-- End Facebook Pixel Code -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-169012183-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-169012183-1');
</script>
</head>


<body>



    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

    @if (Session::has('themvaogiohang'))

        <script type="text/javascript">

            let timerInterval

            Swal.fire({

              title: 'Thêm vào giỏ hàng thành công!',

              icon: 'success',

              // html: 'Close <b></b> milliseconds.',

              timer: 2000,

              timerProgressBar: true,

              onBeforeOpen: () => {

                Swal.showLoading()

                timerInterval = setInterval(() => {

                  const content = Swal.getContent()

                  if (content) {

                    const b = content.querySelector('b')

                    if (b) {

                      b.textContent = Swal.getTimerLeft()

                    }

                  }

                }, 300)

              },

              onClose: () => {

                clearInterval(timerInterval)

              }

            }).then((result) => {

              /* Read more about handling dismissals below */

              if (result.dismiss === Swal.DismissReason.timer) {

                console.log('I was closed by the timer')

              }

            })

        </script>

    @endif

    @if (Session::has('dangky'))

        <script type="text/javascript">alert('Gửi thành công')</script>

    @endif

    @if (Session::has('themyeuthich'))

        <script type="text/javascript">alert('Thêm vào yêu thích thành công')</script>

    @endif

    @if (Session::has('themyeuthichroi'))

        <script type="text/javascript">alert('Sản phẩm đã có trong danh sách yêu thích')</script>

    @endif

    @include('frontend.partials.header')

    @yield('content')

    @include('frontend.partials.footer')

    {{-- <a href="tel:{{ $setting->phone }} ">

        <div class="hotline">

            <div id="phonering-alo-phoneIcon" class="phonering-alo-phone phonering-alo-green phonering-alo-show">

                <div class="phonering-alo-ph-circle"></div>

                <div class="phonering-alo-ph-circle-fill"></div>

                <div class="phonering-alo-ph-img-circle"></div>

            </div>

        </div>

    </a> --}}

    @yield('popup')

    <!-- Chạy quảng cáo 2 bên -->

    <!--Quang cao -->

    @foreach ($quangcao as $key => $q)

        @if ($key == 0)

            <div id="divQCRight" style="display: none; position: absolute; top: 0px">   

                <a href="{{ $q->link }}"><img src="{{ asset($q->image) }}" width="120" /></a>

            </div>

        @endif

        @if ($key == 1)

            <div id="divQCLeft" style="display: none; position: absolute; top: 0px">   

                <a href="{{ $q->link }}"><img src="{{ asset($q->image) }}" width="120" /></a>   

            </div>

        @endif

    @endforeach

    <script type='text/javascript'>

        //<![CDATA[

           function FloatTopDiv()   

            {   

               startLX = ((document.body.clientWidth -MainContentW)/2)-LeftBannerW-LeftQCjust , startLY = TopQCjust+80;   

                startRX = ((document.body.clientWidth -MainContentW)/2)+MainContentW+RightQCjust , startRY = TopQCjust+80;   

                var d = document;   

                function ml(id)   

                {   

                    var el=d.getElementById?d.getElementById(id):d.all?d.all[id]:d.layers[id];   

                    el.sP=function(x,y){this.style.left=x + 'px';this.style.top=y + 'px';};   

                    el.x = startRX;   

                    el.y = startRY;   

                    return el;   

                }   

                function m2(id)   

                {   

                    var e2=d.getElementById?d.getElementById(id):d.all?d.all[id]:d.layers[id];   

                    e2.sP=function(x,y){this.style.left=x + 'px';this.style.top=y + 'px';};   

                    e2.x = startLX;   

                    e2.y = startLY;   

                    return e2;   

                }   

                window.stayTopLeft=function()   

                {   

                    if (document.documentElement && document.documentElement.scrollTop)   

                        var pY =  document.documentElement.scrollTop;   

                    else if (document.body)   

                        var pY =  document.body.scrollTop;   

                     if (document.body.scrollTop > 30){startLY = 3;startRY = 3;} else  {startLY = TopQCjust;startRY = TopQCjust;};   

                    ftlObj.y += (pY+startRY-ftlObj.y)/16;   

                    ftlObj.sP(ftlObj.x, ftlObj.y);   

                    ftlObj2.y += (pY+startLY-ftlObj2.y)/16;   

                    ftlObj2.sP(ftlObj2.x, ftlObj2.y);   

                    setTimeout("stayTopLeft()", 1);   

                }   

                ftlObj = ml("divQCRight");   

                //stayTopLeft();   

                ftlObj2 = m2("divQCLeft");   

                stayTopLeft();   

            }   

            function ShowQCDiv()   

            {   

                var objQCDivRight = document.getElementById("divQCRight");   

                var objQCDivLeft = document.getElementById("divQCLeft");     

                if (document.body.clientWidth < 1300)   

                {   

                    objQCDivRight.style.display = "none";   

                    objQCDivLeft.style.display = "none";   

                }   

                else   

                {   

                    objQCDivRight.style.display = "block";   

                    objQCDivLeft.style.display = "block";   

                    FloatTopDiv();   

                }   

            }

        //]]>

    </script>

    <script type='text/javascript'>

        //<![CDATA[

        document.write("<script type='text/javascript' language='javascript'>MainContentW = 1200;LeftBannerW = 90;RightBannerW = 90;LeftQCjust = 25;RightQCjust = 25;TopQCjust = 160;ShowQCDiv();window.onresize=ShowQCDiv;;<\/script>");   

        //]]>

    </script>



   <!-- Subiz -->

  {{--   <script>

        (function(s, u, b, i, z){

            u[i]=u[i]||function(){

                u[i].t=+new Date();

                (u[i].q=u[i].q||[]).push(arguments);

            };

            z=s.createElement('script');

            var zz=s.getElementsByTagName('script')[0];

            z.async=1; z.src=b; z.id='subiz-script';

            zz.parentNode.insertBefore(z,zz);

        })(document, window, 'https://widgetv4.subiz.com/static/js/app.js', 'subiz');

        subiz('setAccount', 'acqrtavhytufmwmtygev');

    </script> --}}

    <!-- End Subiz -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">

    <div class="stickyss">

        <div class="icon">

            <a href="tel:{{ $setting->hotline }}" class="icon-Calculator"><i class="fa fa-phone" aria-hidden="true"></i></a>

            <div class="text"><a href="tel:{{ $setting->hotline }}" target="_blank">{{ $setting->hotline }}</a></div>

        </div>

        <div class="icon">

            <a href="{{ $setting->xd5 }}" target="_blank" class="icon-Drive"><i class="fa fa-facebook" aria-hidden="true"></i></a>

            <div class="text"><a href="{{ $setting->xd5 }}" target="_blank">Facebook</a></div>

        </div>

        <div class="icon">

            <a href="https://zalo.me/{{ $setting->hotline }}" target="_blank" class="icon-Catalogue"><img src="{{asset('images/zalo-icon.png')}}" alt="">    </a>

            <div class="text"><a href="https://zalo.me/{{ $setting->hotline }}" target="_blank">Zalo</a></div>

        </div>

        <div class="icon">

            <a href="https://www.youtube.com/" target="_blank" class="icon-PriceTag"><i class="fa fa-youtube" aria-hidden="true"></i></a>

            <div class="text"><a href="https://www.youtube.com/" target="_blank">Youtube</a></div>

        </div>

        <style type="text/css">

            .stickyss {

              position: fixed;

              top: calc(50% - 130px);

              z-index: 2000;

              right: 0;

            }

            .stickyss .icon {

              background-color: #fff;

              border-bottom: solid 1px #cdcdcd;

              position: relative;

              text-align: center;

            }

            .stickyss .icon .text, .stickyss .icon:hover {

              background-color: #064b98;

            }

            .stickyss .icon a {

              color: #064b98;

              text-decoration: none;

            }

            .stickyss .icon:hover a {

              color: #fff;

            }

            .stickyss .icon .text {

                padding: 15px;

            }

            .stickyss .icon .text, .stickyss .icon:hover {

                background-color: #064b98;

            }

            .stickyss .icon .text {

              color: #fff;

              position: absolute;

              z-index: -1;

              top: 0;

              left: 0;

              width: 150px;

              font-size: 13px;

              transition: left .5s;

              -webkit-transition: left .5s;

              -moz-transition: left .5s;

              -o-transition: left .5s;

            }

            .stickyss .icon:hover .text {

              left: -150px;

            }

            .stickyss .icon .text, .stickyss .icon:hover {

              background-color: #064b98;

            }

            .stickyss .icon {

                padding: 13px;

                font-size: 16px;

            }

        </style>

    </div>

    <script src="{{ asset('assets/js/jquery-1.12.0.min.js') }}"></script>

    <script src="{{ asset('assets/js/popper.js') }}"></script>

    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('assets/js/jquery.counterup.min.js') }}"></script>

    <script src="{{ asset('assets/js/waypoints.min.js') }}"></script>

    <script src="{{ asset('assets/js/elevetezoom.js') }}"></script>

    <script src="{{ asset('assets/js/ajax-mail.js') }}"></script>

    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>

    <script src="{{ asset('assets/js/plugins.js') }}"></script>

    <script src="{{ asset('assets/js/main.js') }}"></script>

    

    @yield('script')

    <!-- Subiz -->

    



<!-- End Subiz -->

</body>

</html>

