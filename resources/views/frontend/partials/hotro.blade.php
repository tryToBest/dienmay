	@php
$hotrkinhdoanh = DB::table('uudais')->where('status',1)->where('display',1)->orderBy('position','ASC')->get();
$truchotline = DB::table('uudais')->where('status',1)->where('display',2)->orderBy('position','ASC')->get();
@endphp

<div class="bongoai-boxright">
	<div class="head-titleright">
		<p>Hỗ Trợ Kinh Doanh</p>
	</div>
	<div class="padingngoai">
@foreach($hotrkinhdoanh as $key =>$kinhdoanh)
	<div class="content-rigt">
		<div class="leftname">
			<div class="name">{{$kinhdoanh->name_vi}}</div>
			<a href="tel:{{$kinhdoanh->description_vi}}"><div class="phone">{{$kinhdoanh->description_vi}}</div></a>
		</div>
		<div class="phoneicon">
			<a href="tel:{{$kinhdoanh->description_vi}}"><i class="fa fa-phone"></i></a>
		</div>
	</div>
	@endforeach
	
	
	</div>
</div>
<div class="bongoai-boxright">
	<div class="head-titleright">
		<p>Hỗ Trợ Trực Tuyến</p>
	</div>
	<div class="padingngoai">
@foreach($truchotline as $key =>$thl)
	<div class="content-rigt">
		<div class="leftname">
			<div class="name">{{$thl->name_vi}}</div>
			<a href="tel:{{$thl->description_vi}}"><div class="phone">{{$thl->description_vi}}</div></a>
		</div>
		<div class="phoneicon">
			<a href="tel:{{$thl->description_vi}}"><i class="fa fa-phone"></i></a>
		</div>
	</div>
	@endforeach
	
	
	</div>
</div>