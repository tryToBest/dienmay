{{-- @php
    function checkIsMobile()
    {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
@endphp --}}
<header>
            <div class="bx-top-header">
                
                @if(isMobile())
                <a href="#" title="">
                    <img src="{{asset($setting->banner_mobile)}}" alt="{{$setting->name_vi}}">
                </a>
                @else
                <a href="#" title="">
                    <img src="{{asset($setting->banner)}}" alt="{{$setting->name_vi}}">
                </a>
                @endif
                
            </div>
            <div class="bx-logo-search">
                <div class="container">
                    <div class="logo">
                        <a href="{{route('index')}}" title="{{$setting->name_vi}}">
                            <img src="{{$setting->logo}}" alt="{{$setting->name_vi}}">
                        </a>
                    </div>
                    <div class="bx-search">
                        <form action="{{route('postSearch')}}" method="GET">
                            @csrf
                            <input type="text" placeholder="Bạn tìm gì..." name="search" required="">
                            <button><i class="fas fa-search"></i></button>
                        </form>
                    </div>
                    <div class="option">
                        <div class="item-option option-phone">
                            <h4><a href="tel:{{$setting->hotline}}"><i class="fas fa-phone"> </i>{{$setting->hotline}}</a></h4>
                            {{-- <p>Tư vấn bán hàng</p> --}}
                        </div>
                  {{--       <div class="item-option opsub">
                            <h4>Kinh nghiệm hay</h4>
                            <p>Tin khuyến mại <i class="fas fa-caret-down"></i></p>
                                   @php
$danhmuc_mn = DB::Table('cate_posts')->select('name_vi','id','slug_vi')->where('menu',1)->orderBy('position','ASC')->get();
@endphp
                            <ul>
                                @foreach($danhmuc_mn as $mn)
                                <li><a href="{{route('list_post',$mn->slug_vi)}}" title="">{{$mn->name_vi}}</a></li>
                                @endforeach
                            </ul>
                        </div> --}}
                    </div>
                </div>
            </div>
            @php
                $path = Request::path();  
                $cate_home = DB::table('cate_products')->where('status', 1)->where('is_home', 1)->where('parent_id',0)->orderBy('position', 'ASC')->get(); 
            @endphp
            <div class="bx-categories">
                <div class="container">
                    <div class="header header-page">
                        <h3><i class="fas fa-bars"></i> Danh mục sản phẩm <i class="fas fa-caret-down"></i></h3>
                    </div>    
                    <div class="content-categories ">
                                    {{-- <div class="content-categories {{$path === '/' ? "active" : ""}}"> --}}
                        <ul>
                            @foreach($cate_home as $key => $cate)
                            <li>
                                <img src="{{asset($cate->image)}}" alt="{{$cate->name_vi}}">
                                <a href="{{route('productByCate',$cate->slug_vi)}}" title="{{$cate->name_vi}}">{{$cate->name_vi}}
                                    <i class="fas fa-chevron-right"></i>
                                </a>
                                @php
                                    $children = DB::table('cate_products')->where('parent_id',$cate->id)->where('is_home', 1)->get();
                                @endphp
                                @if(isset($children))
                                <div class="list-menu-item">
                                    @foreach($children as $key => $child)
                                    <a href="{{route('productByCate',[$cate->slug_vi,$child->slug_vi])}}" title="">{{$child->name_vi}}</a>
                                    @endforeach
                                </div>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </div>
               @php
// $danhmuc_mn = DB::Table('cate_posts')->select('name_vi','id','slug_vi')->where('menu',1)->orderBy('position','ASC')->get();
                $cate_home_ngang = DB::table('cate_products')->where('status', 1)->where('is_home', 1)->where('parent_id',0)->orderBy('position', 'ASC')->take(6)->get(); 
@endphp


                    <div class="bx-list-text-link">
             @foreach($cate_home_ngang as $mn)
                        <a href="{{route('productByCate',$mn->slug_vi)}}" title="{{$mn->name_vi}}">  <img src="{{asset($mn->image)}}" alt="{{$mn->name_vi}}" style="">{{$mn->name_vi}}</a>
                        @endforeach
                        <a href="{{route('cart.index')}}" target="_bank" class="gio_hang"><i class="fas fa-cart-plus"></i> Giỏ hàng</a>
                    </div>
                </div>
            </div>
        </header>