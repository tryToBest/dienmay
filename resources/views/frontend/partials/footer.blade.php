<style>
  .contact_footer{padding:30px 0;overflow: hidden;}
  .contact_footer .col-3{
width: 33.33333%;
  }
  .canbang_icon{display: inline-flex;align-items: center;justify-content: center;}
  .icon_ft{
     border:1px solid #333333;
    padding: 20px 30px;margin-right: 20px;
  }
  .icon_ft i{
   font-size: 54pt;color: #333333;
  }
  .content_icon_contact p{margin-bottom: 10px;}
  .contact_footer .title-box{text-transform: uppercase;text-align: center;margin-bottom: 30px}
  @media(max-width: 767px){
    .icon_ft i {
    font-size: 14pt;

}
.canbang_icon {
    display: inline-flex;
    align-items: flex-start;
    justify-content: end;
}
.icon_ft {
    padding: 7px 12px;
    min-width: 52px;
    text-align: center;
    /*margin-bottom: 15px;*/
}
.contact_footer .col-3{
width: 100%;    min-height: 53px;
  }
  .content_icon_contact p {
    margin-bottom: 0;
}
/*  .contact_footer .col-3.col-last{
  width: 100%;
}
*/




  }
</style>
<div style="clear: both;"></div>
<div class="contact_footer">
  <div class="container">

     <div class="title-box">
            <h2>Thông tin liên hệ</h2>
        </div>

     <div class="row">



            <div class="col-3 col-footer">
           <div class="canbang_icon">
                <div class="icon_ft">
                <i class="fas fa-phone-volume" aria-hidden="true"></i>
              </div>
              <div class="content_icon_contact">
              {!!$setting->cc2!!}
              </div>
           </div>
            </div>
              <div class="col-3 col-footer">
       <div class="canbang_icon">
                <div class="icon_ft">
       <i class="fas fa-envelope"></i>

              </div>
              <div class="content_icon_contact">
         <p>  <a href="mailto:{!!$setting->email!!}">Email: {!!$setting->email!!}</a></p>
              </div>
           </div>
            </div>
              <div class="col-3 col-footer col-last">
                       <div class="canbang_icon">
                <div class="icon_ft">
        <i class="fas fa-map-marker-alt" aria-hidden="true"></i>

              </div>
              <div class="content_icon_contact">
                       <p> {{$setting->address_vi}}</p>
             @if($setting->address2nd != '')<p> {{$setting->address2nd}}</p> @endif
              </div>
           </div>
            </div>
      </div>
  </div>
</div>


@php
    $postFooter1 = DB::table('posts')->select('slug_vi','name_vi')->where('footer1',1)->get();
    $postFooter2 = DB::table('posts')->select('slug_vi','name_vi')->where('footer2',1)->get();
@endphp
<footer>
    <div class="container">
        <div class="row">
            <div class="col-3 col-footer">
                <h3>Thông tin {{$setting->name_vi}}</h3>
                <p>{{$setting->name_vi}}</p>
                <ul>
                    @foreach($postFooter1 as $post)
                    <li>
                        <a href="{{route('detail',$post->slug_vi)}}" title="{{$post->name_vi}}">{{$post->name_vi}}</a>
                    </li>
                    @endforeach
                </ul>
                 <div>
                    <img style='max-height: 60px;margin-top: 10px;' src='{{asset('images/bct.png')}}'/>
                </div>
            </div>
            <div class="col-3 col-footer">
                <h3>Chính sách mua hàng</h3>
                <ul>
                    @foreach($postFooter2 as $post)
                    <li>
                        <a href="{{route('detail',$post->slug_vi)}}" title="{{$post->name_vi}}">{{$post->name_vi}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
         
            <div class="col-3 col-footer">
                <div class="bx-like">
                     <div id="fb-root"></div>
   <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.3&appId=&autoLogAppEvents=1"></script>
                    <div class="fb-page" data-href="{{$setting->xd5}}" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
                </div>
               
            </div>
               <div class="col-3 col-footer map_ft">
                {{-- <h3>TƯ VẤN MIỄN PHÍ (24/7)</h3> --}}
              {!!$setting->tt3!!}
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
    <div class="copyright">
        <div class="container">
            {!!$setting->tt2!!}    
    </div>
    </div>
    <div style="clear: both;"></div>
</footer>
{{-- <div class='bx-zalo'>
    <a target="_blank" href="{{$setting->xd4}}">
        <img src="{{asset('images/zalo.webp')}}"/>
    </a>








</div>
 --}}

<div class="zalo-container right">
      <a id="zalo-btn" href="{{$setting->xd4}}" target="_blank" rel="noopener noreferrer nofollow external" data-wpel-link="external">
                <div class="animated_zalo infinite zoomIn_zalo cmoz-alo-circle"></div>
        <div class="animated_zalo infinite pulse_zalo cmoz-alo-circle-fill"></div>
        <span><img data-lazyloaded="1" src="https://xaydungdanghuong.com/frontend/images/zalo-2.png" data-src="https://xaydungdanghuong.com/frontend/images/zalo-2.png" alt="Contact Me on Zalo" data-ll-status="loaded" class="entered litespeed-loaded"><noscript><img src="https://xaydungdanghuong.com/frontend/images/zalo-2.png" alt="Contact Me on Zalo"></noscript></span>
              </a>




    </div>



<style>
    .zalo-container img {
  max-width: 100%;
  height: auto;
}

.zalo-container {
  position: fixed;
  width: 40px;
  height: 40px;
  bottom: 160px;
  z-index: 9999999;
}



.zalo-container.right {
  left: 3.6rem;
}

.zalo-container a {
    display: block;
}

.zalo-container span {
  display: -webkit-flex;
  display: -moz-flex;
  display: -ms-flex;
  display: -o-flex;
  display: flex;
  -ms-align-items: center;
  align-items: center;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  background: #1182FC;
  position: relative;
}

@keyframes zoomIn_zalo {
    from {
        opacity: 0;
        transform: scale3d(.3, .3, .3);
    }
    50% {
        opacity: 1;
    }
}

.zoomIn_zalo {
  animation-name: zoomIn_zalo;
}

.animated_zalo {
  animation-duration: 1s;
  animation-fill-mode: both;
}

.animated_zalo.infinite {
  animation-iteration-count: infinite;
}

.cmoz-alo-circle {
  width: 50px;
  height: 50px;
  top: -5px;
  right: -5px;
  position: absolute;
  background-color: transparent;
  -webkit-border-radius: 100%;
  -moz-border-radius: 100%;
  border-radius: 100%;
  border: 2px solid rgba(17, 130, 252, .8);
  opacity: .1;
  border-color: #1182FC;
  opacity: .5;
}

.cmoz-alo-circle-fill {
  width: 60px;
  height: 60px;
  top: -10px;
  position: absolute;
  -webkit-transition: all 0.2s ease-in-out;
  -moz-transition: all 0.2s ease-in-out;
  -ms-transition: all 0.2s ease-in-out;
  -o-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
  -webkit-border-radius: 100%;
  -moz-border-radius: 100%;
  border-radius: 100%;
  border: 2px solid transparent;
  -webkit-transition: all .5s;
  -moz-transition: all .5s;
  -o-transition: all .5s;
  transition: all .5s;
  background-color: rgba(17, 130, 252, 0.45);
  opacity: .75;
  right: -10px;
}

@-webkit-keyframes pulse_zalo {
  from {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  50% {
    -webkit-transform: scale3d(1.05, 1.05, 1.05);
    transform: scale3d(1.05, 1.05, 1.05);
  }

  to {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}

@keyframes pulse_zalo {
  from {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  50% {
    -webkit-transform: scale3d(1.05, 1.05, 1.05);
    transform: scale3d(1.05, 1.05, 1.05);
  }

  to {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}

.pulse_zalo {
  -webkit-animation-name: pulse_zalo;
  animation-name: pulse_zalo;
}
@media(max-width: 767px){
    .zalo-container.right {
  left: 1rem;
}
.zalo-container {

  bottom: 130px;

}

}
</style>






<div class='quick-call-button'></div>
<div class="call-now-button">
    <div><p class="call-text">{{$setting->hotline}}</p>
        <a href="tel:{{$setting->hotline}}" title="Call Now">
            <div class="quick-alo-ph-circle active"></div>
            <div class="quick-alo-ph-circle-fill active"></div>
            <div class="quick-alo-ph-img-circle shake"></div>
        </a>
    </div>
</div>