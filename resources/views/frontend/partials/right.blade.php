<div class="bannerquangcao1">
	@php
$quangcaotren = DB::table('slides')->where('status',1)->where('dislay',10)->orderBy('position','ASC')->take(1)->get();

@endphp
<div class="anhquangcao borderxanh">
    @foreach($quangcaotren as $qc1)
    <a href="{{$qc1->link}}">
    	<div class="img-right" style="background: url('{{asset($qc1->image)}}')center no-repeat; background-size: cover;padding-top: 50%;">	
    	</div>
    </a>
    @endforeach
</div>
</div>
@include('frontend/partials/hotro')

<div class="bannerquangcao1 borderxanh">
	@php
$quangcaohome = DB::table('slides')->where('status',1)->where('dislay',8)->orderBy('position','ASC')->take(1)->get();
@endphp
<div class="anhquangcao">
    @foreach($quangcaohome as $qc2)
    <a href="{{$qc2->link}}">
    	<div class="img-right" style="background: url('{{asset($qc2->image)}}')center no-repeat; background-size: cover;padding-top: 150%;">	
    	</div>
    </a>
    @endforeach
</div>
</div>

@php
$spmoi = DB::table('products')->where('status',1)->where('is_home',1)->orderBy('position','ASC')->take(5)->get();
@endphp
<div class="bongoai-boxright2">
	<div class="head-titleright2">
		<p>CÁC SẢN PHẨM MỚI</p>
	</div>
	<div class="padingngoai">
		@foreach($spmoi as $key => $spm)
	<div class="content-rigt noflex chinhsanpham_right">
	<div class="row colbanghaiben">
		<div class="col-xs-4 col-sm-3 " style="padding-right: 0;">
			<a href="{{route('detail_product',$spm->slug_vi)}}">
				<div class="img-sanpham " style="background:url('{{asset($spm->image)}}')center no-repeat;background-size: cover;min-height: 70px;margin-right: 7px;"></div>
			</a>
		</div>
		<div class="col-xs-8 col-sm-9 margingiua" style="padding-left: 0;">
			<a href="{{route('detail_product',$spm->slug_vi)}}" class="haidong">{{str_limit($spm->name_vi,50)}}</a>
		</div>
	</div>
	</div>
		@endforeach
	</div>
	<div class="head-titleright2 bold">
		<p>SẢN PHẨM BÁN CHẠY</p>
	</div>
	<div class="padingngoai">
		@foreach($spmoi as $key => $spm)
	<div class="content-rigt noflex chinhsanpham_right">
	<div class="row colbanghaiben">
		
		<div class="col-xs-4 col-sm-3" style="padding-right: 0;">
			<a href="{{route('detail_product',$spm->slug_vi)}}"><div class="img-sanpham " style="background:url('{{asset($spm->image)}}')center no-repeat;background-size: cover;min-height: 70px;margin-right: 7px;"></div></a>
		</div>
		<div class="col-xs-8 col-sm-9 margingiua"  style="padding-left: 0;">
			<a href="{{route('detail_product',$spm->slug_vi)}}" class="haidong">{{str_limit($spm->name_vi,50)}}</a>
		</div>
	</div>
	</div>
		@endforeach
	</div>
</div>

<div class="mucluc chaytheo">
	<div class="head-titleright2 bold">
		<p>Mục Lục</p>
	</div>
	@if(isset($detail_product))

<div class="mucluc_right all-questions">
	
</div>







	@else
	@php
$baiviet = DB::table('posts')->where('status',1)->where('is_home',1)->orderBy('position','ASC')->take(5)->get();
@endphp
	<ul >
		@foreach($baiviet as $key => $bv)
		<li><a href="{{route('detail',$bv->slug_vi)}}">{{$bv->name_vi}}</a></li>
		@endforeach
	</ul>
	@endif
</div>

