<!DOCTYPE html>
<html lang="vi" xml:lang="vi">
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8">
      <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">    
    <meta name="description" content="@yield('meta_des')">
    <meta name="keywords" content="@yield('meta_key')">
     <meta name='revisit-after' content='1 days' />
        <meta name="robots" content="noodp,index,follow" />
    <meta property="og:site_name" content="{{ $setting->$get_name }}" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:title" content="@yield('title_seo')" />
    <meta name="twitter:description" content="@yield('meta_des')" />
    <meta name="twitter:title" content="@yield('title_seo')" />
    <title>@yield('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset($setting->icon) }}">
    <link rel="canonical" href="@yield('canonical')"/>
<link rel="stylesheet" href="{{asset('frontend/css/checkout.vendor.min.css')}}?{{time()}}">
<link rel="stylesheet" href="{{asset('frontend/css/checkout.min.css')}}?{{time()}}">
<!-- Begin checkout custom css -->
<style>
.main__footer {
  color: #747474;
}
h1,h2,h3,h4,h5,h6{
  font-family:  ;
}
</style>
<!-- End checkout custom css -->
<script src="{{asset('frontend/js/checkout.vendor.min.js')}}?{{time()}}"></script>
<script src="{{asset('frontend/js/checkout.min.js')}}?{{time()}}"></script>
    @yield('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! $setting->thead !!}
</head>
<body data-no-turbolink>
        {!! $setting->tbody !!}
  {{--   <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://www.promisejs.org/polyfills/promise-6.1.0.js"></script>
    @if (Session::has('themvaogiohang'))
        <script type="text/javascript">
            let timerInterval
            Swal.fire({
              title: 'Thêm vào giỏ hàng thành công!',
              icon: 'success',
              // html: 'Close <b></b> milliseconds.',
              timer: 2000,
              timerProgressBar: true,
              onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                  const content = Swal.getContent()
                  if (content) {
                    const b = content.querySelector('b')
                    if (b) {
                      b.textContent = Swal.getTimerLeft()
                    }
                  }
                }, 300)
              },
              onClose: () => {
                clearInterval(timerInterval)
              }
            }).then((result) => {
              /* Read more about handling dismissals below */
              if (result.dismiss === Swal.DismissReason.timer) {
                console.log('I was closed by the timer')
              }
            })
        </script>
    @endif --}}
    @if (Session::has('dangky'))
        <script type="text/javascript">alert('Gửi thành công')</script>
    @endif
    @yield('content')
    @yield('script')
</body>
</html>
