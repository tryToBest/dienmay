@php
$sanphamkhuyenmai = DB::table('products')->where('status',1)->where('price_unit','<>',0)->where('is_hot',1)->orderBy('position','ASC')->take(4)->get();
@endphp

<div class="sanphamkhuyenmai bg-gray">



@include('frontend/partials/quangcaohome')
	<div class="container">
		<div class="row">
			<div class="title_km"><a href="{{route('spkm')}}">Khuyến mãi</a></div>
			<div class="baoquanh_sanphamkhuyenmai">
				@foreach($sanphamkhuyenmai as $km)
				<div class="col-xs-6 col-sm-3">
				<div class="baoquanh_sp">
					<a href="{{route('detail_product',$km->slug_vi)}}">
						<div class="img-sanpham hieuungzoom_bg" style="background:url('{{asset($km->image)}}')center no-repeat;background-size: cover;min-height: 250px;">
							{{-- <img src="{{asset($km->image)}}" alt="{{$km->name_vi}}"> --}}
					</div>
					<div class="name-sanpham">
						{{$km->name_vi}}
					</div>
					<div class="thuonghieu">
			<p>	Mã sản phẩm: {{$km->code}}</p>
					</div>
					<div class="tinhtrang">
						<p>		Tình trạng : còn hàng</p>
					</div>
					<div class="price">
						 <div class="price gachngang marginright inline-block mauden">@if($km->price_unit !=null) {{number_format($km->price_unit)}}đ  @endif </div><b>@if($km->price != 0) {{number_format($km->price)}} đ @else Liên hệ @endif</b>
					</div>
					</a>	
				</div>
				</div>
				@endforeach


				<div style="clear: both;"></div>
				<div class="xemtatca text-center">
					<a href="{{route('spkm')}}">Xem tất cả</a>
				</div>
				</div>
		</div>
	</div>
</div>