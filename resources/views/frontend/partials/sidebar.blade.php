        <div class="col-lg-3">

            <div class="shop-sidebar">

                <div class="shop-widget">

                    <h4 class="shop-sidebar-title">{{ trans('home.timkiemsanpham') }}</h4>

                    <div class="shop-search mt-25 mb-50">

                        <form class="shop-search-form" action="{{ route('timten') }}" method="get">

                            @csrf

                            <input type="text" placeholder="{{ trans('home.nhaptensanpham') }}" name="search">

                            <button type="submit">

                                <i class="icon-magnifier"></i>

                            </button>

                        </form>

                    </div>

                </div>
                <div class="shop-widget timtheogia">

                    <h4 class="shop-sidebar-title">{{ trans('home.timtheogia') }}</h4>

                        <div class="layered-content slider-range filter-price">


                            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>

                            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


                            <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>

                            <input type="text" id="js-range-slider" name="my_range" value="" />

                            <script type="text/javascript">

                                $("#js-range-slider").ionRangeSlider({

                                    type: "double",

                                    min: 0,

                                    max: 1000000,

                                    from: 0,

                                    to: 500000,

                                    grid: true,

                                    onChange: function (datas) {

                                    price1 = datas.from;

                                    price2 = datas.to;

                                    $.ajax({

                                        type: 'get',

                                        dataType: 'html',

                                        url: '{{ route('index') }}/product/productsCat',

                                        data: 'price1=' + price1 + '&price2=' + price2,

                                        success:function(response){

                                            $("#pd_collection").html(response);

                                        }

                                    });

                                },

                                });

                            </script>

                        </div>

                    </form>

                </div>

                @php

                    $cap1 = DB::table('cate_products')->where('status', 1)->where('parent_id', 0)->orderBy('position', 'ASC')->get();

                @endphp

                @foreach ($cap1 as $c1)

                    @php

                        $cap2 = DB::table('cate_products')->where('status', 1)->where('parent_id', $c1->id)->orderBy('position', 'ASC')->get();

                    @endphp

                    <div class="shop-widget mt-50">

                        <h4 class="shop-sidebar-title">{{ $c1->$get_name }}</h4>

                         <div class="shop-list-style mt-20">

                            <ul>

                                @foreach ($cap2 as $c2)

                                    <li><a href="{{ route('productByCate', $c2->slug_vi) }}">{{ $c2->$get_name }}</a></li>

                                @endforeach

                            </ul>

                        </div>

                    </div>

                @endforeach

                </div>

            </div>

        </div>