<section id="ketnoi">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 text-center">
				<div class="box bovien cong ketnoi_canbang">
					<div class=" mauxanh cangiua bold sub-title">
			            <span>MUA SẢN PHẨM TẠI</span>
			        </div>
			        <div class="content-sp cangiua mauxanh">
			        	<a href="{{route('index')}}"><img src="{{asset('frontend/images/cart.png')}}" alt=""></a>
			        	<a href="{{$setting->g4}}"><img src="{{asset('frontend/images/tiki.png')}}" alt=""></a>
			        	<a href="{{$setting->g1}}"><img src="{{asset('frontend/images/shoppe.png')}}" alt=""></a>
			        	<a href="{{$setting->cc1}}"><img src="{{asset('frontend/images/ic_lazd.png')}}" alt=""></a>
			        
			        </div>
				</div>
			</div>
			<div class="col-sm-6 text-center">
				<div class="box bovien cong ketnoi_canbang">
					<div class=" mauxanh cangiua bold sub-title">
			            <span>KẾT NỐI VỚI NAM DƯƠNG</span>
			        </div>
			        <div class="content-sp cangiua mauxanh ">
			        	<a href="{{$setting->xd2}}"><img src="{{asset('frontend/images/YOUTUBE.png')}}" alt=""></a>
			        	<a href="{{$setting->xd5}}"><img src="{{asset('frontend/images/FACEBOOK.png')}}" alt=""></a>
			        	
			        </div>
				</div>
			</div>
		</div>
	</div>
</section>