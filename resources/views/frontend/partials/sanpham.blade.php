<div class="section-sanpham">
<div class="title_danhmucsp_home">
	<div class="col-xs-12">
		<span><a href="{{route('hotsale')}}" class="link">Sản phẩm bán chạy</a></span>
	</div>
</div>
<div style="clear: both;"></div>
<div class="show_allsanpham">
	@foreach($all_sanpham_banchay as $spbc)
			<div class="col-xs-6 col-sm-25">
				<div class="baoquanh_sp" data-id="{{$spbc->id}}" >
					<a href="{{route('detail_product',$spbc->slug_vi)}}">
						<div class="img-sanpham " style="background:url('{{asset($spbc->image)}}')center no-repeat;background-size: contain;min-height: 250px;">
					</div>
					<div class="name-sanpham">
						{{str_limit($spbc->name_vi,400)}}
					</div>
					{{-- <div class="thuonghieu mauden">
					<p>	{!!str_limit($spbc->code,40)!!}</p>
					</div> --}}
					<div class="price">
						<b>   <div class="price gachngang marginright inline-block mauden">@if($spbc->price_unit !=null) {{number_format($spbc->price_unit)}}đ  @endif </div>@if($spbc->price != 0) {{number_format($spbc->price)}} đ @else Liên hệ @endif</b>
					</div>
					</a>
					<button class="nutgiohangsp clickthemgiohang bold click_themvaogio">Thêm vào giỏ hàng</button>	
					<div class="tieudedanhmuc">
						Sản phẩm bán chạy
					</div>
				</div>
				</div>
	@endforeach
</div>
<div style="clear: both;"></div>
<div class="linexemthem">
	{{-- <div class="line-ngang"></div> --}}
	<a href="{{route('hotsale')}}">Xem thêm sản phẩm ></a>
</div>	
</div>

			<div class="section-sanpham">
<div class="title_danhmucsp_home">
	<div class="col-xs-12">
		<span><a href="{{route('allProduct')}}" class="link">Sản phẩm mới</a></span>
	</div>
</div>
<div style="clear: both;"></div>
<div class="show_allsanpham">
	@foreach($all_sanpham_moi as $spm)
			<div class="col-xs-6 col-sm-25">
				<div class="baoquanh_sp" data-id="{{$spm->id}}">
					<a href="{{route('detail_product',$spm->slug_vi)}}">
						<div class="img-sanpham " style="background:url('{{asset($spm->image)}}')center no-repeat;background-size: contain;min-height: 250px;">
					</div>
					<div class="name-sanpham">
						{{str_limit($spm->name_vi,400)}}
					</div>
					{{-- <div class="thuonghieu mauden">
					<p>	{!!str_limit($spm->code,40)!!}</p>
					</div> --}}
					<div class="price">
						<b><div class="price gachngang marginright inline-block mauden">@if($spm->price_unit !=null) {{number_format($spm->price_unit)}}đ  @endif </div> @if($spm->price != 0) {{number_format($spm->price)}} đ @else Liên hệ @endif</b>
					</div>
					</a>
					<button class="nutgiohangsp bold click_themvaogio">Thêm vào giỏ hàng</button>	
					<div class="tieudedanhmuc">
						Sản phẩm mới
					</div>
				</div>
				</div>
	@endforeach
</div>
<div style="clear: both;"></div>
<div class="linexemthem">
	{{-- <div class="line-ngang"></div> --}}
	<a href="{{route('allProduct')}}">Xem thêm sản phẩm ></a>
</div>	
			</div>


@foreach($cate_home as $key => $ct)
<div class="section-sanpham">
<div class="title_danhmucsp_home">
	<div class="col-xs-12">
		<span><a href="{{route('productByCate',$ct->slug_vi)}}" class="link">{{$ct->name_vi}}</a></span>
	</div>
</div>
<div style="clear: both;"></div>
<div class="show_allsanpham">
	@php
	$sanpham = DB::table('products')->where('status',1)->where('cate_product_id', $ct->id)->orderBy('id','DESC')->take(5)->get();
	@endphp
	@foreach($sanpham  as $sp)
			<div class="col-xs-6 col-sm-25">
				<div class="baoquanh_sp" data-id="{{$sp->id}}">
					<a href="{{route('detail_product',$sp->slug_vi)}}">
						<div class="img-sanpham" style="background:url('{{asset($sp->image)}}')center no-repeat;background-size: contain;min-height: 250px;">
					</div>
					<div class="name-sanpham">
						{{str_limit($sp->name_vi,400)}}
					</div>
					{{-- <div class="thuonghieu mauden">
					<p>	{!!str_limit($sp->code,40)!!}</p>
					</div> --}}
					<div class="price">
						<b><div class="price gachngang marginright inline-block mauden">@if($sp->price_unit !=null) {{number_format($sp->price_unit)}}đ  @endif </div>@if($sp->price != 0) {{number_format($sp->price)}} đ @else Liên hệ @endif</b>
					</div>
					</a>
					<button class="nutgiohangsp bold click_themvaogio">Thêm vào giỏ hàng</button>	
					<div class="tieudedanhmuc">
						{{$ct->name_vi}}
					</div>
				</div>
				</div>
	@endforeach
</div>
<div style="clear: both;"></div>
<div class="linexemthem">
	{{-- <div class="line-ngang"></div> --}}
	<a href="{{route('productByCate',$ct->slug_vi)}}">Xem thêm sản phẩm ></a>
</div>	
			</div>
		@endforeach
