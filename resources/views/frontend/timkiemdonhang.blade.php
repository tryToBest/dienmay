@extends('frontend.partials.master')
@section('title','Tìm kiếm')
@section('canonical')
{{ URL::current() }}
@stop
@section('body')

@stop
@section('css')
<style>
    .anhquangcao{
        margin-bottom: 15px;
    }
    .title_danhmucsp_home{margin-top: 20px;}
    .main-account {
        overflow: hidden;
    }
</style>
@stop
@section('content')


<div class="all_sanpham_home">
    
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3">
            @include('frontend/partials/left')
@php
$quangcaohome = DB::table('slides')->where('status',1)->where('dislay',8)->orderBy('position','ASC')->take(1)->get();
@endphp
<div class="anhquangcao">
    @foreach($quangcaohome as $qc2)
    <a href="{{$qc2->link}}"><img src="{{asset($qc2->image)}}" alt="{{$qc2->name_vi}}"></a>
    @endforeach
</div>

        </div>
        <div class="col-xs-12 col-sm-9">
<div class="title_danhmucsp_home">
    <div class="col-xs-12">
        <span>Tìm kiếm  "{{$input}}" ({{count($donhang)}})</span>
   
    </div>
</div>

<div style="clear: both;"></div>

       
            <section class="main-account">
                <div class="">
                    <div class="welcom-acc">
                        <div class="row">
                            <div class="col-md-12">
                                <span>Cần giúp đỡ hoàn thành đơn hàng của bạn? Gọi chăm sóc khách hàng của chúng tôi
                                    theo số {{$setting->hotline}}</span>
                            </div>
                        
                        </div>
                    </div>
                    <div class="page-heading-acc">
                        <h1 class="title_danhmuc">Danh sách đơn hàng</h1>
                    </div>
              
                    <div class="personal-details">

            <div class="member_guanli">
                <div class="row">
                    <div class="col-md-12 col-xs-12 float-left">
                        <h4>Đơn hàng ({{count($donhang)}})</h4>
                    </div>
                  
                </div>

              @if(count($donhang)>0)
                        <table id="cart" class="table table-hover table-condensed">
                            <thead>
                                <tr>
                                    <th style="width: 10%;">Mã đơn hàng</th>
                                    <th >Ngày mua</th>
                                    <th >Sản phẩm</th>
                             
                                    <th >Tổng tiền</th>
                                    <th >Trạng thái đơn</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($donhang as $b)
                                <tr>
                                    <td >
                                        <div class="price">
                                            <span class="bt" style="font-size: 14px;">{{$b->orderId}}</span>
                                        </div>
                                    </td>
                                    <td >
                                        <div class="price">
                                            <span class="bt" style="font-size: 14px;">{{$b->created_at}}</span>
                                        </div>
                                    </td>

                                    <td >
                                        @php
                                        $bill_detail = DB::table('bill_details')->where('bill_id',$b->id)->get();
                                        @endphp
                                        @foreach($bill_detail as $d)
                                        <div class="row">
                                            <div class="info">
                                                @php
                                                $tim_product = DB::table('products')->where('id',$d->product_id)->first();
                                             
                                                @endphp
                                                <h4 class="nomargin">{{$tim_product->name_vi}}</h4>
                                             
                                            </div>
                                            <div class="price">
                                                <span class="bt" style="font-size: 14px;">{{number_format($d->price)}} đ</span>
                                            </div>
                                            <div>
                                                Số lượng: {{$d->quantity}}
                                            </div>
                                        </div>
                                        @endforeach
                                        
                                    </td>
                                
                                    <td >
                                        <div class="price">
                                            <span class="bt">{{number_format($b->total)}}</span>
                                        </div>
                                    </td>
                                    <td >
                                        @if($b->status == 1)
                                        Đơn mới chờ xử lý
                                        @endif

                                @if($b->status == 2) Đang xử lý @endif
                                @if($b->status == 10) Đang giao hàng @endif

                                @if($b->status == 3) Hoàn thành @endif

                                @if($b->status == 4) Hệ thống Hủy @endif
                                 @if($b->status == 5) Thất bại @endif

                                @if($b->status == 6) Khách hủy @endif

                                @if($b->status == 7) Hãng vận chuyển hủy @endif

                                @if($b->status == 8) Đang chuyển hoàn @endif
                                 @if($b->status == 9) Đã chuyển hoàn @endif
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                @else
                <p>Không tìm thấy đơn hàng nào</p>
                @endif
            </div>
        </div>
  
                </div>
            </section>


<div style="clear: both;"></div>

            
        </div>

        </div>

    </div>
</div>



@endsection
@section('script')
<script>
        $('.acnav__label').click(function () {
    var label = $(this);
    var parent = label.parent('.has-children');
    var list = label.siblings('.acnav__list');
    if ( parent.hasClass('is-open') ) {
        list.slideUp('fast');
        parent.removeClass('is-open');
    }
    else {
        list.slideDown('fast');
        parent.addClass('is-open');
    }
});
</script>
<script src='{{asset('frontend/js/slick.min.js')}}'></script>
<script  src="{{asset('frontend/js/slick2.js')}}?{{time()}}"></script>
@endsection