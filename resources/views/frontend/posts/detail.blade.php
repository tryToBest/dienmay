@extends('frontend.partials.master')
@section('title', $detail->name_vi)
@section('title_seo', $detail->title_seo_vi)
@section('meta_key', $detail->meta_key_vi)
@section('meta_des', $detail->meta_des_vi)
@section('body')
@stop
@section('css')
<style>
    .layout_blog_detail{padding-top: 20px}
.baivietlienquan{padding-bottom: 40px}
.baivietlienquan a{color: #389047;}
.baivietlienquan ul{padding-left: 20px;}
.baivietlienquan ul li{list-style-type: disc;}
.baivietlienquan ul li::maker{color:#389047; }

.toc_cap2{
  padding-left: 20px;
}
.content_detail_blog ul{
  padding-left: 20px;
}

@media(max-width: 767px){
    .content_detail_blog img{height: auto!important;}
    .baivietlienquan {
    padding: 10px 15px;
}
  .layout_blog_detail{padding-top: 5px;}
.layout_blog_detail .cot1:before{
content:"";
position:absolute;
width:100%;bottom:0;
height: 1px;background-color: #389047;
}


}
</style>
@stop
@section('content')
{{-- @include('frontend/partials/quangcaohome') --}}
{{-- @include('frontend/partials/slide2') --}}
 <section class="list-cate-product-news">
    <div class="container">
        <ul>
            <li><a href="{{route('index')}}" title="trang chủ">
                <img src="{{asset('images/icon-1.png')}}" alt="trang chủ"/></a>
            </li>
            <li><span>{{$cate_post->name_vi}}</span></li>
        </ul>
    </div>
</section>
<section class="bx-main-news">
    <div class="container">
        <div class="bx-left">
            <div class="cover-categories">
                <img src="{{asset($cate_post->image)}}" alt=""/>
            </div>
            <div class="list-cate">
                <ul>
                    @foreach($list_cate as $cate)
                    <li class="{{$cate->slug_vi == $cate_post->slug_vi ? 'active' : ''}}">
                        <a href="{{route('list_post',$cate->slug_vi)}}" title="{{$cate->name_vi}}">{{$cate->name_vi}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="bx-center">
            <div class="bx-detail-news">
                <div class="bx-breadcrum">
                    <ul>
                        <li><a href="{{route('index')}}" title="Trang chủ">Trang chủ</a></li>
                        <li><span><i class="fas fa-angle-right"></i></span></li>
                        <li><a href="{{route('list_post',$cate_post->slug_vi)}}" title="{{$cate_post->name_vi}}">{{$cate_post->name_vi}}</a></li>
                        <li><span><i class="fas fa-angle-right"></i></span></li>
                        <li><span>{{$detail->name_vi}}</span></li>
                    </ul>
                </div>
                <h1>{{$detail->name_vi}}</h1>
                <div class="content-news">{!! $detail->description_vi !!}
                </div>
                
                <div class="post-by-tag">
                    <h3>Xem thêm</h3>
                    @foreach($bvnb as $bn)
                    <a href="{{route('detail',$bn->slug_vi)}}" title="{{$bn->name_vi}}">{{$bn->name_vi}}</a>
                    @endforeach
                    
                </div>
                <div class="news-related">
                    <h3>bài viết liên quan</h3>
                    <div class="list-item">
                        @foreach($bvlq as $bv)
                        <div class="item">
                            <div class="thumb">
                                <a href="{{route('detail',$bv->slug_vi)}}" title="{{$bv->name_vi}}">
                                    <img src="{{asset($bv->image)}}" alt="{{$bv->name_vi}}"/>
                                </a>
                            </div>
                            <a href="{{route('detail',$bv->slug_vi)}}" title="{{$bv->name_vi}}" class="title">{{$bv->name_vi}}</a>
                        </div>
                        @endforeach
                        
                    </div>
                </div>
            </div>

        </div>
        <div class="bx-right">
            <div class="widget wg-news">
                <h3>BÀI VIẾT ĐƯỢC XEM NHIỀU NHẤT</h3>
                <div class="list-news">
                    @foreach($mostViewedPosts as $post)
                    <div class="item">
                        <div class="thumb">
                            <a href="{{route('detail',$post->slug_vi)}}" title="{{$post->name_vi}}">
                                <img src="{{asset($post->image)}}" alt="{{$post->name_vi}}"/>
                            </a>
                        </div>
                        <div class="title">
                            <a href="{{route('detail',$post->slug_vi)}}" title="{{$post->name_vi}}">{{$post->name_vi}}</a>
                        </div>
                    </div>
                    @endforeach 
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
<script src='{{asset('frontend/js/slick.min.js')}}'></script>
<script  src="{{asset('frontend/js/slick2.js')}}?{{time()}}"></script>

<script>

  $(document).on('click','.gotodiv', function(event) {
    event.preventDefault();
    var target = "#" + this.getAttribute('data-target');
    $('html, body').animate({
      scrollTop: $(target).offset().top-80
    }, 800);
  });


    var ToC ="<ul >";
var newLine, el, title, link ,id;
$(".content_detail_blog h2,.content_detail_blog h3").each(function() {
  el = $(this);
  title = el.text();
  id = title.toLowerCase().replace(/\s+/,'-').replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e").replace(/ì|í|ị|ỉ|ĩ/g,"i").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u").replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y").replace(/đ/g,"d").replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"-").replace(/-+-/g,"-").replace(/^\-+|\-+$/g,"");
  // link = "#" + id;
  $(this).attr('id', 'goto_'+id);
       if($(this).is("h2")){ 
  newLine =
    "<li >" +
      "<a class='gotodiv ' href='#' data-target='"+"goto_"+ id + "'>" +
        title +
      "</a>" +
    "</li>";
}else{
  newLine =
    "<li class='toc_cap2'>" +
      "<a class='gotodiv' href='#' data-target='"+"goto_"+ id + "'>" +
        title +
      "</a>" +
    "</li>";
}


  ToC += newLine;
});







ToC +=
   "</ul>";
$(".all-questions").prepend(ToC);







</script>
@endsection