@extends('frontend.partials.master')
@section('title', $cate_post->name_vi)
@section('title_seo', $cate_post->title_seo_vi)
@section('meta_key', $cate_post->meta_key_vi)
@section('meta_des', $cate_post->meta_des_vi)
@section('canonical')
{{ URL::current() }}
@stop
@section('body')

@stop
@section('css')
<style type="text/css">
    .phantrang ul li {float: left;padding:3px 10px ;margin-right: 3px;}
    .phantrang ul li.active{
        background-color: #d81313;color: #fff;
    }
     .phantrang ul li:hover{
        background-color: #d3d3d3;
     }
</style>
@stop
@section('content')
{{-- @include('frontend/partials/slide2') --}}
{{-- @include('frontend/partials/quangcaohome') --}}
<section class="list-cate-product-news">
    <div class="container">
        <ul>
            <li><a href="{{route('index')}}" title="trang chủ">
                <img src="{{asset('images/icon-1.png')}}" alt="trang chủ"/></a>
            </li>
            <li><span>{{$cate_post->name_vi}}</span></li>
        </ul>
    </div>
</section>
<section class="bx-main-news">
    <div class="container">
        <div class="bx-left">
            <div class="cover-categories">
                <img src="{{asset($cate_post->image)}}" alt=""/>
            </div>
            <div class="list-cate">
                <ul>
                    @foreach($list_cate as $cate)
                    <li class="{{$cate->slug_vi == $cate_post->slug_vi ? 'active' : ''}}">
                        <a href="{{route('list_post',$cate->slug_vi)}}" title="{{$cate->name_vi}}">{{$cate->name_vi}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="bx-center">
            <h1>{{$cate_post->name_vi}}</h1>
            <div class="list-item">
                @foreach($posts as $post)
                <div class="item">
                    <div class="thumb">
                        <a href="{{route('detail',$post->slug_vi)}}" title="{{$post->name_vi}}">
                            <img src="{{asset($post->image)}}" alt="{{$post->name_vi}}"/>
                        </a>
                    </div>
                    <div class="info">
                        <a href="{{route('detail',$post->slug_vi)}}" title="{{$post->name_vi}}" class="title">{{$post->name_vi}}</a>
                        {{-- <p>{!! mb_strimwidth($post->title_vi, 0, 100, '...') !!}</p> --}}
                    </div>
                </div>
                @endforeach
                                        
                   <div class="phantrang">
                        {{$posts->links()}}
                   </div>
                                </div>
        </div>
        <div class="bx-right">
            <div class="widget wg-news">
                <h3>BÀI VIẾT ĐƯỢC XEM NHIỀU NHẤT</h3>
                <div class="list-news">
                    @foreach($mostViewedPosts as $post)
                    <div class="item">
                        <div class="thumb">
                            <a href="{{route('detail',$post->slug_vi)}}" title="{{$post->name_vi}}">
                                <img src="{{asset($post->image)}}" alt="{{$post->name_vi}}"/>
                            </a>
                        </div>
                        <div class="title">
                            <a href="{{route('detail',$post->slug_vi)}}" title="{{$post->name_vi}}">{{$post->name_vi}}</a>
                        </div>
                    </div>
                    @endforeach 
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script src='{{asset('frontend/js/slick.min.js')}}'></script>
<script  src="{{asset('frontend/js/slick2.js')}}?{{time()}}"></script>
 
<script>
    $(function() {
  var top = $('.sidebar_scroll').offset().top - parseFloat($('.sidebar_scroll').css('marginTop').replace(/auto/, 0));
  var footTop = $('#footer').offset().top - parseFloat($('#footer').css('marginTop').replace(/auto/, 0));
  var maxY = footTop - $('.sidebar_scroll').outerHeight();

  if($(window).width() >= 768){
  $(window).scroll(function(evt) {
    var y = $(this).scrollTop();
    if (y > top) {
      if (y < maxY) {
        $('.sidebar_scroll').addClass('fixed').removeAttr('style');
        var abc = $('.sidebar_scroll').parent().width();
        $('.sidebar_scroll.fixed').css('width',abc);




      } else {
        $('.sidebar_scroll').removeClass('fixed').css({
          position: 'absolute',
          top: (maxY - top) + 'px'
        });
      }
    } else {
      $('.sidebar_scroll').removeClass('fixed');
    }
  });
  }

});
</script> 
@endsection