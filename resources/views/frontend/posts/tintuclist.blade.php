@php
$danhmuc_tt_h = DB::table('cate_posts')->where('status',1)->where('is_home',1)->orderBy('position','ASC')->get();
$tintuchome = DB::table('posts')->where('status', 1)->where('cate_post_id', 32)->orderBy('id','DESC')->paginate(20);
@endphp

<section id="tintuc">
 
       <div class="head-section bold">
            <span>{{$cate_post->name_vi}}</span>
        </div>
        <div class="content">
            <div class="row">
                @foreach($tintuchome as $tth)
                <div class="col-xs-6 col-sm-6 ">
                    <div class="border-ngoai">
                        <div class="row">
                        <div class="col-xs-12 col-md-5">
                            <a href="{{route('detail',$tth->slug_vi)}}">
                                <div class="img-news" style="background:url('{{asset($tth->image)}}')center no-repeat;background-size: cover;width: 100%; padding-top: 80%;" alt="{{$tth->name_vi}}">
                                    
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 col-md-7 ">
                            <div class="title-news ">
                               <a class="mauden bold" href="{{route('detail',$tth->slug_vi)}}"> {{$tth->name_vi}}</a>
                            </div>
                            <div class="des-news">
                                <?php
                                  $descsk = substr(strip_tags($tth->title_vi ), 0, 50);
                                  if ((strlen($tth->title_vi )  ) > 50) {
                                   // trim the description back to the last period or space so words aren't cut off
                                   $period_pos = strrpos($descsk, ".");
                                   $space_pos = strrpos($descsk, " ");
                                   // find the character that we should trim back to. -1 on space pos for a space that follows a period, so we dont end up with 4 periods
                                   if ($space_pos - 1 > $period_pos) {
                                    $pos = $space_pos;
                                   }
                                   else {
                                    $pos = $period_pos;
                                   }
                                   $descsk = substr($descsk, 0, $pos);
                                   $descsk .= "...";
                                   echo $descsk;
                                  }else echo $tth->title_vi ;
                                ?>
                            </div>
                            <a class="mauxanh bold" style="float: right;" href="{{route('detail',$tth->slug_vi)}}">Xem Chi tiết ></a>
                        </div>
                         </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
     <div class="phantrang">
    {{$tintuchome->links()}}
</div>
</section>

    <div class="line"></div>
