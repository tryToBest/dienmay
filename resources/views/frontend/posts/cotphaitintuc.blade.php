

<div class="bannerquangcao1">
	@php
$quangcaotren = DB::table('slides')->where('status',1)->where('dislay',10)->orderBy('position','ASC')->take(1)->get();
$truchotline = DB::table('uudais')->where('status',1)->orderBy('position','ASC')->get();
$bvnb = DB::table('posts')->where('status',1)->where('cate_post_id','<>',57)->where('is_home',1)->orderBy('position','ASC')->take(6)->get();
@endphp
<div class="anhquangcao">
    @foreach($quangcaotren as $qc1)
    <a href="{{$qc1->link}}">
    	<div class="img-right" style="background: url('{{asset($qc1->image)}}')center no-repeat; background-size: cover;padding-top: 50%;">	
    	</div>
    </a>
    @endforeach
</div>
</div>
<div class="bongoai-boxright">
	<div class="head-titleright">
		<p>Hỗ Trợ Trực Tuyến</p>
	</div>
	<div class="padingngoai">
@foreach($truchotline as $key =>$thl)
	<div class="content-rigt">
		<div class="leftname">
			<div class="name">{{$thl->name_vi}}</div>
			<a href="tel:{{$thl->description_vi}}"><div class="phone">{{$thl->description_vi}}</div></a>
		</div>
		<div class="phoneicon">
			<a href="tel:{{$thl->description_vi}}"><i class="fa fa-phone"></i></a>
		</div>
	</div>
	@endforeach
	
	
	</div>
</div>

<div class="bannerquangcao1">
	@php
$quangcaohome = DB::table('slides')->where('status',1)->where('dislay',8)->orderBy('position','ASC')->take(1)->get();
@endphp
<div class="anhquangcao">
    @foreach($quangcaohome as $qc2)
    <a href="{{$qc2->link}}">
    	<div class="img-right" style="background: url('{{asset($qc2->image)}}')center no-repeat; background-size: cover;padding-top: 150%;">	
    	</div>
    </a>
    @endforeach
</div>
</div>

@php
$spmoi = DB::table('products')->where('status',1)->where('is_home',1)->orderBy('position','ASC')->take(5)->get();
@endphp
<div class="bongoai-boxright2">
	<div class="head-titleright2">
		<p>CÁC SẢN PHẨM MỚI</p>
	</div>
	<div class="padingngoai">
		@foreach($spmoi as $key => $spm)
	<div class="content-rigt noflex">
	<div class="row">
		<div class="col-xs-4">
			<a href="{{route('detail_product',$spm->slug_vi)}}">
				<div class="img-sanpham " style="background:url('{{asset($spm->image)}}')center no-repeat;background-size: contain;min-height: 50px;"></div>
			</a>
		</div>
		<div class="col-xs-8">
			<a href="{{route('detail_product',$spm->slug_vi)}}" class="haidong">{{str_limit($spm->name_vi,20)}}</a>
		</div>
	</div>
	</div>
		@endforeach
	</div>
	<div class="head-titleright2 bold">
		<p>SẢN PHẨM BÁN CHẠY</p>
	</div>
	<div class="padingngoai">
		@foreach($spmoi as $key => $spm)
	<div class="content-rigt noflex">
	<div class="row colbanghaiben">
		
		<div class="col-xs-4">
			<a href="{{route('detail_product',$spm->slug_vi)}}"><div class="img-sanpham " style="background:url('{{asset($spm->image)}}')center no-repeat;background-size: contain;min-height: 50px;"></div></a>
		</div>
		<div class="col-xs-8 margingiua">
			<a href="{{route('detail_product',$spm->slug_vi)}}" class="haidong">{{str_limit($spm->name_vi,20)}}</a>
		</div>
	</div>
	</div>
		@endforeach
	</div>
</div>


<div class="bongoai-boxright2 sidebar_scroll">
	<div class="head-titleright2">
		<p>BÀI VIẾT NỔI BẬT</p>
	</div>
	<div class="padingngoai">
		@foreach($bvnb as $key => $bvnb)
	<div class="content-rigt">
	<div class="row colbanghaiben">
		<div class="col-xs-4">
			<a href="{{route('detail',$bvnb->slug_vi)}}"><div class="img-sanpham hieuungzoom_bg" style="background:url('{{asset($bvnb->image)}}')center no-repeat;background-size: contain;min-height: 50px;"></div></a>
		</div>
		<div class="col-xs-8 margingiua">
			<a href="">{{$bvnb->name_vi}}</a>
		</div>
	
	</div>
	</div>
		@endforeach
	</div>
	
</div>