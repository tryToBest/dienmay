<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=	, initial-scale=	">
	<title>	Get product Info</title>
</head>
<body>
	@if(session('success'))
	    <div class="alert alert-success">
	        {{ session('success') }}
	    </div>
	@endif

	<div class="container">
		danh sách các id các danh mục sản phẩm
		<ul>	
			@foreach($cate_products as $cate)
			<li>Danh mục: {{$cate->name_vi}} có id: {{$cate->id}}</li>
			@endforeach
		</ul>
	</div>
		<div class="container">
			<h3>update bộ lọc</h3>	
			<form action="{{route('product.updateboloc')}}" method="get"	>
				<input type="text" name="boloc_id" required placeholder="nhập vào id bộ lọc">		
				<input type="text" name="name_vi" placeholder="name_vi">		
				<input type="text" name="thongtin_vi" placeholder="thongtin_vi">		
				<input type="number" name="min" placeholder="giá trị bé">		
				<input type="number" name="max" placeholder="giá trị lớn">		
				<button type="submit">Ấn vào đây nè</button>	
			</form>
		</div>

		<div class="container">
			<h3>Lấy thông tin dung tích hoặc cái gì đó tương tự</h3>	
			<form action="{{route('product.getDungtich')}}" method="get">
				<input type="text" name="id" placeholder="Nhập vào id của loại danh mục">	
				<button type="submit">Ấn vào đây nè</button>	
			</form>
		</div>
		
		<div class="container">
			<h3>Lấy tất cả các sản phẩm của từng danh mục</h3>	
			<form action="{{route('product.getProductInfo')}}" method="get"	>		
				<button type="submit">Ấn vào đây nè</button>	
			</form>
		</div>	
		<div class="container">
			<h3>Lấy tất cả các sản phẩm trong ti nè</h3>	
			<form action="{{route('product.getTivi')}}" method="get"	>		
				<button type="submit">Ấn vào đây nè</button>	
			</form>
		</div>	
		<div class="container">
			<h3>Lấy tất cả các sản phẩm trong ti nè</h3>	
			<form action="{{route('product.updateInfoProduct')}}" method="get"	>		
				<button type="submit">Ấn vào đây nè</button>	
			</form>
		</div>	
		<div class="container">
			<h3>gắn id sản phẩm vào ảnh</h3>	
			<form action="{{route('product.ganImage')}}" method="get"	>		
				<button type="submit">Ấn vào đây nè</button>	
			</form>
		</div>	
		<div class="container">
			<h3>Đổi giá sản phẩm</h3>	
			<form action="{{route('product.swapPrice')}}" method="get"	>		
				<button type="submit">Ấn vào đây nè</button>	
			</form>
		</div>	
		<div class="container">
			<h3>Gán id category</h3>	
			<form action="{{route('product.ganCateProduct')}}" method="get"	>	
				<button type="submit">Ấn vào đây nè</button>	
			</form>
		</div>	
		<div class="container">
			<h3>lấy id sản phẩm không có ảnh</h3>	
			<form action="{{route('product.fixImage')}}" method="get"	>	
				<button type="submit">Ấn vào đây nè</button>	
			</form>
		</div>	
			
		
</body>
</html>									