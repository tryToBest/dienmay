<?php



    return [

        'home'               =>'Home',

        'sanpham'            =>'Product',

        'tintuc'             =>'NEWS',

        'vechungtoi'         =>'About us',

        'lienhe'             =>'Contact',

        'timkiem'            =>'Search',

        'gia'                =>'Price',

        'ketquatimkiem'      =>'Search results with keywords',

        'khongtimthayketqua' =>'No results were found with keywords',

        'chungtoinuoithucung' =>'We keep pets for pleasure.',

        'thucan' =>'Food & Vitamins',

        'chotatca' =>'For all Pets',

        'muangay' => 'Buy now',
   'xemthem' => 'Read more',
        'thucpham' => 'Food',


                'chitiet' => 'Detail',


        'themvaogio' => 'Add to cart',

        'timkiemsanpham' => 'Search products',

        'price' => 'Price',

        'timtheogia' => 'Search by price',

        'nhaptensanpham' => 'Enter the product name',

        'conhang' => 'In stock',

        'masanpham' => 'Product SKU',

        'themvaogiohang' => 'Add to carts',

        'mieuta' => 'Description',

        'thongtinsanpham' => 'Product information',

        'danhgia' => 'Evaluation',

        'sanphamlienquan' => 'Related products',

        'danhgia' => 'Evaluation',

        'hoten' => 'Name',

        'xacnhan' => 'Confirm',

        'gioithieu' => 'About Us',

        'tongcong' => 'Total',

        'giohang' => 'Shopping cart',

        'muangay' => 'Buy now',

        'tensanpham' => 'Product name',

        'soluong' => 'Quantity',

        'anh' => 'Image',

        'tong' => 'Sum',

        'giohangrong' => 'The shopping cart is empty',

        'tieptucmuahang' => 'Continue shopping',

        'sotienthanhtoan' => 'Payment amount',

        'muahang' => 'Purchase',

        'sodienthoai' => 'Phone number',

        'diachi' => 'Address',

        'ghichu' => 'Notes',

        'hinhthucthanhtoan' => 'Form of payment',

        'xacnhan' => 'Confirm',

        'thongtinmuahang' => 'Purchase information',

        'taikhoan' => 'Account',

        'dangky' => 'Sign up',

        'dangnhap' => 'Login',

        'matkhau' => 'Password',

        'ghinho' => 'Remember my account',

        'quenmatkhau' => 'Forgot password',

        'xacnhanmatkhau' => 'Confirm password',

        'dangxuat' => 'Log out',

        'luu' => 'Save',

        'matkhaumoi' => 'New password',

        'sanphamtotnhat' => 'Best products',

        'giamgiatrongtuan' => 'Deal of the Week',

        'danhmuc' => 'Category',

        'theodoi' => 'Sign up for the latest news',

        'gui' => 'Submit',

        'taisaochon' => 'Why Choose Petchoy',

        'traicayvarau' => 'Fruits and vegetables',

        'bencanhdai' => 'Besides great service and gentle care for your pet, here are some great reasons!',

        'thanhphandocdao' => '_____ Unique ingredients from Australia',

        'mieutathanhphan' => 'Besides great service and gentle care for your pet, here are some great reasons!',

        'sanphamnoibat' => 'Most popupler',

        'thongtintaikhoan' => 'Account information',

        'lichsumuahang' => 'Purchase history',

        'danhsachyeuthich' => 'Favorites',

        'ngaydat' => 'Booking date',

        'tongtien' => 'Total amount',

        'trangthai' => 'Status',

        'choxuly' => 'Pending',

        'dangxuly' => 'Processing',

        'hoanthanh' => 'Done',

        'huy' => 'Cancel',

        'dienthoai' => 'Phone',

        'noidung' => 'Content',



        'taotkmoi' =>'Create a Pet Choy member account with the above information?',

        'dangkytc' =>'Thank you for registering an account successfully with your username:',

        'luuy' =>'Uppercase password, minimum 6 characters, no special characters',

        'ctct' =>'Program details',
        'ctuudai' =>'Promotion of the month',


    ]



?>