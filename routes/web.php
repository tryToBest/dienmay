<?php
Route::group(['middleware' => ['web','locale']], function() {
    Auth::routes();
    Route::get('language/{locale}','Frontend\HomeController@changeLanguage');
    Route::get('admin/login', 'Admin\LoginController@getLogin')->name('admin.getLogin');
    Route::post('admin/login', 'Admin\LoginController@postLogin')->name('admin.postLogin');
    Route::get('/', 'Frontend\HomeController@index')->name('index');
    Route::get('trang-chu.html', 'Frontend\HomeController@trangchu')->name('trangchu');
    Route::get('gioi-thieu', 'Frontend\IntroController@index')->name('intro');
    Route::get('lien-he', 'Frontend\ContactController@index')->name('contact');
    Route::get('video', 'Frontend\ContactController@video')->name('video');
    Route::get('doi-tac', 'Frontend\ContactController@doitac')->name('doitac');
    Route::post('lien-he', 'Frontend\ContactController@postContact')->name('post_contact');
    Route::get('traloicomment', 'Frontend\HomeController@traloicomment')->name('traloicomment');
    Route::post('/commentsp', 'Frontend\HomeController@commentsp')->name('commentsp');
    Route::get('detail-tintuc', 'Frontend\PostController@ajax_detail_tintuc')->name('ajax_detail_tintuc');
    Route::get('detail-sanpham', 'Frontend\ProductController@ajax_product_detail')->name('ajax_product_detail');
    Route::get('tongquan-sanpham', 'Frontend\ProductController@ajax_load_tongquan')->name('ajax_load_tongquan');

    Route::get('thongsokythuat-sanpham', 'Frontend\ProductController@ajax_load_thongsokythuat')->name('ajax_load_thongsokythuat');
    Route::get('tinhnang-sanpham', 'Frontend\ProductController@ajax_load_tinhnang')->name('ajax_load_tinhnang');

    Route::get('chinh-sach', 'Frontend\ContactController@chinhsach')->name('chinhsach');
    Route::get('thu-vien', 'Frontend\ContactController@thuvien')->name('thuvien');
    Route::get('search', 'Frontend\SearchController@postSearch')->name('postSearch');
    Route::get('get-state-list','Frontend\CartController@getStateList')->name('getStateList');
    Route::get('get-city-list','Frontend\CartController@getCityList')->name('getCityList');
    Route::get('gancate_boloc_id/{id}','Admin\ProductController@getProductByIdCate')->name('admin.product.ganCatebolocid');
    Route::get('unlink-mo-ta','Frontend\ProductController@unlinkDes')->name('unlink');
    Route::post('unlink-mo-ta','Frontend\ProductController@updateUnlinkDes')->name('updateUnlink');


    Route::get('tim-kiem-don-hang', 'Frontend\SearchController@timkiemdonhang')->name('timkiemdonhang');

Route::get('api-caodata', 'Frontend\SearchController@caodata')->name('caodata');


    Route::group(['prefix' => 'san-pham'], function() {
        Route::get('/', 'Frontend\ProductController@allProduct')->name('allProduct');
        Route::get('khuyen-mai', 'Frontend\ProductController@spkm')->name('spkm');
        Route::get('duy-nhat', 'Frontend\ProductController@spdm')->name('spdm');
        Route::get('hot-sale', 'Frontend\ProductController@hotsale')->name('hotsale');
        Route::get('tim-ten', 'Frontend\ProductController@timten')->name('timten');
        Route::get('productsCat','Frontend\ProductController@productsCat')->name('productsCat');
        Route::get('{cateSlug}/{slug?}', 'Frontend\ProductController@productByCate')->name('productByCate'); 
        // Route::get('/detail/{slug}', 'Frontend\ProductController@detailProduct')->name('detailProduct'); 
        Route::get('/thuonghieu/{id}', 'Frontend\ProductController@thuonghieu')->name('thuonghieu');
    });
    Route::get('/product/info-product','Frontend\ProductController@productInfo')->name('product.productInfo');
    Route::get('/product/get-info-product','Frontend\ProductController@getProductInfo')->name('product.getProductInfo');
    Route::get('/product/get-info-tivi','Frontend\ProductController@getTivi')->name('product.getTivi');
    Route::get('/product/post-info-tivi','Frontend\ProductController@updateInfoProduct')->name('product.updateInfoProduct');
    Route::get('/product/post-info-image','Frontend\ProductController@ganImage')->name('product.ganImage');
    Route::get('/product/post-info-ganCateProduct','Frontend\ProductController@ganCateProduct')->name('product.ganCateProduct');
    Route::get('/product/swapPrice','Frontend\ProductController@swapPrice')->name('product.swapPrice');
    Route::get('/product/updateboloc','Frontend\ProductController@updateboloc')->name('product.updateboloc');
    Route::get('/product/getDungtich','Frontend\ProductController@getDungtich')->name('product.getDungtich');
    Route::get('/product/getIdProductNoImage','Frontend\ProductController@fixImage')->name('product.fixImage');

    Route::group(['prefix' => 'danh-muc'], function() {
            // Route::get('/', 'Frontend\PostController@allpost')->name('allpost');
        Route::get('/{slug}', 'Frontend\PostController@listPost')->name('list_post');
        Route::get('/detail/{slug}', 'Frontend\PostController@detail')->name('detail');
    });

    Route::group(['prefix' => 'dich-vu'], function() {
       Route::get('/{slug}', 'Frontend\Post3Controller@list_dichvu')->name('list_dichvu');
   });

    Route::group(['prefix' => 'du-an'], function() {
        Route::get('/{slug}', 'Frontend\Post2Controller@list_duan')->name('list_duan');
        Route::get('/chi-tiet-du-an/{slug}', 'Frontend\Post2Controller@detail_duan')->name('detail_duan');
    });
    
    Route::group(['prefix' => 'video'], function() {
        Route::get('/', 'Frontend\ContactController@video')->name('video');
    });

    Route::group(['prefix' => 'cart'], function() {
        Route::get('/success/{orderId}', 'Frontend\CartController@thanhcong')->name('thanhcong');
        // Route::get('apn', 'Frontend\CartController@apn')->name('apn');
        Route::get('/', 'Frontend\CartController@index')->name('cart.index');
        Route::get('themvaogiohang', 'Frontend\CartController@themvaogiohang')->name('themvaogiohang');
        Route::get('add-cart/{id}', 'Frontend\CartController@addCart')->name('add-cart');
        Route::post('add-cart', 'Frontend\CartController@addCart2')->name('add-cart2');
        Route::post('add-cart3', 'Frontend\CartController@addCart3')->name('add-cart3');
        Route::get('mua-ngay/{id}', 'Frontend\CartController@muangay')->name('muangay');
        Route::get('destroy-cart/{id}', 'Frontend\CartController@destroyCart')->name('destroy_cart');
        Route::get('update', 'Frontend\CartController@updateCart')->name('updateCart');
        Route::get('order', 'Frontend\CartController@order')->name('order');
        Route::get('order/{id}', 'Frontend\CartController@order_giamgia')->name('order_giamgia');
        Route::post('order', 'Frontend\CartController@postOrder')->name('postOrder');
        Route::post('voucher', 'Frontend\CartController@giamgia')->name('giamgia');
    });
    Route::group(['prefix' => 'review'], function() {
        Route::post('/create', 'Admin\SizeController@postCreate')->name('admin.size.createPost');
    });
    Route::get('register/verify/{confirmationCode}', [
        'as' => 'confirmation_path',
        'uses' => 'Frontend\HomeController@confirm'
    ]);

    Route::get('/register_dk', 'Frontend\HomeController@register_dk')->name('register_dk');
    Route::post('/dangky', 'Frontend\HomeController@dangky')->name('dangky');
    Route::post('/dangnhap', 'Frontend\HomeController@dangnhap')->name('dangnhap');
    Route::get('/{slug}', 'Frontend\ProductController@detailProduct')->name('detail_product');
});
Route::group(['middleware' => ['userLogin', 'web']], function() {
 Route::get('/tai-khoan', 'Frontend\HomeController@taikhoan')->name('taikhoan');

 Route::get('/donhang-hopdong-hotro', 'Frontend\HomeController@donhang_hopdong_hotro')->name('donhang_hopdong_hotro');


 Route::get('/chi-tiet-don/{id}', 'Frontend\HomeController@chitietdon')->name('chitietdon');
 Route::post('/thaydoitaikhoan', 'Frontend\HomeController@thaydoitaikhoan')->name('thaydoitaikhoan');
 Route::get('/yeuthich/{id}', 'Frontend\HomeController@themyeuthich')->name('themyeuthich');
});



// admin

Route::group(['middleware' => ['adminLogin', 'web']], function() {	


    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    
    Route::any('connector', 'Admin\CkfinderController@connector')->name('kakaka');


    Route::group(['prefix' => 'admin'], function() {
       Route::group(['prefix' => 'ckfinder'], function() {
        Route::get('ckfinder-customer', 'Admin\CkfinderController@ckfinderView')->name('ckfinder-customer');
    });
       Route::get('/home', function () {return view('admin/index');})->name('admin.index');
       Route::get('/logout', 'Admin\LoginController@logout')->name('admin.logout');
       Route::group(['prefix' => 'cart'], function() {
        Route::get('bill', 'Admin\OrderController@order')->name('admin.order');
        Route::get('bill/{id}', 'Admin\OrderController@bill')->name('admin.bill');
        Route::get('order/destroy/{id}', 'Admin\OrderController@destroyOrder')->name('admin.destroyOrder');
        Route::post('postStatus/{id}', 'Admin\OrderController@postStatus')->name('admin.postStatus');
        Route::post('/checkbox', 'Admin\OrderController@checkbox')->name('admin.cart.checkbox');
        Route::get('xuat-excel', 'Admin\OrderController@export')->name('xuat_excel');
    });
       Route::group(['prefix' => 'cate_product'], function() {
        Route::get('/', 'Admin\CateProductController@index')->name('admin.cate_product.home');
        Route::get('/create', 'Admin\CateProductController@create')->name('admin.cate_product.create');
        Route::post('/create', 'Admin\CateProductController@postCreate')->name('admin.cate_product.createPost');
        Route::get('/update/{id}', 'Admin\CateProductController@update')->name('admin.cate_product.update');
        Route::post('/update/{id}', 'Admin\CateProductController@postUpdate')->name('admin.cate_product.postUpdate');
        Route::get('/destroy/{id}', 'Admin\CateProductController@destroy')->name('admin.cate_product.destroy');
        Route::post('/status', 'Admin\CateProductController@status')->name('admin.cate_product.status');
        Route::post('/is_home', 'Admin\CateProductController@is_home')->name('admin.cate_product.is_home');
    });
       Route::group(['prefix' => 'product'], function() {
        Route::get('/', 'Admin\ProductController@index')->name('admin.product.index');
        Route::get('/create', 'Admin\ProductController@create')->name('admin.product.create');
        Route::post('/get-boloc', 'Admin\ProductController@getboloc')->name('admin.product.getboloc');
        Route::post('/create', 'Admin\ProductController@postCreate')->name('admin.product.createPost');
        Route::get('/update/{slug}', 'Admin\ProductController@update')->name('admin.product.update');
        Route::post('/update/{slug}', 'Admin\ProductController@postUpdate')->name('admin.product.postUpdate');
        Route::get('/destroy/{id}', 'Admin\ProductController@destroy')->name('admin.product.destroy');
        Route::get('/search', 'Admin\ProductController@search')->name('admin.product.search');
        Route::post('/checkbox', 'Admin\ProductController@checkbox')->name('checkbox');
        Route::post('/status', 'Admin\ProductController@status')->name('admin.product.status');
        Route::post('/is_home', 'Admin\ProductController@is_home')->name('admin.product.is_home');
        Route::post('/is_hot', 'Admin\ProductController@is_hot')->name('admin.product.is_hot');
        Route::post('/is_sale', 'Admin\ProductController@is_sale')->name('admin.product.is_sale');
        Route::post('/is_khongsosanh', 'Admin\ProductController@is_khongsosanh')->name('admin.product.is_khongsosanh');
        Route::get('delImage','Admin\ProductController@delImage')->name('admin.product.dellimg');
        Route::post('import','Admin\ProductController@import')->name('import');
    });
        Route::get('/spnoibat', 'Admin\ProductController@spnoibat')->name('admin.spnoibat.index');
        Route::post('/spnoibat', 'Admin\ProductController@updatePositionHot')->name('admin.product.positionSpHot');
        Route::get('/danh-sach-da-xoa','Admin\ProductController@trashIndex')->name('admin.trash.index');
        Route::post('khoi-phuc/checkbox/danh-sach-da-xoa','Admin\ProductController@checkboxTrash')->name('admin.trash.checkbox');
        Route::post('/da-xoa','Admin\ProductController@destroyProduct')->name('admin.trash.destroy');
        Route::post('/khoi-phuc-sp','Admin\ProductController@recoverProduct')->name('admin.trash.recover');
        // flash sale
        Route::group(['prefix'=>'flash-sale','namespace'=>'Admin'], function(){
            Route::get('/','ProductController@flashsaleindex')->name('admin.flashsale.setting');
            Route::post('/update','ProductController@flashsaleUpdate')->name('admin.flashsale.update');
            Route::post('/searchByName','ProductController@searchByName')->name('admin.flashsale.searchByName');
            Route::post('/searchByCateProductName','ProductController@searchByCateProductName')->name('admin.flashsale.searchByCateProductName');
        });

        Route::group(['prefix' => 'faq'], function() {
            Route::get('/', 'Admin\FaqController@index')->name('admin.faq.home');
            Route::get('/create', 'Admin\FaqController@create')->name('admin.faq.create');
            Route::post('/create', 'Admin\FaqController@postCreate')->name('admin.faq.createPost');
            Route::get('/update/{slug}', 'Admin\FaqController@update')->name('admin.faq.update');
            Route::post('/update/{slug}', 'Admin\FaqController@postUpdate')->name('admin.faq.postUpdate');
            Route::get('/destroy/{id}', 'Admin\FaqController@destroy')->name('admin.faq.destroy');
            Route::post('/status', 'Admin\FaqController@status')->name('admin.faq.status');
            
        });
       Route::group(['prefix' => 'cate_post'], function() {
        Route::get('/', 'Admin\CatePostController@index')->name('admin.cate_post.home');
        Route::get('/create', 'Admin\CatePostController@create')->name('admin.cate_post.create');
        Route::post('/create', 'Admin\CatePostController@postCreate')->name('admin.cate_post.createPost');
        Route::get('/update/{slug}', 'Admin\CatePostController@update')->name('admin.cate_post.update');
        Route::post('/update/{slug}', 'Admin\CatePostController@postUpdate')->name('admin.cate_post.postUpdate');
        Route::get('/destroy/{id}', 'Admin\CatePostController@destroy')->name('admin.cate_post.destroy');
        Route::post('/status', 'Admin\CatePostController@status')->name('admin.cate_post.status');
        Route::post('/menu', 'Admin\CatePostController@menu')->name('admin.cate_post.menu');
        Route::post('/ft', 'Admin\CatePostController@ft')->name('admin.cate_post.ft');
        Route::post('/is_home', 'Admin\CatePostController@is_home')->name('admin.cate_post.is_home');
    });
       
       Route::group(['prefix' => 'cate_ttbh'], function() {
        Route::get('/', 'Admin\CatettbhController@index')->name('admin.cate_ttbh.home');
        Route::get('/create', 'Admin\CatettbhController@create')->name('admin.cate_ttbh.create');
        Route::post('/create', 'Admin\CatettbhController@postCreate')->name('admin.cate_ttbh.createPost');
        Route::get('/update/{slug}', 'Admin\CatettbhController@update')->name('admin.cate_ttbh.update');
        Route::post('/update/{slug}', 'Admin\CatettbhController@postUpdate')->name('admin.cate_ttbh.postUpdate');
        Route::get('/destroy/{id}', 'Admin\CatettbhController@destroy')->name('admin.cate_ttbh.destroy');
        Route::post('/status', 'Admin\CatettbhController@status')->name('admin.cate_ttbh.status');
        Route::post('/menu', 'Admin\CatettbhController@menu')->name('admin.cate_ttbh.menu');
        Route::post('/ft', 'Admin\CatettbhController@ft')->name('admin.cate_ttbh.ft');
        Route::post('/is_home', 'Admin\CatettbhController@is_home')->name('admin.cate_ttbh.is_home');
    });


       Route::group(['prefix' => 'post'], function() {
        Route::get('/', 'Admin\PostController@index')->name('admin.post.index');
        Route::get('/create', 'Admin\PostController@create')->name('admin.post.create');
        Route::post('/create', 'Admin\PostController@postCreate')->name('admin.post.createPost');
        Route::get('/update/{slug}', 'Admin\PostController@update')->name('admin.post.update');
        Route::post('/update/{slug}', 'Admin\PostController@postUpdate')->name('admin.post.postUpdate');
        Route::get('/destroy/{id}', 'Admin\PostController@destroy')->name('admin.post.destroy');
        Route::get('/search', 'Admin\PostController@search')->name('admin.post.search');
        Route::post('/checkbox', 'Admin\PostController@checkbox')->name('post.checkbox');
        Route::post('/status', 'Admin\PostController@status')->name('admin.post.status');
        Route::post('/is_home', 'Admin\PostController@is_home')->name('admin.post.is_home');
        Route::post('/footer1', 'Admin\PostController@footer1')->name('admin.post.footer1');
        Route::post('/footer2', 'Admin\PostController@footer2')->name('admin.post.footer2');
    });
       Route::group(['prefix' => 'dich-vu'], function() {
           Route::group(['prefix' => 'cate_post3'], function() {
            Route::get('/', 'Admin\CatePost3Controller@index')->name('admin.cate_post3.home');
            Route::get('/create', 'Admin\CatePost3Controller@create')->name('admin.cate_post3.create');
            Route::post('/create', 'Admin\CatePost3Controller@post3Create')->name('admin.cate_post3.createPost3');
            Route::get('/update/{slug}', 'Admin\CatePost3Controller@update')->name('admin.cate_post3.update');
            Route::post('/update/{slug}', 'Admin\CatePost3Controller@post3Update')->name('admin.cate_post3.post3Update');
            Route::get('/destroy/{id}', 'Admin\CatePost3Controller@destroy')->name('admin.cate_post3.destroy');
            Route::post('/status', 'Admin\CatePost3Controller@status')->name('admin.cate_post3.status');
            Route::post('/menu', 'Admin\CatePost3Controller@menu')->name('admin.cate_post3.menu');
            Route::post('/ft', 'Admin\CatePost3Controller@ft')->name('admin.cate_post3.ft');
            Route::post('/is_home', 'Admin\CatePost3Controller@is_home')->name('admin.cate_post3.is_home');
        });

           Route::group(['prefix' => 'post3'], function() {
            Route::get('/', 'Admin\Post3Controller@index')->name('admin.post3.index');
            Route::get('/create', 'Admin\Post3Controller@create')->name('admin.post3.create');
            Route::post('/create', 'Admin\Post3Controller@post3Create')->name('admin.post3.createPost3');
            Route::get('/update/{slug}', 'Admin\Post3Controller@update')->name('admin.post3.update');
            Route::post('/update/{slug}', 'Admin\Post3Controller@post3Update')->name('admin.post3.post3Update');
            Route::get('/destroy/{id}', 'Admin\Post3Controller@destroy')->name('admin.post3.destroy');
            Route::get('/search', 'Admin\Post3Controller@search')->name('admin.post3.search');
            Route::post('/checkbox', 'Admin\Post3Controller@checkbox')->name('post3.checkbox');
            Route::post('/status', 'Admin\Post3Controller@status')->name('admin.post3.status');
            Route::post('/is_home', 'Admin\Post3Controller@is_home')->name('admin.post3.is_home');
            Route::post('/sinhnhat_home', 'Admin\Post3Controller@sinhnhat_home')->name('admin.post3.sinhnhat_home');
            Route::post('/tvnb', 'Admin\Post3Controller@tvnb')->name('admin.post3.tvnb');
            
        }); 

       });
 Route::group(['prefix' => 'du-an'], function() {
         Route::group(['prefix' => 'cate_post2'], function() {
            Route::get('/', 'Admin\CatePost2Controller@index')->name('admin.cate_post2.home');
            Route::get('/create', 'Admin\CatePost2Controller@create')->name('admin.cate_post2.create');
            Route::post('/create', 'Admin\CatePost2Controller@post2Create')->name('admin.cate_post2.createPost2');
            Route::get('/update/{slug}', 'Admin\CatePost2Controller@update')->name('admin.cate_post2.update');
            Route::post('/update/{slug}', 'Admin\CatePost2Controller@post2Update')->name('admin.cate_post2.post2Update');
            Route::get('/destroy/{id}', 'Admin\CatePost2Controller@destroy')->name('admin.cate_post2.destroy');
            Route::post('/status', 'Admin\CatePost2Controller@status')->name('admin.cate_post2.status');
            Route::post('/menu', 'Admin\CatePost2Controller@menu')->name('admin.cate_post2.menu');
            Route::post('/ft', 'Admin\CatePost2Controller@ft')->name('admin.cate_post2.ft');
            Route::post('/is_home', 'Admin\CatePost2Controller@is_home')->name('admin.cate_post2.is_home');
        });
        Route::group(['prefix' => 'post2'], function() {
            Route::get('/', 'Admin\Post2Controller@index')->name('admin.post2.index');
            Route::get('/create', 'Admin\Post2Controller@create')->name('admin.post2.create');
            Route::post('/create', 'Admin\Post2Controller@post2Create')->name('admin.post2.createPost2');
            Route::get('/update/{slug}', 'Admin\Post2Controller@update')->name('admin.post2.update');
            Route::post('/update/{slug}', 'Admin\Post2Controller@post2Update')->name('admin.post2.post2Update');
            Route::get('/destroy/{id}', 'Admin\Post2Controller@destroy')->name('admin.post2.destroy');
            Route::get('/search', 'Admin\Post2Controller@search')->name('admin.post2.search');
            Route::post('/checkbox', 'Admin\Post2Controller@checkbox')->name('post2.checkbox');
            Route::post('/status', 'Admin\Post2Controller@status')->name('admin.post2.status');
            Route::post('/is_home', 'Admin\Post2Controller@is_home')->name('admin.post2.is_home');
            Route::post('/sinhnhat_home', 'Admin\Post2Controller@sinhnhat_home')->name('admin.post2.sinhnhat_home');
                Route::post('/tvnb', 'Admin\Post2Controller@tvnb')->name('admin.post2.tvnb');
                Route::post('importthanhvien','Admin\Post2Controller@importthanhvien')->name('importthanhvien');
        });     
                 });           
        Route::group(['prefix' => 'intro'], function() {
            Route::get('/', 'Admin\IntroController@index')->name('admin.intro.index');
            Route::get('/create', 'Admin\IntroController@create')->name('admin.intro.create');
            Route::post('/create', 'Admin\IntroController@postCreate')->name('admin.intro.createPost');
            Route::get('/update/{slug}', 'Admin\IntroController@update')->name('admin.intro.update');
            Route::post('/update/{slug}', 'Admin\IntroController@postUpdate')->name('admin.intro.postUpdate');
            Route::get('/destroy/{slug}', 'Admin\IntroController@destroy')->name('admin.intro.destroy');
            Route::post('/status', 'Admin\IntroController@status')->name('admin.intro.status');
        });
         Route::group(['prefix' => 'comment'], function() {
            Route::get('/', 'Admin\CommentController@index')->name('admin.comment.index');
   
            Route::get('/destroy/{id}', 'Admin\CommentController@destroy')->name('admin.comment.destroy');
            Route::post('/status', 'Admin\CommentController@status')->name('admin.comment.status');
        });
        Route::group(['prefix' => 'uudai'], function() {
            Route::get('/', 'Admin\UudaiController@index')->name('admin.uudai.index');
            Route::get('/create', 'Admin\UudaiController@create')->name('admin.uudai.create');
            Route::post('/create', 'Admin\UudaiController@postCreate')->name('admin.uudai.createPost');
            Route::get('/update/{slug}', 'Admin\UudaiController@update')->name('admin.uudai.update');
            Route::post('/update/{slug}', 'Admin\UudaiController@postUpdate')->name('admin.uudai.postUpdate');
            Route::get('/destroy/{slug}', 'Admin\UudaiController@destroy')->name('admin.uudai.destroy');
            Route::post('/status', 'Admin\UudaiController@status')->name('admin.uudai.status');
        });
        Route::group(['prefix' => 'contact'], function() {
            Route::get('/', 'Admin\ContactController@index')->name('admin.contact.index');
            Route::get('/destroy/{id}', 'Admin\ContactController@destroy')->name('admin.contact.destroy');
        });
        Route::group(['prefix' => 'slide'], function() {
            Route::get('/', 'Admin\SlideController@index')->name('admin.slide.index');
            Route::get('/create', 'Admin\SlideController@create')->name('admin.slide.create');
            Route::post('/create', 'Admin\SlideController@postCreate')->name('admin.slide.createPost');
            Route::get('/update/{id}', 'Admin\SlideController@update')->name('admin.slide.update');
            Route::post('/update/{id}', 'Admin\SlideController@postUpdate')->name('admin.slide.postUpdate');
            Route::get('/destroy/{id}', 'Admin\SlideController@destroy')->name('admin.slide.destroy');
            Route::post('/status', 'Admin\SlideController@status')->name('admin.slide.status');
        });
        Route::group(['prefix' => 'banner'], function() {
            Route::get('/', 'Admin\BannerController@index')->name('admin.banner.index');
            Route::get('/create', 'Admin\BannerController@create')->name('admin.banner.create');
            Route::post('/create', 'Admin\BannerController@postCreate')->name('admin.banner.createPost');
            Route::get('/update/{id}', 'Admin\BannerController@update')->name('admin.banner.update');
            Route::post('/update/{id}', 'Admin\BannerController@postUpdate')->name('admin.banner.postUpdate');
            Route::get('/destroy/{id}', 'Admin\BannerController@destroy')->name('admin.banner.destroy');
            Route::post('/status', 'Admin\BannerController@status')->name('admin.banner.status');
        });
        Route::group(['prefix' => 'gioi-thieu'], function() {
            Route::get('/', 'Admin\GioithieuController@index')->name('admin.gioithieu.index');
            Route::get('/create', 'Admin\GioithieuController@create')->name('admin.gioithieu.create');
            Route::post('/create', 'Admin\GioithieuController@postCreate')->name('admin.gioithieu.createPost');
            Route::get('/update/{id}', 'Admin\GioithieuController@update')->name('admin.gioithieu.update');
            Route::post('/update/{id}', 'Admin\GioithieuController@postUpdate')->name('admin.gioithieu.postUpdate');
            Route::get('/destroy/{id}', 'Admin\GioithieuController@destroy')->name('admin.gioithieu.destroy');
            Route::post('/status', 'Admin\GioithieuController@status')->name('admin.gioithieu.status');
        });
          Route::group(['prefix' => 'album-anh'], function() {
            Route::get('/', 'Admin\AlbumController@index')->name('admin.album.index');
            Route::get('/create', 'Admin\AlbumController@create')->name('admin.album.create');
            Route::post('/create', 'Admin\AlbumController@postCreate')->name('admin.album.createPost');
            Route::get('/update/{id}', 'Admin\AlbumController@update')->name('admin.album.update');
            Route::post('/update/{id}', 'Admin\AlbumController@postUpdate')->name('admin.album.postUpdate');
            Route::get('/destroy/{id}', 'Admin\AlbumController@destroy')->name('admin.album.destroy');
            Route::post('/status', 'Admin\AlbumController@status')->name('admin.album.status');
        });
        Route::group(['prefix' => 'administrator'], function() {
            Route::get('/', 'Admin\LoginController@index')->name('admin.administrator.home');
            Route::get('/anyData', 'Admin\LoginController@anyData')->name('anyData');
            Route::get('/create', 'Admin\LoginController@create')->name('admin.administrator.create');
            Route::post('/create', 'Admin\LoginController@postCreate')->name('admin.administrator.createPost');
            Route::get('/update/{id}', 'Admin\LoginController@update')->name('admin.administrator.update');
            Route::post('/update/{id}', 'Admin\LoginController@postUpdate')->name('admin.administrator.postUpdate');
            Route::get('/destroy/{id}', 'Admin\LoginController@destroy')->name('admin.administrator.destroy');
        });
        Route::get('guimail', 'Admin\SettingController@guimail')->name('admin.guimail');
        Route::post('guimail/update', 'Admin\SettingController@updatemail')->name('admin.guimail.update');
        Route::get('setting', 'Admin\SettingController@index')->name('admin.setting');
        Route::post('setting/update', 'Admin\SettingController@update')->name('admin.setting.update');
        Route::group(['prefix' => 'size'], function() {
            Route::get('/', 'Admin\SizeController@index')->name('admin.size.index');
            Route::get('/create', 'Admin\SizeController@create')->name('admin.size.create');
            Route::get('/update/{id}', 'Admin\SizeController@update')->name('admin.size.update');
            Route::post('/update/{id}', 'Admin\SizeController@postUpdate')->name('admin.size.postUpdate');
            Route::get('/destroy/{id}', 'Admin\SizeController@destroy')->name('admin.size.destroy');
            Route::post('/status', 'Admin\SizeController@status')->name('admin.size.status');
        });
        Route::group(['prefix' => 'color'], function() {
            Route::get('/', 'Admin\ColorController@index')->name('admin.color.index');
            Route::get('/create', 'Admin\ColorController@create')->name('admin.color.create');
            Route::post('/create', 'Admin\ColorController@postCreate')->name('admin.color.createPost');
            Route::get('/update/{id}', 'Admin\ColorController@update')->name('admin.color.update');
            Route::post('/update/{id}', 'Admin\ColorController@postUpdate')->name('admin.color.postUpdate');
            Route::get('/destroy/{id}', 'Admin\ColorController@destroy')->name('admin.color.destroy');
        });
    });
});
