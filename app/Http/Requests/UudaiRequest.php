<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UudaiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_vi' => 'required|unique:uudais,name_vi,',
            // 'name_en' => 'required|unique:uudais,name_en,',
            'position' => 'numeric|min:0|nullable|unique:uudais,position,',
        ];
    }
}
