<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Size;
use Image;

class SizeController extends Controller
{
    public function index()
    {
    	$size = Size::orderBy('id', 'DESC')->get();

    	return view('admin/size/index', compact('size'));
    }

    public function create()
    {

    	return view('admin/size/create');
    }

    public function postCreate(Request $req)
    {
        $size                 = new Size;
        $size->name        = $req['name'];
     $size->position       = $req['position'];
         $size->dislay        = $req['dislay'];
        $size->email        = $req['email'];
        $size->note        = $req['note'];
        $size->vote        = $req['vote'];
        // $size->product_id        = $req['product_id'];
        $size->status        = 0;
          if ($req->hasFile('image')) {
            $image = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/video/'.$filename));
            $size->image = ('upload/images/video/'.$filename);
        }
    	$size->save();

    	  return redirect()->route('admin.size.index');
    }

    public function update($id)
    {
        $size = Size::findOrFail($id);

    	return view('admin.size.edit', compact('size'));
    }

    public function postUpdate($id, Request $req)
    {
        $size                 = Size::findOrFail($id);
        $size->name        = $req['name'];
        // $size->phone        = $req['phone'];
           $size->position       = $req['position'];
        $size->email        = $req['email'];         $size->dislay        = $req['dislay'];
        $size->note        = $req['note'];
        $size->vote        = $req['vote'];
         if ($req->hasFile('image')) {
            if(file_exists($size->image)) {
                unlink($size->image);
            }
            $image = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/video/'.$filename));
            $size->image = ('upload/images/video/'.$filename);
        }
    	$size->save();

    	return redirect()->route('admin.size.index')->with('success','Sửa thành công');
    }

    public function destroy($id)
    {
        $result  = Size::findOrFail($id);
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        
        $result->delete();

    	return redirect()->back()->with('success', 'Xóa thành công');
    }

    public function status(Request $req)
    {
        if ($req->ajax()) {
            $result = Size::find($req->id);
            if ($result->status == 0) {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();

            return response($result);
        }
    }
}
