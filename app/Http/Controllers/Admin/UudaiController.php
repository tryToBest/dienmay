<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Uudai;
use App\Http\Requests\UudaiRequest;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
class UudaiController extends Controller
{
    public function index()
    {
        $uudai = Uudai::orderBy('id', 'DESC')->paginate(20);

        return view('admin.uudai.index', compact('uudai'));
    }

    public function create()
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[8]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=uudais,$quyen[9]=gioithieus  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        return view('admin.uudai.create');
          }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm logo đối tác!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }

    public function postCreate(UudaiRequest $req)
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[8]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=uudais,$quyen[9]=gioithieus  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $uudai              = new Uudai;
        $uudai->name_vi        = $req['name_vi'];
        $uudai->name_en        = $req['name_en'];
        $uudai->slug_vi        = str_slug($uudai->name_vi);
        $uudai->slug_en        = str_slug($uudai->name_en);
        $uudai->description_vi = $req['description_vi'];
        $uudai->description_en = $req['description_en'];
        $uudai->link        = $req['link'];
                $uudai->display        = $req['display'];
        $uudai->time        = $req['time'];
        $uudai->position    = $req['position'];
        $uudai->status      = (is_null($req['status']) ? '0' : '1');
        if($req->hasFile('image')){
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/uudai/'.$filename));
            $uudai->image = ('upload/images/uudai/'.$filename);
        }
        $uudai->save();

        return redirect()->route('admin.uudai.index');
        }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm logo đối tác!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }

    public function update($id)
    {
             $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[8]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=uudais,$quyen[9]=gioithieus  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $uudai = Uudai::where('id', $id)->first();

        return view('admin.uudai.edit', compact('uudai'));
        }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa logo đối tác!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }

    public function postUpdate($id, Request $req)
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[8]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=uudais,$quyen[9]=gioithieus  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $uudai              = Uudai::where('id', $id)->first();
        $uudai->name_vi        = $req['name_vi'];
        $uudai->name_en        = $req['name_en'];
         $uudai->slug_vi        = str_slug($uudai->name_vi);
        $uudai->slug_en        = str_slug($uudai->name_en);
        $uudai->description_vi = $req['description_vi'];
        $uudai->description_en = $req['description_en'];
        $uudai->link        = $req['link'];
           $uudai->display        = $req['display'];
        $uudai->position    = $req['position'];
         $uudai->time        = $req['time'];
        $uudai->status      = (is_null($req['status']) ? '0' : '1');
        if($req->hasFile('image'))
        {
            if(file_exists($uudai->image))
            {
                unlink($uudai->image);
            }
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/uudai/'.$filename));
            $uudai->image = ('upload/images/uudai/'.$filename);
        }
        $uudai->save();

        return redirect()->route('admin.uudai.index')->with('success', 'Sửa thành công');
              }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa logo đối tác!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }

    public function destroy($id)
    {
             $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[8]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=uudais,$quyen[9]=gioithieus  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {
        $result = Uudai::findOrFail($id);
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        $result->delete();

        return redirect()->back()->with('success', 'Xóa thành công');
              }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền xóa logo đối tác!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }

    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Uudai::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();

            return response($result);
        }
    }
}

