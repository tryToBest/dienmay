<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\Guimail;
use App\Http\Requests\SettingRequest;
use File;
use Illuminate\Support\Facades\Auth;
use Image;
class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::first();
        if($settings == null)
        {
            $settings       = new Setting;
            $settings->name_vi = 'Nhập tên công ty';
            $settings->name_en = 'Name city';
            $settings->save();
        }
        File::exists("robots.txt");
        $file = File::get("robots.txt");
        return view('admin.setting.index', compact('settings', 'file'));
    }
    public function update(Request $req)
    {
        $quyen=explode('|',Auth::user()->quyen);
        $sub=explode('-',$quyen[7]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
    $settings               = Setting::first();
    $settings->name_vi      = $req['name_vi'];
    $settings->name_en      = $req['name_en'];
    $settings->name_tq      = $req['name_tq'];
    $settings->email        = $req['email'];
    $settings->address_vi   = $req['address_vi'];
    $settings->address_en   = $req['address_en'];
    $settings->address_tq   = $req['address_tq'];
    $settings->address2nd   = $req['address2nd'];
    $settings->address3rd   = $req['address3rd'];
    $settings->slogan_vi    = $req['slogan_vi'];
    $settings->slogan_en    = $req['slogan_en'];
    $settings->website      = $req['website'];
    $settings->hotline      = $req['hotline'];
    $settings->phone        = $req['phone'];
    $settings->phone2       = $req['phone2'];
    $settings->thead        = $req['thead'];
    $settings->tt1          = $req['tt1'];
    $settings->tt2          = $req['tt2'];
    $settings->tt3          = $req['tt3'];
    $settings->xd1          = $req['xd1'];
    $settings->xd2          = $req['xd2'];
    $settings->xd3          = $req['xd3'];
    $settings->xd4          = $req['xd4'];
    $settings->xd5          = $req['xd5'];
    $settings->cc1          = $req['cc1'];
    $settings->cc2          = $req['cc2'];
    $settings->ship         = $req['ship'];
    $settings->phantramsale = $req['phantramsale'];
    $settings->g1           = $req['g1'];
    $settings->g2           = $req['g2'];
    $settings->g3           = $req['g3'];
    $settings->g4           = $req['g4'];
    $settings->video        = $req['video'];
    $settings->tbody        = $req['tbody'];
    $settings->title_seo    = $req['title_seo'];
    $settings->meta_des     = $req['meta_des'];
    $settings->meta_key     = $req['meta_key'];
    $settings->meta_key     = $req['meta_key'];
    $robot                  = $req['robot'];
        File::put("robots.txt", $robot);
        if($req->hasFile('logo')){
            if(file_exists(public_path().$settings->logo))
            {
                unlink(public_path().$settings->logo);
            }
            $logo = $req->file('logo');
            $filename = date('Y_d_m_H_i_s').'-'. $logo->getClientOriginalName();
               $path = public_path().'/images/upload/';
            // Image::make($logo)->save(public_path('images/upload/'.$filename));
                 $logo->move($path,$filename);
            $settings->logo = ('/images/upload/'.$filename);
        }
 


        if($req->hasFile('icon')){
            if(file_exists($settings->icon))
            {
                unlink($settings->icon);
            }
            $icon = $req->file('icon');
            $filename = date('Y_d_m_H_i_s').'-'. $icon->getClientOriginalName();
            Image::make($icon)->save(public_path('images/upload/'.$filename));
            $settings->icon = ('images/upload/'.$filename);
        }
        if($req->hasFile('banner')){
            if(file_exists($settings->banner))
            {
                unlink($settings->banner);
            }
            $banner = $req->file('banner');
            $filename = date('Y_d_m_H_i_s').'-'. $banner->getClientOriginalName();
            Image::make($banner)->save(public_path('images/upload/'.$filename));
            $settings->banner = ('images/upload/'.$filename);
        }
        if($req->hasFile('banner_mobile')){
            if(file_exists($settings->banner_mobile))
            {
                unlink($settings->banner_mobile);
            }
            $banner_mobile = $req->file('banner_mobile');
            $filename = date('Y_d_m_H_i_s').'-'. $banner_mobile->getClientOriginalName();
            Image::make($banner_mobile)->save(public_path('images/upload/'.$filename));
            $settings->banner_mobile = ('images/upload/'.$filename);
        }
        
        if($req->hasFile('meta_og_image')){
            if(file_exists($settings->meta_og_image))
            {
                unlink($settings->meta_og_image);
            }
            $meta_og_image = $req->file('meta_og_image');
            $filename = date('Y_d_m_H_i_s').'-'. $meta_og_image->getClientOriginalName();
            Image::make($meta_og_image)->save(public_path('images/upload/'.$filename));
            $settings->meta_og_image = ('images/upload/'.$filename);
        }
        $settings->save();
        return redirect()->route('admin.setting');
    }else{
         echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa cấu hình!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }
    // public function guimail()
    // {
    //     $guimail = Guimail::first();
    //     if($guimail == null)
    //     {
    //         $guimail       = new Guimail;
    //         $guimail->mail1 = '';
    //         $guimail->mail2 = '';
    //         $guimail->mail3 = '';
    //         $guimail->mail4 = '';
    //         $guimail->save();
    //     }
    //     return view('admin.setting.guimail', compact('guimail'));
    // }
    // public function updatemail(Request $req)
    // {
    //     $guimail            = Guimail::first();
    //     $guimail->mail1      = $req['mail1'];
    //     $guimail->mail2      = $req['mail2'];
    //     $guimail->mail3     = $req['mail3'];
    //     $guimail->mail4     = $req['mail4'];
    //     $guimail->save();
    //     return redirect()->route('admin.guimail');
    // }
}
