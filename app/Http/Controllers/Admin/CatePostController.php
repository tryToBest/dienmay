<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cate_post;
use App\Http\Requests\Cate_postRequest;
use Image;
use File;
use Illuminate\Support\Facades\Auth;
class CatePostController extends Controller
{
    public function index()
    {
        $cate_post = Cate_post::all();
        return view('admin/cate_post/index', compact('cate_post'));
    }
    public function create()
    {
  $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {

        $data=Cate_post::select('id','name_vi','parent_id')->get()->toArray();
        return view('admin/cate_post/create', compact('data'));

                        }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}


    }
    public function postCreate(Cate_postRequest $req)
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $cate_post                 = new Cate_post;
        $cate_post->name_vi        = $req['name_vi'];
        $cate_post->name_en        = $req['name_en'];
                $cate_post->name_tq        = $req['name_tq'];
        $cate_post->parent_id      = $req->parent_id;
        $cate_post->slug_vi        = str_slug($cate_post->name_vi);
        // $cate_post->slug_en        = str_slug($cate_post->name_en);
        $cate_post->description_vi = $req['description_vi'];
        $cate_post->description_en = $req['description_en'];
                $cate_post->description_tq = $req['description_tq'];
        $cate_post->position       = $req['position'];
        $cate_post->status         = (is_null($req['status']) ? '0' : '1');
        $cate_post->menu         = (is_null($req['menu']) ? '0' : '1');
        $cate_post->ft         = (is_null($req['ft']) ? '0' : '1');
        $cate_post->title_seo_vi   = $req['title_seo_vi'];
        $cate_post->title_seo_en   = $req['title_seo_en'];
        $cate_post->meta_key_vi    = $req['meta_key_vi'];
        $cate_post->meta_key_en    = $req['meta_key_en'];
        $cate_post->meta_des_vi    = $req['meta_des_vi'];
        $cate_post->meta_des_en    = $req['meta_des_en'];
        if($req->hasFile('image')){
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/catepost/'.$filename));
            $cate_post->image = ('upload/images/catepost/'.$filename);
        }else{
            $cate_post->image = ('upload/images/catepost/avatar5.png');
        }
         if($req->hasFile('banner')){
            $banner    = $req->file('banner');
            $filename = date('Y_d_m_H_i_s').'-'. $banner->getClientOriginalName();
            Image::make($banner)->save(public_path('upload/images/catepost/'.$filename));
            $cate_post->banner = ('upload/images/catepost/'.$filename);
        }
        $cate_post->save();
        return redirect()->route('admin.cate_post.home')->with('success','Thêm thành công');
            }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function update($id)
    {
         $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $data      = Cate_post::select('id','name_vi','parent_id')->get();
          $get_cha      = Cate_post::select('id','name_vi','parent_id')->where('parent_id',0)->get();
        $cate_post = Cate_post::findOrFail($id);
        return view('admin.cate_post.edit', compact('cate_post', 'data','get_cha'));

            }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}

    }
    public function postUpdate($id, Request $req)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $cate_post                 = Cate_post::findOrFail($id);
        $cate_post->name_vi        = $req['name_vi'];
        $cate_post->name_en        = $req['name_en'];
                $cate_post->name_tq        = $req['name_tq'];
        $cate_post->parent_id      = $req->parent_id;
        $cate_post->slug_vi        = str_slug($cate_post->name_vi);
        // $cate_post->slug_en        = str_slug($cate_post->name_en);
        $cate_post->position       = $req['position'];
        $cate_post->description_vi = $req['description_vi'];
        $cate_post->description_en = $req['description_en'];
                $cate_post->description_tq = $req['description_tq'];
        $cate_post->status         = (is_null($req['status']) ? '0' : '1');
        $cate_post->menu         = (is_null($req['menu']) ? '0' : '1');
        $cate_post->ft         = (is_null($req['ft']) ? '0' : '1');
        $cate_post->title_seo_vi   = $req['title_seo_vi'];
        $cate_post->title_seo_en   = $req['title_seo_en'];
        $cate_post->meta_key_vi    = $req['meta_key_vi'];
        $cate_post->meta_key_en    = $req['meta_key_en'];
        $cate_post->meta_des_vi    = $req['meta_des_vi'];
        $cate_post->meta_des_en    = $req['meta_des_en'];
        if($req->hasFile('image')){
            if(file_exists($cate_post->image))
            {
                unlink($cate_post->image);
            }
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/catepost/'.$filename));
            $cate_post->image = ('upload/images/catepost/'.$filename);
        }
        if($req->hasFile('banner')){
            if(file_exists($cate_post->banner))
            {
                unlink($cate_post->banner);
            }
            $banner    = $req->file('banner');
            $filename = date('Y_d_m_H_i_s').'-'. $banner->getClientOriginalName();
            image::make($banner)->save(public_path('upload/images/catepost/'.$filename));
            $cate_post->banner = ('upload/images/catepost/'.$filename);
        }
        $validatedData = $req->validate([
            'name_vi'     => 'required|unique:cate_posts,name_vi,' .$cate_post->id,
            // 'name_en'     => 'required|unique:cate_posts,name_en,' .$cate_post->id,
            'position' => 'numeric|nullable|min:0|unique:cate_posts,position,' .$cate_post->id,
        ]);
        $cate_post->save();
        return redirect()->route('admin.cate_post.home')->with('success','Sửa thành công');
                    }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function destroy($id)
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {
        $result = Cate_post::findOrFail($id);
        $result2 = Cate_post::where('parent_id', $id)->first();
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        if(isset($result2))
        {
            if(file_exists($result2->image))
            {
                unlink($result2->image);
            }
            $result2->delete();
        }
        $result->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
                    }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền xóa danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_post::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();
            return response($result);
        }
    }
    public function menu(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_post::find($req->id);
            if ($result->menu == 0)
            {
                $result->menu = 1;
            } else {
                $result->menu = 0;
            }
            $result->save();
            return response($result);
        }
    }
    public function ft(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_post::find($req->id);
            if ($result->ft == 0)
            {
                $result->ft = 1;
            } else {
                $result->ft = 0;
            }
            $result->save();
            return response($result);
        }
    }
        public function is_home(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_post::find($req->id);
            if ($result->is_home == 0)
            {
                $result->is_home = 1;
            } else {
                $result->is_home = 0;
            }
            $result->save();
            return response($result);
        }
    }
}
