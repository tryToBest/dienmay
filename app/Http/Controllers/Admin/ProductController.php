<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Http\Requests\ProductRequest;
use App\Models\Cate_product;
use App\Models\Cate_post;
use App\Models\Post;
use App\Models\Setting;
use App\Models\Faq;
use Image, File;
use DB;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Images;
use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;
class ProductController extends Controller
{
    public function index()
    {
        $product = Product::orderBy('id', 'DESC')->where('soft_delete',0)->get();
        $data    = Cate_product::where('parent_id',0)->get();
        return view('admin.product.index', compact('product', 'data'));
    }
    public function getProductByIdCate($id, Request $request){
        $cateProduct = Cate_product::find($id);
        $product = Product::orderBy('id', 'DESC')->where('soft_delete', 0);

        if($cateProduct){
            if($cateProduct->parent_id == 0){ // Assuming $cateProduct is the retrieved category
                $listCateProductId = Cate_product::where('parent_id', $id)->pluck('id')->toArray();
                $listCateProductId[] = $id;
                $product->whereIn('cate_product_id', $listCateProductId);
            } else {
                $product->where('cate_product_id', $id);
            }
        }

        $product = $product->get();
        $data = Cate_product::where('parent_id', 0)->get();

        return view('admin.product.index', compact('product', 'data'));
    }

    public function create()
    {
        $quyen=explode('|',Auth::user()->quyen);
        $sub=explode('-',$quyen[1]);  //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
        if ($sub[3]==1) {
            $data = Cate_product::select('id','name_vi','parent_id')->get()->toArray();
            $bolocs = Faq::where('parent_id',0)->get();
            if($bolocs->first() != null){
                $boloc_child_first = $bolocs->first();
                $boloc_child = Faq::where('parent_id',$boloc_child_first->id)->get();
            }
            else{
                $boloc_child = null;
            }
            if ($data != null) {
                return view('admin.product.create', compact('data','bolocs','boloc_child'));
            } else {
                return redirect()->route('admin.cate_product.home')->with('error', 'Chưa có danh mục');
            }
        }else{
            echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền add sản phẩm!') . "');</script>";
            echo "<script>history.back();</script>"; die;
        }
    }
    public function postCreate(ProductRequest $req)
    {
        $quyen=explode('|',Auth::user()->quyen);
        $sub=explode('-',$quyen[1]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
        if ($sub[3]==1 ) {
        $product                  = new Product;
        $product->name_vi         = $req['name_vi'];
        $product->slug_vi         = str_slug($req['name_vi']);
        $product->price           = $req['price'];
        $product->cate_product_id = $req['cate_product_id'];
        $product->position        = $req['position'];
        $product->status          = (is_null($req['status']) ? '0' : '1');
        $product->is_home         = (is_null($req['is_home']) ? '0' : '1');
        $product->is_hot         = (is_null($req['is_hot']) ? '0' : '1');
        $product->title_vi        = $req['title_vi'];
        $product->description_vi  = $req['description_vi'];
        $product->congsuat         = $req['congsuat'];
        $product->mausac         = $req['mausac'];
        $product->boloc_id = '|'.implode('|', $req['chitietboloc']).'|';

        $product->kichthuoc  = $req['kichthuoc'];
        $product->chucnang  = $req['chucnang'];
        $product->lazada  = $req['lazada'];
        $product->shoppe  = $req['shoppe'];
        $product->tiki  = $req['tiki'];
        $product->tiktok  = $req['tiktok'];
        $product->namthuchien  = $req['namthuchien'];
        $product->donvithuchien  = $req['donvithuchien'];
        $product->title_seo_vi    = $req['title_seo_vi'];
        $product->title_seo_en    = $req['title_seo_en'];
        $product->meta_key_vi     = $req['meta_key_vi'];
        $product->meta_key_en     = $req['meta_key_en'];
        $product->meta_des_vi     = $req['meta_des_vi'];
        $product->meta_des_en     = $req['meta_des_en'];
        $product->code     = $req['code'];
        $product->price_unit     = $req['price_unit'];
        $product->thongtin_vi     = $req['thongtin_vi'];
        $product->thongtin_en     = $req['thongtin_en'];
        if ($req->hasFile('image')) {
            $image = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/product/'.$filename));
            $product->image = ('upload/images/product/'.$filename);
        }
             if ($req->hasFile('image_truoc')) {
            $image_truoc = $req->file('image_truoc');
            $filename = date('Y_d_m_H_i_s').'-'. $image_truoc->getClientOriginalName();
            Image::make($image_truoc)->save(public_path('upload/images/product/'.$filename));
            $product->image_truoc = ('upload/images/product/'.$filename);
        }

    if ($req->hasFile('image_sau')) {
            $image_sau = $req->file('image_sau');
            $filename = date('Y_d_m_H_i_s').'-'. $image_sau->getClientOriginalName();
            Image::make($image_sau)->save(public_path('upload/images/product/'.$filename));
            $product->image_sau = ('upload/images/product/'.$filename);
        }
                   if($req->hasFile('banner')){
            $banner = $req->file('banner');
            $filename = $banner->getClientOriginalName();
            $path = public_path().'/images/upload/';
            $banner->move($path,$filename);
            $product->banner = ('/images/upload/'.$filename);
        }


        $product->save();
        if ($req->hasFile('img')) {
            $img = $req->file('img');
            foreach ($img as $i) {
                $filename = date('Y_d_m_H_i_s').'-'. $i->getClientOriginalName();
                Image::make($i)->save(public_path('upload/images/detailproduct/'.$filename));
                $i = new Images;
                $i->product_id = $product->id;
                $i->name = ('upload/images/detailproduct/'.$filename);
                $i->save();
            }
        }
        return redirect()->route('admin.product.index');
        }else{
            echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền add sản phẩm!') . "');</script>";
            echo "<script>history.back();</script>"; die;
}
    }
    public function update($id)
    {
        $quyen=explode('|',Auth::user()->quyen);
        $sub=explode('-',$quyen[1]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
        if ($sub[2]==1) {
            $product = Product::findOrFail($id);


            if($product->boloc_id != null && $product->boloc_id != 'boloc_id'){
                $listBolocIdOfProduct = array_values(array_filter(explode('|',$product->boloc_id), function($value){
                    return $value != "";
                }));

                $listBoLocParentOfProduct = array_map(function($item){
                    $boloc_id = Faq::findOrFail($item);
                    return Faq::where('id',$boloc_id->parent_id)->first()->id;
                }, $listBolocIdOfProduct);




                $id_boloc_parent = Faq::where('id',$listBoLocParentOfProduct[0])->first()->parent_id;
                $boloc_boss = Faq::where('id',$id_boloc_parent)->first()->id;

      

            }else{

                $listBolocIdOfProduct = null;
                $listBoLocParentOfProduct = null;
                $boloc_boss = null;
            }



            $img_detail = Images::where('product_id', $product->id)->get();
            
            $danhmuccha    = Cate_product::where('status',1)->where('parent_id',0)->get();
            $data    = Cate_product::select('id','name_vi','parent_id')->get();
            

            $cate_of_product = $product->cate_product_id;

            $bolocs = Faq::where('parent_id',0)->get();

            if($boloc_boss != null){
                $boloc_child = Faq::where('parent_id',$boloc_boss)->get();
            }elseif($bolocs->first() != null){
                $boloc_child_first = $bolocs->first();
                $boloc_child = Faq::where('parent_id',$boloc_child_first->id)->get();
            }
            else{
                $boloc_child = null;
            }

            
            return view('admin.product.edit', compact('data', 'product', 'img_detail','danhmuccha','bolocs','boloc_child','listBolocIdOfProduct','listBoLocParentOfProduct','boloc_boss'));
        }else{
            echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa sản phẩm!') . "');</script>";
            echo "<script>history.back();</script>"; die;
        }
    }
    public function postUpdate($id, Request $req)
    {

        $quyen=explode('|',Auth::user()->quyen);
        $sub=explode('-',$quyen[1]);  //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
        if ($sub[2]==1) {
            $product                  = Product::findOrFail($id);
            $product->name_vi         = $req['name_vi'];
            $product->name_en         = $req['name_en'];
            $product->name_tq         = $req['name_tq'];
            $product->slug_vi         = str_slug($req['name_vi']);
            $product->price           = $req['price'];
            $product->cate_product_id = $req['cate_product_id'];
            $product->position        = $req['position'];
            $product->status          = (is_null($req['status']) ? '0' : '1');
            $product->is_home         = (is_null($req['is_home']) ? '0' : '1');
            $product->is_hot          = (is_null($req['is_hot']) ? '0' : '1');
            $product->congsuat        = $req['congsuat'];
            $product->mausac          = $req['mausac'];
            $product->kichthuoc       = $req['kichthuoc'];
            $product->chucnang        = $req['chucnang'];
            $product->lazada          = $req['lazada'];
            $product->shoppe          = $req['shoppe'];
            $product->tiki            = $req['tiki'];
            $product->tiktok          = $req['tiktok'];
            $product->namthuchien     = $req['namthuchien'];
            $product->donvithuchien   = $req['donvithuchien'];
            $product->title_vi        = $req['title_vi'];
            $product->title_en        = $req['title_en'];
            $product->title_tq        = $req['title_tq'];
            $product->description_vi  = $req['description_vi'];
            $product->description_en  = $req['description_en'];
            $product->description_tq  = $req['description_tq'];
            $product->title_seo_vi    = $req['title_seo_vi'];
            $product->title_seo_en    = $req['title_seo_en'];
            $product->meta_key_vi     = $req['meta_key_vi'];
            $product->meta_key_en     = $req['meta_key_en'];
            $product->meta_des_vi     = $req['meta_des_vi'];
            $product->meta_des_en     = $req['meta_des_en'];
            $product->code            = $req['code'];
            $product->price_unit      = $req['price_unit'];
            $product->thongtin_vi     = $req['thongtin_vi'];
            $product->thongtin_en     = $req['thongtin_en'];
            $product->boloc_id        = $req->chitietboloc != null ? '|'.implode('|', $req['chitietboloc']).'|' : null;
         
        if ($req->hasFile('image')) {
            if(file_exists($product->image)) {
                unlink($product->image);
            }
            $image = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/product/'.$filename));
            $product->image = ('upload/images/product/'.$filename);
        }
         if ($req->hasFile('image_truoc')) {
            if(file_exists($product->image_truoc)) {
                unlink($product->image_truoc);
            }
            $image_truoc = $req->file('image_truoc');
            $filename = date('Y_d_m_H_i_s').'-'. $image_truoc->getClientOriginalName();
            Image::make($image_truoc)->save(public_path('upload/images/product/'.$filename));
            $product->image_truoc = ('upload/images/product/'.$filename);
        }
            if ($req->hasFile('image_sau')) {
            if(file_exists($product->image_sau)) {
                unlink($product->image_sau);
            }
            $image_sau = $req->file('image_sau');
            $filename = date('Y_d_m_H_i_s').'-'. $image_sau->getClientOriginalName();
            Image::make($image_sau)->save(public_path('upload/images/product/'.$filename));
            $product->image_sau = ('upload/images/product/'.$filename);
        }
        $validatedData = $req->validate([
            'name_vi'     => 'required|unique:products,name_vi,' .$product->id,
            // 'name_en'     => 'required|unique:products,name_en,' .$product->id,
            // 'price'    => 'numeric|nullable|min:0',
            'position' => 'numeric|nullable|min:0|unique:products,position,' .$product->id,
        ]);
        if($req->hasFile('banner')){
            if(file_exists($product->banner))
            {
                unlink($product->banner);
            }
            $banner = $req->file('banner');
            $filename = $banner->getClientOriginalName();
            $path = public_path().'/images/upload/';
            $banner->move($path,$filename);
            $product->banner = ('/images/upload/'.$filename);
        }
        $product->save();
        if ($req->hasFile('img')) {
            $img = $req->file('img');
            foreach ($img as $key => $i) {
                $filename = date('Y_d_m_H_i_s').'-'. $i->getClientOriginalName();
                Image::make($i)->save(public_path('upload/images/detailproduct/'.$filename));
                $i = new Images;
                $i->product_id = $product->id;
                $i->name = ('upload/images/detailproduct/'.$filename);
                $i->save();
            }
        }
        return redirect()->back()->with('success', 'Sửa thành công');
        }else{
  echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa sản phẩm!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}

    }
    public function destroy($id)
    {

        $quyen=explode('|',Auth::user()->quyen);
        $sub=explode('-',$quyen[1]);  //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
        if ($sub[1]==1) {
        $result = Product::findOrFail($id);
        $img = Images::where('product_id', $result->id)->get();
            // if (file_exists($result->image)) {
            //     unlink($result->image);
            // }
            // foreach ($img as $key => $i) {
            //     if (File::exists($i->name)) {
            //         File::delete($i->name);
            //     }
            // }
        $result->status = 0;
        $result->is_hot = 0;
        $result->soft_delete = 1;
        $result->save();
        return redirect()->back()->with('success', 'Xóa thành công');

    }else{
      echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền xóa sản phẩm!') . "');</script>";
      echo "<script>history.back();</script>"; die;
    }


    }
    public function delImage(Request $request)
    {

        $del = Images::find($request->id);
        $path = $del->name;
        if(File::exists($path)) {
            File::delete($path);
        }
        $del->delete();
        echo "Xóa thành công";
    }
    public function search(Request $req)
    {
        $id_cate_product = $req->cate_product;
        if($id_cate_product == 0){
            return redirect()->route('admin.product.index');
        }
        session()->put('id',$id_cate_product);
        $data    = Cate_product::select('id','name','parent_id')->get()->toArray();
        $product = Product::orderBy('position','ASC')->where(function($query)
        {
            $pro = $query;
            $id  = session('id');
            $cate_product = Cate_product::find($id);
            $pro = $pro->orWhere('cate_product_id',$cate_product->id); // bài viết có id của danh muc cha cấp 1.
            $com = Cate_product::where('parent_id',$cate_product->id)->get();//danh mục cha cấp 2.
            foreach ($com as $dt) {
                $pro = $pro->orWhere('cate_product_id',$dt->id);// bài viết có id của danh muc cha cấp 2.
            }
            session()->forget('id');//xóa session;
        })->get();
        return view('admin.product.search', compact('product', 'data', 'id_cate_product'));
    }
    
    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Product::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();
            return response($result);
        }
    }
    public function is_home(Request $req)
    {
        if ($req->ajax())
        {
            $result = Product::find($req->id);
            if ($result->is_home == 0)
            {
                $result->is_home = 1;
            } else {
                $result->is_home = 0;
            }
            $result->save();
            return response($result);
        }
    }
    public function is_hot(Request $req)
    {
        if ($req->ajax())
        {
            $result = Product::find($req->id);
            if ($result->is_hot == 0)
            {
                $result->is_hot = 1;
            } else {
                $result->is_hot = 0;
            }
            $result->save();
            return response($result);
        }
    }
    public function is_sale(Request $req)
    {
        if ($req->ajax())
        {
            $result = Product::find($req->id);
            if ($result->is_sale == 0)
            {
                $result->is_sale = 1;
            } else {
                $result->is_sale = 0;
            }
            $result->save();
            return response($result);
        }
    }

        public function is_khongsosanh(Request $req)
    {
        if ($req->ajax())
        {
            $result = Product::find($req->id);
            if ($result->is_khongsosanh == 0)
            {
                $result->is_khongsosanh = 1;
            } else {
                $result->is_khongsosanh = 0;
            }
            $result->save();
            return response($result);
        }
    }


    public function import(Request $req)
    {
        $validatedData = $req->validate([
            'file'     => 'required|mimes:xlsx',
        ]);
        Excel::import(new ProductsImport, $req->file);
        return back()->with('success', 'All good!');
    }

    public function getboloc(Request $req){
        if($req->ajax()){
            $boloc = Faq::where('parent_id',$req->boloc_id)->get();
            foreach($boloc as $b){
                $boloc_child = Faq::where('parent_id',$b->id)->select('id','name_vi')->get();
                $b->boloc_child = $boloc_child;
            }
            return response()->json($boloc);
        }
    }
    public function spnoibat(){
        $productHots = Product::where('status',1)->where('is_hot',1)->limit(60)->get();
        return view('admin.product.listspnoibat',compact('productHots'));
    }
    public function updatePositionHot(Request $req){
        $product = Product::findOrFail($req->id);
        $product->position_hot = $req->position;
        $product->save();
        return response()->json($req->position);   
    }
    public function trashIndex(){
        $products = Product::where('soft_delete',1)->get();
        return view('admin.product.trashIndex',compact('products'));
    }

    public function destroyProduct(Request $req) {
        $product = Product::findOrFail($req->productId);
        if (file_exists($product->image)) {
                unlink($product->image);
            }
        $images = DB::table('images')->where('product_id', $product->id)->get();
        foreach ($images as $image) {
            if (File::exists($image->name)) {
                File::delete($image->name);
            }
            DB::table('images')->where('id', $image->id)->delete();
        }

        if ($product->delete()) {
            return response()->json(['success' => true]);
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function recoverProduct(Request $request){
        $product = Product::findOrFail($request->productId);
        $product->status = 1;
        $product->soft_delete = 0;

        if ($product->save()) {
            return response()->json(['success' => true]);
        } else {
            return response()->json(['success' => false]);
        }
    }
    public function flashsaleindex(){
        $cate_products = Cate_product::where('parent_id',0)->where('status',1)->orderBy('position','ASC')->get();
        $flashSale = Setting::select('title_flash_sale','date_start_sale','date_end_sale','status_flash_sale')->get();
        $saleProducts = Product::where('is_sale',1)->get();
        return view('admin.flashsale.index', compact('flashSale','saleProducts','cate_products'));
    }
    public function flashsaleUpdate(Request $request){
        
        $timePart = explode('-', $request->time);
        $startTime = \DateTime::createFromFormat('m/d/Y h:i A', trim($timePart[0]))->format('Y-m-d H:i:s');
        $endTime = \DateTime::createFromFormat('m/d/Y h:i A', trim($timePart[1]))->format('Y-m-d H:i:s');
        $oldPathImage = Setting::where('id',1)->first()->image_sale;
        $filename = null;
        if ($request->hasFile("image_sale")) {
            $image_sale = $request->file('image_sale');
            $filename = $image_sale->getClientOriginalName();
            $path = public_path().'/images/upload/';
            $image_sale->move($path, $filename);
            $imagePath = '/images/upload/' . $filename;
            
            // Delete the previous image if it exists
            if (file_exists($request->image_sale)) {
                unlink($request->image_sale);
            }
        }

        $flashSale = Setting::where('id', 1)->update([
            'title_flash_sale' => $request->title,
            'date_start_sale' => $startTime,
            'date_end_sale' => $endTime,
            'image_sale' => $filename ? $imagePath : $oldPathImage, // Use the new image path or the existing one
            'status_image_sale' => $request->has('status_image_sale') ? '1' : '0',
            'status_flash_sale' => $request->has('statusFlashSale') ? '1' : '0',
            'flash_sale_auto' => $request->has('flash_sale_auto') ? '1' : '0',
        ]);

        return back()->with('success', 'Update successful');
    }

    public function searchByName(Request $request){
        $perPage = 10; // Số sản phẩm trên mỗi trang
        $products = Product::where('status', 1)
        ->with('Cate_product')
        ->where('name_vi', 'like', '%' . $request->keyword . '%')
        ->select('id', 'name_vi', 'image', 'cate_product_id', 'status', 'is_sale', 'updated_at', 'position', 'is_home')
        ->paginate($perPage);

        return response()->json($products);
    }
    public function searchByCateProductName(Request $request){
        $perPage = 10; // Số sản phẩm trên mỗi trang
        $listCateProductId = Cate_product::where('parent_id',$request->keyword)->pluck('id')->toArray();
        $listCateProductId[] = $request->keyword;
        $products = Product::with('Cate_product')
        ->whereIn('cate_product_id', $listCateProductId)
        ->select('id', 'name_vi', 'image', 'cate_product_id', 'status', 'is_sale', 'updated_at', 'position', 'is_home')
        ->orderBy('updated_at','DESC')
        ->paginate($perPage);

        return response()->json($products);
    }
    public function checkbox(Request $req)
    {
        $checkbox = $req->checkbox;
        if (!isset($req->checkbox)) {

            return back()->with('success', 'Chưa chọn sản phẩm');
        }
        if ($req->select_action == 1) {
            $checkbox = $req->checkbox;
            foreach ($checkbox as $c) {
                $result = Product::findOrFail($c);
                // if(file_exists($result->image)) {
                //     unlink($result->image);
                // }
                $result->soft_delete = 1;
                $result->save();
            }

            return redirect()->back()->with('success', 'Xóa thành công');
        }
        
        if ($req->select_action == 0) {

            return back()->with('success', 'Chưa chọn thao tác');
        }
        if ($checkbox == NULL){

            return back()->with('success', 'Bạn chưa chọn cái nào');
        }
    }
    public function checkboxTrash(Request $req)
    {
        $checkbox = $req->checkbox;
        if (!isset($req->checkbox)) {

            return back()->with('success', 'Chưa chọn sản phẩm');
        }
        if ($req->select_action == 1) {
            $checkbox = $req->checkbox;
            foreach ($checkbox as $c) {
                $result = Product::findOrFail($c);
                if(file_exists($result->image)) {
                    unlink($result->image);
                }
                $image = Image::where('product_id',$c)->get();
                $image->delete();
                $result->delete();
            }

            return redirect()->back()->with('success', 'Xóa thành công');
        }
        if ($req->select_action == 2) {
            $checkbox = $req->checkbox;
            foreach ($checkbox as $c) {
                $result = Product::findOrFail($c);
                $result->soft_delete = 0;
                $result->save();
            }

            return redirect()->back()->with('success', 'Khôi phục thành công');
        }
        
        if ($req->select_action == 0) {

            return back()->with('success', 'Chưa chọn thao tác');
        }
        if ($checkbox == NULL){

            return back()->with('success', 'Bạn chưa chọn cái nào');
        }
    }
}
