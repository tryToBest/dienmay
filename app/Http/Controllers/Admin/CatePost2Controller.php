<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cate_post2;
use App\Http\Requests\Cate_post2Request;
use Image;
use File;
use Illuminate\Support\Facades\Auth;
class CatePost2Controller extends Controller
{
    public function index()
    {
        $cate_post2 = Cate_post2::all();
        return view('admin/cate_post2/index', compact('cate_post2'));
    }
    public function create()
    {
  $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post2s,$quyen[4]=post2s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {

        $data=Cate_post2::select('id','name_vi','parent_id')->get()->toArray();
        return view('admin/cate_post2/create', compact('data'));

                        }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}


    }
    public function post2Create(Cate_post2Request $req)
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post2s,$quyen[4]=post2s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $cate_post2                 = new Cate_post2;
        $cate_post2->name_vi        = $req['name_vi'];
        $cate_post2->name_en        = $req['name_en'];
                $cate_post2->name_tq        = $req['name_tq'];
        $cate_post2->parent_id      = $req->parent_id;
        $cate_post2->slug_vi        = str_slug($cate_post2->name_vi);
        // $cate_post2->slug_en        = str_slug($cate_post2->name_en);
        $cate_post2->description_vi = $req['description_vi'];
        $cate_post2->description_en = $req['description_en'];
                $cate_post2->description_tq = $req['description_tq'];
        $cate_post2->position       = $req['position'];
        $cate_post2->status         = (is_null($req['status']) ? '0' : '1');
        $cate_post2->menu         = (is_null($req['menu']) ? '0' : '1');
        $cate_post2->ft         = (is_null($req['ft']) ? '0' : '1');
        $cate_post2->title_seo_vi   = $req['title_seo_vi'];
        $cate_post2->title_seo_en   = $req['title_seo_en'];
        $cate_post2->meta_key_vi    = $req['meta_key_vi'];
        $cate_post2->meta_key_en    = $req['meta_key_en'];
        $cate_post2->meta_des_vi    = $req['meta_des_vi'];
        $cate_post2->meta_des_en    = $req['meta_des_en'];
        if($req->hasFile('image')){
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/catepost2/'.$filename));
            $cate_post2->image = ('upload/images/catepost2/'.$filename);
        }else{
            $cate_post2->image = ('upload/images/catepost2/avatar5.png');
        }
         if($req->hasFile('banner')){
            $banner    = $req->file('banner');
            $filename = date('Y_d_m_H_i_s').'-'. $banner->getClientOriginalName();
            Image::make($banner)->save(public_path('upload/images/catepost2/'.$filename));
            $cate_post2->banner = ('upload/images/catepost2/'.$filename);
        }
        $cate_post2->save();
        return redirect()->route('admin.cate_post2.home')->with('success','Thêm thành công');
            }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function update($id)
    {
         $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post2s,$quyen[4]=post2s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $data      = Cate_post2::select('id','name_vi','parent_id')->get();
          $get_cha      = Cate_post2::select('id','name_vi','parent_id')->where('parent_id',0)->get();
        $cate_post2 = Cate_post2::findOrFail($id);
        return view('admin.cate_post2.edit', compact('cate_post2', 'data','get_cha'));

            }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}

    }
    public function post2Update($id, Request $req)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post2s,$quyen[4]=post2s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $cate_post2                 = Cate_post2::findOrFail($id);
        $cate_post2->name_vi        = $req['name_vi'];
        $cate_post2->name_en        = $req['name_en'];
                $cate_post2->name_tq        = $req['name_tq'];
        $cate_post2->parent_id      = $req->parent_id;
        $cate_post2->slug_vi        = str_slug($cate_post2->name_vi);
        // $cate_post2->slug_en        = str_slug($cate_post2->name_en);
        $cate_post2->position       = $req['position'];
        $cate_post2->description_vi = $req['description_vi'];
        $cate_post2->description_en = $req['description_en'];
                $cate_post2->description_tq = $req['description_tq'];
        $cate_post2->status         = (is_null($req['status']) ? '0' : '1');
        $cate_post2->menu         = (is_null($req['menu']) ? '0' : '1');
        $cate_post2->ft         = (is_null($req['ft']) ? '0' : '1');
        $cate_post2->title_seo_vi   = $req['title_seo_vi'];
        $cate_post2->title_seo_en   = $req['title_seo_en'];
        $cate_post2->meta_key_vi    = $req['meta_key_vi'];
        $cate_post2->meta_key_en    = $req['meta_key_en'];
        $cate_post2->meta_des_vi    = $req['meta_des_vi'];
        $cate_post2->meta_des_en    = $req['meta_des_en'];
        if($req->hasFile('image')){
            if(file_exists($cate_post2->image))
            {
                unlink($cate_post2->image);
            }
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/catepost2/'.$filename));
            $cate_post2->image = ('upload/images/catepost2/'.$filename);
        }
        if($req->hasFile('banner')){
            if(file_exists($cate_post2->banner))
            {
                unlink($cate_post2->banner);
            }
            $banner    = $req->file('banner');
            $filename = date('Y_d_m_H_i_s').'-'. $banner->getClientOriginalName();
            image::make($banner)->save(public_path('upload/images/catepost2/'.$filename));
            $cate_post2->banner = ('upload/images/catepost2/'.$filename);
        }
        $validatedData = $req->validate([
            'name_vi'     => 'required|unique:cate_post2s,name_vi,' .$cate_post2->id,
            // 'name_en'     => 'required|unique:cate_post2s,name_en,' .$cate_post2->id,
            'position' => 'numeric|nullable|min:0|unique:cate_post2s,position,' .$cate_post2->id,
        ]);
        $cate_post2->save();
        return redirect()->route('admin.cate_post2.home')->with('success','Sửa thành công');
                    }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function destroy($id)
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post2s,$quyen[4]=post2s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {
        $result = Cate_post2::findOrFail($id);
        $result2 = Cate_post2::where('parent_id', $id)->first();
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        if(isset($result2))
        {
            if(file_exists($result2->image))
            {
                unlink($result2->image);
            }
            $result2->delete();
        }
        $result->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
                    }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền xóa danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_post2::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();
            return response($result);
        }
    }
    public function menu(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_post2::find($req->id);
            if ($result->menu == 0)
            {
                $result->menu = 1;
            } else {
                $result->menu = 0;
            }
            $result->save();
            return response($result);
        }
    }
    public function ft(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_post2::find($req->id);
            if ($result->ft == 0)
            {
                $result->ft = 1;
            } else {
                $result->ft = 0;
            }
            $result->save();
            return response($result);
        }
    }
        public function is_home(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_post2::find($req->id);
            if ($result->is_home == 0)
            {
                $result->is_home = 1;
            } else {
                $result->is_home = 0;
            }
            $result->save();
            return response($result);
        }
    }
}
