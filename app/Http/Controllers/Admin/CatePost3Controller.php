<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cate_post3;
use App\Models\Faq;
use App\Http\Requests\Cate_post3Request;
use Image;
use File;
use Illuminate\Support\Facades\Auth;
class CatePost3Controller extends Controller
{
    public function index()
    {
        $cate_post3 = Cate_post3::all();
        return view('admin/cate_post3/index', compact('cate_post3'));
    }
    public function create()
    {
  $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post3s,$quyen[4]=post3s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {

        $data=Cate_post3::select('id','name_vi','parent_id')->get()->toArray();
        return view('admin/cate_post3/create', compact('data'));

                        }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}


    }
    public function post3Create(Cate_post3Request $req)
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post3s,$quyen[4]=post3s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $cate_post3                 = new Cate_post3;
        $cate_post3->name_vi        = $req['name_vi'];
         $cate_post3->name2_vi        = $req['name2_vi'];
          $cate_post3->name3_vi        = $req['name3_vi'];
        $cate_post3->name_en        = $req['name_en'];
                $cate_post3->name_tq        = $req['name_tq'];
        $cate_post3->parent_id      = $req->parent_id;
        $cate_post3->slug_vi        = str_slug($cate_post3->name_vi);
        // $cate_post3->slug_en        = str_slug($cate_post3->name_en);
        $cate_post3->description_vi = $req['description_vi'];
            $cate_post3->title_vi        = $req['title_vi'];
        $cate_post3->description_en = $req['description_en'];
                $cate_post3->description_tq = $req['description_tq'];
        $cate_post3->position       = $req['position'];
        $cate_post3->status         = (is_null($req['status']) ? '0' : '1');
        $cate_post3->menu         = (is_null($req['menu']) ? '0' : '1');
        $cate_post3->ft         = (is_null($req['ft']) ? '0' : '1');
        $cate_post3->title_seo_vi   = $req['title_seo_vi'];
        $cate_post3->title_seo_en   = $req['title_seo_en'];
        $cate_post3->meta_key_vi    = $req['meta_key_vi'];
        $cate_post3->meta_key_en    = $req['meta_key_en'];
        $cate_post3->meta_des_vi    = $req['meta_des_vi'];
        $cate_post3->meta_des_en    = $req['meta_des_en'];
        if($req->hasFile('image')){
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/dichvu/'.$filename));
            $cate_post3->image = ('upload/images/dichvu/'.$filename);
        }else{
            $cate_post3->image = ('upload/images/dichvu/avatar5.png');
        }
         if($req->hasFile('banner')){
            $banner    = $req->file('banner');
            $filename = date('Y_d_m_H_i_s').'-'. $banner->getClientOriginalName();
            Image::make($banner)->save(public_path('upload/images/dichvu/'.$filename));
            $cate_post3->banner = ('upload/images/dichvu/'.$filename);
        }
        $cate_post3->save();
        return redirect()->route('admin.cate_post3.home')->with('success','Thêm thành công');
            }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function update($id)
    {
         $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post3s,$quyen[4]=post3s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $data      = Cate_post3::select('id','name_vi','parent_id')->get();
          $get_cha      = Cate_post3::select('id','name_vi','parent_id')->where('parent_id',0)->get();
        $cate_post3 = Cate_post3::findOrFail($id);

         $get_Faq      = Faq::select('id','name_vi','parent_id')->where('status',1)->where('parent_id',0)->get()->toArray();
            $list_Faq =array();
            $dem_faq = 0;
            foreach($get_Faq as $k=>$v){
               $list_Faq[$dem_faq]['title']=$v;
               $list_Faq[$dem_faq]['id']=$k;
               $list_Faq[$dem_faq++]['sub']= Faq::select('id','name_vi','parent_id')->where('status',1)->where('parent_id',$v)->get()->toArray();
            }
        return view('admin.cate_post3.edit', compact('cate_post3', 'data','get_cha','list_Faq'));

            }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}

    }
    public function post3Update($id, Request $req)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post3s,$quyen[4]=post3s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $cate_post3                 = Cate_post3::findOrFail($id);
        $cate_post3->name_vi        = $req['name_vi'];
                 $cate_post3->name2_vi        = $req['name2_vi'];
          $cate_post3->name3_vi        = $req['name3_vi'];
        $cate_post3->name_en        = $req['name_en'];
                $cate_post3->name_tq        = $req['name_tq'];
        $cate_post3->parent_id      = $req->parent_id;
        $cate_post3->slug_vi        = str_slug($cate_post3->name_vi);
             $cate_post3->title_vi        = $req['title_vi'];
        // $cate_post3->slug_en        = str_slug($cate_post3->name_en);
        $cate_post3->position       = $req['position'];
        $cate_post3->description_vi = $req['description_vi'];
        $cate_post3->description_en = $req['description_en'];
                $cate_post3->description_tq = $req['description_tq'];
        $cate_post3->status         = (is_null($req['status']) ? '0' : '1');
        $cate_post3->menu         = (is_null($req['menu']) ? '0' : '1');
        $cate_post3->ft         = (is_null($req['ft']) ? '0' : '1');
        $cate_post3->title_seo_vi   = $req['title_seo_vi'];
        $cate_post3->title_seo_en   = $req['title_seo_en'];
        $cate_post3->meta_key_vi    = $req['meta_key_vi'];
        $cate_post3->meta_key_en    = $req['meta_key_en'];
        $cate_post3->meta_des_vi    = $req['meta_des_vi'];
        $cate_post3->meta_des_en    = $req['meta_des_en'];
               if(isset($_POST['faq'])){
                $faq='|';
                foreach($_POST['faq'] as $k=>$v) $faq.=$v.'|';
                $cate_post3->faq_id  = $faq;
            }else{
              $cate_post3->faq_id  = null;
            }
            
        if($req->hasFile('image')){
            if(file_exists($cate_post3->image))
            {
                unlink($cate_post3->image);
            }
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/dichvu/'.$filename));
            $cate_post3->image = ('upload/images/dichvu/'.$filename);
        }
        if($req->hasFile('banner')){
            if(file_exists($cate_post3->banner))
            {
                unlink($cate_post3->banner);
            }
            $banner    = $req->file('banner');
            $filename = date('Y_d_m_H_i_s').'-'. $banner->getClientOriginalName();
            image::make($banner)->save(public_path('upload/images/dichvu/'.$filename));
            $cate_post3->banner = ('upload/images/dichvu/'.$filename);
        }
               if($req->hasFile('image_gioithieu')){
            if(file_exists($cate_post3->image_gioithieu))
            {
                unlink($cate_post3->image_gioithieu);
            }
            $image_gioithieu    = $req->file('image_gioithieu');
            $filename = date('Y_d_m_H_i_s').'-'. $image_gioithieu->getClientOriginalName();
            image::make($image_gioithieu)->save(public_path('upload/images/dichvu/'.$filename));
            $cate_post3->image_gioithieu = ('upload/images/dichvu/'.$filename);
        }
          if($req->hasFile('image_gioithieu2')){
            if(file_exists($cate_post3->image_gioithieu2))
            {
                unlink($cate_post3->image_gioithieu2);
            }
            $image_gioithieu2    = $req->file('image_gioithieu2');
            $filename = date('Y_d_m_H_i_s').'-'. $image_gioithieu2->getClientOriginalName();
            image::make($image_gioithieu2)->save(public_path('upload/images/dichvu/'.$filename));
            $cate_post3->image_gioithieu2 = ('upload/images/dichvu/'.$filename);
        }
        $validatedData = $req->validate([
            'name_vi'     => 'required|unique:cate_post3s,name_vi,' .$cate_post3->id,
            // 'name_en'     => 'required|unique:cate_post3s,name_en,' .$cate_post3->id,
            'position' => 'numeric|nullable|min:0|unique:cate_post3s,position,' .$cate_post3->id,
        ]);
        $cate_post3->save();
        return redirect()->route('admin.cate_post3.home')->with('success','Sửa thành công');
                    }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function destroy($id)
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post3s,$quyen[4]=post3s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {
        $result = Cate_post3::findOrFail($id);
        $result2 = Cate_post3::where('parent_id', $id)->first();
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        if(isset($result2))
        {
            if(file_exists($result2->image))
            {
                unlink($result2->image);
            }
            $result2->delete();
        }
        $result->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
                    }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền xóa danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_post3::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();
            return response($result);
        }
    }
    public function menu(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_post3::find($req->id);
            if ($result->menu == 0)
            {
                $result->menu = 1;
            } else {
                $result->menu = 0;
            }
            $result->save();
            return response($result);
        }
    }
    public function ft(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_post3::find($req->id);
            if ($result->ft == 0)
            {
                $result->ft = 1;
            } else {
                $result->ft = 0;
            }
            $result->save();
            return response($result);
        }
    }
        public function is_home(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_post3::find($req->id);
            if ($result->is_home == 0)
            {
                $result->is_home = 1;
            } else {
                $result->is_home = 0;
            }
            $result->save();
            return response($result);
        }
    }
}
