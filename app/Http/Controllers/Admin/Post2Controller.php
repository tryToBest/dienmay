<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post2;
use App\Models\Cate_product;
use App\Http\Requests\Post2Request;
use App\Models\Cate_post2;
use App\Models\Image_duan;
use Image,File;

use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Imports\Post2sImport;
use Maatwebsite\Excel\Facades\Excel;

class Post2Controller extends Controller
{
    public function index()
    {
        $post2 = Post2::orderBy('position', 'DESC')->get();
        $data = Cate_post2::select('id','name_vi','parent_id')->get()->toArray();

        return view('admin.post2.index', compact('post2', 'data'));
    }

    public function create()
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post2s,$quyen[4]=post2s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {

        $data = Cate_post2::select('id','name_vi','parent_id')->get()->toArray();
        if ($data != null) {

            return view('admin.post2.create', compact('data'));
        } else {

            return redirect()->route('admin.cate_post2.home')->with('error', 'Chưa có danh mục');
        }
 }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}

    }

    public function post2Create(Request $req)
    {
                  $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post2s,$quyen[4]=post2s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $post2                 = new Post2;
        $post2->name_vi        = $req['name_vi'];
        $post2->name_en        = $req['name_en'];
                $post2->name_tq        = $req['name_tq'];
        $post2->slug_vi        = str_slug($req['name_vi']);
        $post2->slug_en        = str_slug($req['name_en']);
        $post2->cate_post2_id   = $req['cate_post2_id'];
        $post2->position       = $req['position'];
        $post2->status         = (is_null($req['status']) ? '0' : '1');
        $post2->is_home        = (is_null($req['is_home']) ? '0' : '1');
        $post2->title_vi       = $req['title_vi'];
        $post2->title_en       = $req['title_en'];
  $post2->hangmuc  = $req['hangmuc'];
    $post2->khachhang  = $req['khachhang'];
      $post2->diadiem  = $req['diadiem'];
        $post2->dientich  = $req['dientich'];
          $post2->phongcach  = $req['phongcach'];
            $post2->namthuchien  = $req['namthuchien'];
              $post2->thoigianthicong        = $req['thoigianthicong'];
                $post2->thoigianthietke        = $req['thoigianthietke'];
                $post2->title_tq       = $req['title_tq'];
        $post2->description_vi = $req['description_vi'];
        $post2->description_en = $req['description_en'];
        $post2->title_seo_vi   = $req['title_seo_vi'];
        $post2->title_seo_en   = $req['title_seo_en'];
        $post2->meta_key_vi    = $req['meta_key_vi'];
        $post2->meta_key_en    = $req['meta_key_en'];
        $post2->meta_des_vi    = $req['meta_des_vi'];
        $post2->meta_des_en    = $req['meta_des_en'];
        if ($req->hasFile('image')) {
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/duan/'.$filename));
            $post2->image = ('upload/images/duan/'.$filename);
        }

        //            if($req->hasFile('pdf_tailieu')){
         
        //     $pdf_tailieu = $req->file('pdf_tailieu');
        //     $filename = $pdf_tailieu->getClientOriginalName();
        //     $path = public_path().'/images/upload/';
        //     $pdf_tailieu->move($path,$filename);
        //     $post2->pdf_tailieu = ('/images/upload/'.$filename);
        // }

        $post2->save();

        return redirect()->route('admin.post2.index');
         }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }

    public function update($id)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post2s,$quyen[4]=post2s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $data = Cate_post2::select('id','name_vi','parent_id')->get()->toArray();
        $post2 = Post2::findOrFail($id);


 $cate_product_cha = Cate_product::select('id','name_vi','parent_id')->where('status',1)->where('parent_id',23)->get()->toArray();
            $list_filter_video =array();
            $dem = 0;
            foreach($cate_product_cha as $k=>$v){
               $list_filter_video[$dem]['title']=$v;
               $list_filter_video[$dem]['id']=$k;
               $list_filter_video[$dem++]['sub']= Cate_product::select('id','name_vi','parent_id')->where('status',1)->where('parent_id',$v)->get()->toArray();
            }


 $img_detail = Image_duan::where('post2_id', $post2->id)->get();

        return view('admin.post2.edit', compact('data', 'post2','list_filter_video','img_detail'));
          }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }

    public function post2Update($id, Request $req)
    {
                   $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post2s,$quyen[4]=post2s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $post2               = Post2::findOrFail($id);
        $post2->name_vi         = $req['name_vi'];
        $post2->name_en         = $req['name_en'];
          $post2->name_tq         = $req['name_tq'];
        $post2->slug_vi         = str_slug($req['name_vi']);
        $post2->slug_en         = str_slug($req['name_en']);
        $post2->cate_post2_id = $req['cate_post2_id'];
        $post2->position     = $req['position'];
        $post2->status       = (is_null($req['status']) ? '0' : '1');
        $post2->is_home      = (is_null($req['is_home']) ? '0' : '1');
        $post2->title_vi        = $req['title_vi'];
        $post2->title_en        = $req['title_en'];
                $post2->title_tq        = $req['title_tq'];
                  $post2->link_yt  = $req['link_yt'];
                           $post2->thoigianthicong        = $req['thoigianthicong'];
                $post2->thoigianthietke        = $req['thoigianthietke'];

  $post2->hangmuc  = $req['hangmuc'];
    $post2->khachhang  = $req['khachhang'];
      $post2->diadiem  = $req['diadiem'];
        $post2->dientich  = $req['dientich'];
          $post2->phongcach  = $req['phongcach'];
            $post2->namthuchien  = $req['namthuchien'];


        $post2->description_vi  = $req['description_vi'];
        $post2->description_en  = $req['description_en'];
                $post2->description_tq  = $req['description_tq'];
        $post2->title_seo_vi    = $req['title_seo_vi'];
        $post2->title_seo_en    = $req['title_seo_en'];
        $post2->meta_key_vi     = $req['meta_key_vi'];
        $post2->meta_key_en     = $req['meta_key_en'];
        $post2->meta_des_vi     = $req['meta_des_vi'];
        $post2->meta_des_en     = $req['meta_des_en'];
                 if(isset($_POST2['filter_video'])){
                $filter_video='|';
                foreach($_POST2['filter_video'] as $k=>$v) $filter_video.=$v.'|';
                $post2->video_id_product  = $filter_video;
            }


        if ($req->hasFile('image')){
            if (file_exists($post2->image)) {
                unlink($post2->image);
            }
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/duan/'.$filename));
            $post2->image = ('upload/images/duan/'.$filename);
        }

        //         if($req->hasFile('pdf_tailieu')){
        //     if(file_exists($post2->pdf_tailieu))
        //     {
        //         unlink($post2->pdf_tailieu);
        //     }
        //     $pdf_tailieu = $req->file('pdf_tailieu');
        //     $filename = $pdf_tailieu->getClientOriginalName();
        //     $path = public_path().'/images/upload/';
        //     $pdf_tailieu->move($path,$filename);
        //     $post2->pdf_tailieu = ('/images/upload/'.$filename);
        // }


        $validatedData = $req->validate([
            'name_vi'     => 'required|unique:post2s,name_vi,' .$post2->id,
            // 'name_en'     => 'required|unique:post2s,name_en,' .$post2->id,
            'position' => 'numeric|nullable|min:0|unique:post2s,position,' .$post2->id,
        ]);
        $post2->save();

                if ($req->hasFile('img')) {

            $img = $req->file('img');

            foreach ($img as $key => $i) {

                $filename = date('Y_d_m_H_i_s').'-'. $i->getClientOriginalName();

                Image::make($i)->save(public_path('upload/images/duan/'.$filename));

                $i = new Image_duan;

                $i->post2_id = $post2->id;

                $i->name = ('upload/images/duan/'.$filename);

                $i->save();

            }

        }

        return redirect()->route('admin.post2.index')->with('success', 'Sửa thành công');
                  }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }

    public function destroy($id)
    {
         $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post2s,$quyen[4]=post2s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {
        $result = Post2::findOrFail($id);
        if (file_exists($result->image)) {
            unlink($result->image);
        }
        $result->delete();

        return redirect()->back()->with('success', 'Xóa thành công');
                  }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền Xóa bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }

    public function search(Request $req)
    {
        $id_cate_post2 = $req->cate_post2;
        if ($id_cate_post2 == 0) {

            return redirect()->route('admin.post2.index');
        }
        session()->put('id',$id_cate_post2);
        $data = Cate_post2::select('id','name_vi','parent_id')->get()->toArray();
        $post2 = Post2::orderBy('position','ASC')->where(function($query)
        {
            $pro       = $query;
            $id        = session('id');
            $cate_post2 = Cate_post2::find($id);
            $pro = $pro->orWhere('cate_post2_id',$cate_post2->id); // bài viết có id của danh muc cha cấp 1.
            $com = Cate_post2::where('parent_id',$cate_post2->id)->get();//danh mục cha cấp 2.
            foreach ($com as $dt) {
                $pro = $pro->orWhere('cate_post2_id',$dt->id);// bài viết có id của danh muc cha cấp 2.
            }
            session()->forget('id');//xóa session;
        })->get();

        return view('admin.post2.search', compact('post2', 'data', 'id_cate_post2'));
    }

    public function checkbox(Request $req)
    {
        $checkbox = $req->checkbox;
        if (!isset($req->checkbox)) {

            return back()->with('success', 'Chưa chọn bài');
        }
        if ($req->select_action == 1) {
            $checkbox = $req->checkbox;
            foreach ($checkbox as $c) {
                $result = Post2::findOrFail($c);
                if(file_exists($result->image)) {
                    unlink($result->image);
                }
                $result->delete();
            }

            return redirect()->back()->with('success', 'Xóa thành công');
        }
        if ($req->select_action == 2) {
            $checkbox = $req->checkbox;
            foreach ($checkbox as $c) {
                $result = Post2::where('id', $c)->first();
                $result->status = 1;
                $result->save();
            }

            return back()->with('success', 'Thao tác thành công');
        }

        if ($req->select_action == 3) {
            $checkbox = $req->checkbox;
            foreach ($checkbox as $c) {
                $result = Post2::where('id', $c)->first();
                $result->status = 0;
                $result->save();
            }

            return back()->with('success', 'Thao tác thành công');
        }
        if ($req->select_action == 0) {

            return back()->with('success', 'Chưa chọn thao tác');
        }
        if ($checkbox == NULL){

            return back()->with('success', 'Bạn chưa chọn cái nào');
        }
    }

    public function status(Request $req)
    {
        if ($req->ajax()) {
            $result = Post2::find($req->id);
            if ($result->status == 0) {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();

            return response($result);
        }
    }

    public function is_home(Request $req)
    {
        if ($req->ajax()) {
            $result = Post2::find($req->id);
            if ($result->is_home == 0) {
                $result->is_home = 1;
            } else {
                $result->is_home = 0;
            }
            $result->save();

            return response($result);
        }
    }
        public function sinhnhat_home(Request $req)
    {
        if ($req->ajax()) {
            $result = Post2::find($req->id);
            if ($result->sinhnhat_home == 0) {
                $result->sinhnhat_home = 1;
            } else {
                $result->sinhnhat_home = 0;
            }
            $result->save();

            return response($result);
        }
    }
       public function tvnb(Request $req)
    {
        if ($req->ajax()) {
            $result = Post2::find($req->id);
            if ($result->tvnb == 0) {
                $result->tvnb = 1;
            } else {
                $result->tvnb = 0;
            }
            $result->save();

            return response($result);
        }
    }

        public function importthanhvien(Request $req)
    {

       // dd($req->file);
       //  $validatedData = $req->validate([
       //      'file'     => 'required|mimes:xlsx',
       //  ]);
        Excel::import(new Post2sImport, $req->file);
        return back()->with('success', 'All good!');
    }


}
