<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Cate_product;
use App\Http\Requests\PostRequest;
use App\Models\Cate_post;
use Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function index()
    {
        $post = Post::select('id','name_vi','slug_vi','image','status','is_home','cate_post_id','created_at','updated_at','position','footer1','footer2')->orderBy('id', 'DESC')->get();
        $data = Cate_post::select('id','name_vi','parent_id')->get()->toArray();

        return view('admin.post.index', compact('post', 'data'));
    }

    public function create()
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {

        $data = Cate_post::select('id','name_vi','parent_id')->get()->toArray();
        if ($data != null) {

            return view('admin.post.create', compact('data'));
        } else {

            return redirect()->route('admin.cate_post.home')->with('error', 'Chưa có danh mục');
        }
 }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}

    }

    public function postCreate(PostRequest $req)
    {
                  $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $post                 = new Post;
        $post->name_vi        = $req['name_vi'];
        $post->name_en        = $req['name_en'];
                $post->name_tq        = $req['name_tq'];
        $post->slug_vi        = str_slug($req['name_vi']);
        $post->slug_en        = str_slug($req['name_en']);
        $post->cate_post_id   = $req['cate_post_id'];
        $post->position       = $req['position'];
        $post->status         = (is_null($req['status']) ? '0' : '1');
        $post->is_home        = (is_null($req['is_home']) ? '0' : '1');
        $post->title_vi       = $req['title_vi'];
        $post->title_en       = $req['title_en'];
                $post->title_tq       = $req['title_tq'];
        $post->description_vi = $req['description_vi'];
        $post->description_en = $req['description_en'];         $post->loaihinh_vi         = $req['loaihinh_vi'];
        $post->title_seo_vi   = $req['title_seo_vi'];
        $post->title_seo_en   = $req['title_seo_en'];
        $post->meta_key_vi    = $req['meta_key_vi'];
        $post->meta_key_en    = $req['meta_key_en'];
        $post->meta_des_vi    = $req['meta_des_vi'];
        $post->meta_des_en    = $req['meta_des_en'];
        if ($req->hasFile('image')) {
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/post/'.$filename));
            $post->image = ('upload/images/post/'.$filename);
        }

        //            if($req->hasFile('pdf_tailieu')){
         
        //     $pdf_tailieu = $req->file('pdf_tailieu');
        //     $filename = $pdf_tailieu->getClientOriginalName();
        //     $path = public_path().'/images/upload/';
        //     $pdf_tailieu->move($path,$filename);
        //     $post->pdf_tailieu = ('/images/upload/'.$filename);
        // }

        $post->save();

        return redirect()->route('admin.post.index');
         }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }

    public function update($id)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $data = Cate_post::select('id','name_vi','parent_id')->get()->toArray();
        $post = Post::findOrFail($id);


 $cate_product_cha = Cate_product::select('id','name_vi','parent_id')->where('status',1)->where('parent_id',23)->get()->toArray();
            $list_filter_video =array();
            $dem = 0;
            foreach($cate_product_cha as $k=>$v){
               $list_filter_video[$dem]['title']=$v;
               $list_filter_video[$dem]['id']=$k;
               $list_filter_video[$dem++]['sub']= Cate_product::select('id','name_vi','parent_id')->where('status',1)->where('parent_id',$v)->get()->toArray();
            }




        return view('admin.post.edit', compact('data', 'post','list_filter_video'));
          }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }

    public function postUpdate($id, Request $req)
    {
                   $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $post               = Post::findOrFail($id);
        $post->name_vi         = $req['name_vi'];
        $post->name_en         = $req['name_en'];
          $post->name_tq         = $req['name_tq'];
        $post->slug_vi         = str_slug($req['name_vi']);
        $post->slug_en         = str_slug($req['name_en']);
        $post->cate_post_id = $req['cate_post_id'];
          $post->loaihinh_vi         = $req['loaihinh_vi'];
        $post->position     = $req['position'];
        $post->status       = (is_null($req['status']) ? '0' : '1');
        $post->is_home      = (is_null($req['is_home']) ? '0' : '1');
        $post->title_vi        = $req['title_vi'];
        $post->title_en        = $req['title_en'];
                $post->title_tq        = $req['title_tq'];
                  $post->link_yt  = $req['link_yt'];
        $post->description_vi  = $req['description_vi'];
        $post->description_en  = $req['description_en'];
                $post->description_tq  = $req['description_tq'];
        $post->title_seo_vi    = $req['title_seo_vi'];
        $post->title_seo_en    = $req['title_seo_en'];
        $post->meta_key_vi     = $req['meta_key_vi'];
        $post->meta_key_en     = $req['meta_key_en'];
        $post->meta_des_vi     = $req['meta_des_vi'];
        $post->meta_des_en     = $req['meta_des_en'];
                 if(isset($_POST['filter_video'])){
                $filter_video='|';
                foreach($_POST['filter_video'] as $k=>$v) $filter_video.=$v.'|';
                $post->video_id_product  = $filter_video;
            }


        if ($req->hasFile('image')){
            if (file_exists($post->image)) {
                unlink($post->image);
            }
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/post/'.$filename));
            $post->image = ('upload/images/post/'.$filename);
        }

        //         if($req->hasFile('pdf_tailieu')){
        //     if(file_exists($post->pdf_tailieu))
        //     {
        //         unlink($post->pdf_tailieu);
        //     }
        //     $pdf_tailieu = $req->file('pdf_tailieu');
        //     $filename = $pdf_tailieu->getClientOriginalName();
        //     $path = public_path().'/images/upload/';
        //     $pdf_tailieu->move($path,$filename);
        //     $post->pdf_tailieu = ('/images/upload/'.$filename);
        // }


        $validatedData = $req->validate([
            'name_vi'     => 'required|unique:posts,name_vi,' .$post->id,
            // 'name_en'     => 'required|unique:posts,name_en,' .$post->id,
            'position' => 'numeric|nullable|min:0|unique:posts,position,' .$post->id,
        ]);
        $post->save();

        return redirect()->route('admin.post.index')->with('success', 'Sửa thành công');
                  }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }

    public function destroy($id)
    {
         $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {
        $result = Post::findOrFail($id);
        if (file_exists($result->image)) {
            unlink($result->image);
        }
        $result->delete();

        return redirect()->back()->with('success', 'Xóa thành công');
                  }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền Xóa bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }

    public function search(Request $req)
    {
        $id_cate_post = $req->cate_post;
        if ($id_cate_post == 0) {

            return redirect()->route('admin.post.index');
        }
        session()->put('id',$id_cate_post);
        $data = Cate_post::select('id','name_vi','parent_id')->get()->toArray();
        $post = Post::orderBy('position','ASC')->where(function($query)
        {
            $pro       = $query;
            $id        = session('id');
            $cate_post = Cate_post::find($id);
            $pro = $pro->orWhere('cate_post_id',$cate_post->id); // bài viết có id của danh muc cha cấp 1.
            $com = Cate_post::where('parent_id',$cate_post->id)->get();//danh mục cha cấp 2.
            foreach ($com as $dt) {
                $pro = $pro->orWhere('cate_post_id',$dt->id);// bài viết có id của danh muc cha cấp 2.
            }
            session()->forget('id');//xóa session;
        })->get();

        return view('admin.post.search', compact('post', 'data', 'id_cate_post'));
    }

    public function checkbox(Request $req)
    {
        $checkbox = $req->checkbox;
        if (!isset($req->checkbox)) {

            return back()->with('success', 'Chưa chọn bài');
        }
        if ($req->select_action == 1) {
            $checkbox = $req->checkbox;
            foreach ($checkbox as $c) {
                $result = Post::findOrFail($c);
                if(file_exists($result->image)) {
                    unlink($result->image);
                }
                $result->delete();
            }

            return redirect()->back()->with('success', 'Xóa thành công');
        }
        if ($req->select_action == 2) {
            $checkbox = $req->checkbox;
            foreach ($checkbox as $c) {
                $result = Post::where('id', $c)->first();
                $result->status = 1;
                $result->save();
            }

            return back()->with('success', 'Thao tác thành công');
        }

        if ($req->select_action == 3) {
            $checkbox = $req->checkbox;
            foreach ($checkbox as $c) {
                $result = Post::where('id', $c)->first();
                $result->status = 0;
                $result->save();
            }

            return back()->with('success', 'Thao tác thành công');
        }
        if ($req->select_action == 0) {

            return back()->with('success', 'Chưa chọn thao tác');
        }
        if ($checkbox == NULL){

            return back()->with('success', 'Bạn chưa chọn cái nào');
        }
    }

    public function status(Request $req)
    {
        if ($req->ajax()) {
            $result = Post::find($req->id);
            if ($result->status == 0) {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();

            return response($result);
        }
    }

    public function is_home(Request $req)
    {
        if ($req->ajax()) {
            $result = Post::find($req->id);
            if ($result->is_home == 0) {
                $result->is_home = 1;
            } else {
                $result->is_home = 0;
            }
            $result->save();

            return response($result);
        }
    }
    public function footer1(Request $req)
    {
        if ($req->ajax()) {
            $result = Post::find($req->id);
            if ($result->footer1 == 0) {
                $result->footer1 = 1;
            } else {
                $result->footer1 = 0;
            }
            $result->save();

            return response($result);
        }
    }
    public function footer2(Request $req)
    {
        if ($req->ajax()) {
            $result = Post::find($req->id);
            if ($result->footer2 == 0) {
                $result->footer2 = 1;
            } else {
                $result->footer2 = 0;
            }
            $result->save();

            return response($result);
        }
    }
}
