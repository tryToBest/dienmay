<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slide;
use App\Http\Requests\SlideRequest;
use Image;
use Illuminate\Support\Facades\Validator;

class AlbumController extends Controller
{
    public function index()
    {
        $album = Slide::where('dislay', 12)->orWhere('dislay', 13)->orderBy('id', 'DESC')->paginate(20);

        return view('admin.album.index', compact('album'));
    }

    public function create()
    {
        return view('admin.album.create');
    }

    public function postCreate(SlideRequest $req)
    {
        $album              = new Slide;
        $album->name_vi        = $req['name_vi'];
        $album->name_en        = $req['name_en'];
        $album->description_vi = $req['description_vi'];
        $album->description_en = $req['description_en'];
        $album->link        = $req['link'];
        $album->position    = $req['position'];
        $album->dislay      = $req['dislay'];
        $album->status      = (is_null($req['status']) ? '0' : '1');
        if($req->hasFile('image')){
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/slide/'.$filename));
            $album->image = ('upload/images/slide/'.$filename);
        }
        $album->save();

        return redirect()->route('admin.album.index');
    }

    public function update($id)
    {
        $album = Slide::where('id', $id)->first();

        return view('admin.album.edit', compact('album'));
    }

    public function postUpdate($id, Request $req)
    {
        $album              = Slide::where('id', $id)->first();
        $album->name_vi        = $req['name_vi'];
        $album->name_en        = $req['name_en'];
        $album->description_vi = $req['description_vi'];
        $album->description_en = $req['description_en'];
        $album->link        = $req['link'];
        $album->position    = $req['position'];
        $album->dislay      = $req['dislay'];
        $album->status      = (is_null($req['status']) ? '0' : '1');
        if($req->hasFile('image'))
        {
            if(file_exists($album->image))
            {
                unlink($album->image);
            }
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/slide/'.$filename));
            $album->image = ('upload/images/slide/'.$filename);
        }
        $album->save();

        return redirect()->route('admin.album.index')->with('success', 'Sửa thành công');
    }

    public function destroy($id)
    {
        $result = Slide::findOrFail($id);
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        $result->delete();

        return redirect()->back()->with('success', 'Xóa thành công');
    }

    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Slide::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();

            return response($result);
        }
    }
}

