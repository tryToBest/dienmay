<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;
use Datatables;

class LoginController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/admin';

    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }
    public function index()
    {

            if(Auth::user()->role==2){
                 $administrator = User::all();
             }else{
 $administrator = User::where('status',1)->where('role',3)->get();
             }
       
        

        return view('admin.administrator.index', compact('administrator'));
    }

    public function create()
    {
        return view('admin.administrator.create');
    }

    public function postCreate(UserRequest $data)
    {
        $administrator           = new User;
        $administrator->name     = $data['name'];
           $administrator->nguoitao_id     = $data['nguoitao_id'];
            $administrator->role     = $data['role'];
// if (Auth::user()->role ==2) {
//     $bien='';
//             for($i=1;$i<12;$i++){
//                 $modul=explode('-',$_POST["modul$i"]);
              
//                 $b=isset($_POST["sub2$i"])?$_POST["sub2$i"]:'0';
//                 $c=isset($_POST["sub3$i"])?$_POST["sub3$i"]:'0';
//                 $d=isset($_POST["sub4$i"])?$_POST["sub4$i"]:'0';
           
                                
//                 $bien=$bien.$modul[0].'-'.$b.'-'.$c.'-'.$d.'|';
//                 //if(isset($modul[1])) $bien=$bien.$modul[1].'_'.$a.'-'.$b.'-'.$c.'-'.$d.'-'.$e.'|';
//             }


//   $administrator->quyen     = $bien;
// }
        $administrator->password = Hash::make($data['password']);
        $administrator->email    = $data['email'];
        $administrator->status   = (is_null($data['status']) ? '0' : '1');
        $administrator->save();

        return redirect()->route('admin.administrator.home');

    }
    public function getLogin()
    {

        return view('admin.administrator.login');
    }
    public function postLogin(Request $req)
    {
        $login = [
            'email'     => $req->email,
            'password' => $req->password,
            'role'     => [3,2],
        ];
        if (Auth::attempt($login)) {

            return redirect()->route('admin.index');
        }else {

            return redirect()->back()->with('message', 'Email hoặc mật khẩu không đúng');
        }
    }

    public function update($id)
    {
        $administrator = User::findOrFail($id);

        return view('admin.administrator.edit', compact('administrator'));
    }

    public function postUpdate($id, Request $data)
    {
        $administrator        = User::findOrFail($id);
        $administrator->name  = $data['name'];
        $administrator->email = $data['email'];
        $administrator->role = $data['role'];

// if (Auth::user()->role ==2) {
 

// $bien='';
//             for($i=1;$i<12;$i++){
//                 $modul=explode('-',$_POST["modul$i"]);
                
//                 $b=isset($_POST["sub2$i"])?$_POST["sub2$i"]:'0';
//                 $c=isset($_POST["sub3$i"])?$_POST["sub3$i"]:'0';
//                 $d=isset($_POST["sub4$i"])?$_POST["sub4$i"]:'0';
               
                            
//                 $bien=$bien.$modul[0].'-'.$b.'-'.$c.'-'.$d.'|';
//                 //if(isset($modul[1])) $bien=$bien.$modul[1].'_'.$a.'-'.$b.'-'.$c.'-'.$d.'-'.$e.'|';
//             }
//             // dd($bien);
//                 $administrator->quyen = $bien;
// }



        $validatedData = $data->validate([
                'name'  => 'required|unique:users,name,' .$administrator->id,
                'email' => 'required|email|max:200|unique:users,email,' .$administrator->id,
            ]);
        if($data->password == null){
            $administrator->save();
        }else{
            $administrator->password = Hash::make($data['password']);
            $administrator->save();
        }

        return redirect()->route('admin.administrator.home');

    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('admin.index');
    }

    public function destroy($id)
    {
        User::findOrFail($id)->delete();

        return redirect()->route('admin.administrator.home');
    }
}
