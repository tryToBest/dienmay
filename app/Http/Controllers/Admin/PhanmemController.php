<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Phanmem;
use App\Http\Requests\PhanmemRequest;
use Image;
use File;
use Illuminate\Support\Facades\Auth;
class PhanmemController extends Controller
{
    public function index()
    {
        $phanmem = Phanmem::all();
        return view('admin/phanmem/index', compact('phanmem'));
    }
    public function create()
    {
          $quyen=explode('|',Auth::user()->quyen);

 $sub=explode('-',$quyen[10]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=gioithieus,$quyen[9]=uudais,$quyen[10]=phanmems  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
        // dd($sub);
if ($sub[3]==1) {
        $data=Phanmem::select('id','name_vi','parent_id')->get()->toArray();
        return view('admin/phanmem/create', compact('data'));
         }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm phần mềm!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function postCreate(PhanmemRequest $req)
    {
             $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[10]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=gioithieus,$quyen[9]=uudais,$quyen[10]=phanmems  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $phanmem                 = new Phanmem;
        $phanmem->name_vi        = $req['name_vi'];
        $phanmem->parent_id      = $req->parent_id;
        $phanmem->slug_vi        = str_slug($phanmem->name_vi);
        $phanmem->description_vi = $req['description_vi'];
        $phanmem->position       = $req['position'];
        $phanmem->status         = (is_null($req['status']) ? '0' : '1');
        $phanmem->title_seo_vi   = $req['title_seo_vi'];
        $phanmem->meta_key_vi    = $req['meta_key_vi'];
        $phanmem->meta_des_vi    = $req['meta_des_vi'];

      if($req->hasFile('tailieu')){
         
            $tailieu = $req->file('tailieu');
            $filename = date('Y_d_m_H_i_s').'-'. $tailieu->getClientOriginalName();
            $path = public_path().'/tailieu/upload/';
            
            // Image::make($tailieu)->save(public_path('images/upload/'.$filename));
            $tailieu->move($path,$filename);
            $phanmem->tailieu = ('/tailieu/upload/'.$filename);
        }


  




        $phanmem->save();
        return redirect()->route('admin.phanmem.home')->with('success','Thêm thành công');
                 }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm phần mềm!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function update($id)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[10]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=gioithieus,$quyen[9]=uudais,$quyen[10]=phanmems  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $data      = Phanmem::select('id','name_vi','parent_id')->get()->toArray();
        $phanmem = Phanmem::findOrFail($id);
        return view('admin.phanmem.edit', compact('phanmem', 'data'));
                 }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa phần mềm!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function postUpdate($id, Request $req)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[10]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=gioithieus,$quyen[9]=uudais,$quyen[10]=phanmems  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $phanmem                 = Phanmem::findOrFail($id);
        $phanmem->name_vi        = $req['name_vi'];
        $phanmem->parent_id      = $req->parent_id;
        $phanmem->slug_vi        = str_slug($phanmem->name_vi);
        // $phanmem->slug_en        = str_slug($phanmem->name_en);
        $phanmem->position       = $req['position'];
        $phanmem->description_vi = $req['description_vi'];
        $phanmem->status         = (is_null($req['status']) ? '0' : '1');
        $phanmem->title_seo_vi   = $req['title_seo_vi'];
        $phanmem->meta_key_vi    = $req['meta_key_vi'];
        $phanmem->meta_des_vi    = $req['meta_des_vi'];
        $validatedData = $req->validate([
            'name_vi'     => 'required|unique:phanmems,name_vi,' .$phanmem->id,
            'position' => 'numeric|nullable|min:0|unique:phanmems,position,' .$phanmem->id,
        ]);
              if($req->hasFile('tailieu')){
            if(file_exists($phanmem->tailieu))
            {
                unlink($phanmem->tailieu);
            }
            $tailieu = $req->file('tailieu');
            $filename = date('Y_d_m_H_i_s').'-'. $tailieu->getClientOriginalName();
            $path = public_path().'/tailieu/upload/';
            
            // Image::make($tailieu)->save(public_path('images/upload/'.$filename));
            $tailieu->move($path,$filename);
            $phanmem->tailieu = ('/tailieu/upload/'.$filename);
        }

        $phanmem->save();
        return redirect()->route('admin.phanmem.home')->with('success','Sửa thành công');
        }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa phần mềm!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function destroy($id)
    {
               $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[10]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=gioithieus,$quyen[9]=uudais,$quyen[10]=phanmems  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {
        $result = Phanmem::findOrFail($id);
        $result2 = Phanmem::where('parent_id', $id)->first();
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        if(isset($result2))
        {
            if(file_exists($result2->image))
            {
                unlink($result2->image);
            }
            $result2->delete();
        }
        $result->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
        }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền xóa phần mềm!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Phanmem::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();
            return response($result);
        }
    }
}
