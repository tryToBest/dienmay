<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Intro;
use App\Models\Imageintros;
use App\Http\Requests\IntroRequest;
use Image;
use Illuminate\Support\Facades\Validator;

class IntroController extends Controller
{
    public function index()
    {
        $intro = Intro::paginate(10);

        return view('admin.intro.index', compact('intro'));
    }

    // public function loadData()
    // {
    //     $intro = Intro::all();

    //     return view('admin.intro.data', compact('intro'));
    // }

    // public function store(Request $req)
    // {
    //     if ($req->ajax()) {
    //         $intro              = new Intro;
    //         $intro->name        = $req['name'];
    //         $intro->slug        = str_slug($req['name']);
    //         $intro->position    = $req['position'];
    //         $intro->status      = (is_null($req['status']) ? '0' : '1');
    //         $intro->title_seo_vi   = $req['title_seo_vi'];
    //         $intro->meta_key_vi    = $req['meta_key_vi'];
    //         $intro->meta_des_vi    = $req['meta_des_vi'];
    //         $intro->save();
    //         echo json_encode($intro);
    //     }
    // }

    // public function delete(Request $req)
    // {
    //     if ($req->ajax()) {
    //         $result = Intro::findOrFail($req->id);
    //         Intro::destroy($result);
    //         return response(['message' => 'xóa thành công']);
    //     }
    // }

    // public function getUpdate(Request $req)
    // {
    //     if ($req->ajax()) {
    //         $intro = Intro::find($req->id);
    //         return response($intro);
    //     }
    // }

    // public function updates(Request $req)
    // {
    //     if ($req->ajax()) {
    //         $intro = Intro::find($req->id);
    //         $intro->name        = $req['name'];
    //         $intro->slug        = str_slug($req['name']);
    //         $intro->position    = $req['position'];
    //         $intro->status      = (is_null($req['status']) ? '0' : '1');
    //         $intro->title_seo_vi   = $req['title_seo_vi'];
    //         $intro->meta_key_vi    = $req['meta_key_vi'];
    //         $intro->meta_des_vi    = $req['meta_des_vi'];
    //         $intro->save();
    //         echo json_encode($intro);
    //     }
    // }

    public function create()
    {

        return view('admin.intro.create');
    }

    public function postCreate(Request $req)
    {
        $intro              = new Intro;
        $intro->name_vi        = $req['name_vi'];
        $intro->slug        = str_slug($req['name_vi']);
        $intro->position    = $req['position'];
        $intro->status      = (is_null($req['status']) ? '0' : '1');
        $intro->description_vi = $req['description_vi'];
        $intro->title_seo_vi   = $req['title_seo_vi'];
        $intro->meta_key_vi    = $req['meta_key_vi'];
        $intro->meta_des_vi    = $req['meta_des_vi'];
        $intro->save();

        return redirect()->route('admin.intro.index');
    }

    public function update($slug)
    {
        $intro = Intro::where('slug', $slug)->first();
 $img_detail = Imageintros::where('intro_id', $intro->id)->get();
        return view('admin.intro.edit', compact('intro','img_detail'));
    }

    public function postUpdate($slug, Request $req)
    {
        $intro              = Intro::where('slug', $slug)->first();
        $intro->name_vi        = $req['name_vi'];
        $intro->slug        = str_slug($req['name_vi']);
        $intro->position    = $req['position'];
             $intro->video = $req['video'];
                 $intro->ten_video = $req['ten_video'];
                     $intro->diadiem = $req['diadiem'];
        $intro->status      = (is_null($req['status']) ? '0' : '1');
                $intro->title_vi = $req['title_vi'];
                        $intro->title2_vi = $req['title2_vi'];
                        $intro->title3_vi = $req['title3_vi'];
                        $intro->title4_vi = $req['title4_vi'];
                        $intro->title5_vi = $req['title5_vi'];
                        $intro->title6_vi = $req['title6_vi'];
                        $intro->title7_vi = $req['title7_vi'];
                        $intro->title8_vi = $req['title8_vi'];
                        $intro->title9_vi = $req['title9_vi'];
                        $intro->title10_vi = $req['title10_vi'];
                        $intro->tieude_giatri1 = $req['tieude_giatri1'];
                          $intro->tieude_giatri2 = $req['tieude_giatri2'];
                            $intro->tieude_giatri3 = $req['tieude_giatri3'];
                              $intro->tieude_giatri4 = $req['tieude_giatri4'];
                                   $intro->content_giatri1 = $req['content_giatri1'];
                                       $intro->content_giatri2 = $req['content_giatri2'];
                                           $intro->content_giatri3 = $req['content_giatri3'];
                                               $intro->content_giatri4 = $req['content_giatri4'];
        $intro->description_vi = $req['description_vi'];
        $intro->title_seo_vi   = $req['title_seo_vi'];
        $intro->meta_key_vi    = $req['meta_key_vi'];
        $intro->meta_des_vi    = $req['meta_des_vi'];
        $validatedData = $req->validate([
            'name_vi'     => 'required|unique:intros,name_vi,' .$intro->id,
            'position' => 'numeric|nullable|min:0|unique:intros,position,' .$intro->id,
        ]);

              if ($req->hasFile('image')) {
            if(file_exists($intro->image)) {
                unlink($intro->image);
            }
            $image = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/intro/'.$filename));
            $intro->image = ('upload/images/intro/'.$filename);
        }

       if ($req->hasFile('image2')) {
            if(file_exists($intro->image2)) {
                unlink($intro->image2);
            }
            $image2 = $req->file('image2');
            $filename = date('Y_d_m_H_i_s').'-'. $image2->getClientOriginalName();
            Image::make($image2)->save(public_path('upload/images/intro/'.$filename));
            $intro->image2 = ('upload/images/intro/'.$filename);
        }

                      if($req->hasFile('image_giatri1')){
            if(file_exists($intro->image_giatri1))
            {
                unlink($intro->image_giatri1);
            }
            $image_giatri1 = $req->file('image_giatri1');
            $filename = $image_giatri1->getClientOriginalName();
            $path = public_path().'/upload/images/intro/';
            $image_giatri1->move($path,$filename);
            $intro->image_giatri1 = ('/upload/images/intro/'.$filename);
        }

                      if($req->hasFile('image_giatri2')){
            if(file_exists($intro->image_giatri2))
            {
                unlink($intro->image_giatri2);
            }
            $image_giatri2 = $req->file('image_giatri2');
            $filename = $image_giatri2->getClientOriginalName();
            $path = public_path().'/upload/images/intro/';
            $image_giatri2->move($path,$filename);
            $intro->image_giatri2 = ('/upload/images/intro/'.$filename);
        }


                      if($req->hasFile('image_giatri3')){
            if(file_exists($intro->image_giatri3))
            {
                unlink($intro->image_giatri3);
            }
            $image_giatri3 = $req->file('image_giatri3');
            $filename = $image_giatri3->getClientOriginalName();
            $path = public_path().'/upload/images/intro/';
            $image_giatri3->move($path,$filename);
            $intro->image_giatri3 = ('/upload/images/intro/'.$filename);
        }
         if($req->hasFile('image_giatri4')){
            if(file_exists($intro->image_giatri4))
            {
                unlink($intro->image_giatri4);
            }
            $image_giatri4 = $req->file('image_giatri4');
            $filename = $image_giatri4->getClientOriginalName();
            $path = public_path().'/upload/images/intro/';
            $image_giatri4->move($path,$filename);
            $intro->image_giatri4 = ('/upload/images/intro/'.$filename);
        }






        $intro->save();
 if ($req->hasFile('img')) {
            $img = $req->file('img');
            foreach ($img as $key => $i) {
                $filename = date('Y_d_m_H_i_s').'-'. $i->getClientOriginalName();
                Image::make($i)->save(public_path('upload/images/intro/'.$filename));
                $i = new Imageintros;
                $i->intro_id = $intro->id;
                $i->name = ('upload/images/intro/'.$filename);
                $i->save();
            }
        }
        return redirect()->route('admin.intro.index')->with('success', 'Sửa thành công');
    }

    public function destroy($id)
    {
        $result = Intro::findOrFail($id);
        if (file_exists($result->image))
        {
            unlink($result->image);
        }
        $result->delete();

        return redirect()->back()->with('success', 'Xóa thành công');
    }

    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Intro::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();

            return response($result);
        }
    }
}
