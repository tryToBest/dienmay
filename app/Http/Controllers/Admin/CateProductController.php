<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cate_product;
use App\Models\Filter;
use App\Models\Faq;
use App\Http\Requests\Cate_productRequest;
use App\Http\Requests\UpdateCate_productRequest;
use Image;
use Illuminate\Support\Facades\Auth;
class CateProductController extends Controller
{
    public function index()
    {
        $cate_product = Cate_product::all();
        return view('admin/cate_product/index', compact('cate_product'));
    }
    public function create(){
       $quyen=explode('|',Auth::user()->quyen);
        $sub=explode('-',$quyen[0]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
        if ($sub[3]==1) {
            $faqs = Faq::where('parent_id',0)->get();
            $data = Cate_product::select('id','name_vi','parent_id')->get()->toArray();
            return view('admin/cate_product/create', compact('data','faqs'));
        }else{
           echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm danh mục sản phẩm!') . "');</script>";
           echo "<script>history.back();</script>"; die;
        }
    }
    public function postCreate(Cate_productRequest $req)
    {
        $quyen=explode('|',Auth::user()->quyen);
        $sub=explode('-',$quyen[0]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
        if ($sub[3]==1) {
            if($req->parent_id != 0){
                $boloc_id = Cate_product::where('id',$req->parent_id)->first()->id_loc;
                $id_loc = $boloc_id;
            }else{
                $id_loc = $req->faq;
            }
            $cate_product                 = new Cate_product;
            $cate_product->name_vi        = $req['name_vi'];
            $cate_product->parent_id      = $req->parent_id;
            $cate_product->id_loc         = $id_loc;
            $cate_product->description_vi = $req['description_vi'];
            $cate_product->position       = $req['position'];
            $cate_product->status         = (is_null($req['status']) ? '0' : '1');
            if($req->hasFile('image')){
                $image    = $req->file('image');
                $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
                Image::make($image)->save(public_path('upload/images/cateproduct/'.$filename));
                $cate_product->image = ('upload/images/cateproduct/'.$filename);
            }
            if($req->hasFile('banner')){
                $banner    = $req->file('banner');
                $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
                Image::make($banner)->save(public_path('upload/images/cateproduct/'.$filename));
                $cate_product->banner = ('upload/images/cateproduct/'.$filename);
            }
            $cate_product->save();
            $slug = str_slug($req->name_vi);
            $check = Cate_product::where('slug_vi', $slug)->first();
            if ($check) {
                $link = $slug.'-'.$cate_product->id;
            } else {
                $link = $slug;
            }
            $cate_product->slug_vi        = $link;
            $cate_product->save();
            return redirect()->route('admin.cate_product.home')->with('success','Thêm thành công');
        }else{
            echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm danh mục sản phẩm!') . "');</script>";
            echo "<script>history.back();</script>"; die;
    }
    }
    public function update($id)
    {
        $quyen=explode('|',Auth::user()->quyen);
        $sub=explode('-',$quyen[0]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
        if ($sub[2]==1) {
            $data         = Cate_product::select('id','name_vi','parent_id')->get()->toArray();
            $cate_product = Cate_product::findOrFail($id);
            $getboloc = array_values(array_filter(explode('|',$cate_product->id_loc)));
            $boloc = $getboloc ? Faq::find($getboloc[0]) : [];
            $idBoloc = $boloc ? $boloc->parent_id : null; 
            $faqs = Faq::where('parent_id',0)->get();
            return view('admin.cate_product.edit', compact('cate_product', 'data','faqs','idBoloc'));
        }else{
            echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa danh mục sản phẩm!') . "');</script>";
            echo "<script>history.back();</script>"; die;
        }
    }

    public function postUpdate($id, Request $req)
    {

             $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[0]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $cate_product                 = Cate_product::findOrFail($id);
        $cate_product->name_vi        = $req['name_vi'];
        $cate_product->name_en        = $req['name_en'];
        $cate_product->name_tq        = $req['name_tq'];
        $cate_product->parent_id      = $req->parent_id;
      
        $cate_product->description_vi = $req['description_vi'];
        $cate_product->description_tq = $req['description_tq'];
        $cate_product->description_en = $req['description_en'];
        $cate_product->position       = $req['position'];
        $cate_product->status         = (is_null($req['status']) ? '0' : '1');


        
        $id_loc = $req->id_loc ? '|'.implode('|', Faq::where('parent_id', $req->id_loc)->pluck('id')->toArray()).'|' : '';

            
        
        $cate_product->id_loc  = $id_loc;
        

        if($req->hasFile('image'))
        {
            if(file_exists($cate_product->image))
            {
                unlink($cate_product->image);
            }
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/cateproduct/'.$filename));
            $cate_product->image = ('upload/images/cateproduct/'.$filename);
        }
        if($req->hasFile('banner'))
        {
            if(file_exists($cate_product->banner))
            {
                unlink($cate_product->banner);
            }
            $banner    = $req->file('banner');
            $filename = date('Y_d_m_H_i_s').'-'. $banner->getClientOriginalName();
            Image::make($banner)->save(public_path('upload/images/cateproduct/'.$filename));
            $cate_product->banner = ('upload/images/cateproduct/'.$filename);
        }
        $cate_product->save();
         $slug = str_slug($req->name_vi);
        $check = Cate_product::where('slug_vi', $slug)->first();
        if ($check) {
            $link = $slug;
        } else {
            $link = $slug;
        }
        $cate_product->slug_vi        = $link;
        $cate_product->save();
        return redirect()->route('admin.cate_product.home')->with('success','Sửa thành công');
                }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa danh mục sản phẩm!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function destroy($id)
    {
           $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[0]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {
        $result  = Cate_product::findOrFail($id);
        $result2 = Cate_product::where('parent_id', $id)->first();
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        if(isset($result2))
        {
            if(file_exists($result2->image))
            {
                unlink($result2->image);
            }
            $result2->delete();
        }
        $result->delete();
        return redirect()->back()->with('success', 'Xóa thành công');

                }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền Xóa danh mục sản phẩm!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_product::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();
            return response($result);
        }
    }
     public function is_home(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_product::find($req->id);
            if ($result->is_home == 0)
            {
                $result->is_home = 1;
            } else {
                $result->is_home = 0;
            }
            $result->save();
            return response($result);
        }
    }
}
