<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post3;
use App\Models\Cate_product;
use App\Http\Requests\Post3Request;
use App\Models\Cate_post3;
// use App\Models\Image_duan;
use Image,File;

use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Imports\Post3sImport;
use Maatwebsite\Excel\Facades\Excel;

class Post3Controller extends Controller
{
    public function index()
    {
        $post3 = Post3::orderBy('position', 'DESC')->get();
        $data = Cate_post3::select('id','name_vi','parent_id')->get()->toArray();

        return view('admin.post3.index', compact('post3', 'data'));
    }

    public function create()
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post3s,$quyen[4]=post3s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {

        $data = Cate_post3::select('id','name_vi','parent_id')->get()->toArray();
        if ($data != null) {

            return view('admin.post3.create', compact('data'));
        } else {

            return redirect()->route('admin.cate_post3.home')->with('error', 'Chưa có danh mục');
        }
 }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}

    }

    public function post3Create(Request $req)
    {
                  $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post3s,$quyen[4]=post3s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $post3                 = new Post3;
        $post3->name_vi        = $req['name_vi'];
        $post3->name_en        = $req['name_en'];
                $post3->name_tq        = $req['name_tq'];
        $post3->slug_vi        = str_slug($req['name_vi']);
        $post3->slug_en        = str_slug($req['name_en']);
        $post3->cate_post3_id   = $req['cate_post3_id'];
        $post3->position       = $req['position'];
        $post3->status         = (is_null($req['status']) ? '0' : '1');
        $post3->is_home        = (is_null($req['is_home']) ? '0' : '1');
        $post3->title_vi       = $req['title_vi'];
        $post3->title_en       = $req['title_en'];
  
                $post3->title_tq       = $req['title_tq'];
        $post3->description_vi = $req['description_vi'];
        $post3->description_en = $req['description_en'];
        $post3->title_seo_vi   = $req['title_seo_vi'];
        $post3->title_seo_en   = $req['title_seo_en'];
        $post3->meta_key_vi    = $req['meta_key_vi'];
        $post3->meta_key_en    = $req['meta_key_en'];
        $post3->meta_des_vi    = $req['meta_des_vi'];
        $post3->meta_des_en    = $req['meta_des_en'];
        if ($req->hasFile('image')) {
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/duan/'.$filename));
            $post3->image = ('upload/images/duan/'.$filename);
        }

        //            if($req->hasFile('pdf_tailieu')){
         
        //     $pdf_tailieu = $req->file('pdf_tailieu');
        //     $filename = $pdf_tailieu->getClientOriginalName();
        //     $path = public_path().'/images/upload/';
        //     $pdf_tailieu->move($path,$filename);
        //     $post3->pdf_tailieu = ('/images/upload/'.$filename);
        // }

        $post3->save();

        return redirect()->route('admin.post3.index');
         }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }

    public function update($id)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post3s,$quyen[4]=post3s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $data = Cate_post3::select('id','name_vi','parent_id')->get()->toArray();
        $post3 = Post3::findOrFail($id);


 $cate_product_cha = Cate_product::select('id','name_vi','parent_id')->where('status',1)->where('parent_id',23)->get()->toArray();
            $list_filter_video =array();
            $dem = 0;
            foreach($cate_product_cha as $k=>$v){
               $list_filter_video[$dem]['title']=$v;
               $list_filter_video[$dem]['id']=$k;
               $list_filter_video[$dem++]['sub']= Cate_product::select('id','name_vi','parent_id')->where('status',1)->where('parent_id',$v)->get()->toArray();
            }


 // $img_detail = Image_duan::where('post3_id', $post3->id)->get();

        return view('admin.post3.edit', compact('data', 'post3','list_filter_video'));
          }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }

    public function post3Update($id, Request $req)
    {
                   $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post3s,$quyen[4]=post3s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $post3               = Post3::findOrFail($id);
        $post3->name_vi         = $req['name_vi'];
        $post3->name_en         = $req['name_en'];
          $post3->name_tq         = $req['name_tq'];
        $post3->slug_vi         = str_slug($req['name_vi']);
        $post3->slug_en         = str_slug($req['name_en']);
        $post3->cate_post3_id = $req['cate_post3_id'];
        $post3->position     = $req['position'];
        $post3->status       = (is_null($req['status']) ? '0' : '1');
        $post3->is_home      = (is_null($req['is_home']) ? '0' : '1');
        $post3->title_vi        = $req['title_vi'];
        $post3->title_en        = $req['title_en'];
                $post3->title_tq        = $req['title_tq'];
                  $post3->link_yt  = $req['link_yt'];
   


        $post3->description_vi  = $req['description_vi'];
        $post3->description_en  = $req['description_en'];
                $post3->description_tq  = $req['description_tq'];
        $post3->title_seo_vi    = $req['title_seo_vi'];
        $post3->title_seo_en    = $req['title_seo_en'];
        $post3->meta_key_vi     = $req['meta_key_vi'];
        $post3->meta_key_en     = $req['meta_key_en'];
        $post3->meta_des_vi     = $req['meta_des_vi'];
        $post3->meta_des_en     = $req['meta_des_en'];
                 if(isset($_POST3['filter_video'])){
                $filter_video='|';
                foreach($_POST3['filter_video'] as $k=>$v) $filter_video.=$v.'|';
                $post3->video_id_product  = $filter_video;
            }


        if ($req->hasFile('image')){
            if (file_exists($post3->image)) {
                unlink($post3->image);
            }
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/duan/'.$filename));
            $post3->image = ('upload/images/duan/'.$filename);
        }

        //         if($req->hasFile('pdf_tailieu')){
        //     if(file_exists($post3->pdf_tailieu))
        //     {
        //         unlink($post3->pdf_tailieu);
        //     }
        //     $pdf_tailieu = $req->file('pdf_tailieu');
        //     $filename = $pdf_tailieu->getClientOriginalName();
        //     $path = public_path().'/images/upload/';
        //     $pdf_tailieu->move($path,$filename);
        //     $post3->pdf_tailieu = ('/images/upload/'.$filename);
        // }


        $validatedData = $req->validate([
            'name_vi'     => 'required|unique:post3s,name_vi,' .$post3->id,
            // 'name_en'     => 'required|unique:post3s,name_en,' .$post3->id,
            'position' => 'numeric|nullable|min:0|unique:post3s,position,' .$post3->id,
        ]);
        $post3->save();

                if ($req->hasFile('img')) {

            $img = $req->file('img');

            foreach ($img as $key => $i) {

                $filename = date('Y_d_m_H_i_s').'-'. $i->getClientOriginalName();

                Image::make($i)->save(public_path('upload/images/duan/'.$filename));

                $i = new Image_duan;

                $i->post3_id = $post3->id;

                $i->name = ('upload/images/duan/'.$filename);

                $i->save();

            }

        }

        return redirect()->route('admin.post3.index')->with('success', 'Sửa thành công');
                  }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }

    public function destroy($id)
    {
         $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[4]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_post3s,$quyen[4]=post3s,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {
        $result = Post3::findOrFail($id);
        if (file_exists($result->image)) {
            unlink($result->image);
        }
        $result->delete();

        return redirect()->back()->with('success', 'Xóa thành công');
                  }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền Xóa bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }

    public function search(Request $req)
    {
        $id_cate_post3 = $req->cate_post3;
        if ($id_cate_post3 == 0) {

            return redirect()->route('admin.post3.index');
        }
        session()->put('id',$id_cate_post3);
        $data = Cate_post3::select('id','name_vi','parent_id')->get()->toArray();
        $post3 = Post3::orderBy('position','ASC')->where(function($query)
        {
            $pro       = $query;
            $id        = session('id');
            $cate_post3 = Cate_post3::find($id);
            $pro = $pro->orWhere('cate_post3_id',$cate_post3->id); // bài viết có id của danh muc cha cấp 1.
            $com = Cate_post3::where('parent_id',$cate_post3->id)->get();//danh mục cha cấp 2.
            foreach ($com as $dt) {
                $pro = $pro->orWhere('cate_post3_id',$dt->id);// bài viết có id của danh muc cha cấp 2.
            }
            session()->forget('id');//xóa session;
        })->get();

        return view('admin.post3.search', compact('post3', 'data', 'id_cate_post3'));
    }

    public function checkbox(Request $req)
    {
        $checkbox = $req->checkbox;
        if (!isset($req->checkbox)) {

            return back()->with('success', 'Chưa chọn bài');
        }
        if ($req->select_action == 1) {
            $checkbox = $req->checkbox;
            foreach ($checkbox as $c) {
                $result = Post3::findOrFail($c);
                if(file_exists($result->image)) {
                    unlink($result->image);
                }
                $result->delete();
            }

            return redirect()->back()->with('success', 'Xóa thành công');
        }
        if ($req->select_action == 2) {
            $checkbox = $req->checkbox;
            foreach ($checkbox as $c) {
                $result = Post3::where('id', $c)->first();
                $result->status = 1;
                $result->save();
            }

            return back()->with('success', 'Thao tác thành công');
        }

        if ($req->select_action == 3) {
            $checkbox = $req->checkbox;
            foreach ($checkbox as $c) {
                $result = Post3::where('id', $c)->first();
                $result->status = 0;
                $result->save();
            }

            return back()->with('success', 'Thao tác thành công');
        }
        if ($req->select_action == 0) {

            return back()->with('success', 'Chưa chọn thao tác');
        }
        if ($checkbox == NULL){

            return back()->with('success', 'Bạn chưa chọn cái nào');
        }
    }

    public function status(Request $req)
    {
        if ($req->ajax()) {
            $result = Post3::find($req->id);
            if ($result->status == 0) {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();

            return response($result);
        }
    }

    public function is_home(Request $req)
    {
        if ($req->ajax()) {
            $result = Post3::find($req->id);
            if ($result->is_home == 0) {
                $result->is_home = 1;
            } else {
                $result->is_home = 0;
            }
            $result->save();

            return response($result);
        }
    }
        public function sinhnhat_home(Request $req)
    {
        if ($req->ajax()) {
            $result = Post3::find($req->id);
            if ($result->sinhnhat_home == 0) {
                $result->sinhnhat_home = 1;
            } else {
                $result->sinhnhat_home = 0;
            }
            $result->save();

            return response($result);
        }
    }
       public function tvnb(Request $req)
    {
        if ($req->ajax()) {
            $result = Post3::find($req->id);
            if ($result->tvnb == 0) {
                $result->tvnb = 1;
            } else {
                $result->tvnb = 0;
            }
            $result->save();

            return response($result);
        }
    }

        public function importthanhvien(Request $req)
    {

       // dd($req->file);
       //  $validatedData = $req->validate([
       //      'file'     => 'required|mimes:xlsx',
       //  ]);
        Excel::import(new Post3sImport, $req->file);
        return back()->with('success', 'All good!');
    }


}
