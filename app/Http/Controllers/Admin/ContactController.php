<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Bill;
use Mail;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
    public function index()
    {
           $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[5]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $contact = Contact::paginate(10);

        return view('admin.contact.index', compact('contact'));
        }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền xem liên hệ!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}

    }

    public function destroy($id)
    {
     $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[5]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {

        Contact::findOrFail($id)->delete();

        return redirect()->back()->with('success', 'Xóa thành công');
}else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền Xóa liên hệ!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}


    }
//      public function postMail($mail,Request $req)
//     {
       

//         return back()->with('success', 'Gửi thành công');
//     }
}
