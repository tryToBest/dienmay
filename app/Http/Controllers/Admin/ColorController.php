<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Color;
use App\Models\Bill;
use Image;
use Session;

class ColorController extends Controller
{
    public function index()
    {
        $color = Color::all();
        $color2 = Color::all('value')->toArray();
        
    	return view('admin/color/index', compact('color','trangthai','color2'));
    }

    public function create()
    {

    	return view('admin/color/create', compact('data'));
    }

    public function postCreate(Request $req)
    {
        $color                 = new Color;
        $color->value        = $req['value'];
        $color->ma        = $req['ma'];
        if($req->hasFile('image')){
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/color'.$filename));
            $color->image = ('upload/color'.$filename);
        }
        
    	$color->save();

    	return redirect()->route('admin.color.index')->with('success','Thêm thành công');
    }

    public function update($id)
    {
        $color = Color::findOrFail($id);

    	return view('admin.color.edit', compact('color', 'data'));
    }

    public function postUpdate($id, Request $req)
    {
        $color                 = Color::findOrFail($id);
        $color->value        = $req['value'];
        $color->ma        = $req['ma'];
        if($req->hasFile('image')){
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/color'.$filename));
            $color->image = ('upload/color'.$filename);
        }
        
    	$color->save();

    	return redirect()->route('admin.color.index')->with('success','Sửa thành công');
    }

    public function destroy($id)
    {
        $result  = Color::findOrFail($id);
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        
        $result->delete();
        if (Session::get('magiamgia') != null) {
            session()->forget('magiamgia');
        }

    	return redirect()->back()->with('success', 'Xóa thành công');
    }
}
