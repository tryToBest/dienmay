<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Http\Requests\BannerRequest;
use Image;
use Illuminate\Support\Facades\Validator;

class BannerController extends Controller
{
    public function index()
    {
        $banner = Banner::orderBy('id', 'DESC')->paginate(20);

        return view('admin.banner.index', compact('banner'));
    }

    public function create()
    {
        return view('admin.banner.create');
    }

    public function postCreate(bannerRequest $req)
    {
        $banner              = new banner;
        $banner->name        = $req['name'];
        $banner->description = $req['description'];
        $banner->link        = $req['link'];
        $banner->position    = $req['position'];
                $banner->ts1    = $req['ts1'];
                        $banner->ts2    = $req['ts2'];
                                $banner->ts3    = $req['ts3'];
                                        $banner->ts4    = $req['ts4'];
        $banner->dislay      = $req['dislay'];
        $banner->status      = (is_null($req['status']) ? '0' : '1');
        if($req->hasFile('image')){
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/banner/'.$filename));
            $banner->image = ('upload/images/banner/'.$filename);
        }
        $banner->save();

        return redirect()->route('admin.banner.index');
    }

    public function update($id)
    {
        $banner = Banner::where('id', $id)->first();

        return view('admin.banner.edit', compact('banner'));
    }

    public function postUpdate($id, Request $req)
    {
        $banner              = Banner::where('id', $id)->first();
        $banner->name        = $req['name'];
        $banner->description = $req['description'];
        $banner->link        = $req['link'];
        $banner->position    = $req['position'];
                     $banner->ts1    = $req['ts1'];
                        $banner->ts2    = $req['ts2'];
                                $banner->ts3    = $req['ts3'];
                                        $banner->ts4    = $req['ts4'];
        $banner->dislay      = $req['dislay'];
        $banner->status      = (is_null($req['status']) ? '0' : '1');
        if($req->hasFile('image'))
        {
            if(file_exists($banner->image))
            {
                unlink($banner->image);
            }
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/banner/'.$filename));
            $banner->image = ('upload/images/banner/'.$filename);
        }
        $banner->save();

        return redirect()->route('admin.banner.index')->with('success', 'Sửa thành công');
    }

    public function destroy($id)
    {
        $result = Banner::findOrFail($id);
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        $result->delete();

        return redirect()->back()->with('success', 'Xóa thành công');
    }

    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Banner::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();

            return response($result);
        }
    }
}

