<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cate_ttbh;
use App\Http\Requests\Cate_ttbhRequest;
use Image;
use File;
use Illuminate\Support\Facades\Auth;
class CatettbhController extends Controller
{
    public function index()
    {
        $cate_ttbh = Cate_ttbh::all();
        return view('admin/cate_ttbh/index', compact('cate_ttbh'));
    }
    public function create()
    {
  $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_ttbhs,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {

        $data=Cate_ttbh::select('id','name_vi','parent_id')->get()->toArray();
        return view('admin/cate_ttbh/create', compact('data'));

                        }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}


    }
    public function postCreate(Cate_ttbhRequest $req)
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_ttbhs,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $cate_ttbh                 = new Cate_ttbh;
        $cate_ttbh->name_vi        = $req['name_vi'];
        $cate_ttbh->name_en        = $req['name_en'];
                $cate_ttbh->name_tq        = $req['name_tq'];
        $cate_ttbh->parent_id      = $req->parent_id;
        $cate_ttbh->slug_vi        = str_slug($cate_ttbh->name_vi);
        // $cate_ttbh->slug_en        = str_slug($cate_ttbh->name_en);
        $cate_ttbh->description_vi = $req['description_vi'];
                $cate_ttbh->diachi_vi = $req['diachi_vi'];
                        $cate_ttbh->hotline = $req['hotline'];
                                $cate_ttbh->email = $req['email'];
        $cate_ttbh->description_en = $req['description_en'];
                $cate_ttbh->description_tq = $req['description_tq'];
        $cate_ttbh->position       = $req['position'];
        $cate_ttbh->status         = (is_null($req['status']) ? '0' : '1');
        $cate_ttbh->menu         = (is_null($req['menu']) ? '0' : '1');
        $cate_ttbh->ft         = (is_null($req['ft']) ? '0' : '1');
        $cate_ttbh->title_seo_vi   = $req['title_seo_vi'];
        $cate_ttbh->title_seo_en   = $req['title_seo_en'];
        $cate_ttbh->meta_key_vi    = $req['meta_key_vi'];
        $cate_ttbh->meta_key_en    = $req['meta_key_en'];
        $cate_ttbh->meta_des_vi    = $req['meta_des_vi'];
        $cate_ttbh->meta_des_en    = $req['meta_des_en'];
        if($req->hasFile('image')){
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/chinhanh/'.$filename));
            $cate_ttbh->image = ('upload/images/chinhanh/'.$filename);
        }else{
            $cate_ttbh->image = ('upload/images/chinhanh/avatar5.png');
        }
         if($req->hasFile('banner')){
            $banner    = $req->file('banner');
            $filename = date('Y_d_m_H_i_s').'-'. $banner->getClientOriginalName();
            Image::make($banner)->save(public_path('upload/images/chinhanh/'.$filename));
            $cate_ttbh->banner = ('upload/images/chinhanh/'.$filename);
        }
        $cate_ttbh->save();
        return redirect()->route('admin.cate_ttbh.home')->with('success','Thêm thành công');
            }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function update($id)
    {
         $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_ttbhs,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $data      = Cate_ttbh::select('id','name_vi','parent_id')->get();
          $get_cha      = Cate_ttbh::select('id','name_vi','parent_id')->where('parent_id',0)->get();
        $cate_ttbh = Cate_ttbh::findOrFail($id);
        return view('admin.cate_ttbh.edit', compact('cate_ttbh', 'data','get_cha'));

            }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}

    }
    public function postUpdate($id, Request $req)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_ttbhs,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $cate_ttbh                 = Cate_ttbh::findOrFail($id);
        $cate_ttbh->name_vi        = $req['name_vi'];
        $cate_ttbh->name_en        = $req['name_en'];
                $cate_ttbh->name_tq        = $req['name_tq'];
        $cate_ttbh->parent_id      = $req->parent_id;
        $cate_ttbh->slug_vi        = str_slug($cate_ttbh->name_vi);
        // $cate_ttbh->slug_en        = str_slug($cate_ttbh->name_en);
        $cate_ttbh->position       = $req['position'];
        $cate_ttbh->description_vi = $req['description_vi'];
        $cate_ttbh->description_en = $req['description_en'];
              $cate_ttbh->diachi_vi = $req['diachi_vi'];
                        $cate_ttbh->hotline = $req['hotline'];
                                $cate_ttbh->email = $req['email'];
                $cate_ttbh->description_tq = $req['description_tq'];
        $cate_ttbh->status         = (is_null($req['status']) ? '0' : '1');
        $cate_ttbh->menu         = (is_null($req['menu']) ? '0' : '1');
        $cate_ttbh->ft         = (is_null($req['ft']) ? '0' : '1');
        $cate_ttbh->title_seo_vi   = $req['title_seo_vi'];
        $cate_ttbh->title_seo_en   = $req['title_seo_en'];
        $cate_ttbh->meta_key_vi    = $req['meta_key_vi'];
        $cate_ttbh->meta_key_en    = $req['meta_key_en'];
        $cate_ttbh->meta_des_vi    = $req['meta_des_vi'];
        $cate_ttbh->meta_des_en    = $req['meta_des_en'];
        if($req->hasFile('image')){
            if(file_exists($cate_ttbh->image))
            {
                unlink($cate_ttbh->image);
            }
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/chinhanh/'.$filename));
            $cate_ttbh->image = ('upload/images/chinhanh/'.$filename);
        }
        if($req->hasFile('banner')){
            if(file_exists($cate_ttbh->banner))
            {
                unlink($cate_ttbh->banner);
            }
            $banner    = $req->file('banner');
            $filename = date('Y_d_m_H_i_s').'-'. $banner->getClientOriginalName();
            image::make($banner)->save(public_path('upload/images/chinhanh/'.$filename));
            $cate_ttbh->banner = ('upload/images/chinhanh/'.$filename);
        }
        $validatedData = $req->validate([
            'name_vi'     => 'required|unique:cate_ttbhs,name_vi,' .$cate_ttbh->id,
            // 'name_en'     => 'required|unique:cate_ttbhs,name_en,' .$cate_ttbh->id,
            'position' => 'numeric|nullable|min:0|unique:cate_ttbhs,position,' .$cate_ttbh->id,
        ]);
        $cate_ttbh->save();
        return redirect()->route('admin.cate_ttbh.home')->with('success','Sửa thành công');
                    }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function destroy($id)
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[3]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_ttbhs,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {
        $result = Cate_ttbh::findOrFail($id);
        $result2 = Cate_ttbh::where('parent_id', $id)->first();
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        if(isset($result2))
        {
            if(file_exists($result2->image))
            {
                unlink($result2->image);
            }
            $result2->delete();
        }
        $result->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
                    }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền xóa danh mục bài viết!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_ttbh::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();
            return response($result);
        }
    }
    public function menu(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_ttbh::find($req->id);
            if ($result->menu == 0)
            {
                $result->menu = 1;
            } else {
                $result->menu = 0;
            }
            $result->save();
            return response($result);
        }
    }
    public function ft(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_ttbh::find($req->id);
            if ($result->ft == 0)
            {
                $result->ft = 1;
            } else {
                $result->ft = 0;
            }
            $result->save();
            return response($result);
        }
    }
        public function is_home(Request $req)
    {
        if ($req->ajax())
        {
            $result = Cate_ttbh::find($req->id);
            if ($result->is_home == 0)
            {
                $result->is_home = 1;
            } else {
                $result->is_home = 0;
            }
            $result->save();
            return response($result);
        }
    }
}
