<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slide;
use App\Http\Requests\SlideRequest;
use Image, File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
class SlideController extends Controller
{
    public function index()
    {
        $slide = Slide::where('dislay', 1)->orWhere('dislay', 2)->orWhere('dislay', 3)->orWhere('dislay', 8)->orWhere('dislay', 9)->orWhere('dislay', 10)->orderBy('id', 'DESC')->paginate(20);
        return view('admin.slide.index', compact('slide'));
    }
    public function create()
    {

              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[6]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {

        return view('admin.slide.create');
    }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm slide!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }

    }
    public function postCreate(Request $req)
    {
                      $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[6]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $slide              = new Slide;
        $slide->name_vi        = $req['name_vi'];
        $slide->name_en        = $req['name_en'];
        $slide->description_vi = $req['description_vi'];
        $slide->description_en = $req['description_en'];
        $slide->link        = $req['link'];
        $slide->position    = $req['position'];
        $slide->dislay      = $req['dislay'];
        $slide->status      = (is_null($req['status']) ? '0' : '1');
     

          if($req->hasFile('image')){
          
            $image = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
               $path = public_path().'/upload/images/slide/';
          
                 $image->move($path,$filename);
            $slide->image = ('/upload/images/slide/'.$filename);
        }


        $slide->save();
        return redirect()->route('admin.slide.index');
            }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm slide!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }
    public function update($id)
    {
                      $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[6]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $slide = Slide::where('id', $id)->first();
        return view('admin.slide.edit', compact('slide'));
          }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa slide!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }
    public function postUpdate($id, Request $req)
    {
                       $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[6]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $slide              = Slide::where('id', $id)->first();
        $slide->name_vi        = $req['name_vi'];
        $slide->name_en        = $req['name_en'];
        $slide->description_vi = $req['description_vi'];
        $slide->description_en = $req['description_en'];
        $slide->link        = $req['link'];
        $slide->position    = $req['position'];
        $slide->dislay      = $req['dislay'];
        $slide->status      = (is_null($req['status']) ? '0' : '1');
   

        if($req->hasFile('image')){
            // if(file_exists($slide->image))
            // {
            //     unlink($slide->image);
            // }
            $image = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
               $path = public_path().'/upload/images/slide/';
            // Image::make($logo)->save(public_path('images/upload/'.$filename));
                 $image->move($path,$filename);
            $slide->image = ('/upload/images/slide/'.$filename);
        }
        $slide->save();
        return redirect()->route('admin.slide.index')->with('success', 'Sửa thành công');
          }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa slide!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }
    public function destroy($id)
    {
        $quyen=explode('|',Auth::user()->quyen);
        $sub=explode('-',$quyen[6]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
        if ($sub[1]==1) {
            $result = Slide::findOrFail($id);

            
            // unlink($result->image);
            
            $result->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
                  }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền xóa slide!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }
    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Slide::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();
            return response($result);
        }
    }
}
