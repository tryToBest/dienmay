<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Http\Requests\FaqRequest;
use Image;
use File;
use Illuminate\Support\Facades\Auth;
class FaqController extends Controller
{
    public function index()
    {
        $faq = Faq::all();
        return view('admin/faq/index', compact('faq'));
    }
    public function create()
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[8]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[8]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=faqs  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $data=Faq::select('id','name_vi','parent_id')->get()->toArray();
        return view('admin/faq/create', compact('data'));
         }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm FAQ!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function postCreate(FaqRequest $req)
    {
             $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[8]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[8]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=faqs  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $faq                 = new Faq;
        $faq->name_vi        = $req['name_vi'];
        $faq->parent_id      = $req->parent_id;
        // $faq->slug_vi        = str_slug($faq->name_vi);
        $faq->description_vi = $req['description_vi'];
        $faq->position       = $req['position'];
        $faq->status         = (is_null($req['status']) ? '0' : '1');
        $faq->title_seo_vi   = $req['title_seo_vi'];
        $faq->meta_key_vi    = $req['meta_key_vi'];
        $faq->meta_des_vi    = $req['meta_des_vi'];
          if ($req->hasFile('image')) {
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/thuonghieu/'.$filename));
            $faq->image = ('upload/images/thuonghieu/'.$filename);
        }
        $faq->save();
        return redirect()->route('admin.faq.home')->with('success','Thêm thành công');
                 }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm FAQ!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function update($id)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[8]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[8]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=faqs  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $data      = Faq::select('id','name_vi','parent_id')->get()->toArray();
          $get_cha      = Faq::select('id','name_vi','parent_id')->where('parent_id',0)->get();
        $faq = Faq::findOrFail($id);
        return view('admin.faq.edit', compact('faq', 'data','get_cha'));
                 }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa FAQ!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function postUpdate($id, Request $req)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[8]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[8]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=faqs  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $faq                 = Faq::findOrFail($id);
        $faq->name_vi        = $req['name_vi'];
        $faq->parent_id      = $req->parent_id;
        // $faq->slug_vi        = str_slug($faq->name_vi);
        // $faq->slug_en        = str_slug($faq->name_en);
        $faq->position       = $req['position'];
        $faq->description_vi = $req['description_vi'];
        $faq->status         = (is_null($req['status']) ? '0' : '1');
        $faq->title_seo_vi   = $req['title_seo_vi'];
        $faq->meta_key_vi    = $req['meta_key_vi'];
        $faq->meta_des_vi    = $req['meta_des_vi'];
        $validatedData = $req->validate([
            'name_vi'     => 'required:faqs,name_vi,' .$faq->id,
            'position' => 'numeric|nullable|min:0|unique:faqs,position,' .$faq->id,
        ]);
            if ($req->hasFile('image')){
            if (file_exists($faq->image)) {
                unlink($faq->image);
            }
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/thuonghieu/'.$filename));
            $faq->image = ('upload/images/thuonghieu/'.$filename);
        }
        $faq->save();
        return redirect()->route('admin.faq.home')->with('success','Sửa thành công');
        }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa FAQ!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function destroy($id)
    {
               $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[8]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[8]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=faqs  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {
        $result = Faq::findOrFail($id);
        $result2 = Faq::where('parent_id', $id)->first();
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        if(isset($result2))
        {
            if(file_exists($result2->image))
            {
                unlink($result2->image);
            }
            $result2->delete();
        }
        $result->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
        }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền xóa FAQ!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Faq::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();
            return response($result);
        }
    }
}
