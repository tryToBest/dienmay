<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slide;
use App\Http\Requests\SlideRequest;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
class GioithieuController extends Controller
{
    public function index()
    {
        $gioithieu = Slide::where('dislay', 4)->orWhere('dislay', 5)->orWhere('dislay', 6)->orWhere('dislay', 7)->orderBy('id', 'DESC')->paginate(20);

        return view('admin.gioithieu.index', compact('gioithieu'));
    }

    public function create()
    {
                $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[9]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=uudais,$quyen[9]=gioithieus  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        return view('admin.gioithieu.create');
         }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm giới thiệu!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }

    public function postCreate(SlideRequest $req)
    {
                  $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[9]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=uudais,$quyen[9]=gioithieus  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $gioithieu              = new Slide;
        $gioithieu->name_vi        = $req['name_vi'];
        $gioithieu->name_en        = $req['name_en'];
        $gioithieu->description_vi = $req['description_vi'];
        $gioithieu->description_en = $req['description_en'];
        $gioithieu->link        = $req['link'];
        $gioithieu->position    = $req['position'];
        $gioithieu->dislay      = $req['dislay'];
        $gioithieu->status      = (is_null($req['status']) ? '0' : '1');
        if($req->hasFile('image')){
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/slide/'.$filename));
            $gioithieu->image = ('upload/images/slide/'.$filename);
        }
        $gioithieu->save();

        return redirect()->route('admin.gioithieu.index');
         }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm giới thiệu!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }

    public function update($id)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[9]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=uudais,$quyen[9]=gioithieus  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $gioithieu = Slide::where('id', $id)->first();

        return view('admin.gioithieu.edit', compact('gioithieu'));
         }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa giới thiệu!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }

    public function postUpdate($id, Request $req)
    {
            $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[9]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=uudais,$quyen[9]=gioithieus  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $gioithieu              = Slide::where('id', $id)->first();
        $gioithieu->name_vi        = $req['name_vi'];
        $gioithieu->name_en        = $req['name_en'];
        $gioithieu->description_vi = $req['description_vi'];
        $gioithieu->description_en = $req['description_en'];
        $gioithieu->link        = $req['link'];
        $gioithieu->position    = $req['position'];
        $gioithieu->dislay      = $req['dislay'];
        $gioithieu->status      = (is_null($req['status']) ? '0' : '1');
        if($req->hasFile('image'))
        {
            if(file_exists($gioithieu->image))
            {
                unlink($gioithieu->image);
            }
            $image    = $req->file('image');
            $filename = date('Y_d_m_H_i_s').'-'. $image->getClientOriginalName();
            Image::make($image)->save(public_path('upload/images/slide/'.$filename));
            $gioithieu->image = ('upload/images/slide/'.$filename);
        }
        $gioithieu->save();

        return redirect()->route('admin.gioithieu.index')->with('success', 'Sửa thành công');
                }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa giới thiệu!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }

    public function destroy($id)
    {
           $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[9]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings,$quyen[8]=uudais,$quyen[9]=gioithieus  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {
        $result = Slide::findOrFail($id);
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        $result->delete();

        return redirect()->back()->with('success', 'Xóa thành công');
                }else{
 echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền Xóa giới thiệu!') . "');</script>";
                echo "<script>history.back();</script>"; die;
    }
    }

    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Slide::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();

            return response($result);
        }
    }
}

