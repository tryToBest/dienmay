<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Filter;
use App\Http\Requests\FilterRequest;
use Image;
use File;
use Illuminate\Support\Facades\Auth;
class FilterController extends Controller
{
    public function index()
    {
        $filter = Filter::all();
        return view('admin/filter/index', compact('filter'));
    }
    public function create()
    {
          $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[2]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $data=Filter::select('id','name_vi','parent_id')->get()->toArray();
        return view('admin/filter/create', compact('data'));
         }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm bộ đặc điểm!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function postCreate(FilterRequest $req)
    {
             $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[2]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[3]==1) {
        $filter                 = new Filter;
        $filter->name_vi        = $req['name_vi'];
        $filter->parent_id      = $req->parent_id;
        $filter->slug_vi        = str_slug($filter->name_vi);
        $filter->description_vi = $req['description_vi'];
        $filter->position       = $req['position'];
        $filter->status         = (is_null($req['status']) ? '0' : '1');
        $filter->title_seo_vi   = $req['title_seo_vi'];
        $filter->meta_key_vi    = $req['meta_key_vi'];
        $filter->meta_des_vi    = $req['meta_des_vi'];
        $filter->save();
        return redirect()->route('admin.filter.home')->with('success','Thêm thành công');
                 }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền thêm bộ đặc điểm!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function update($id)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[2]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $data      = Filter::select('id','name_vi','parent_id')->get()->toArray();
        $filter = Filter::findOrFail($id);
        return view('admin.filter.edit', compact('filter', 'data'));
                 }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa bộ đặc điểm!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function postUpdate($id, Request $req)
    {
              $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[2]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[2]==1) {
        $filter                 = Filter::findOrFail($id);
        $filter->name_vi        = $req['name_vi'];
        $filter->parent_id      = $req->parent_id;
        $filter->slug_vi        = str_slug($filter->name_vi);
        // $filter->slug_en        = str_slug($filter->name_en);
        $filter->position       = $req['position'];
        $filter->description_vi = $req['description_vi'];
        $filter->status         = (is_null($req['status']) ? '0' : '1');
        $filter->title_seo_vi   = $req['title_seo_vi'];
        $filter->meta_key_vi    = $req['meta_key_vi'];
        $filter->meta_des_vi    = $req['meta_des_vi'];
        $validatedData = $req->validate([
            'name_vi'     => 'required|unique:filters,name_vi,' .$filter->id,
            'position' => 'numeric|nullable|min:0|unique:filters,position,' .$filter->id,
        ]);
        $filter->save();
        return redirect()->route('admin.filter.home')->with('success','Sửa thành công');
        }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền sửa bộ đặc điểm!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function destroy($id)
    {
               $quyen=explode('|',Auth::user()->quyen);
 $sub=explode('-',$quyen[2]); //$quyen[0]=cateproducts,$quyen[1]=products,$quyen[2]=filters,$quyen[3]=cate_posts,$quyen[4]=posts,$quyen[5]=contacts,$quyen[6]=slides,$quyen[7]=settings  // sub[1] là xóa, sub[2] là sửa, sub[3] là thêm
if ($sub[1]==1) {
        $result = Filter::findOrFail($id);
        $result2 = Filter::where('parent_id', $id)->first();
        if(file_exists($result->image))
        {
            unlink($result->image);
        }
        if(isset($result2))
        {
            if(file_exists($result2->image))
            {
                unlink($result2->image);
            }
            $result2->delete();
        }
        $result->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
        }else{
     echo "<script>alert('" . json_encode('Tài khoản của bạn không có quyền xóa bộ đặc điểm!') . "');</script>";
                echo "<script>history.back();</script>"; die;
}
    }
    public function status(Request $req)
    {
        if ($req->ajax())
        {
            $result = Filter::find($req->id);
            if ($result->status == 0)
            {
                $result->status = 1;
            } else {
                $result->status = 0;
            }
            $result->save();
            return response($result);
        }
    }
}
