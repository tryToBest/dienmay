<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Cate_post;
use Illuminate\Support\Collection;

class PostController extends Controller
{
    public function listPost($slug)
    {
        $cate_post = Cate_post::where('slug_vi', $slug)->first();
        $list_cate = Cate_post::where('parent_id',0)->where('ft',1)->get();
        $get_cha = Cate_post::where('id', $cate_post->parent_id)->first();
        $id        = $cate_post->id;
        session()->put('id',$id);
        // dd($id);
        $posts = Post::orderBy('created_at','DESC')->where('status', 1)->where(function($query)
        {
            $pro       = $query;
            $id        = session('id');
            $cate_post = Cate_post::find($id);
            $pro       = $pro->orWhere('cate_post_id',$cate_post->id); // bài viết có id của danh muc cha cấp 1.
            $com       = Cate_post::where('parent_id',$cate_post->id)->get();//danh mục cha cấp 2.
            foreach ($com as $dt) {
                $pro = $pro->orWhere('cate_post_id',$dt->id);// bài viết có id của danh muc cha cấp 2.
            }
            session()->forget('id');//xóa session;
        })->paginate(10);
        $mostViewedPosts = Post::where('status',1)->orderBy('viewed','DESC')->limit(9)->get();
        return view('frontend.posts.list', compact('posts', 'cate_post','get_cha','list_cate','mostViewedPosts'));

      
    }

    public function detail($slug)
    {
        $detail = Post::where('status', 1)->where('slug_vi', $slug)->first();
        $cate_post = Cate_post::where('id', $detail->cate_post_id)->first();
        $bvlq = Post::where('cate_post_id', $detail->cate_post_id)->where('status', 1)
            ->where('id', '<>', $detail->id)->orderBy('id', 'DESC')->paginate(8);
        $bvnb = Post::where('status', 1)
            ->where('is_home', 1)->orderBy('id', 'DESC')->take(8)->get();
        $mostViewedPosts = Post::where('status',1)->orderBy('viewed','DESC')->limit(9)->get();
        $list_cate = Cate_post::where('parent_id',0)->where('ft',1)->get();
        return view('frontend.posts.detail', compact('detail','cate_post','bvlq','bvnb','mostViewedPosts','list_cate'));
    }
     public function ajax_detail_tintuc(Request $req)
    {


         if($req->get('id'))
        {
            $id = $req->get('id');
            $data = Post::where('id',$id)->first();
           $output = '';
          
 

                      $output .= '<div class="container">';
                           $output .= '<div class="title_tintuc_detail text-center">';
                                        $output .= '<span>';
                                         $output .= $data->name_vi;
                                         $output .= '</span>';
                            $output .= '</div>';


                 $output .= '<div class="baongoai_detail_tintuc">';

                     $output .= $data->description_vi;
               
         $output .= '</div>';
             $output .= '</div>';



       
     
           echo $output;
       }

    }
    public function postSearch(Request $req)
    {
        $input  = $req->search;
        $post = Post::where('name_vi', 'LIKE',"%$input%")->orWhere('name_en', 'LIKE',"%$input%")->where('status', 1)->paginate(8);

        return view('frontend.search', compact('post', 'input'));
    }
}
