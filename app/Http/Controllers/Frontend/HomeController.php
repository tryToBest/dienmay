<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cate_Slide;
use App\Models\Slide;
use App\Models\Cate_post;
use App\Models\Cate_post2;
use App\Models\Cate_product;
use App\Models\Product;
use App\Models\Bill;
use App\Models\Banner;
use App\Models\Post;
use App\Models\Uudai;
use App\Models\Images;
use App\Models\Yeuthich;
use App\Models\BillDetail;
use App\User;
use App\Models\Comment;
use Session;
use Auth;
use Image, File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;



class HomeController extends Controller
{

  protected function validator(array $data)
    {
        $customMessages = [
            'regex' => 'Mật khẩu phải 6 kí tự trở lên',
        ];
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'name_truycap' => 'required|string|max:255|unique:users',
            'phone' => 'required',
                      'email' => 'required',
            'gender' => 'required',
            // 'address' => 'required',

            'password' => 'required|string|min:6|confirmed',
        ],$customMessages);
    }


    public function traloicomment(Request $req){
        echo 'haha';
    }
    public function commentsp($product_id=null,Request $req){
        if($_POST['name']!='' && $_POST['content'] !='' && $_POST['phone'] !='') {
            $comment          = new Comment;
            $comment->name = $req['name'];
         // dd($req['quick_tag']);
            $comment->parent_id = $req['parent_id'];
            $quick_tag ='|';
            if($req['quick_tag'] != null){
                foreach($req['quick_tag'] as $k=>$v) {
                    $quick_tag.=$v.'|';
                    $comment->quick_tag  = $quick_tag;
                }
            }
          // $comment->quick_tag = $req['quick_tag'];
            $comment->email = $req['email'];
            $comment->phone = $req['phone'];
            $comment->content = $req['content'];
            $comment->rating = $req['rating'];
            $comment->product_id = $req['product_id'];
            $comment->slug_id = $req['slug_id'];
            $comment->title = $req['title'];
            $comment->set = $req['set'];
            $img = null;


        if($req->hasFile('userfile')){
           $userfile = $req->file('userfile');
           $filename = date('Y_d_m_H_i_s').'-'. $userfile->getClientOriginalName();
           Image::make($userfile)->save(public_path('upload/images/comment/'.$filename));
           $img = $filename;

        }

        // dd($img);
        if ($img != '') {
            $comment->images = ('upload/images/comment/'.$img);
        }else{
            $comment->images = '';
     }
        $comment->status = 0;
        $comment->save(); 
        return back()->with('binhluan', 'Bình luận thành công, quản trị viên sẽ kiểm duyệt bình luận của bạn!');
    }
}




public function index()
{
    $slides= Slide::where('status', 1)->where('dislay', 1)->get();
    $cate_home = Cate_product::where('status', 1)->where('is_hot',1)->where('is_home', 1)->where('parent_id',0)->orderBy('position', 'ASC')->get();
    $sanphamsale = Product::where('status',1)->where('is_sale',1)->get();
    $posts = Post::where('status', 1)->where('is_home', 1)->orderBy('id', 'DESC')->limit(6)->get();
    $sphots = Product::where('status', 1)->where('is_hot', 1)->limit(60)->orderBy('position_hot','ASC')->get();
    $banners = Slide::where('status', 1)->where('dislay', 3)->orderBy('created_at','DESC')->get();
    $catePostHome = Cate_post::where('status', 1)->where('is_home', 1)->get();
    $tinmoinhat = Post::where('status', 1)->where('is_home', 1)->orderBy('position', 'ASC')->take(4)->get();
    return view('frontend.index', compact('cate_home','tinmoinhat','slides','posts','sphots','banners','catePostHome','sanphamsale'));
}

    // public function trangchu()
    // {
    //     $cate_home = Cate_product::where('status', 1)->where('parent_id', 0)->orderBy('position', 'ASC')->get();
    //     // $spnb = Product::where('status', 1)->where('is_home', 1)->orderBy('id', 'DESC')->get();
    //     // $sphot = Product::where('status', 1)->where('is_hot', 1)->first();
    //     // $danhgia = Slide::where('status', 1)->where('dislay', 2)->get();
    //     $doitac = Uudai::where('status', 1)->get();
    //     // $chinhsach = Slide::where('status', 1)->where('dislay', 3)->get();
    //     // $popup = Slide::where('status', 1)->where('dislay', 8)->first();
    //       $cate_tintuc = Cate_post::where('status', 1)->where('is_home', 1)->orderBy('position', 'ASC')->take(2)->get();
    //       $cate_dmhome = Cate_product::where('status', 1)->where('is_home', 1)->orderBy('position', 'ASC')->take(4)->get();
    //     $tintuc = Post::where('status', 1)->where('is_home', 1)->orderBy('id', 'DESC')->take(3)->get();
    //     return view('frontend.trangchu', compact('cate_home', 'tintuc','doitac','cate_tintuc','cate_dmhome'));
    // }
    public function changeLanguage($locale)
    {
        Session::put('locale',$locale);
        return redirect()->back();
    }
    public function taikhoan()
    {
        $bill = Bill::where('user_id', Auth::id())->orderBy('id', 'DESC')->get();
        $yeuthich = Yeuthich::where('user_id', Auth::id())->get();
        return view('frontend.taikhoan', compact('bill', 'yeuthich'));
    }
    public function register_dk()
    {
        $bill = Bill::where('user_id', Auth::id())->orderBy('id', 'DESC')->get();
        $yeuthich = Yeuthich::where('user_id', Auth::id())->get();
        return view('frontend.register_dk', compact('bill', 'yeuthich'));
    }

  public function dangky(Request $req)
    {

        $confirmation_code = str_random(30);
          $dangky                  = new User;

        $dangky->name            = $req['name'];
         $dangky->phone            = $req['phone'];
               $dangky->email            = $req['email'];
          $dangky->gender            = $req['gender'];
          $dangky->name_truycap            = $req['name_truycap'];
           $dangky->name_truycap = $confirmation_code;
           $dangky->password            = Hash::make($req['password']);

        $validatedData = $req->validate([

          'name' => 'required|string|max:255',
            'name_truycap' => 'required|string|max:255|unique:users',
            'phone' => 'required',
              'email' => 'required',
             'gender' => 'required',
            // 'address' => 'required',

            'password' => 'required|string|min:6',

        ]);



$dangky->save();

     return redirect()->route('login')->with('dangkytaikhoan','thành công!');
    }
  public function dangnhap(Request $req)
    {
 $login = [
            'name_truycap'     => $req->name_truycap,
            'password' => $req->password,
            'role'     => 1,
        ];
        if (Auth::attempt($login)) {

            return redirect()->route('index');
        }else {

            return redirect()->back()->with('message', 'Tên hoặc mật khẩu không đúng');
        }

}

  public function confirm($confirmation_code)
    {
        if(!isset($confirmation_code))
        {
             session()->flash('message', 'wrong');
        }
        $user = User::whereConfirmationCode($confirmation_code)->first();
        if (isset($user))
        {
            $user->status = 1;
        $user->confirmation_code = null;
        $user->save();
  session()->flash('message', 'You have successfully verified your account.');
}else{
                  session()->flash('message', 'Không tồn tại');
        }

         return back()->with('xacnhanemailthanhcong', 'Thành công!');
  
         // return redirect()->route('index');
    }





    public function donhang_hopdong_hotro()
    {
        $bill = Bill::where('user_id', Auth::id())->orderBy('id', 'DESC')->get();
          $all_hopdong = Contact2::where('user_id', Auth::id())->orderBy('id', 'DESC')->get();
        $yeuthich = Yeuthich::where('user_id', Auth::id())->get();
  $tinh = DB::table("province")->get();
   $product_all = Product::where('status', 1)->get();
        return view('frontend.donhang_hopdong_hotro', compact('bill', 'yeuthich','tinh','product_all','all_hopdong'));
    }
    public function thaydoitaikhoan(Request $data)
    {
        $administrator        = User::findOrFail(Auth::id());
        $administrator->name  = $data['name'];
        $administrator->phone = $data['phone'];
        $administrator->address = $data['address'];
        if($data->password == null){
            $administrator->save();
        }else{
            $administrator->password = Hash::make($data['password']);
            $administrator->save();
        }
        return back()->with('success', 'Thay đổi thành công');
    }
    public function themyeuthich($id)
    {
        $check = Yeuthich::where('user_id', Auth::id())->where('product_id', $id)->get();
        if (count($check) != 0) {
            return back()->with('themyeuthichroi', 'Có rồi');
        } else {
            $yeuthich = new Yeuthich;
            $yeuthich->user_id = Auth::id();
            $yeuthich->product_id = $id;
            $yeuthich->save();
            return back()->with('themyeuthich', 'Thành công');
        }
    }
    public function chitietdon($id)
    {
        $bill  = BillDetail::where('bill_id', $id)->get();
        $order = Bill::where('id', $id)->first();
        return view('frontend.chitietdon', compact('bill', 'order'));
    }
}
