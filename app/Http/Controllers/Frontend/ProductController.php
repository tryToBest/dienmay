<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Goutte\Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\HttpClient\HttpClient;

use App\Models\Cate_product;
use App\Models\Cate_post;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Faq;
use App\Models\Filter;
use App\Models\Product;
use App\Models\Productdiemmay;
use App\Models\Images;
use App\Models\Size;
use Illuminate\Support\Str;
use DB;
class ProductController extends Controller
{
    public function allProduct()
    {   $countProduct = Product::where('status', 1)->orderBy('id', 'DESC')->count();
        $product = Product::where('status', 1)->orderBy('id', 'DESC')->paginate(24);
        $cate_product_999 = Cate_product::where('status', 1)->orderBy('position', 'ASC')->take(3)->get();
        return view('frontend.products.allProduct', compact('product','cate_product_999','countProduct'));
    }
 public function spkm()
    {
        $product = Product::where('status', 1)->where('is_hot', 1)->orderBy('id', 'DESC')->paginate(24);
          $cate_product_999 = Cate_product::where('status', 1)->orderBy('position', 'ASC')->take(3)->get();
        return view('frontend.products.spkm', compact('product','cate_product_999'));
    }
public function spdm()
    {
        $product = Product::where('status', 1)->where('is_home', 1)->where('price_unit','<>', 0)->orderBy('id', 'DESC')->paginate(24);
          $cate_product_999 = Cate_product::where('status', 1)->orderBy('position', 'ASC')->take(3)->get();
        return view('frontend.products.spdm', compact('product','cate_product_999'));
    }
    public function hotsale()
    {
        $product = Product::where('status', 1)->where('is_hot', 1)->orderBy('id', 'DESC')->paginate(24);
          $cate_product_999 = Cate_product::where('status', 1)->orderBy('position', 'ASC')->take(3)->get();
        return view('frontend.products.hotsale', compact('product','cate_product_999'));
    }
public function thuonghieu($id)
    {

$tenth = FAQ::where('id',$id)->first();

        $product = Product::where('status', 1)->where('thuonghieu_id','LIKE',"%$id%")->orderBy('id', 'DESC')->paginate(24);
       
        return view('frontend.products.thuonghieu', compact('product','tenth'));
    }

    public function productByCate($cateSlug,$slug=null,Request $req)
    {
        if($slug == null){
            $cate_product = Cate_product::where('slug_vi', $cateSlug)->first();
        }else{
            $parentCate = Cate_product::where('slug_vi', $cateSlug)->first();
            $cate_product = Cate_product::where('parent_id',$parentCate->id)->where('slug_vi', $slug)->first();
        }
        if($cate_product == null){
            return redirect()->route('index');
        }else{
            

// $cate_product_c2 = Cate_product::where('parent_id', $cate_product->id)->get();
            $hotProducts  = Product::where('status',1)->where('is_hot',1)->get();



// Lấy danh sách tất cả các danh mục con và con cháu của danh mục hiện tại
            $subcategories = Cate_product::where('parent_id', $cate_product->id)->pluck('id')->toArray();
            $descendants = Cate_product::whereIn('parent_id', $subcategories)->pluck('id')->toArray();

// Xây dựng câu truy vấn chính
            $productsQuery = Product::where('status', 1)
            ->where(function ($query) use ($cate_product, $subcategories, $descendants) {
                $query->orWhere('cate_product_id', $cate_product->id)
                ->orWhereIn('cate_product_id', $subcategories)
                ->orWhereIn('cate_product_id', $descendants);
            });



            $subcategories_boloc = Faq::where('parent_id', 0)->pluck('id')->toArray();
            $descendants_boloc = Faq::whereIn('parent_id', $subcategories_boloc)->pluck('id')->toArray();


            foreach($descendants_boloc as $vl){
              $filter =  'filter-'.$vl;
              
              if (isset($req->$filter)) {
                  $productsQuery->where('boloc_id', 'LIKE', '%|'.$req->$filter.'|%');
              }

        }


//lọc giá
        if ($req->pricef == '0-5000000') {
            $productsQuery->where('price', '>=', 0)->where('price', '<=', 5000000);
        } elseif ($req->pricef == '5000000-7000000') {
          $productsQuery->where('price', '>=', 5000000)->where('price', '<=', 7000000);
      }
      elseif ($req->pricef == '7000000-10000000') {
          $productsQuery->where('price', '>=', 7000000)->where('price', '<=', 10000000);
      }
      elseif ($req->pricef == '10000000-12000000') {
          $productsQuery->where('price', '>=', 10000000)->where('price', '<=', 12000000);
      }
      elseif ($req->pricef == '12000000-15000000') {
          $productsQuery->where('price', '>=', 12000000)->where('price', '<=', 15000000);
      }
      elseif ($req->pricef == '15000000-20000000') {
          $productsQuery->where('price', '>=', 15000000)->where('price', '<=', 20000000);
      }
      elseif ($req->pricef == '20000000-50000000') {
          $productsQuery->where('price', '>=', 20000000)->where('price', '<=', 50000000);
      }

      elseif ($req->pricef == '50000000-900000000') {
          $productsQuery->where('price', '>=', 50000000)->where('price', '<=', 900000000);
      }else{

    }

// Áp dụng sắp xếp
      if ($req->order == 'DESC') {
        $productsQuery->orderBy('price', 'DESC');
    } elseif ($req->order == 'ASC') {
        $productsQuery->orderBy('price', 'ASC');
    } else {
        $productsQuery->orderBy('id', 'DESC');
    }
// Lấy kết quả phân trang
    $products = $productsQuery->paginate(24);

    $countProduct = $productsQuery->count();

    return view('frontend.products.productByCate', compact('cate_product','products','hotProducts','countProduct'));
}
    }
    public function detailProduct($slug)
{
        $detail_product = Product::where('status', 1)->where('slug_vi', $slug)->first();
        if (isset($detail_product)) {
           $detail_product->viewed += 1;

           $detail_product->save();

           $cate_product = Cate_product::where('id', $detail_product->cate_product_id)->first();
           $splq = Product::where('cate_product_id', $detail_product->cate_product_id)
           ->where('id', '<>', $detail_product->id)->orderBy('id', 'DESC')->get();
           $img = Images::where('product_id', $detail_product->id)->orderBy('id', 'ASC')->get();
           $comment_detail= Comment::where('status', 1)->where('product_id', $detail_product->id)->get();
           return view('frontend.products.detailProduct', compact('detail_product', 'cate_product', 'splq', 'img','comment_detail'));
        }else{
         return view('errors.404');
     }
 }
    public function ajax_product_detail(Request $req)
    {
         if($req->get('id'))
        {
            $id = $req->get('id');
            $data = Product::where('id',$id)->first();
           $output = '';
   $output .= '<div class="clear-fix"></div>';
                      $output .= '<div class="">';
                 $output .= '<div class="baongoai_detail_product">';
                                   $output .= '<div class="cot1 text-center col-xs-9 col-sm-5">';
                                    $output .= '<img src="'.asset($data->image).'" alt="'.$data->name_vi.'">';
                                     $output .= '</div>';
                                    $output .= '<div class="cot2 col-xs-9 col-sm-7">';
                                       $output .= '<div class="mota_content_detail_product load_noidung_'.$data->id.'">';
                                      $output .= $data->description_vi;
                                              $output .= '</div>';
            $output .= '<div class="ba_nut">';
            $output .= '<a href="javascript:void(0)" onclick="javascript:load_tongquan_product('.$data->id.')"> Tổng quan </a>';   $output .= '<a href="javascript:void(0)" onclick="javascript:load_thongsokythuat_product('.$data->id.')"> Thông số kỹ thuật </a>';   $output .= '<a href="javascript:void(0)" onclick="javascript:load_tinhnang_product('.$data->id.')"> Tính năng </a>';
 $output .= '</div>';
                                        $output .= '</div>';
         $output .= '</div>';
   $output .= '<div class="clear-fix"></div>';
             $output .= '</div>';
           echo $output;
       }
    }
  public function ajax_load_tongquan(Request $req)
    {
         if($req->get('id'))
        {
            $id = $req->get('id');
            $data = Product::where('id',$id)->first();
           $output = '';
            $output .= $data->description_vi;
           echo $output;
       }
    }
  public function ajax_load_thongsokythuat(Request $req)
    {
         if($req->get('id'))
        {
            $id = $req->get('id');
            $data = Product::where('id',$id)->first();
           $output = '';
            $output .= $data->thongtin_vi;
           echo $output;
       }
    }
      public function ajax_load_tinhnang(Request $req)
    {
         if($req->get('id'))
        {
            $id = $req->get('id');
            $data = Product::where('id',$id)->first();
           $output = '';
            $output .= $data->title_vi;
           echo $output;
       }
    }
    public function timten(Request $req)
    {
        $input  = $req->search;
        $product = Product::where('name_vi', 'LIKE',"%$input%")->where('status', 1)->orWhere('name_en', 'LIKE',"%$input%")->paginate(24);
        $countProduct = Product::where('name_vi', 'LIKE',"%$input%")->where('status', 1)->orWhere('name_en', 'LIKE',"%$input%")->count();
        return view('frontend.products.timten', compact('product', 'input','countProduct'));
    }
    public function productsCat(Request $req)
    {
        $price1 = $req->price1;
        $price2 = $req->price2;
        $product = Product::orderBy('price', 'ASC')->where('status', 1)
            ->where('price', '>=', $price1)->where('price', '<=', $price2)
            ->paginate(24);
        return view('frontend.products.timgia', compact('product'));
    }
    public function productInfo()
    {
        $cate_products = Cate_product::where("parent_id",0)->get();
        return view('products.getProductInfo',compact('cate_products'));
    }
    public function updateInfoProduct()
    {
        $products = Product::all();
        foreach ($products as $key => $product) {
            $info = DB::table('productinfos')->where('link','like',$product->link)->first();
            if($info){
                $product->description_vi = $info->description_vi;
                $product->title_vi = $info->title_vi;
                $product->thongtin_vi = $info->thongtin_vi;
                $product->save();
            }
        }
    }

    public function getProductInfo()
        {
        }
    public function getDungtich(Request $request){
        $cate_product = Cate_product::where('parent_id',$request->id)->pluck("id")->toArray();
        $products = Product::whereIn('cate_product_id',$cate_product)->get();
        
        $pattern = '/(\d+(?:\.\d+)?)\s?kg/';
        foreach ($products as $key => $product) {
            $productName = $product->name_vi;
            $matches = [];

            if (preg_match($pattern, $productName, $matches)) {
                $weight = $matches[1];
                $product->kichthuoc = $weight;
                $product->save();
            }
        }
        $productCount = $products->count();
        $thongbao = 'Đã update được '.$productCount.' sản phẩm';
        return redirect()->back()->with('success', $thongbao);
    }
    public function updateboloc(Request $request)
    {   
        $boloc_id = $request->boloc_id;
        $cate_product = Cate_product::where("parent_id",48)->pluck("id")->toArray();
        $min = intval($request->min);
        $max = intval($request->max);
        
        if($min != null){
            $products = Product::whereBetween('kichthuoc',[$min,$max])->whereIn('cate_product_id',$cate_product)->get();
            foreach($products as $key => $product){
                if($product->boloc_id == null){
                    $product->boloc_id = '|'.$boloc_id.'|';
                }else{
                    $product->boloc_id = $product->boloc_id.$boloc_id.'|';
                }
                $product->save();
            }
        }else{
           
            $thongtin_vi = $request->thongtin_vi;
            $name_vi = $request->name_vi;
            $column = $thongtin_vi != null ? 'thongtin_vi' : 'name_vi';
            $keyword = $thongtin_vi != null ? $thongtin_vi : $name_vi;
            $products = Product::where($column,'like','%'.$keyword.'%')->whereIn('cate_product_id',$cate_product)->get();
            
            $productsCount = $products->count();

            foreach ($products as $key => $product) {
                if($product->boloc_id == null){
                    $product->boloc_id = '|'.$boloc_id.'|';
                }else{
                    $product->boloc_id = $product->boloc_id.$boloc_id.'|';
                }
                $product->save();
            } 
        }
        
        return redirect()->route("product.productInfo");
    }

    public function ganImage()
    {
        // Lấy tất cả sản phẩm có ít nhất một hình ảnh
        $productsWithImages = Product::get();

        // Duyệt qua từng sản phẩm có ít nhất một hình ảnh
        foreach ($productsWithImages as $product) {
            // Lấy danh sách ảnh của sản phẩm hiện tại
            $images = Images::where('link', $product->link)->get();

            // Sử dụng một truy vấn SQL để cập nhật product_id cho tất cả các ảnh cùng link
            Images::where('link', $product->link)->update(['product_id' => $product->id]);
        }

        return redirect()->back();
    }

    
    public function swapPrice(){
    
        $products = Product::get();
        foreach($products as $product){
            $price = $product->price_unit;
            $sale = $product->price;
            if($sale == null || $sale == 0){
                $sale = $price;
                $price = 0;
            }
            elseif($sale > $price){
                $tam = $sale;
                $sale = $price;
                $price = $tam;
            }

            $luu_pr = Product::findOrFail($product->id);

            $luu_pr->price = $sale;
            $luu_pr->price_unit = $price;
            $luu_pr->save();
        }
        return redirect()->back();
    }

    public function ganCateProduct(){

        $cate_products = Cate_product::where('parent_id',48)->get();
        foreach($cate_products as $cate_product){
            $products = Product::where('name_vi','like','%'.$cate_product->name_vi.'%')->where('cate_product_id',99)->update(['cate_product_id'=>$cate_product->id]);
        }
        return redirect()->back();
    }
    public function fixImage(){
        $getCateId = Cate_product::where('parent_id',1)->pluck('id')->toArray();
        $productId = Product::whereIn('cate_product_id',$getCateId)->pluck('id')->toArray();
        $listIdNeedGet = [];
        foreach ($productId as $key => $id) {
            $countImage = Images::where('product_id',$id)->count();
            if($countImage == 0){
                $listIdNeedGet[] = $id;
            }
        }
        dd($listIdNeedGet);
        return redirect()->back();
    }
    public function unlinkDes(){
        return view('frontend.unlink');
    }
    public function updateUnlinkDes(Request $req){
        $product_mota = Product::select('id','description_vi','thongtin_vi')->get();     
        // $product_mota = Product::where('id',1741)->get();     
        
        foreach ($product_mota as $key => $value) {
            
            // dd($value->description_vi);
            $thongtin_vi = preg_replace('/<a[^>]*>(.*?)<\/a>/i', '$1', $value->thongtin_vi);
            $description_vi = preg_replace('/<a[^>]*>(.*?)<\/a>/i', '$1', $value->description_vi);
            // dd($description_vi);
            $product = Product::findOrFail($value->id);
            $product->thongtin_vi = $thongtin_vi;
            $product->description_vi = $description_vi;
            $product->save();
        }
        return back()->with('thanhcong','ok rồi nhé');
    }
    
}
