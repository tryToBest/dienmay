<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post2;
use App\Models\Cate_post2;
use App\Models\Image_duan;
use Illuminate\Support\Collection;

class Post2Controller extends Controller
{
    public function list_duan($slug)
    {
        $cate_post2 = Cate_post2::where('slug_vi', $slug)->first();
        $get_cha = Cate_post2::where('id', $cate_post2->parent_id)->first();
        $id        = $cate_post2->id;
        session()->put('id',$id);
        // dd($id);
        $post2 = Post2::orderBy('created_at','DESC')->where('status', 1)->where(function($query)
        {
            $pro       = $query;
            $id        = session('id');
            $cate_post2 = Cate_post2::find($id);
            $pro       = $pro->orWhere('cate_post2_id',$cate_post2->id); // bài viết có id của danh muc cha cấp 1.
            $com       = Cate_post2::where('parent_id',$cate_post2->id)->get();//danh mục cha cấp 2.
            foreach ($com as $dt) {
                $pro = $pro->orWhere('cate_post2_id',$dt->id);// bài viết có id của danh muc cha cấp 2.
            }
            session()->forget('id');//xóa session;
        })->paginate(12);


        $all_project = Post2::orderBy('position','DESC')->where('status', 1)->get();

  return view('frontend.duan.list_duan', compact('post2', 'cate_post2','get_cha','all_project'));
   
    }
  //    public function thanhvien()
  //   {
       

  // return view('frontend.post2s.thanhvien');
   
  //   }


    

    public function detail_duan($slug)
    {
        $detail_duan = Post2::where('status', 1)->where('slug_vi', $slug)->first();

          $tinlq = Post2::where('cate_post2_id', $detail_duan->cate_post2_id)->where('status', 1)->where('id', '<>', $detail_duan->id)->orderBy('id', 'DESC')->get();

        $img = Image_duan::where('post2_id', $detail_duan->id)->orderBy('id', 'DESC')->get();

        $cate_post2 = Cate_post2::where('id', $detail_duan->cate_post2_id)->first();
        return view('frontend.duan.detail_duan', compact('detail_duan','cate_post2','tinlq','img'));
    }
   
   
}
