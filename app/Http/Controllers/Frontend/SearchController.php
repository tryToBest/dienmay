<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Bill;
use App\Models\Product;


use Requests; // Thêm namespace này
use Symfony\Component\DomCrawler\Crawler;


class SearchController extends Controller
{
 
      public function postSearch(Request $req)
    {
        $input  = $req->search;
        $products = Product::where('name_vi', 'LIKE',"%$input%")->where('status', 1)->paginate(24);
        $countProducts = Product::where('name_vi', 'LIKE',"%$input%")->where('status', 1)->count();
        // $post = Post::where('name_vi', 'LIKE',"%$input%")->where('status', 1)->orWhere('title_vi', 'LIKE',"%$input%")->orWhere('description_vi', 'LIKE',"%$input%")->paginate(12);
        return view('frontend.search', compact('products', 'input','countProducts'));
    }

 public function timkiemdonhang(Request $req)
    {
        $input  = $req->search;
        $donhang = Bill::where('orderId', 'LIKE',"%$input%")->orWhere('phone', 'LIKE',"%$input%")->get();
     
        return view('frontend.timkiemdonhang', compact( 'input','donhang'));
    }
 public function caodata(Request $req)
    {


         $url = 'https://dienmaytinphat.com/danh-muc-san-pham/ti-vi/';
     
   $response = Requests::get($url); // Sử dụng thư viện Requests

  if ($response->status_code === 200) {
       $html = $response->body;
            $crawler = new Crawler($html);

            $products = $crawler->filter('.product')->each(function ($node) {
                $name = $node->attr('title');
                $image = $node->filter('img')->attr('src');
                $price = $node->filter('span')->text();

                return [
                    'name' => $name,
                    'image' => $image,
                    'price' => $price,
                ];
            });

            return view('products', ['products' => $products]);
        }



        return 'Failed to fetch data';




        
    }


}
