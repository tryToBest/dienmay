<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\User;
use Cart;
use App\Http\Requests\OrderRequest;
use App\Models\Bill;
use App\Models\BillDetail;
use App\Models\Color;
use App\Models\Setting;
use Session;
use Auth;
use App\Helpers\BaokimApi;
use Illuminate\Support\Facades\Log;
use Mail;
use File;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class CartController extends Controller
{
    public function index()
    {
        $setting = Setting::first();
        $content = Cart::content();
        $id_product = [];
        foreach($content as $product){
            $id_product[] = $product->id;
        }
        $khuyenmais = Product::whereIn('id',$id_product)->where('chucnang','!=',null)->select('chucnang')->get();
  

        $total_dau = Cart::total(null,null,'');
        $tinh = DB::table("province")->get();
        $demsl = 0;
        foreach($content as $c){
            $demsl += $c->qty;
        }
        // dd($demsl);
        if($demsl>2){
             $total = $total_dau*((100-$setting->phantramsale)/100);
        }else{
            $total = $total_dau;
        }
        $giaduocgiam = $total_dau-$total;
        $total = number_format($total);  
        return view('frontend.carts.index', compact('content', 'total', 'total_dau','giaduocgiam','khuyenmais'));
    }

    public function themvaogiohang(Request $req)
    {
        $id = $req->id;
        $query = Product::where('id', $id)->first();
        if($req->quantity == null)
        {
            Cart::add(array(
                'id'      => $query->id,
                'name'    => $query->name_vi,
                'qty'     => 1,
                'price'   => $query->price,
                'options' => array('img' => $query->image,'price_unit'   => $query->price_unit),
            ));
            return back()->with('themvaogiohang', 'Thêm vào giở hàng thành công');
        }else{
            Cart::add(array(
                'id'      => $query->id,
                'name'    => $query->name_vi,
                'qty'     => $req->quantity,
                'price'   => $query->price,
                'options' => array('img' => $query->image,'price_unit'   => $query->price_unit),
            ));
            return back()->with('themvaogiohang', 'Thêm vào giở hàng thành công');
        }
    }
    public function addCart($id, Request $req)
    {
        $query = Product::where('id', $id)->first();
        if($req->quantity == null)
        {
            Cart::add(array(
                'id'      => $query->id,
                'name'    => $query->name_vi,
                'qty'     => 1,
                'price'   => $query->price,
                'options' => array('img' => $query->image),
            ));
            return back()->with('themvaogiohang', 'Thêm vào giở hàng thành công');
        }else{
            Cart::add(array(
                'id'      => $query->id,
                'name'    => $query->name_vi,
                'qty'     => $req->quantity,
                'price'   => $query->price,
                'options' => array('img' => $query->image),
            ));
            return back()->with('themvaogiohang', 'Thêm vào giở hàng thành công');
        }
    }
    public function addCart2(Request $req)
    {
        if ($req->ajax())
        {
            $result = Product::find($req->id);
            Cart::add(array(
                'id'      => $result->id,
                'name'    => $result->name_vi,
                'qty'     => 1,
                'price'   => $result->price,
                'options' => array('img' => $result->image),
            ));
            $haha = count(Cart::content());
            return response($haha);
        }
    }
    public function addCart3(Request $req)
    {
        if ($req->ajax())
        {
            $result = Product::find($req->id);
            Cart::add(array(
                'id'      => $result->id,
                'name'    => $result->name_vi,
                'qty'     => 1,
                'price'   => $result->price,
                'options' => array('img' => $result->image),
            ));
            $haha = count(Cart::content());
            return response()->json($haha);
        }
    }
    public function muangay($id, Request $req)
    {
        $query = Product::where('id', $id)->first();
        Cart::add(array(
            'id'      => $query->id,
            'name'    => $query->name_vi,
            'qty'     => $req->quantity,
            'price'   => $query->price,
            'options' => array('img' => $query->image),
        ));
        return redirect()->route('cart.index');
    }
    public function updateCart(Request $request)
    {
         $setting = Setting::first();
        $id    = $request->id;
        $qty   = $request->qty;
        $rowId = $request->rowId;
        Cart::update($rowId,$qty);
        $voucher = $request->voucher;
        $total_2 = Cart::total(null,null,'');
        $data['giatridonhang'] = number_format(Cart::total(null,null,''));

        if ($voucher != null) {
            $giamgia = Color::findOrFail($voucher);
            $data['TongGiaTriDonHang'] = number_format(Cart::total(null,null,'')*(100-$giamgia->ma)/100);
        } else {
       
 $data['TongGiaTriDonHang'] = Cart::total(null,null,'');

        }


$data['TongGiaTriDonHang'] = number_format($data['TongGiaTriDonHang']).'₫';
        // $demsl = 0;
        foreach (Cart::content() as $key => $value) {
    // $demsl += $value->qty;
            if($value->rowId ==  $rowId)
            {
    $data['TongTien'] = number_format($value->price*$value->qty).'₫';
            }
        }
        echo json_encode($data);
    }
    public function destroyCart($id)
    {
        Cart::remove($id);
        return back();
    }
    public function order()
    {
        $setting = Setting::first();
        $content = Cart::content();
        $total_dau = Cart::total(null,null,'');
        $tinh = DB::table("province")->get();
        if (Session::get('magiamgia') != null) {
            $giamgia = Color::findOrFail(Session::get('magiamgia'));
            $tiengiam = ($giamgia->ma)*$total_dau/100;
            $total = number_format(Cart::total(null,null,'')*(100-$giamgia->ma)/100+$setting->ship);
             return view('frontend.carts.order', compact('content', 'total', 'tinh', 'giamgia', 'total_dau', 'tiengiam'));
        } else {
    $demsl = 0;
        foreach (Cart::content() as $key => $value) {
     $demsl += $value->qty;
}
  if($demsl>2){
  $total = number_format((Cart::total(null,null,'')+$setting->ship)*((100-$setting->phantramsale)/100));
}else{
  $total = number_format(Cart::total(null,null,'')+$setting->ship);
}
             return view('frontend.carts.order', compact('content', 'total', 'tinh', 'total_dau'));
        }
    }
    public function order_giamgia($id)
    {
        $giamgia = Color::findOrFail($id);
        $content = Cart::content();
        $total   = Cart::total();
        $tinh = DB::table("province")->pluck("_name","_code");
        return view('frontend.carts.order', compact('content', 'total', 'tinh'));
    }
    public function getStateList(Request $request)
    {
        $states = DB::table("district")->where("_province_id",$request->country_id)->pluck("_name","id");
        return response()->json($states);
    }
    public function getCityList(Request $request)
    {
        $cities = DB::table("ward")->where("_district_id",$request->state_id)->pluck("_name","id");
        return response()->json($cities);
    }
    public function postOrder(Request $req)
    {
        
        $now = date('y',strtotime(Carbon::now()));
        $setting = Setting::first();
        $content = Cart::content();
        $total_dau = Cart::total(null,null,'');
        if (Session::get('magiamgia') != null) {
            $giamgia = Color::findOrFail(Session::get('magiamgia'));
            $tiengiam = ($giamgia->ma)*$total_dau/100;
            $total = Cart::total(null,null,'')*(100-$giamgia->ma)/100+$setting->ship;
        } else {
            $total = Cart::total(null,null,'')+$setting->ship;
        }
                // $invID = str_pad(1, 6, '0', STR_PAD_LEFT);
                $bill          = new Bill;
                $bill->name    = $req['name'];
                $bill->user_id    = Auth::id();
                $bill->email   = $req['email'];
                $bill->address = $req['address'];
                $bill->tinh = $req['tinhthanh'];
                $bill->huyen = $req['quanhuyen'];
                $bill->xa = $req['xa'];
                $bill->phone   = $req['phone2'] == null ? $req['phone'] : $req['phone'].'-'.$req['phone2'];
                $bill->total   = $total;
                $bill->note    = $req['note'];
                $bill->payment = $req['payment'];
                $bill->status  = 1;
                $bill->save();
                $bill->orderId    = sprintf("%04s",$bill->id).$now;
                $bill->save();
                foreach ($content as $c) {
                    $bill_detail             = new BillDetail;
                    $bill_detail->bill_id    = $bill->id;
                    $bill_detail->product_id = $c->id;
                    $bill_detail->quantity   = $c->qty;
                    $bill_detail->price      = $c->price;
                    $bill_detail->save();
                }
           Session::forget('cart');

                    return redirect()->route('thanhcong',sprintf("%04s",$bill->id).$now)->with('dathangthanhcong','Đặt hàng thành công!');
        }
    public function thanhcong($orderId)
    {
      
     
        $order = Bill::where('orderId', $orderId)->first();
        return view('frontend.thanhcong', compact('order'));
    }
}
