<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Cate_ttbh;
use App\Models\Uudai;
use App\Models\Size;
use App\Models\Post;
use Mail;
class ContactController extends Controller
{
    public function index()
    {
        $contact = Post::where('id',492)->get();
        return view('frontend.contact',compact('contact'));
    }
     public function video()
    {
       $video_all = Size::where('status',1)->orderBy('position','ASC')->get();
        return view('frontend.video',compact('video_all'));
    }
      public function doitac()
    {
        $doitac_all = Uudai::where('status',1)->orderBy('position','ASC')->get();


        return view('frontend.doitac',compact('doitac_all'));
    }
          public function chinhsach()
    {
        $chinhsach_all = Uudai::where('status',1)->orderBy('position','ASC')->get();


        return view('frontend.chinhsach',compact('chinhsach_all'));
    }


    public function thuvien()
    {
        $thuvien_all = Uudai::where('status',1)->orderBy('position','ASC')->get();


        return view('frontend.thuvien',compact('thuvien_all'));
    }
        public function ajax_trungtambaohanh(Request $request)
    {
              if($request->get('id'))
        {
            $id = $request->get('id');
            $data = Cate_ttbh::where('parent_id',$id)->get();
           $output = '';
           foreach ($data as $key => $value) {
 

                        $output .= '<div class="col-xs-12 col-sm-3">';
                          
                          $output .= '<div class="baongoai_trungtam">';
                            $output .= '<div class="name_trungtam text-center">';
                    $output .= $value->name_vi;
                            $output .= '</div>';
                            $output .= '<div class="motan_trungtam">';
                             $output .= $value->description_vi;
                            $output .= '</div>';
                 $output .= '</div>';
                        $output .= '</div>';



           }
     
           echo $output;
       }
    }

    



    public function postContact(Request $req)
    {
        $contact          = new Contact;
        $contact->name    = $req['name'];
        $contact->address = $req['address'];

        $contact->phone   = $req['phone'];
        $contact->email   = $req['email'];
            $contact->company = $req['company'];
        $contact->title   = $req['title'];
        $contact->content = $req['content'];
           $contact->content2 = $req['content2'];
        $contact->save();
        // Mail::send('mailfb', array('name'=>$req["name"],'email'=>$req["email"], 'address'=>$req['address'], 'phone'=>$req['phone'],'content'=>$req['content']), function($message){
        //     $message->to('anvietgardencons@gmail.com', 'Visitor')->subject('Bạn có khách hàng có nhu cầu liên hệ tại ');
        // });
        return back()->with('dangky', 'Đăng ký thành công');
    }
}
