<?php
    namespace App\Http\Composers;
    use Illuminate\Contracts\View\View;
    use Cart;
    use App\Models\Banner;
    class HeaderComposer
    {
        public function compose(View $view)
        {
            $count = Cart::count();
            $total = Cart::total(null,null,'');
            $contents = Cart::content();
        $demsl = 0;
          foreach ($contents as $c){
            $demsl += $c->qty;
          }
if($demsl>2){
     $total = number_format($total*95/100);
}else{
    $total = number_format($total);
}
            $datas = [
                'count' => $count,
                'total' => $total,
                'contents' => $contents,
            ];
            $view->with($datas);
        }
    }
