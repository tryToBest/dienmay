<?php
    namespace App\Http\Composers;
    use Illuminate\Contracts\View\View;
    use App\Models\Setting;
    use App\Models\Slide;
    use Cart;
    class SettingComposer
    {
        public function compose(View $view)
        {
            $setting = Setting::first();
            $get_name = 'name_'.\App::getLocale();
            $get_des = 'description_'.\App::getLocale();
            $get_title = 'title_'.\App::getLocale();
            $get_address = 'address_'.\App::getLocale();
            $get_thongtin = 'thongtin_'.\App::getLocale();
            $get_slogan = 'slogan_'.\App::getLocale();
            $sptrng = Cart::content();
            $sptg = count($sptrng);
            $quangcao = Slide::where('status', 1)->where('dislay', 9)->take(2)->get();
            $data = [
                'setting' => $setting,
                'get_name' => $get_name,
                'get_des' => $get_des,
                'get_title' => $get_title,
                'get_address' => $get_address,
                'get_thongtin' => $get_thongtin,
                'get_slogan' => $get_slogan,
                'sptg' => $sptg,
                'quangcao' => $quangcao,
            ];
            $view->with($data);
        }
    }
