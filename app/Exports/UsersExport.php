<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\Bill;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UsersExport implements FromView
{
	use Exportable;

    public function __construct($start,$end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    public function view(): View
    {
        return view('admin.carts.excel', [
            'order' => Bill::where('created_at','>=', $this->start)->where('created_at','<=', $this->end)->get()
        ]);
    }
}