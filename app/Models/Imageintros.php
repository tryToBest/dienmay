<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Imageintros extends Model
{
    protected $table = 'imageintros';
    protected $guarded = [];

    public function intro()
    {
        return $this->hasMany(Intro::class, 'intro_id');
    }
}
