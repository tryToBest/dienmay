<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image_duan extends Model
{
    protected $table = 'image_duans';
    protected $guarded = [];

    public function post2()
    {
        return $this->hasMany(Post2::class, 'post2_id');
    }
}
