<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $guarded = [];

    protected $table = 'faqs';
    public function Boloc(){
        return $this->hasMany(Faq::class,'parent_id');
    }
    public function BolocParent(){
        return $this->belongsTo(Faq::class,'id','parent_id');
    }
}
