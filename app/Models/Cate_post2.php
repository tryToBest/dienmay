<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cate_post2 extends Model
{
    protected $guarded = [];

    public function Post2()
    {
        return $this->hasMany(Post2::class, 'cate_post2_id');
    }
}
