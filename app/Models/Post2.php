<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post2 extends Model
{
    protected $guarded = [];

    public function Cate_post2()
    {

        return $this->belongsTo('App\Models\Cate_post2', 'cate_post2_id', 'id');
    }
    public function getShortDec(){
        if (strlen($this->title) > 150){
            return substr($this->title, 0,strpos($this->title, ' ', 150)) . ' ...';
        } else {
        return $this->title;
        }
    }
}
