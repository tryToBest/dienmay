<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Yeuthich extends Model
{
    protected $table = 'yeuthich';
    protected $guarded = [];

    public function User()
    {

        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function Product()
    {

        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
}
