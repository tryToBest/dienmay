<?php

namespace App\Imports;

use App\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Http\Requests\ProductRequest;
use App\Models\Cate_product;
use Image, File;

class ProductsImport implements ToCollection
{
    
    public function collection(Collection $rows)
    {
        // $input = $rows->toArray();
        // dd($input);
        // $rules = [
        //     'name' => 'required',
        //     'email' => 'required|email',
        //     'message' => 'required|max:250',
        // ];
        // $customMessages = [
        //     'required' => 'The :attribute field is required.'
        // ];
        // Validator::make($input, $rules, $customMessages)->validate();

        $input = $rows->toArray();
        $rules = [
            '*.1' => 'required',
            '*.4' => 'required',
            '*.6' => 'required',
        ];
        $customMessages = [
            'required' => 'Dòng :attribute không được bỏ trống',
        ];
        Validator::make($input, $rules, $customMessages)->validate();

        foreach ($rows as $key => $row) {
            if ($key > 0) {
                $product                  = new Product;
                $product->name_vi         = $row[1];
                $product->name_en         = $row[2];
                $product->slug_vi         = $row[1];
                $product->slug_en         = $row[2];
                $product->price           = $row[6];
                $product->cate_product_id = $row[4];
                $product->status          = 1;
                $product->is_home         = 0;
                $product->is_hot         = 0;
                $product->code     = $row[3];
                $product->image     = 'upload/images/product/'.$row[5];
                $product->price_unit     = $row[7];
                $product->title_vi        = $row[8];
                $product->title_en        = $row[9];
                $product->description_vi  = $row[10];
                $product->description_en  = $row[11];
                $product->thongtin_vi     = $row[12];
                $product->thongtin_en     = $row[13];
                $product->save();
            }
        }
    }
}
