$('.slideshow').slick({
  arrows:true,
        infinite:true,
        slidesToShow:1,
        accessibility:true,
        autoplay:true,
        autoplaySpeed:4500,
  prevArrow:"<span class='arrow prev'><i class='fa fa-angle-left'></i></span>",
  nextArrow:"<span class='arrow next'><i class='fa fa-angle-right'></i></span>",
  customPaging : function(slider, i) {
    },
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
$('.slide_thuonghieu').slick({
  arrows:true,
        infinite:true,
        slidesToShow:4,
       
        accessibility:true,
        autoplay:false,
        autoplaySpeed:1500,
  prevArrow:"<span class='arrow prev'><i class='fa fa-angle-left'></i></span>",
  nextArrow:"<span class='arrow next'><i class='fa fa-angle-right'></i></span>",
  customPaging : function(slider, i) {
    },
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
  ]
});
$('.slide_chinhanh').slick({
  arrows:true,
        infinite:true,
        slidesToShow:2,
       
        accessibility:true,
        autoplay:true,
        autoplaySpeed:1500,
  prevArrow:"<span class='arrow prev'><i class='fa fa-angle-left'></i></span>",
  nextArrow:"<span class='arrow next'><i class='fa fa-angle-right'></i></span>",
  customPaging : function(slider, i) {
    },
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.slide_tintuchome').slick({
  arrows:true,
        infinite:true,
        slidesToShow:3,
       
        accessibility:true,
        autoplay:false,
        autoplaySpeed:1500,
  prevArrow:"<span class='arrow prev'><i class='fa fa-angle-left'></i></span>",
  nextArrow:"<span class='arrow next'><i class='fa fa-angle-right'></i></span>",
  customPaging : function(slider, i) {
    },
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.slide_qc_home').slick({
  arrows:true,
        infinite:true,
        slidesToShow:1,
        accessibility:true,
        autoplay:true,
        autoplaySpeed:4500,
  prevArrow:"<span class='arrow prev'><i class='fa fa-angle-left'></i></span>",
  nextArrow:"<span class='arrow next'><i class='fa fa-angle-right'></i></span>",
  customPaging : function(slider, i) {
    },
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
          arrows:false,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
            arrows:false,
        slidesToScroll: 1
      }
    }
  ]
});
