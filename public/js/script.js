var slider = [];
$(document).ready(function () {
    $(".header-page").on('click', function () {
        $(".content-categories").slideToggle();
    });
    slider[99901] = $('#list-slider-home').lightSlider({
        item: 1,
        loop: true,
        slideMove: 1,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 1000,
        slideMargin: 0,
        mode: 'slide',
        auto: true,
        pause: 5000,
        controls: true,
        pager: false
    });
    slider[99902] = $('#list-product-feature').lightSlider({
        item: 4,
        loop: true,
        slideMove: 1,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 1000,
        slideMargin: 0,
        mode: 'slide',
        auto: true,
        pause: 5000,
        controls: false,
        pager: false,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    item: 2,
                    slideMove: 1,
                    slideMargin: 6,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    item: 2,
                    slideMove: 1
                }
            }
        ]
    });

        slider[99903] = $('#list-slider-hinhanh').lightSlider({
        item: 1,
        loop: true,
        slideMove: 1,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 1000,
        slideMargin: 0,
        mode: 'slide',
        auto: true,
        pause: 5000,
        controls: true,
        pager: true
    });

 slider[99904] = $('#list-slider-qc').lightSlider({
        item: 1,
        loop: true,
        slideMove: 1,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 1000,
        slideMargin: 0,
        mode: 'slide',
        auto: true,
        pause: 5000,
        controls: true,
        pager: false
    });
    
});
function goToSlider(index, item, that) {
    slider[index].goToSlide(item);
    $(".list-text-slider ul li").removeClass('active');
    $(that).addClass('active');
}
function prevSlide(index) {
    slider[index].goToPrevSlide();
}
function nextSlide(index) {
    slider[index].goToNextSlide();
}
function readmoreContent(that) {
    $(".bx-left .content").addClass('active');
    $(that).hide();
}
function readmoreDacDiem(that) {
    $(".description-product .content ul").addClass('active');
    $(that).hide();
}
function showHideAttr() {
    $(".fullparametertran").slideToggle();
    $(".fullparameter").slideToggle();
}
function showRate() {
    $(".bx-frm-rate").show();
}